﻿#ifndef NPP_LMBCS_H
#define NPP_LMBCS_H

#include <gouda/npp/npp_global.h>

#include <QByteArray>
#include <QList>
#include <QVector> // LmbcsList対策
#include <QSet> // LmbcsList対策

#ifdef NT
#pragma pack(push, 1)
#endif

#include <global.h> // WORD
#include <nsfdata.h> // TYPE_TEXT

#ifdef NT
#pragma pack(pop)
#endif

namespace npp {

class LmbcsList;

/**
 * @brief LMBCS文字列クラス
 */
class NPPSHARED_EXPORT Lmbcs
{
public:
  Lmbcs() noexcept;

  Lmbcs(const char *c_str, int lmbcsSize = -1) noexcept;
  Lmbcs(QByteArray &&lmbcsBytes) noexcept;

  const QByteArray &ref() const noexcept { return value_; }
  const char *constData() const noexcept { return value_.constData(); }

  bool isEmpty() const noexcept;

  int size() const noexcept;
  int byteSize() const noexcept { return value_.size(); }
  Lmbcs replace(char before, char after);

  LmbcsList split(const Lmbcs &delimiter) const;

  Lmbcs toString() const { return *this; }

  static const WORD value_type = TYPE_TEXT;

  const QByteArray &value() const { return value_; }

  // for QVariant
  NPPSHARED_EXPORT
  friend QDataStream &operator <<(QDataStream &str, const Lmbcs &lmbcs);

  // for QVariant
  NPPSHARED_EXPORT
  friend QDataStream &operator >>(QDataStream &str, Lmbcs &lmbcs);

  NPPSHARED_EXPORT
  friend Lmbcs operator +(const Lmbcs &lhs, const Lmbcs &rhs);

  /**
   * @brief LmbcsList対策で必要な二項演算子は自由関数ではなくメンバ関数だった件
   * @param other
   * @return
   */
  inline bool operator ==(const Lmbcs &other) const noexcept
  {
    return value() == other.value();
  }

  inline bool operator !=(const Lmbcs &other) const noexcept
  {
    return !operator ==(other);
  }

private:
  QByteArray value_;
};

/**
 * @brief LmbcsListでQList<Lmbcs>を継承するために必要
 * @param key
 * @param seed
 * @return
 */
inline uint qHash(const npp::Lmbcs &key, uint seed)
{
  return qHash(key.value(), seed);
}

/**
 * @brief LMBCS文字列をQStringオブジェクトに変換する。
 */
struct NPPSHARED_EXPORT LmbcsToQString
{
  QString operator ()(const Lmbcs &lmbcs) const noexcept;
};

/**
 * @brief QStringオブジェクトをLMBCS文字列に変換する。
 */
struct NPPSHARED_EXPORT QStringToLmbcs
{
  Lmbcs operator ()(const QString &qs) const noexcept;
};

/**
 * @brief LMBCS文字列リストクラス
 */
class NPPSHARED_EXPORT LmbcsList
    : public QList<Lmbcs>
{
public:
  LmbcsList() : QList<Lmbcs>() {}
};

/**
 * @brief LMBCS文字列リストをQStringリストに変換する。
 */
struct NPPSHARED_EXPORT LmbcsListToQStringList
{
  QStringList operator ()(const LmbcsList &llist) const noexcept;
};

/**
 * @brief QStringリストをLMBCS文字列リストに変換する。
 */
struct NPPSHARED_EXPORT QStringListToLmbcsList
{
  LmbcsList operator ()(const QStringList &qlist) const noexcept;
};

NPPSHARED_EXPORT
QString lq(const Lmbcs &lmbcs) noexcept;

NPPSHARED_EXPORT
Lmbcs ql(const QString &qs) noexcept;

NPPSHARED_EXPORT
QStringList lq(const LmbcsList &llist) noexcept;

NPPSHARED_EXPORT
LmbcsList ql(const QStringList &qlist) noexcept;

} // namespace npp

//Q_DECLARE_TYPEINFO(npp::Lmbcs, Q_MOVABLE_TYPE);

#endif // NPP_LMBCS_H
