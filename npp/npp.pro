#-------------------------------------------------
#
# Project created by QtCreator 2019-06-25T22:17:40
#
#-------------------------------------------------

QT       -= gui

TARGET = npp
TEMPLATE = lib

DEFINES += NPP_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11 c++14

SOURCES += \
        lmbcs.cpp \
        nls.cpp \
        object.cpp \
        server.cpp \
        status.cpp \
        translate.cpp

HEADERS += \
        addin/logmessagetext.hpp \
        lmbcs.h \
        nls.h \
        object.h \
        server.h \
        status.h \
        npp_global.h  \
        translate.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

win32: QMAKE_CXXFLAGS += /utf-8

include(../../environment.pri)

#
# RxCpp
#
INCLUDEPATH += $$RxCppPath/Rx/v2/src
DEPENDPATH += $$RxCppPath/Rx/v2/src

#
# OpenSSL 1.1
#
#INCLUDEPATH += $$OpenSSLIncludePath
#DEPENDPATH += $$OpenSSLIncludePath
#LIBS += -llibssl -llibcrypto -L$$OpenSSLLibPath

#
# Notes C API
#
include($$NotesCAPIPath/qt_notes_c_api.pri)
#include(../notescapi_cmp.pri)

INCLUDEPATH -= $$PWD
DEPENDPATH -= $$PWD

INCLUDEPATH += $$PWD/../..
DEPENDPATH += $$PWD/../..

DISTFILES += \
  README.md
