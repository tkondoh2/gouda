Notes/Domino C API wrapper Library
==================================

# History

## v0.0.3

* NLSStatusをtranslate.hから独立。
* GetServerLatencyを追加。
* Translate関数テンプレートを非テンプレートに変更。

## v0.0.2-5

* バグ修正

## v0.0.2-4

* プロジェクトファイル内のNotes C APIの書き方を再度整理する。

## v0.0.2-3

* プロジェクトファイル内のNotes C APIの書き方を整理する。

## v0.0.2-1

* lq関数、ql関数をWindows用にエキスポートする。

## v0.0.2

* Lmbcsとその変換処理では例外が発生しないようにする(例外のループ防止)。
* Lmbcsの変換関数ショートカットを、()の記述も省略できるようにする。
* Object::getをObject::handleに変更する。
* void *Object::lockをtemplate <typename R> R *Object::lockに変更する。
* GetServerList::operator ()のロジックを整理する。
* Status::what()のロジックを整理する。

## v0.0.1

* `Status`クラスを追加する。
* `NLSStatus`クラスを追加する。
* `Translate`テンプレート関数オブジェクトを追加する。
* `unicodeToLmbcs`関数を追加する。
* `lmbcsToUnicode`関数を追加する。
* `Lmbcs`クラスを追加する。
* `LmbcsList`クラスを追加する。
* `LmbcsToQString`関数を追加する。
* `QStringToLmbcs`関数を追加する。
* `LmbcsListToQStringList`関数を追加する。
* `QStringListToLmbcsList`関数を追加する。
* `addin::logMessageText`テンプレート関数を追加する。
* `_Object`テンプレートクラスを追加する。
* `Object`クラスを追加する。
* `make_qshared`テンプレート関数を追加する。
