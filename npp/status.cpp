﻿#include "status.h"

#include <QByteArray>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <misc.h> // MAXSPRINTF
#include <osmisc.h> // OSLoadString

#ifdef NT
#pragma pack(pop)
#endif

namespace npp {

const char *Status::what() const noexcept
{
  if (what_.isEmpty()) {
    QByteArray buffer(MAXSPRINTF, '\0');
    WORD len = OSLoadString(
          NULLHANDLE,
          status_,
          buffer.data(),
          MAXSPRINTF
          );
    what_ = Lmbcs(buffer.constData(), len);
  }
  return what_.constData();
}

} // namespace npp
