﻿#include "server.h"

#include "status.h"
#include "object.h"

#ifdef NT
#pragma pack(push, 1)
#endif

#include <ns.h>
#include <nsfdb.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace npp {

observable<Lmbcs> GetServerList::operator ()(const Lmbcs &port) noexcept
{
  return observable<>::create<Lmbcs>([port](subscriber<Lmbcs> s) {
    DHANDLE handle = NULLHANDLE;
    Status result = NSGetServerList(
          port.isEmpty() ? nullptr : const_cast<char*>(port.constData()),
          &handle
          );
    if (result.hasError()) {
      s.on_error(std::make_exception_ptr(result));
    }
    else {
      Q_ASSERT(handle);
      ObjectPtr list = make_qshared<Object>(handle);
      WORD *pwNameLen = list->lock<WORD>();
      int count = static_cast<int>(*pwNameLen++);
      char *pName = reinterpret_cast<char*>(pwNameLen + count);
      for (int i = 0; i < count; ++i) {
        Lmbcs name(pName, static_cast<int>(pwNameLen[i]));
        s.on_next(name);
        pName += pwNameLen[i];
      }
    }
    s.on_completed();
  });
}

GetServerLatency::Value GetServerLatency::GetServerLatency::operator ()(
    const Lmbcs &server,
    DWORD timeout
    ) noexcept(false)
{
  GetServerLatency::Value value;
  Status status = NSFGetServerLatency(
        const_cast<char*>(server.constData()),
        timeout,
        &value.clientToServerMS,
        &value.serverToClientMS,
        &value.serverVersion
        );
  if (status.hasError())
    throw status;

  return value;
}


} // namespace npp
