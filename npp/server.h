﻿#ifndef NPP_SERVER_H
#define NPP_SERVER_H

#include <gouda/npp/lmbcs.h>

#include <rxcpp/rx.hpp>
using namespace rxcpp;
using namespace rxcpp::operators;

namespace npp {

/**
 * @brief サーバーリストを取得する。
 */
struct NPPSHARED_EXPORT GetServerList
{
  observable<Lmbcs> operator ()(const Lmbcs &port = Lmbcs())
  noexcept;
};

struct NPPSHARED_EXPORT GetServerLatency
{
  struct Value
  {
    DWORD clientToServerMS;
    DWORD serverToClientMS;
    WORD serverVersion;
  };

  Value operator ()(const Lmbcs &server, DWORD timeout) noexcept(false);
};

} // namespace npp

#endif // NPP_SERVER_H
