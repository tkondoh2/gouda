﻿#include "lmbcs.h"

#include "translate.h"

namespace npp {

Lmbcs::Lmbcs() noexcept
  : value_()
{}

Lmbcs::Lmbcs(const char *c_str, int size) noexcept
  : value_(c_str, size)
{}

Lmbcs::Lmbcs(QByteArray &&lmbcsBytes) noexcept
  : value_(std::move(lmbcsBytes))
{}

bool Lmbcs::isEmpty() const noexcept
{
  return value_.isEmpty();
}

int Lmbcs::size() const noexcept
{
  QByteArray s = value_.left(MAXWORD - 1);
  WORD numChars = 0;
  NLS_STATUS status = NLS_string_chars(
        reinterpret_cast<const BYTE*>(s.constData())
        , NLS_NULLTERM
        , &numChars
        , OSGetLMBCSCLS()
        );
  if (status != NLS_SUCCESS) return -1;

  return static_cast<int>(numChars);
}

Lmbcs Lmbcs::replace(char before, char after)
{
  QByteArray text = value_;
  text.replace(before, after);
  return Lmbcs(text);
}

LmbcsList Lmbcs::split(const Lmbcs &delimiter) const
{
  QString org = lq(*this);
  QStringList list = org.split(lq(delimiter));
  return ql(list);
}

QDataStream &operator <<(QDataStream &str, const Lmbcs &lmbcs)
{
  str << lmbcs.value_;
  return str;
}

QDataStream &operator >>(QDataStream &str, Lmbcs &lmbcs)
{
  str >> lmbcs.value_;
  return str;
}

Lmbcs operator +(const Lmbcs &lhs, const Lmbcs &rhs)
{
  return Lmbcs(lhs.value_ + rhs.value_);
}

QString LmbcsToQString::operator ()(const Lmbcs &lmbcs) const noexcept
{
  // 必要なバッファサイズを割り出す。
  int bufferSize = lmbcs.size() * sizeof(ushort);
  if (bufferSize < 0) return QString();

  // 関数オブジェクトLmbcsToUnicodeでUnicode(UTF-16)に変換する。
  QByteArray utf16 = lmbcsToUnicode(lmbcs.ref(), bufferSize);

  // バイナリ状のUTF-16からQStringオブジェクトを作成する。
  return QString::fromUtf16(
        reinterpret_cast<ushort*>(
          const_cast<char*>(utf16.constData())
          ),
        utf16.size() / sizeof(ushort)
        );
}

Lmbcs QStringToLmbcs::operator ()(const QString &qs) const noexcept
{
  // 必要なバッファサイズを割り出す。
  int bufferSize = qs.size() * 3;

  // QStringをUnicode(UTF-16)にする。
  QByteArray utf16(
        reinterpret_cast<const char*>(qs.utf16()),
        qs.size() * 2
        );

  // LMBCS化してLmbcsオブジェクトに変換して返す。
  return Lmbcs(unicodeToLmbcs(utf16, bufferSize));
}

QStringList LmbcsListToQStringList::operator ()(const LmbcsList &llist) const noexcept
{
  QStringList qlist;
  foreach (Lmbcs entry, llist) {
    qlist << lq(entry);
  }
  return qlist;
}

LmbcsList QStringListToLmbcsList::operator ()(const QStringList &qlist) const noexcept
{
  LmbcsList llist;
  foreach (QString entry, qlist) {
    llist << ql(entry);
  }
  return llist;
}

QString lq(const Lmbcs &lmbcs) noexcept
{
  return LmbcsToQString()(lmbcs);
}

Lmbcs ql(const QString &qs) noexcept
{
  return QStringToLmbcs()(qs);
}

QStringList lq(const LmbcsList &llist) noexcept
{
  return LmbcsListToQStringList()(llist);
}

LmbcsList ql(const QStringList &qlist) noexcept
{
  return QStringListToLmbcsList()(qlist);
}

} // namespace npp
