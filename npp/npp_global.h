#ifndef NPP_GLOBAL_H
#define NPP_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(NPP_LIBRARY)
#  define NPPSHARED_EXPORT Q_DECL_EXPORT
#else
#  define NPPSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // NPP_GLOBAL_H
