#include <gouda/npp/nls.h>

namespace npp {

NLSStatus::NLSStatus(NLS_STATUS status) noexcept
  : std::exception()
  , status_(status)
{}

const char *NLSStatus::what() const noexcept
{
  switch (status_)
  {
  case NLS_SUCCESS: return "Success.";
  case NLS_BADPARM: return "Bad Parameters.";
  case NLS_BUFFERTOOSMALL: return "Buffer too small.";
  case NLS_CHARSSTRIPPED: return "Characters stripped.";
  case NLS_ENDOFSTRING: return "End of string.";
  case NLS_FALLBACKUSED: return "Fallback used.";
  case NLS_FILEINVALID: return "File invalid.";
  case NLS_FILENOTFOUND: return "File not found.";
  case NLS_FINDFAILED: return "Find failed.";
  case NLS_INVALIDCHARACTER: return "Invalid character.";
  case NLS_INVALIDDATA: return "Invalid data.";
  case NLS_INVALIDENTRY: return "Invalid entry.";
  case NLS_INVALIDTABLE: return "Invalid table.";
  case NLS_PROPNOTFOUND: return "Property not found.";
  case NLS_STARTOFSTRING: return "Start of string.";
  case NLS_STRINGSIZECHANGED: return "String size changed.";
  case NLS_TABLEHEADERINVALID: return "Table header invalid.";
  case NLS_TABLENOTFOUND: return "Table not found.";
  }
  return "Unknown NLS Error.";
}

} // namespace npp
