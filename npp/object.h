#ifndef NPP_OBJECT_H
#define NPP_OBJECT_H

#include <gouda/npp/npp_global.h>

#include <QSharedPointer>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <global.h> // NULLHANDLE
#include <pool.h>
#include <osmem.h> // OSLockObject,OSUnlockObject

#ifdef NT
#pragma pack(pop)
#endif

namespace npp {

/**
 * @brief 指定したハンドル型とデリーターでハンドルラッパークラスを作成する。
 */
template <class T, class Deleter>
class _Object
{
  T handle_;
  mutable void *ptr_;
  Deleter deleter_;

protected:
  virtual void doDelete() noexcept
  {
    if (isValid())
      deleter_(handle());
  }

  void unlock() noexcept
  {
    if (isValid()) {
      if (ptr_ != nullptr) {
        OSUnlockObject(handle_);
        ptr_ = nullptr;
      }
      doDelete();
      handle_ = NULLHANDLE;
    }
  }

public:
  _Object(const T &handle, Deleter deleter) noexcept
    : handle_(handle)
    , ptr_(nullptr)
    , deleter_(deleter)
  {}

  virtual ~_Object() noexcept { unlock(); }

  T handle() const noexcept { return handle_; }

  bool isValid() const noexcept { return handle_ != NULLHANDLE; }

  template <typename R>
  R *lock() const
  {
    if (!isValid()) return nullptr;
    if (ptr_ == nullptr) {
      ptr_ = OSLockObject(handle_);
    }
    return reinterpret_cast<R*>(ptr_);
  }
};

/**
 * @brief ハンドルをDHANDLE、デリーターをOSMemFreeで実装したクラス
 */
class NPPSHARED_EXPORT Object
    : public _Object<DHANDLE, decltype(&OSMemFree)>
{
public:
  Object(DHANDLE handle) noexcept;

  virtual ~Object() noexcept {}
};

using ObjectPtr = QSharedPointer<Object>;

/**
 * @brief 指定した型の共有ポインタを任意の引数で生成する。
 */
template <class T, class... Args>
QSharedPointer<T> make_qshared(Args... args)
{
  return QSharedPointer<T>(new T(args...));
}

} // namespace npp

#endif // NPP_OBJECT_H
