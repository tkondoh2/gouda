﻿#ifndef NPP_STATUS_H
#define NPP_STATUS_H

#include <gouda/npp/lmbcs.h>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <global.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace npp {

/**
 * @brief ステータス値のラッパークラス
 */
class NPPSHARED_EXPORT Status
    : public std::exception
{
  STATUS status_;
  mutable Lmbcs what_;

public:
  Status() : status_(NOERROR) {}

  Status(STATUS status) : status_(status) {}

  STATUS error() const noexcept { return ERR(status_); }
  bool hasError() const noexcept { return error() != NOERROR; }
  bool noError() const noexcept { return !hasError(); }

  virtual const char *what() const noexcept override;
};

} // namespace npp

#endif // NPP_STATUS_H
