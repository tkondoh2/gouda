#ifndef NPP_TRANSLATE_H
#define NPP_TRANSLATE_H

#include <gouda/npp/npp_global.h>

#include <QByteArray>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <global.h>
//#include <nls.h>
#include <osmisc.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace npp {

/**
 * @brief 指定したモードで文字列変換する関数オブジェクト
 */
struct Translate
{
  static const int MAX_TRANSLATE_SIZE;

  QByteArray operator ()(
      WORD mode,
      const QByteArray &source,
      int bufferSize
      ) noexcept;
};

/**
 * @brief UNICODE(UTF-16)をLMBCS文字列に変換する。
 * @param source UNICODE(UTF-16)文字列
 * @param bufferSize バッファーサイズ
 * @return LMBCS文字列
 */
NPPSHARED_EXPORT
QByteArray unicodeToLmbcs(
    const QByteArray &source,
    int bufferSize = Translate::MAX_TRANSLATE_SIZE
    ) noexcept;

/**
 * @brief LMBCS文字列をUNICODE(UTF-16)に変換する。
 * @param source LMBCS文字列
 * @param bufferSize バッファーサイズ
 * @return UNICODE(UTF-16)文字列
 */
NPPSHARED_EXPORT
QByteArray lmbcsToUnicode(
    const QByteArray &source,
    int bufferSize = Translate::MAX_TRANSLATE_SIZE
    ) noexcept;

} // namespace npp

#endif // NPP_TRANSLATE_H
