#include "translate.h"

namespace npp {

const int Translate::MAX_TRANSLATE_SIZE = MAXWORD - 1;

QByteArray Translate::operator ()(
    WORD mode,
    const QByteArray &source,
    int bufferSize
    ) noexcept
{
  int size = std::min<int>(source.size(), MAX_TRANSLATE_SIZE);
  int bufSize = std::min<int>(
        std::max<int>(bufferSize, 16)
        , MAX_TRANSLATE_SIZE
        );
  QByteArray buffer(bufSize, '\0');
  WORD len = OSTranslate(
        mode
        , source.constData()
        , static_cast<WORD>(size)
        , buffer.data()
        , static_cast<WORD>(bufSize)
        );
  return buffer.left(static_cast<int>(len));
}

QByteArray unicodeToLmbcs(const QByteArray &source, int bufferSize) noexcept
{
  return Translate()(OS_TRANSLATE_UNICODE_TO_LMBCS, source, bufferSize);
}

QByteArray lmbcsToUnicode(const QByteArray &source, int bufferSize) noexcept
{
  return Translate()(OS_TRANSLATE_LMBCS_TO_UNICODE, source, bufferSize);
}

} // namespace npp
