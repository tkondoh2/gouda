#include "object.h"

namespace npp {

Object::Object(DHANDLE handle) noexcept
  : _Object<DHANDLE, decltype(&OSMemFree)>(handle, &OSMemFree)
{}

} // namespace npp
