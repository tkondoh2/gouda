#ifndef NPP_NLS_H
#define NPP_NLS_H

#include <gouda/npp/npp_global.h>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <global.h>
#include <../include/nls.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace npp {

/**
 * @brief NLS_STATUS値のラッパークラス
 */
class NPPSHARED_EXPORT NLSStatus
    : public std::exception
{
  NLS_STATUS status_;

public:
  explicit NLSStatus(NLS_STATUS status) noexcept;

  virtual const char *what() const noexcept override;
};

} // namespace npp

#endif // NPP_NLS_H
