#ifndef NPP_ADDIN_LOGMESSAGETEXT_HPP
#define NPP_ADDIN_LOGMESSAGETEXT_HPP

#include <gouda/npp/lmbcs.h>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <addin.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace npp {
namespace addin {

/**
 * @brief サーバーコンソールにステータスメッセージと任意のメッセージを出力する。
 */
template <class... Args>
void logMessageText(STATUS status, const Lmbcs &msg, Args... args) noexcept
{
  AddInLogMessageText(const_cast<char*>(msg.constData()), status, args...);
}

/**
 * @brief サーバーコンソールに任意のメッセージを出力する。
 */
template <class... Args>
void logMessageText(const Lmbcs &msg, Args... args) noexcept
{
  logMessageText(NOERROR, msg, args...);
}

} // namespace addin
} // namespace npp

#endif // NPP_ADDIN_LOGMESSAGETEXT_HPP
