﻿#ifndef GOUDA_NOTES_STATUS_HPP
#define GOUDA_NOTES_STATUS_HPP

#include <gouda/notes/notes_global.h>

#include <rxcpp/rx.hpp>
using namespace rxcpp;
using namespace rxcpp::operators;

#ifdef NT
#pragma pack(push, 1)
#endif

#include <global.h> // STATUS
#include <misc.h> // MAXSPRINTF
#include <osmisc.h> // OSLoadString

#ifdef NT
#pragma pack(pop)
#endif

namespace gouda {
namespace notes {

class Status
    : public std::exception
{
  STATUS status_;

public:
  Status() noexcept : std::exception(), status_(NOERROR) {}

  Status(STATUS status) noexcept : std::exception(), status_(status) {}

  STATUS error() const noexcept { return ERR(status_); }
  bool noError() const noexcept { return error() == NOERROR; }
  bool hasError() const noexcept { return !noError(); }

  virtual const char *what() const noexcept override
  {
    static char buffer[MAXSPRINTF] = "";
    WORD len = OSLoadString(
          NULLHANDLE,
          status_,
          buffer,
          MAXSPRINTF - 1
          );
    buffer[len] = '\0';
    return buffer;
  }
};

} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_STATUS_HPP
