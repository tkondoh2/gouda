﻿#ifndef GOUDA_NOTES_LOOKUP_HPP
#define GOUDA_NOTES_LOOKUP_HPP

#include <gouda/notes/lmbcs.hpp>
#include <gouda/notes/lmbcslist.hpp>
#include <gouda/notes/variant.hpp>
#include <gouda/notes/objecthandle.hpp>

#include <QMap>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <lookup.h> // NAMELookup
#include <stdnames.h> //

#ifdef NT
#pragma pack(pop)
#endif

namespace gouda {
namespace notes {

class Lookup
{
public:
  class ResultItem
  {
    WORD type_;
    Variant value_;

  public:
    ResultItem()
      : type_(TYPE_INVALID_OR_UNKNOWN)
      , value_()
    {}

    ResultItem(WORD type, Variant &&value)
      : type_(type)
      , value_(std::move(value))
    {}

    WORD type() const noexcept { return type_; }
    const Variant &value() const noexcept { return value_; }
  };

  class Result
  {
    Lmbcs view_;
    Lmbcs name_;
    int match_;
    QMap<Lmbcs, ResultItem> items_;

  public:
    Result()
      : view_()
      , name_()
      , match_(-1)
      , items_()
    {}

    Result(
        Lmbcs view,
        Lmbcs name,
        int match
        )
      : view_(view)
      , name_(name)
      , match_(match)
      , items_()
    {}

    const Lmbcs &view() const noexcept { return view_; }
    const Lmbcs &name() const noexcept { return name_; }
    int match() const noexcept { return match_; }
    const QMap<Lmbcs, ResultItem> &items() const noexcept { return items_; }

    void insert(const Lmbcs &name, WORD type, Variant &&value)
    {
      ResultItem item(type, std::move(value));
      items_.insert(name, std::move(item));
    }
  };

  class SerializedLmbcsList
  {
    QByteArray bytes_;
    int count_;

  public:
    SerializedLmbcsList(QByteArray &&buffer, int count)
      : bytes_(std::move(buffer))
      , count_(count)
    {}

    const char *constData() const noexcept { return bytes_.constData(); }
    int count() const noexcept { return count_; }
  };

  static SerializedLmbcsList serialize(const LmbcsList &list) noexcept
  {
    int bufSize = list.count(); // 文字列間の'\0'分を加算
    foreach (Lmbcs item, list) {
      bufSize += item.byteSize(); // 文字列バイト数分を加算
    }
    QByteArray buffer(bufSize, '\0');
    int pos = 0;
    foreach (Lmbcs item, list) {
      qstrcpy(buffer.data() + pos, item.constData());
      pos += item.byteSize() + 1; // 文字列間の'\0'
    }
    return SerializedLmbcsList(std::move(buffer), list.count());
  }

  observable<Result> operator ()(
        const Lmbcs &server,
        WORD flags,
        const LmbcsList &views,
        const LmbcsList &names,
        const LmbcsList &items
        ) noexcept(false)
  {
    return observable<>::create<Result>(
          [server, flags, views, names, items](
          subscriber<Result> s
          ) {

      auto viewList = serialize(views);
      auto nameList = serialize(names);
      auto itemList = serialize(items);

      DHANDLE handle = NULLHANDLE;
      Status status = NAMELookup(
            server.isEmpty() ? nullptr : server.constData(),
            flags,
            static_cast<WORD>(viewList.count()), viewList.constData(),
            static_cast<WORD>(nameList.count()), nameList.constData(),
            static_cast<WORD>(itemList.count()), itemList.constData(),
            &handle
            );
      if (status.hasError()) throw status;

      ObjectHandlePtr lookupPtr(new ObjectHandle(std::move(handle)));
      void *pLookup = lookupPtr->lock<void>();

      for (int viewIndex = 0; viewIndex < views.count(); ++viewIndex) {
        void *pName = nullptr;
        for (int nameIndex = 0; nameIndex < names.count(); ++nameIndex) {
          WORD matchCount = 0;
          pName = NAMELocateNextName(pLookup, pName, &matchCount);
          void *pMatch = nullptr;
          for (int matchIndex = 0; matchIndex < matchCount; ++matchIndex) {
            pMatch = NAMELocateNextMatch(pLookup, pName, pMatch);
            Result result(
                  views.at(viewIndex),
                  names.at(nameIndex),
                  matchIndex
                  );
            for (int itemIndex = 0; itemIndex < items.count(); ++itemIndex) {
              WORD dataType = 0, dataSize = 0;
              void *pItem = NAMELocateItem(
                    pMatch,
                    static_cast<WORD>(itemIndex),
                    &dataType,
                    &dataSize
                    );
              QByteArray bytes(static_cast<char*>(pItem), dataSize);
              Variant value(bytes);
              result.insert(
                    items.at(itemIndex),
                    dataType,
                    std::move(value)
                    );
            }
            s.on_next(result);
          }
        }
      }
      s.on_completed();
    });
  }
};

} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_LOOKUP_HPP
