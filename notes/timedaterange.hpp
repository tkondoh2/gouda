﻿#ifndef GOUDA_NOTES_TIMEDATERANGE_HPP
#define GOUDA_NOTES_TIMEDATERANGE_HPP

#include <gouda/notes/range.hpp>
#include <gouda/notes/timedate.hpp>

namespace gouda {
namespace notes {

class TimeDatePair
    : public Pair<TIMEDATE_PAIR>
{
public:
  TimeDatePair() : Pair<TIMEDATE_PAIR>() {}

  TimeDatePair(TIMEDATE_PAIR value) : Pair<TIMEDATE_PAIR>(value) {}

  Lmbcs convertToText(
      TFMT *pTextFormat = nullptr,
      void *pIntlFormat = nullptr
      ) const noexcept(false)
  {
    return convertToText(value(), pTextFormat, pIntlFormat);
  }

  static Lmbcs convertToText(
      TIMEDATE_PAIR value,
      TFMT *pTextFormat = nullptr,
      void *pIntlFormat = nullptr
      ) noexcept(false)
  {
    char buffer[MAXALPHATIMEDATEPAIR];
    WORD len = 0;
    Status status = ConvertTIMEDATEPAIRToText(
          pIntlFormat,
          pTextFormat,
          &value,
          buffer,
          MAXALPHATIMEDATEPAIR,
          &len
          );
    if (status.hasError()) throw status;
    return Lmbcs(buffer, len);
  }
};

class TimeDateRange
    : public Range<TIMEDATE, TIMEDATE_PAIR>
{
public:
  static const WORD value_type = TYPE_TIME_RANGE;

  TimeDateRange() : Range<TIMEDATE, TIMEDATE_PAIR>() {}

  TimeDateRange(const QByteArray &bytes)
    : Range<TIMEDATE, TIMEDATE_PAIR>(bytes)
  {}

  virtual Lmbcs convertSingleToText(
      TIMEDATE value
      ) const noexcept(false) override
  {
    return TimeDate::convertToText(value);
  }

  virtual Lmbcs convertPairToText(
      TIMEDATE_PAIR pair
      ) const noexcept(false) override
  {
    return TimeDatePair::convertToText(pair);
  }
};

} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_TIMEDATERANGE_HPP
