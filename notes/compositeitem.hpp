﻿#ifndef GOUDA_NOTES_COMPOSITEITEM_HPP
#define GOUDA_NOTES_COMPOSITEITEM_HPP

#include <gouda/notes/item.hpp>
#include <gouda/notes/cd/any.hpp>
#include <gouda/notes/cd/paragraph.hpp>
#include <gouda/notes/cd/text.hpp>
#include <gouda/notes/cd/tablebegin.hpp>
#include <gouda/notes/cd/tableend.hpp>
#include <gouda/notes/cd/tablecell.hpp>
#include <gouda/notes/cd/nestedtablebegin.hpp>
#include <gouda/notes/cd/nestedtableend.hpp>
#include <gouda/notes/cd/nestedtablecell.hpp>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <nsfnote.h> // NSFItemScan
#include <nsferr.h> // ERR_ITEM_NOT_FOUND

#ifdef NT
#pragma pack(pop)
#endif

#include <rxcpp/rx.hpp>
using namespace rxcpp;
using namespace rxcpp::operators;

namespace gouda {
namespace notes {

class CompositeItem
    : public Item
{
  cd::ItemList list_;

public:
  CompositeItem() : Item() {}

  CompositeItem(const Lmbcs &name, WORD flags, cd::ItemList &&list)
    : Item(name, flags)
    , list_(std::move(list))
  {}

  virtual ~CompositeItem() {}

  const cd::ItemList &list() const { return list_; }

  Lmbcs textOnly() const noexcept
  {
    Lmbcs text;
    foreach (cd::ItemPtr itemPtr, list_) {
      text += itemPtr->getText();
    }
    return text;
  }

  Lmbcs toHtml() const noexcept
  {
    Lmbcs text;
    foreach (cd::ItemPtr itemPtr, list_) {
      text += itemPtr->getHtml();
    }
    return text;
  }

  observable<cd::ItemPtr> iterate()
  {
    return observable<>::create<cd::ItemPtr>(
          [this](subscriber<cd::ItemPtr> s) {
      foreach (cd::ItemPtr itemPtr, list_) {
        s.on_next(itemPtr);
      }
      s.on_completed();
    });
  }

  static STATUS LNPUBLIC getDataCallback_old(
      char *pRec,
      WORD recType,
      DWORD,
      void *ptr
      )
  {
    auto pSub = static_cast<subscriber<cd::ItemPtr>*>(ptr);

    switch (recType)
    {
    case SIG_CD_TEXT: pSub->on_next(cd::ItemPtr(new cd::Text(&pRec))); break;
    case SIG_CD_PARAGRAPH: pSub->on_next(cd::ItemPtr(new cd::Paragraph(&pRec))); break;
    case SIG_CD_TABLEBEGIN: pSub->on_next(cd::ItemPtr(new cd::TableBegin(&pRec))); break;
    case SIG_CD_TABLEEND: pSub->on_next(cd::ItemPtr(new cd::TableEnd(&pRec))); break;
    case SIG_CD_TABLECELL: pSub->on_next(cd::ItemPtr(new cd::TableCell(&pRec))); break;
    case SIG_CD_NESTEDTABLEBEGIN: pSub->on_next(cd::ItemPtr(new cd::NestedTableBegin(&pRec))); break;
    case SIG_CD_NESTEDTABLEEND: pSub->on_next(cd::ItemPtr(new cd::NestedTableEnd(&pRec))); break;
    case SIG_CD_NESTEDTABLECELL: pSub->on_next(cd::ItemPtr(new cd::NestedTableCell(&pRec))); break;
    default:
      switch (cd::getSizeType(&pRec))
      {
      case cd::Long: pSub->on_next(cd::ItemPtr(new cd::Any<LSIG>(&pRec))); break;
      case cd::Word: pSub->on_next(cd::ItemPtr(new cd::Any<WSIG>(&pRec))); break;
      default: pSub->on_next(cd::ItemPtr(new cd::Any<BSIG>(&pRec))); break;
      }
    }
    return NOERROR;
  }

  static STATUS LNPUBLIC getDataCallback(
      char *pRec,
      WORD /*recType*/,
      DWORD,
      void *ptr
      )
  {
    auto pSub = static_cast<subscriber<cd::ItemPtr>*>(ptr);

    switch (cd::getSizeType(&pRec))
    {
    case cd::Long: pSub->on_next(cd::ItemPtr(new cd::Any<LSIG>(&pRec))); break;
    case cd::Word: pSub->on_next(cd::ItemPtr(new cd::Any<WSIG>(&pRec))); break;
    default: pSub->on_next(cd::ItemPtr(new cd::Any<BSIG>(&pRec))); break;
    }
    return NOERROR;
  }

  static observable<cd::ItemPtr> getData(const Item::Info &info) // ?Item::Info info?
  {
    return observable<>::create<cd::ItemPtr>([info](subscriber<cd::ItemPtr> sub) {
      Status status = EnumCompositeBuffer(
            info.valueId(),
            info.valueSize(),
            getDataCallback,
            &sub
            );
      if (status.hasError()) {
        sub.on_error(std::make_exception_ptr(status));
      }
      sub.on_completed();
    });
  }
};

} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_COMPOSITEITEM_HPP
