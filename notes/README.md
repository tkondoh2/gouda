﻿Notes/Domino C API wrapper Library
==================================

# History

## v0.0.25 (to v1.0.0)

* LinuxでRFC822ITEMDESC構造体をODSからそのままメモリコピーするとパディングによって正しく処理できない。Linuxでは構造体内の変数同士でコピーするようにする。
* RFC822ITEMDESC::body_の行末に0x0D,0x0Aが残ることがあるので、取り除く。

## v0.0.23 (to v1.0.0)

* hookdrv::Options
* observable<Item::Info> Item::getInfo(NOTEHANDLE)
* template<T> mime::Entity::scan(mime::DirectoryPtr,mime::EntityPtr,T)
* template<T> mime::Entity::scan(mime::DirectoryPtr,mime::EntityPtr,T)
* observable<mime::EntityPtr> mime::Entity::scanTree(mime::DirectoryPtr&,mime::EntityPtr&)
* mime::RfcText
* Item Note::getItem(NOTEHANDLE,Lmbcs&) on_errorを追加
* Lmbcs TimeDate::convertToText(TIMEDATE,TFMT*,void*) 修正
* TimeDate TimeDate::fromQDateTime(QDateTime&) 追加
* Lmbcs TimeDateRange::convertToText(TIMEDATE_PAIR,TFMT*,void*) 修正
* Variant mime::Rfc822Text対応

## v0.0.22 (to v1.0.0)

* Fix for g++

## v0.0.22 (to v1.0.0)

* hookdrv::Options
* hookdrv::Context::options()
* hookdrv::NoteContext::options()
* hookdrv::OpenNoteContext::options()
* hookdrv::UpdateNoteContext::options()
* Item::getInfo()内でエラーステータスを丸投げしていたものを、subscriber::on_error()でコントロールするように修正。

## v0.0.21 (to v1.0.0)

* Item::variant()
* Item::getAttachmentFileNames()
* Range<S,P>::toString() => Range<S,P>::toString(char)
* Variant::toNumber()
* Variant::toTimeDate()
* addin::AddinLogSink

## v0.0.20 (to v1.0.0)

* DistinguishedName::canonical追加
* http::Authenticate追加
* http::Context微調整
* http::Context::setAuthName追加
* http::Context::setAuthType追加
* http::Initialize追加
* http::Procedure追加
* http::ProcedureBase追加
* http::AppendProcedure追加
* http::ProcessProcedures追加
* http::Request::content追加
* http::Response修正
* http::makeResponse追加
* http::makeRedirectResponse追加
* LmbcsをQMapのキーにするため、operator<を追加
* LmbcsListコンストラクタにイニシャライザリストを追加
* Lookup追加
* Variantから返すテキスト型で、\0を\nに変換するロジックを追加

## v0.0.19 (to v1.0.0)

* CompositeItem修正
* http::Context修正
* http::Init修正
* http::Response修正

## v0.0.18 (to v1.0.0)

* cd::Any::getSizeType(char**) -> SizeType 見直し
* cd::Any::value() -> QByteArray 追加
* cd::Any::getText() -> Lmbcs 種別と定数がわかるように修正
* cd::Any::name(WORD) -> Lmbcs SIG_INVALIDに対応
* class cd::HotspotBegin 追加
* Item::value() -> QByteArray 追加
* cd::Paragraph::Paragraph() 追加
* cd::Text::Text() 追加
* cd::Text::Text(QByteArray) 追加
* CompositeItem::list() -> cd::ItemList 追加
* CompositeItem::iterate() -> observable<cd::ItemPtr> 追加
* CompositeItem::getDataCallback() -> STATUS 追加

## v0.0.17 (to v1.0.0)

* Lmbcs Item::convertToText(Lmbcs,WORD,bool) const
* observable<Item> Note::getItems(Lmbcs)
* void Note::downloadAttachment(Item::Info,Lmbcs,ENCRYPTION_KEY*) const
* NumberRange Note::getNumberRange(Lmbcs) const
* static void Note::downloadAttachment(NOTEHANDLE,Item::Info,Lmbcs,ENCRYPTION_KEY*)
* static NumberRange Note::getNumberRange(NOTEHANDLE,Lmbcs)
* static TimeDateRange Note::getTimeDateRange(NOTEHANDLE,Lmbcs)
* TIMEDATE TimeDate::value() const
* static QDateTime TimeDate::toQDateTime(TIMEDATE)
* NumberRange Variant::toNumberRange() const
* TimeDateRange Variant::toTimeDateRange() const

## v0.0.16 (to v1.0.0)

* DistinguishedName::getPart(Func)
* DistinguishedName::commonName()
* DistinguishedName::orgName()リファクタリング
* unidToText(UNID)
* Notes::openByUnid(DBHANDLE,UNID,WORD)

## v0.0.15 (to v1.0.0)

* Note::hasComposite(NOTEHANDLE)
* Note::hasMIME(NOTEHANDLE)
* Note::hasMIMEPart(NOTEHANDLE)
* Note::hasRFC822Text(NOTEHANDLE)
* mime::Directory::open(NOTEHANDLE)
* mime::Entity::scan(DirectoryPtr,Func)

## v0.0.14 (to v1.0.0)

* mime::ConversionControl
* Note::hasComposite
* Note::hasMIME
* Note::hasRFC822Text

## v0.0.13 (to v1.0.0)

* CentOS(g++)で通らないため、`Translate::MAX_TRANSLATE_SIZE`を`#defineMAX_TRANSLATE_SIZE`に変更

## v0.0.12 (to v1.0.0)

* BlockHandleのハンドル開放ロジックを、 `dontFreeBlockId` から `doNothing<BLOCKID>` に変更
* class CompositeItem
* ctor Item::Item(Lmbcs,WORD)
* class cd::Item
* tclass cd::BaseItem
* tclass cd::Any
* class cd::Text
* class cd::Paragraph
* class cd::TableBagin
* class cd::TableEnd
* class cd::TableCell
* class cd::NestedTableBegin
* class cd::NestedTableEnd
* class cd::NestedTableCell
* class mime::Directory
* class mime::Entity
* Lmbcs::operator +=(Lmbcs)
* Lmbcs unidToText(UNID)
* Note::getCompositeItems
* Note::hasMIMEPart
* Note::openExt(DBHANDLE,NOTEID,DWORD)
* Note::getItems(NOTEHANDLE,Lmbcs)
* Note::getCompositeItems(NOTEHANDLE,Lmbcs)
* Note::getItem(NOTEHANDLE,Lmbcs)

## v0.0.11 (to v1.0.0)

* Collection::getNoteIdFromFirst()
* class Compute
* class Formulaの継承元をHandle<>からLockableHandle<>に変更
* class hookdrv::Context
* class hookdrv::NoteContext
* class hookdrv::OpenNoteContext
* class hookdrv::UpdateNoteContext
* Session::getDataDirectory()
* TimeDate::toQDateTime()
* bool operator ==(TIMEDATE,TIMEDATE)
* bool operator !=(TIMEDATE,TIMEDATE)
* bool isNullPtr(void*)

## v0.0.10 (to v1.0.0)

* ヘッダーオンリー化
* BlockHandleのTパラメータを削除。lock()にRパラメータを追加。
* Lmbcs::split(char) => split(Lmbcs,char)
* QString queryValue(QUrlQuery,QString,QUrl::ComponentFormattingOption)を追加。
* LmbcsList::fromByteArray => LmbcsList::fromQByteArray
* LmbcsList::joinの変換ロジックから変換関数を除去し、QByteArrayList経由で操作する。
* ライブラリ維持のためVersionクラスを追加。

## v0.0.9 (to v1.0.0)

* Collection::size_type
* Collection::size
* Database::createNote
* Database::createDocument
* Note::getClass => Note::getNoteClass
* Note::setDbHandle
* Note::setId
* Note::setOriginatorId
* Note::setNoteClass
* Note::setModified
* Note::setFlags
* Note::setAccessed
* Note::setParentNoteId
* Note::setResponseCount
* Note::setResponsedIdTable
* Note::setString
* Note::update

## v0.0.8 (to v1.0.0)

* itemScanCallback => Note::itemScanCallback
* itemScan => Note::itemScan
* getItemInfo => Item::getInfo
* => SummaryBuffer
* getSummaryBuffer => SummaryBuffer::getString

## v0.0.7 (to v1.0.0)

* Lmbcs仕様追加。
* LmbcsList仕様追加。
* ObjectHandle仕様修正。
* Handleの追加。
* BlockHandleの追加。
* Pairテンプレートクラスの追加。
* Rangeテンプレートクラスの追加。
* Numberの追加。
* NumberRangeの追加。
* TimeDateの追加。
* TimeDateRangeの追加。
* DistinguishedNameの追加。
* Databaseの追加。
* Collectionの追加。
* Noteの追加。
* Itemの追加。
* Item::Infoの追加。
* Variantの追加。
* FileObjectの追加。
* Formulaの追加。
* Searchテンプレートクラスの追加。
* env::*の追加。
* tools(関数集)の追加。
* addin::logMessageTextの追加。
* http::Contextの追加。
* http::initの追加。
* http::ParsedRequestの追加。
* http::Requestの追加。
* http::Responseの追加。

## v0.0.6 (to v1.0.0)

* Handleテンプレートクラスを追加。
* ObjectHandleクラスを追加。
* make_handle_ptrテンプレート関数を追加。
* Session::getServerListメソッドを追加。
* Session::getServerLatencyメソッドを追加。
* 共有ライブラリ名をnotesからgouda_notesに変更。

## v0.0.5 (to v1.0.0)

* Translateクラスと関連関数を追加。
* Lmbcsクラスと関連関数を追加。
* LmbcsListクラスと関連関数を追加。

## v0.0.4 (to v1.0.0)

* Statusクラスを追加。
* Sessionクラスを追加。
