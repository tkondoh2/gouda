﻿#ifndef GOUDA_NOTES_COMPUTE_HPP
#define GOUDA_NOTES_COMPUTE_HPP

#include <gouda/notes/formula.hpp>
#include <gouda/notes/handle.hpp>
#include <gouda/notes/variant.hpp>
#include <gouda/notes/objecthandle.hpp>

namespace gouda {
namespace notes {

class Compute;
using ComputePtr = QSharedPointer<Compute>;
using _Compute = Handle<
  HCOMPUTE,
  decltype(&NSFComputeStop),
  decltype(&isNullPtr)
>;
//using ResultObject = ObjectHandle<DHANDLE, decltype(&OSMemFree)>;
using ResultObject = ObjectHandle;

class Compute
    : public _Compute
{
public:
  Compute() : _Compute(nullptr, &NSFComputeStop, &isNullPtr) {}

  Compute(HCOMPUTE handle)
    : _Compute(std::move(handle), &NSFComputeStop, &isNullPtr)
  {}

  virtual ~Compute() {}

  Variant evaluate(
      NOTEHANDLE hNote,
      bool *pNoteMatchesFormula = nullptr,
      bool *pNoteShouldBeDeleted = nullptr,
      bool *pNoteModified = nullptr
      )
  {
    DHANDLE valueHandle = NULLHANDLE;
    WORD valueLen = 0;
    Status status = NSFComputeEvaluate(
          handle(),
          hNote,
          &valueHandle,
          &valueLen,
          reinterpret_cast<BOOL*>(pNoteMatchesFormula),
          reinterpret_cast<BOOL*>(pNoteShouldBeDeleted),
          reinterpret_cast<BOOL*>(pNoteModified)
          );
    if (status.hasError()) throw status;
//    ResultObject valueObj(valueHandle, &OSMemFree);
    ResultObject valueObj(std::move(valueHandle));
    QByteArray valueData(
          valueObj.lock<char>(),
          static_cast<int>(valueLen)
          );
    return Variant(valueData);
  }

  static ComputePtr compute(FormulaPtr &f) noexcept(false)
  {
    HCOMPUTE handle = nullptr;
    Status status = NSFComputeStart(0, f->lock<void>(), &handle);
    if (status.hasError()) throw status;
    return make_handle_ptr<Compute>(handle);
  }
};

} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_COMPUTE_HPP
