﻿#ifndef GOUDA_NOTES_SESSION_HPP
#define GOUDA_NOTES_SESSION_HPP

#include <gouda/notes/lmbcs.hpp>

#include <gouda/notes/objecthandle.hpp>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <ns.h>
#include <nsfdb.h>
#include <osfile.h> // OSGetDataDirectory

#ifdef NT
#pragma pack(pop)
#endif

namespace gouda {
namespace notes {

struct ServerLatencyData
{
  DWORD clientToServerMS;
  DWORD serverToClientMS;
  WORD serverVersion;
};

struct Session
{
  template <typename Func>
  int operator ()(int argc, char *argv[], Func func) const noexcept
  {
    STATUS status = NotesInitExtended(argc, argv);
    int retCode = -1;
    if (status == NOERROR) {
      retCode = func(argc, argv);
      NotesTerm();
    }
    return retCode;
  }

  static observable<Lmbcs> getServerList(
      const Lmbcs &port = Lmbcs()
      ) noexcept
  {
    return observable<>::create<Lmbcs>([port](subscriber<Lmbcs> s) {
      DHANDLE handle = NULLHANDLE;
      Status result = NSGetServerList(
            port.isEmpty() ? nullptr : const_cast<char*>(port.constData()),
            &handle
            );
      if (result.hasError()) {
        s.on_error(std::make_exception_ptr(result));
      }
      else {
        ObjectHandlePtr list = make_handle_ptr<ObjectHandle>(std::move(handle));
        if (list->isValid()) {
          WORD *pwNameLen = list->lock<WORD>();
          int count = static_cast<int>(*pwNameLen++);
          char *pName = reinterpret_cast<char*>(pwNameLen + count);
          for (int i = 0; i < count; ++i) {
            Lmbcs name(pName, static_cast<int>(pwNameLen[i]));
            s.on_next(name);
            pName += pwNameLen[i];
          }
        }
      }
      s.on_completed();
    });
  }

  static ServerLatencyData getServerLatency(
      const Lmbcs &server,
      DWORD timeout = 0
      ) noexcept(false)
  {
    ServerLatencyData value;
    Status status = NSFGetServerLatency(
          const_cast<char*>(server.constData()),
          timeout,
          &value.clientToServerMS,
          &value.serverToClientMS,
          &value.serverVersion
          );
    if (status.hasError()) throw status;
    return value;
  }

  static Lmbcs getDataDirectory() noexcept
  {
    QByteArray buffer(MAXPATH, '\0');
    WORD len = OSGetDataDirectory(buffer.data());
    Lmbcs dir(buffer.data(), static_cast<int>(len));
    return dir.replace('\\', '/');
  }
};

} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_SESSION_HPP
