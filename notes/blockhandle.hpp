﻿#ifndef GOUDA_NOTES_BLOCKHANDLE_HPP
#define GOUDA_NOTES_BLOCKHANDLE_HPP

#include <gouda/notes/handle.hpp>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <pool.h> // NULLHANDLE
#include <osmem.h> // OSLockObject,OSUnlockObject

#ifdef NT
#pragma pack(pop)
#endif

namespace gouda {
namespace notes {

using _BlockHandle = Handle<
  BLOCKID,
  decltype(&doNothing<BLOCKID>),
  decltype(&isNullBlockId)
>;

class BlockHandle
    : public _BlockHandle
{
  mutable void *ptr_;

public:
  BlockHandle(BLOCKID &&handle) noexcept
    : _BlockHandle(std::move(handle), &doNothing<BLOCKID>, &isNullBlockId)
    , ptr_(nullptr)
  {}

  virtual ~BlockHandle() noexcept
  {
    unlock();
  }

  template <typename R>
  R *lock() const noexcept
  {
    if (isNull()) return nullptr;
    if (ptr_ == nullptr) {
      ptr_ = OSLockBlock(R, handle());
    }
    return reinterpret_cast<R*>(ptr_);
  }

  void unlock() const noexcept
  {
    if (isValid() && ptr_ != nullptr) {
      OSUnlockBlock(handle());
      ptr_ = nullptr;
    }
  }
};

} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_BLOCKHANDLE_HPP
