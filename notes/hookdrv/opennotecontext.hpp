﻿#ifndef GOUDA_NOTES_HOOKDRV_OPENNOTECONTEXT_HPP
#define GOUDA_NOTES_HOOKDRV_OPENNOTECONTEXT_HPP

#include <gouda/notes/hookdrv/notecontext.hpp>

namespace gouda {
namespace notes {
namespace hookdrv {

class OpenNoteContext
    : public NoteContext
{
protected:
  WORD OpenFlags_;

public:
  explicit OpenNoteContext(
      struct dbhookvec *vec,
      char *UserName,
      LIST *GroupList,
      DBHANDLE hDB,
      NOTEID NoteID,
      NOTEHANDLE hNote,
      WORD OpenFlags
      )
    : NoteContext(vec, UserName, GroupList, hDB, NoteID, hNote),
      OpenFlags_(OpenFlags)
  {}

  virtual ~OpenNoteContext() override {}

  virtual const char *type() const noexcept override { return "Open"; }

  virtual NOTE_OPERATION operation() const override { return Open; }

  virtual Options options() const override
  {
    return Options(NoteID_, hNote_, OpenFlags_, nullptr);
  }
};

} // namespace hookdrv
} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_HOOKDRV_OPENNOTECONTEXT_HPP
