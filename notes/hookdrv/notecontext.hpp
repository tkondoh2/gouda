﻿#ifndef GOUDA_NOTES_HOOKDRV_NOTECONTEXT_HPP
#define GOUDA_NOTES_HOOKDRV_NOTECONTEXT_HPP

#include <gouda/notes/hookdrv/context.hpp>

namespace gouda {
namespace notes {
namespace hookdrv {

class NoteContext
    : public Context
{
protected:
  NOTEID NoteID_;
  NOTEHANDLE hNote_;

public:
  enum NOTE_OPERATION { Open, Update, Create, Delete, DeleteStub };

  explicit NoteContext(
      struct dbhookvec *vec,
      char *UserName,
      LIST *GroupList,
      DBHANDLE hDB,
      NOTEID NoteID,
      NOTEHANDLE hNote
      )
    : Context(vec, UserName, GroupList, hDB),
      NoteID_(NoteID),
      hNote_(hNote)
  {}

  virtual ~NoteContext() override {}

  NOTEID NoteID() const { return NoteID_; }
  NOTEHANDLE hNote() const { return hNote_; }

  virtual NOTE_OPERATION operation() const = 0;

  virtual Options options() const override
  {
    return Options(NoteID_, hNote_, 0, nullptr);
  }
};

} // namespace hookdrv
} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_HOOKDRV_NOTECONTEXT_HPP
