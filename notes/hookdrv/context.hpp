﻿#ifndef GOUDA_NOTES_HOOKDRV_CONTEXT_HPP
#define GOUDA_NOTES_HOOKDRV_CONTEXT_HPP

#include <gouda/notes/database.hpp>

namespace gouda {
namespace notes {
namespace hookdrv {

class Options
{
  NOTEID NoteID_ ;
  NOTEHANDLE hNote_;
  WORD OpenFlags_;
  WORD *pUpdateFlags_;

public:
  Options()
    : NoteID_(0)
    , hNote_(NULLHANDLE)
    , OpenFlags_(0)
    , pUpdateFlags_(nullptr)
  {}

  Options(
      NOTEID NoteID,
      NOTEHANDLE hNote,
      WORD OpenFlags,
      WORD *pUpdateFlags
      )
    : NoteID_(NoteID)
    , hNote_(hNote)
    , OpenFlags_(OpenFlags)
    , pUpdateFlags_(pUpdateFlags)
  {}

  Options(const Options &other)
    : NoteID_(other.NoteID_)
    , hNote_(other.hNote_)
    , OpenFlags_(other.OpenFlags_)
    , pUpdateFlags_(other.pUpdateFlags_)
  {}

  NOTEID noteId() const { return NoteID_; }
  NOTEHANDLE hNote() const { return hNote_; }
  WORD openFlags() const { return OpenFlags_; }
  WORD *pUpdateFlags() const { return pUpdateFlags_; }

  WORD updateFlags() const
  {
    if (pUpdateFlags_ != nullptr) {
      return *pUpdateFlags_;
    }
    return 0;
  }
};

class Context
{
protected:
  dbhookvec *vec_;
  char *UserName_;
  LIST *GroupList_;
  DBHANDLE hDB_;

public:
  explicit Context(
      struct dbhookvec *vec,
      char *UserName,
      LIST *GroupList,
      DBHANDLE hDB
      )
    : vec_(vec),
      UserName_(UserName),
      GroupList_(GroupList),
      hDB_(hDB)
  {}

  virtual ~Context() {}

  char *UserName() const { return UserName_; }
  DBHANDLE hDB() const { return hDB_; }

  DBREPLICAINFO getReplicaInfo() const
  {
    return Database::getReplicaInfo(hDB_);
  }

  virtual const char *type() const noexcept = 0;

  virtual Options options() const
  {
    return Options(0, NULLHANDLE, 0, nullptr);
  }
};

} // namespace hookdrv
} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_HOOKDRV_CONTEXT_HPP
