﻿#ifndef GOUDA_NOTES_HOOKDRV_UPDATENOTECONTEXT_HPP
#define GOUDA_NOTES_HOOKDRV_UPDATENOTECONTEXT_HPP

#include <gouda/notes/hookdrv/notecontext.hpp>

namespace gouda {
namespace notes {
namespace hookdrv {

class UpdateNoteContext
    : public NoteContext
{
protected:
  WORD *pUpdateFlags_;

public:
  explicit UpdateNoteContext(
      struct dbhookvec *vec,
      char *UserName,
      LIST *GroupList,
      DBHANDLE hDB,
      NOTEID NoteID,
      NOTEHANDLE hNote,
      WORD *pUpdateFlags
      )
    : NoteContext(vec, UserName, GroupList, hDB, NoteID, hNote),
      pUpdateFlags_(pUpdateFlags)
  {}

  virtual ~UpdateNoteContext() override {}

  virtual const char *type() const noexcept override
  {
    switch (operation()) {
    case Create: return "Create";
    case Delete: return "Delete";
    case DeleteStub: return "DeleteStub";
    }
    return "Update";
  }

  virtual NOTE_OPERATION operation() const override
  {
    if (NoteID_ == NOTEID_ADD
        || NoteID_ == NOTEID_ADD_OR_REPLACE
        || NoteID_ == NOTEID_ADD_UNID
        )
    {
      if ((*pUpdateFlags_) & UPDATE_DELETED)
        return DeleteStub;
      else
        return Create;
    }
    else
    {
      if ((*pUpdateFlags_) & UPDATE_DELETED)
        return Delete;
    }
    return Update;
  }

  virtual Options options() const override
  {
    return Options(NoteID_, hNote_, 0, pUpdateFlags_);
  }
};

} // namespace hookdrv
} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_HOOKDRV_UPDATENOTECONTEXT_HPP
