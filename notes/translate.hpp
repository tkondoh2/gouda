﻿#ifndef GOUDA_NOTES_TRANSLATE_HPP
#define GOUDA_NOTES_TRANSLATE_HPP

#include <gouda/notes/status.hpp>

#include <QByteArray>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <osmisc.h>

#ifdef NT
#pragma pack(pop)
#endif

#define MAX_TRANSLATE_SIZE (MAXWORD - 1)

namespace gouda {
namespace notes {

/**
 * @brief 指定したモードで文字列変換する関数オブジェクト
 */
struct Translate
{
//  static const int MAX_TRANSLATE_SIZE = MAXWORD - 1;

  QByteArray operator ()(
      WORD mode,
      const QByteArray &source,
      int bufferSize = MAX_TRANSLATE_SIZE
      ) noexcept
  {
    int size = std::min<int>(source.size(), MAX_TRANSLATE_SIZE);
    int bufSize = std::min<int>(
          std::max<int>(bufferSize, 16)
          , MAX_TRANSLATE_SIZE
          );
    QByteArray buffer(bufSize, '\0');
    WORD len = OSTranslate(
          mode
          , source.constData()
          , static_cast<WORD>(size)
          , buffer.data()
          , static_cast<WORD>(bufSize)
          );
    return buffer.left(static_cast<int>(len));
  }
};

struct UnicodeToLmbcs
{
  /**
   * @brief UNICODE(UTF-16)をLMBCS文字列に変換する。
   * @param source UNICODE(UTF-16)文字列
   * @param bufferSize バッファーサイズ
   * @return LMBCS文字列
   */
  QByteArray operator ()(
      const QByteArray &source,
      int bufferSize = MAX_TRANSLATE_SIZE
//      int bufferSize = Translate::MAX_TRANSLATE_SIZE
      ) noexcept
  {
    return Translate()(OS_TRANSLATE_UNICODE_TO_LMBCS, source, bufferSize);
  }
};

struct LmbcsToUnicode
{
  /**
   * @brief LMBCS文字列をUNICODE(UTF-16)に変換する。
   * @param source LMBCS文字列
   * @param bufferSize バッファーサイズ
   * @return UNICODE(UTF-16)文字列
   */
  QByteArray operator ()(
      const QByteArray &source,
      int bufferSize = MAX_TRANSLATE_SIZE
//      int bufferSize = Translate::MAX_TRANSLATE_SIZE
      ) noexcept
  {
    return Translate()(OS_TRANSLATE_LMBCS_TO_UNICODE, source, bufferSize);
  }
};

} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_TRANSLATE_HPP
