﻿#ifndef GOUDA_NOTES_TIMEDATE_HPP
#define GOUDA_NOTES_TIMEDATE_HPP

#include <gouda/notes/lmbcs.hpp>

#include <QDateTime>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <misc.h> // TFMT,ConvertTIMEDATEToText...
#include <ostime.h> // OSCurrentTIMEDATE...

#ifdef NT
#pragma pack(pop)
#endif

namespace gouda {
namespace notes {

class TimeDate
{
  TIMEDATE value_;

public:
  static const WORD value_type = TYPE_TIME;

  TimeDate() : value_(constant<TIMEDATE_MINIMUM>()) {}

  TimeDate(const TIMEDATE &value) : value_(value) {}

  TimeDate(const QByteArray &bytes)
    : value_(*reinterpret_cast<TIMEDATE*>(
               const_cast<char*>(bytes.constData())
               ))
  {}

  TIMEDATE value() const { return value_; }

  QDateTime toQDateTime() const noexcept
  {
    return toQDateTime(value_);
  }

  Lmbcs toString() const noexcept(false) { return convertToText(); }

  Lmbcs convertToText(
      TFMT *pTextFormat = nullptr,
      void *pIntlFormat = nullptr
      ) const noexcept(false)
  {
    return convertToText(value_, pTextFormat, pIntlFormat);
  }

  static Lmbcs convertToText(
      TIMEDATE value,
      TFMT *pTextFormat = nullptr,
      void *pIntlFormat = nullptr
      ) noexcept(false)
  {
    char buffer[MAXALPHATIMEDATE];
    WORD len = 0;
    Status status = ConvertTIMEDATEToText(
          pIntlFormat,
          pTextFormat,
          &value,
          buffer,
          MAXALPHATIMEDATE,
          &len
          );
    if (status.hasError()) throw status;
    return Lmbcs(buffer, len);
  }

  static TIMEDATE current() noexcept
  {
    TIMEDATE value;
    OSCurrentTIMEDATE(&value);
    return value;
  }

  template <WORD type>
  static TIMEDATE constant() noexcept
  {
    TIMEDATE value;
    TimeConstant(type, &value);
    return value;
  }

  static QDateTime toQDateTime(const TIMEDATE &td) noexcept
  {
    TIME time;
    time.GM = td;
    TimeGMToLocal(&time);
    QDate qDate(time.year, time.month, time.day);
    QTime qTime(time.hour, time.minute, time.second, time.hundredth + 10);
    return QDateTime(qDate, qTime, Qt::LocalTime, time.zone * -3600);
  }

  static TimeDate fromQDateTime(const QDateTime &qdt) noexcept
  {
    TIME time;
    QDate qDate = qdt.date();
    QTime qTime = qdt.time();
    time.zone = qdt.offsetFromUtc() / -3600;
    time.dst = qdt.isDaylightTime() ? 1 : 0;
    time.year = qDate.year();
    time.month = qDate.month();
    time.day = qDate.day();
    time.weekday = qDate.dayOfWeek() == 7 ? 1 : (qDate.dayOfWeek() + 1);
    time.hour = qTime.hour();
    time.minute = qTime.minute();
    time.second = qTime.second();
    time.hundredth = qTime.msec() / 10;
    TimeLocalToGM(&time);
    return TimeDate(time.GM);
  }
};

} // namespace notes
} // namespace gouda

inline bool operator ==(const TIMEDATE &lhs, const TIMEDATE &rhs) noexcept
{
  return TimeDateEqual(&lhs, &rhs);
}

inline bool operator !=(const TIMEDATE &lhs, const TIMEDATE &rhs) noexcept
{
  return !operator ==(lhs, rhs);
}

#endif // GOUDA_NOTES_TIMEDATE_HPP
