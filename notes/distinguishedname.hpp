﻿#ifndef GOUDA_NOTES_DISTINGUISHEDNAME_HPP
#define GOUDA_NOTES_DISTINGUISHEDNAME_HPP

#include <gouda/notes/lmbcs.hpp>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <names.h>
#include <dname.h>
#include <kfm.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace gouda {
namespace notes {

class DistinguishedName;
using DName = DistinguishedName;

class DistinguishedName
{
  Lmbcs value_;

public:
  DistinguishedName() : value_() {}

  DistinguishedName(const Lmbcs &name) : value_(name) {}

  Lmbcs canonical() const
  {
    char buffer[MAXUSERNAME];
    WORD len = 0;
    Status status = DNCanonicalize(
          0L,
          nullptr,
          value_.constData(),
          buffer,
          MAXUSERNAME,
          &len
          );
    if (status.hasError()) throw status;
    return Lmbcs(buffer, len);
  }

  template <typename Func>
  Lmbcs getPart(Func extractor) const noexcept(false)
  {
    DN_COMPONENTS dn;
    Status status = DNParse(0, nullptr, value_.constData(), &dn, sizeof(dn));
    if (status.hasError()) throw status;
    return extractor(dn);
  }

  Lmbcs commonName() const noexcept(false)
  {
    return getPart([](const DN_COMPONENTS dn) {
      return Lmbcs(dn.CN, dn.CNLength);
    });
  }

  Lmbcs orgName() const noexcept(false)
  {
    return getPart([](const DN_COMPONENTS dn) {
      return Lmbcs(dn.O, dn.OLength);
    });
  }

  static DistinguishedName currentUserName() noexcept(false)
  {
    char buffer[MAXUSERNAME];
    Status status = SECKFMGetUserName(buffer);
    if (status.hasError()) throw status;
    return DName(buffer);
  }
};

using DName = DistinguishedName;

} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_DISTINGUISHEDNAME_HPP
