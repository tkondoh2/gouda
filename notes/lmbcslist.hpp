﻿#ifndef GOUDA_NOTES_LMBCSLIST_HPP
#define GOUDA_NOTES_LMBCSLIST_HPP

#include <gouda/notes/lmbcs.hpp>
#include <QList>
#include <QVector> // LmbcsList対策
#include <QSet> // LmbcsList対策

#ifdef NT
#pragma pack(push, 1)
#endif

#include <textlist.h> // ListGetNumEntries,ListGetText...

#ifdef NT
#pragma pack(pop)
#endif

namespace gouda {
namespace notes {

/**
 * @brief LMBCS文字列リストクラス
 */
class LmbcsList
    : public QList<Lmbcs>
{
public:
  static const WORD value_type = TYPE_TEXT_LIST;

  LmbcsList() : QList<Lmbcs>() {}

  LmbcsList(const QByteArray &bytes) noexcept(false)
    : QList<Lmbcs>()
  {
    *this = fromQByteArray(bytes);
  }

  LmbcsList(const QByteArrayList &list) noexcept
    : QList<Lmbcs>()
  {
    foreach (QByteArray item, list) {
      append(Lmbcs(std::move(item)));
    }
  }

  LmbcsList(std::initializer_list<Lmbcs> l)
    : QList<Lmbcs>(l)
  {}

  QByteArrayList toQByteArrayList() const noexcept
  {
    QByteArrayList l;
    foreach (Lmbcs item, *this) {
      l.append(item.value());
    }
    return l;
  }

  Lmbcs join(char delimiter) const noexcept
  {
    QByteArrayList l = toQByteArrayList();
    QByteArray v = l.join(delimiter);
    return Lmbcs(std::move(v));
  }

  Lmbcs join(const Lmbcs &delimiter) const noexcept
  {
    QByteArrayList l = toQByteArrayList();
    QByteArray v = l.join(delimiter.value());
    return Lmbcs(std::move(v));
  }

  static LmbcsList fromQByteArray(
      const QByteArray &bytes,
      bool hasPrefix = false
      ) noexcept(false)
  {
    int count = getTextListCount(bytes, hasPrefix);
    LmbcsList list;
    for (int i = 0; i < count; ++i) {
      list << getTextListEntry(bytes, i, hasPrefix);
    }
    return list;
  }

  static int getTextListCount(
      const QByteArray &bytes,
      bool hasPrefix = false
      ) noexcept
  {
    return static_cast<int>(ListGetNumEntries(bytes.constData(), hasPrefix));
  }

  static Lmbcs getTextListEntry(
      const QByteArray &bytes,
      int index,
      bool hasPrefix = false
      ) noexcept(false)
  {
    char *pEntry = nullptr;
    WORD entrySize = 0;
    Status status = ListGetText(
          bytes.constData(),
          hasPrefix,
          static_cast<WORD>(index),
          &pEntry,
          &entrySize
          );
    if (status.hasError()) throw status;
    return Lmbcs(pEntry, static_cast<int>(entrySize));
  }
};

/**
 * @brief LMBCS文字列リストをQStringリストに変換する。
 */
struct LmbcsListToQStringList
{
  QStringList operator ()(const LmbcsList &llist) const noexcept
  {
    QStringList qlist;
    foreach (Lmbcs entry, llist) {
      qlist << lq(entry);
    }
    return qlist;
  }
};

/**
 * @brief QStringリストをLMBCS文字列リストに変換する。
 */
struct QStringListToLmbcsList
{
  LmbcsList operator ()(const QStringList &qlist) const noexcept
  {
    LmbcsList llist;
    foreach (QString entry, qlist) {
      llist << ql(entry);
    }
    return llist;
  }
};

inline QStringList lq(const LmbcsList &llist) noexcept
{
  return LmbcsListToQStringList()(llist);
}

inline LmbcsList ql(const QStringList &qlist) noexcept
{
  return QStringListToLmbcsList()(qlist);
}

inline LmbcsList split(const Lmbcs &text, char delimiter) noexcept
{
  QByteArray v = text.value();
  QByteArrayList l = v.split(delimiter);
  return LmbcsList(l);
}

} // namespace notes
} // namespace gouda

/**
 * @brief LmbcsListでQList<Lmbcs>を継承するために必要
 * @param key
 * @param seed
 * @return
 */
inline uint qHash(const gouda::notes::Lmbcs &key, uint seed)
{
  return qHash(key.value(), seed);
}

#endif // GOUDA_NOTES_LMBCSLIST_HPP
