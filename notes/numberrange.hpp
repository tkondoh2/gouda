﻿#ifndef GOUDA_NOTES_NUMBERRANGE_HPP
#define GOUDA_NOTES_NUMBERRANGE_HPP

#include <gouda/notes/range.hpp>
#include <gouda/notes/number.hpp>

namespace gouda {
namespace notes {

class NumberPair
    : public Pair<NUMBER_PAIR>
{
public:
  NumberPair() : Pair<NUMBER_PAIR>() {}

  NumberPair(NUMBER_PAIR value) : Pair<NUMBER_PAIR>(value) {}

  Lmbcs convertToText(
      NFMT *pTextFormat = nullptr,
      void *pIntlFormat = nullptr
      ) const noexcept(false)
  {
    return convertToText(value(), pTextFormat, pIntlFormat);
  }

  static Lmbcs convertToText(
      NUMBER_PAIR value,
      NFMT *pTextFormat = nullptr,
      void *pIntlFormat = nullptr
      ) noexcept(false)
  {
    Lmbcs lower = Number::convertToText(value.Lower, pTextFormat, pIntlFormat);
    Lmbcs upper = Number::convertToText(value.Upper, pTextFormat, pIntlFormat);
    return lower + " - " + upper;
  }
};

class NumberRange
    : public Range<NUMBER, NUMBER_PAIR>
{
public:
  static const WORD value_type = TYPE_NUMBER_RANGE;

  NumberRange() : Range<NUMBER, NUMBER_PAIR>() {}

  NumberRange(const QByteArray &bytes) : Range<NUMBER, NUMBER_PAIR>(bytes) {}

  virtual Lmbcs convertSingleToText(NUMBER value) const override
  {
    return Number::convertToText(value);
  }

  virtual Lmbcs convertPairToText(NUMBER_PAIR pair) const override
  {
    return NumberPair::convertToText(pair);
  }
};

} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_NUMBERRANGE_HPP
