﻿#ifndef GOUDA_NOTES_FORMULA_HPP
#define GOUDA_NOTES_FORMULA_HPP

#include <gouda/notes/objecthandle.hpp>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <nsfsearc.h>
#include <osmem.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace gouda {
namespace notes {

class CompileError
    : public Status
{
  WORD line_;
  WORD col_;
  WORD offset_;
  WORD length_;

public:
  CompileError()
    : Status()
    , line_(0)
    , col_(0)
    , offset_(0)
    , length_(0)
  {}

  CompileError(STATUS status, WORD line, WORD col, WORD offset, WORD length)
    : Status(status)
    , line_(line)
    , col_(col)
    , offset_(offset)
    , length_(length)
  {}

  WORD line() const { return line_; }

  WORD col() const { return col_; }

  WORD offset() const { return offset_; }

  WORD length() const { return length_; }
};

class Formula;
using FormulaPtr = QSharedPointer<Formula>;
//using _Formula = Handle<
//  FORMULAHANDLE,
//  decltype(&OSMemFree),
//  decltype(&isNullHandle<FORMULAHANDLE>)
//>;
using _Formula = LockableHandle<
  FORMULAHANDLE,
  decltype(&OSMemFree)
>;

class Formula
    : public _Formula
{
public:
//  Formula()
//    : _Formula(
//        NULLHANDLE,
//        &OSMemFree,
//        &isNullHandle<FORMULAHANDLE>
//        )
//  {}

//  Formula(FORMULAHANDLE &&hFormula)
//    : _Formula(
//        std::move(hFormula),
//        &OSMemFree,
//        &isNullHandle<FORMULAHANDLE>
//        )
//  {}
  Formula()
    : _Formula(
        NULLHANDLE,
        &OSMemFree
        )
  {}

  Formula(FORMULAHANDLE &&hFormula)
    : _Formula(
        std::move(hFormula),
        &OSMemFree
      )
  {}

  virtual ~Formula() {}

  static FormulaPtr compile(
      const Lmbcs &text,
      const Lmbcs &column = ""
      ) noexcept(false)
  {
    FORMULAHANDLE handle = NULLHANDLE;
    STATUS compStatus = NOERROR;
    WORD formulaLen = 0, line = 0, col = 0, offset = 0, length = 0;
    Status status = NSFFormulaCompile(
          column.isEmpty() ? nullptr : const_cast<char*>(column.constData()),
          static_cast<WORD>(column.byteSize()),
          const_cast<char*>(text.constData()),
          static_cast<WORD>(text.byteSize()),
          &handle,
          &formulaLen, // バイナリ形式の＠式の長さ
          &compStatus,
          &line,
          &col,
          &offset,
          &length
          );
    if (status.hasError()) throw status;
    CompileError compileError { compStatus, line, col, offset, length };
    if (compileError.hasError()) throw compileError;
    return make_handle_ptr<Formula>(handle);
  }
};

} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_FORMULA_HPP
