﻿#ifndef GOUDA_NOTES_OBJECTHANDLE_HPP
#define GOUDA_NOTES_OBJECTHANDLE_HPP

#include <gouda/notes/handle.hpp>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <pool.h>
#include <osmem.h> // OSLockObject,OSUnlockObject

#ifdef NT
#pragma pack(pop)
#endif

namespace gouda {
namespace notes {

/**
 * @brief 指定したハンドル型とデリーターでハンドルラッパークラスを作成する。
 */
template <typename T, typename Deleter>
class LockableHandle
    : public Handle<
      T,
      Deleter,
      decltype(&isNullHandle<T>)
    >
{
  mutable void *ptr_;

public:
  LockableHandle(T &&handle, Deleter deleter) noexcept
    : Handle<
        T,
        Deleter,
        decltype(&isNullHandle<T>)>(
          std::move(handle),
          deleter,
          &isNullHandle<T>
          )
    , ptr_(nullptr)
  {}

  virtual ~LockableHandle() noexcept
  {
    unlock();
  }

  template <typename R>
  R *lock() const noexcept
  {
    if (this->isNull()) return nullptr;
    if (ptr_ == nullptr) {
      ptr_ = OSLockObject(this->handle());
    }
    return reinterpret_cast<R*>(ptr_);
  }

  void unlock() const noexcept
  {
    if (this->isValid() && ptr_ != nullptr) {
      OSUnlockObject(this->handle());
      ptr_ = nullptr;
    }
  }
};

/**
 * @brief ハンドルをDHANDLE、デリーターをOSMemFreeで実装したクラス
 */
class ObjectHandle
    : public LockableHandle<DHANDLE, decltype(&OSMemFree)>
{
public:
  ObjectHandle(DHANDLE &&handle) noexcept
    : LockableHandle<DHANDLE, decltype(&OSMemFree)>(
      std::move(handle),
      &OSMemFree
      )
  {}

  virtual ~ObjectHandle() noexcept {}
};

using ObjectHandlePtr = QSharedPointer<ObjectHandle>;

} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_OBJECTHANDLE_HPP
