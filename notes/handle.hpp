﻿#ifndef GOUDA_NOTES_HANDLE_HPP
#define GOUDA_NOTES_HANDLE_HPP

#include <gouda/notes/notes_global.h>
#include <gouda/notes/tools.hpp>

#include <QSharedPointer>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <global.h> // NULLHANDLE

#ifdef NT
#pragma pack(pop)
#endif

namespace gouda {
namespace notes {

template <typename T, typename Deleter, typename IsNull>
class Handle
{
  T handle_;
  Deleter deleter_;
  IsNull isNull_;

public:
  Handle(T &&handle, Deleter deleter, IsNull _isNull) noexcept
    : handle_(std::move(handle))
    , deleter_(deleter)
    , isNull_(_isNull)
  {}

  virtual ~Handle() noexcept
  {
    if (isValid()) deleter_(handle());
  }

  T handle() const noexcept { return handle_; }
  bool isValid() const noexcept { return !isNull(); }
  bool isNull() const noexcept
  {
    return isNull_(handle());
  }
};

} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_HANDLE_HPP
