﻿#ifndef GOUDA_NOTES_ADDIN_LOGMESSAGETEXT_HPP
#define GOUDA_NOTES_ADDIN_LOGMESSAGETEXT_HPP

#include <gouda/notes/lmbcs.hpp>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <addin.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace gouda {
namespace notes {
namespace addin {

template <class... Args>
void logMessageText(STATUS status, const Lmbcs &msg, Args... args) noexcept
{
  AddInLogMessageText(const_cast<char*>(msg.constData()), status, args...);
}

template <class... Args>
void logMessageText(const Lmbcs &msg, Args... args) noexcept
{
  logMessageText(NOERROR, msg, args...);
}

} // namespace addin
} // namespace notes
} // amespace gouda

#endif // GOUDA_NOTES_ADDIN_LOGMESSAGETEXT_HPP
