﻿#ifndef GOUDA_NOTES_ADDIN_ADDINLOGSINK_HPP
#define GOUDA_NOTES_ADDIN_ADDINLOGSINK_HPP

#include <gouda/notes/addin/logmessagetext.hpp>

#include <gouda/core/loggerV2/sink.hpp>

namespace gcl {
using namespace gouda::core::loggerv2;
}

namespace gouda {
namespace notes {
namespace addin {

/**
 * @brief アドインログ用のシンク
 */
template <typename Formatter>
class AddinLogSink
    : public gcl::Sink
{
  Formatter formatter_;

public:
  /**
   * @brief コンストラクタ
   * @param threshold ログレベルの閾値
   */
  AddinLogSink(gcl::Level threshold = gcl::Level::Info)
    : gcl::Sink(threshold)
    , formatter_()
  {}

  /**
   * @brief コンストラクタ
   * @param threshold ログレベルの閾値
   */
  AddinLogSink(Formatter &&formatter, gcl::Level threshold = gcl::Level::Info)
    : gcl::Sink(threshold)
    , formatter_(std::move(formatter))
  {}

  virtual ~AddinLogSink() override {}

  /**
   * @brief ログ文字列をアドインログに出力する。
   * @param text ログ文字列
   */
  virtual void _output(
      const QDateTime &time,
      const QString &threadId,
      gcl::Level level,
      const QString &msg
      ) override
  {
    logMessageText(ql(formatter_(time, threadId, level, msg)));
  }
};

} // namespace addin
} // namespace notes
} // amespace gouda

#endif // GOUDA_NOTES_ADDIN_ADDINLOGSINK_HPP
