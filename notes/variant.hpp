﻿#ifndef GOUDA_NOTES_VARIANT_HPP
#define GOUDA_NOTES_VARIANT_HPP

#include <gouda/notes/lmbcs.hpp>
#include <gouda/notes/numberrange.hpp>
#include <gouda/notes/timedaterange.hpp>
#include <gouda/notes/mime/rfc822text.hpp>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <nsfnote.h> // NSFItemScan
#include <nsferr.h> // ERR_ITEM_NOT_FOUND

#ifdef NT
#pragma pack(pop)
#endif

#include <rxcpp/rx.hpp>
using namespace rxcpp;
using namespace rxcpp::operators;

namespace gouda {
namespace notes {

class Variant
{
  QByteArray raw_;

public:
  Variant() : raw_() {}

  Variant(const QByteArray &raw) : raw_(raw) {}

  QByteArray raw() const { return raw_; }

  int rawSize() const { return raw_.size(); }

  bool isValid() const { return rawSize() >= static_cast<int>(sizeof(WORD)); }

  int size() const { return isValid() ? (rawSize() - sizeof(WORD)) : (-1); }

  WORD type() const
  {
    return isValid()
        ? *reinterpret_cast<const WORD*>(raw_.constData())
        : TYPE_INVALID_OR_UNKNOWN;
  }

  Lmbcs typeName() const
  {
    WORD type_ = type();
    switch (type_)
    {
    case TYPE_TEXT: return Lmbcs("Text");
    case TYPE_TEXT_LIST: return Lmbcs("TextList");
    case TYPE_NUMBER: return Lmbcs("Number");
    case TYPE_NUMBER_RANGE: return Lmbcs("NumberRange");
    case TYPE_TIME: return Lmbcs("TimeDate");
    case TYPE_TIME_RANGE: return Lmbcs("TimeDateRange");
    case TYPE_OBJECT: return Lmbcs("Object");
    case TYPE_RFC822_TEXT: return Lmbcs("RFC822Text");
    }
    return ql(QString("Invalid or Unknown(%1)").arg(type_));
  }

  QByteArray value() const
  {
    return isValid() ? raw_.right(size()) : QByteArray();
  }

  template <class T>
  bool canConvert() const
  {
    return (type() == T::value_type);
  }

  template <class T>
  T convert() const
  {
    return T(value());
  }

  Lmbcs toString() const
  {
    switch (type()) {

    case TYPE_TEXT:
    {
      Lmbcs s = convert<Lmbcs>();
      s.replace('\0', '\n');
      return s;
    }

    case TYPE_TEXT_LIST:
      return convert<LmbcsList>().join(",");

    case TYPE_NUMBER:
      return convert<Number>().toString();

    case TYPE_NUMBER_RANGE:
      return convert<NumberRange>().toString();

    case TYPE_TIME:
      return convert<TimeDate>().toString();

    case TYPE_TIME_RANGE:
      return convert<TimeDateRange>().toString();

    case TYPE_RFC822_TEXT:
    {
      auto v = convert<mime::Rfc822Text>();
      if (v.isTextList()) { return v.textList().join(","); }
      else if (v.isDate()) { return v.date().toString(); }
      else { return v.text(); }
    }
      break;
    }

    return Lmbcs();
  }

  LmbcsList toStringList() const
  {
    LmbcsList list;

    switch (type()) {

    case TYPE_TEXT:
      list.append(toString());
      break;

    case TYPE_TEXT_LIST:
      list = convert<LmbcsList>();
      break;

    case TYPE_NUMBER:
      list.append(convert<Number>().toString());
      break;

    case TYPE_NUMBER_RANGE:
    {
      NumberRange values = convert<NumberRange>();
      Lmbcs text = values.toString();
      list = split(text, ',');
    }
      break;

    case TYPE_TIME:
      list.append(convert<TimeDate>().toString());
      break;

    case TYPE_TIME_RANGE:
    {
      TimeDateRange values = convert<TimeDateRange>();
      Lmbcs text = values.toString();
      list = split(text, ',');
    }
      break;

    case TYPE_RFC822_TEXT:
    {
      auto v = convert<mime::Rfc822Text>();
      if (v.isTextList()) { list.append(v.textList()); }
      else if (v.isDate()) { list.append(v.date().toString()); }
      else { list.append(v.text()); }
    }
      break;
    }

    return list;
  }

  Number toNumber() const
  {
    switch (type()) {

    case TYPE_NUMBER:
      return convert<Number>();

    case TYPE_NUMBER_RANGE:
    {
      NumberRange r = convert<NumberRange>();
      return r.singles().at(0);
    }

    default:
      return Number(0);
    }
  }

  NumberRange toNumberRange() const
  {
    switch (type()) {

    case TYPE_NUMBER_RANGE:
      return convert<NumberRange>();

    case TYPE_NUMBER:
    {
      Number v = convert<Number>();
      NumberRange range;
      range.singles().append(v.value());
      return range;
    }

    default:
      return NumberRange();
    }
  }

  TimeDate toTimeDate() const
  {
    switch (type()) {

    case TYPE_TIME:
      return convert<TimeDate>();

    case TYPE_TIME_RANGE:
    {
      TimeDateRange r = convert<TimeDateRange>();
      return r.singles().at(0);
    }

    case TYPE_RFC822_TEXT:
    {
      auto v = convert<mime::Rfc822Text>();
      if (v.isDate()) { return v.date(); }
    }
      break;
    }
    return TimeDate();
  }

  TimeDateRange toTimeDateRange() const
  {
    TimeDateRange result;

    switch (type()) {

    case TYPE_TIME_RANGE:
      return convert<TimeDateRange>();

    case TYPE_TIME:
    {
      auto v = convert<TimeDate>();
      result.singles().append(v.value());
    }
      break;

    case TYPE_RFC822_TEXT:
    {
      auto v = convert<mime::Rfc822Text>();
      if (v.isDate()) {
        result.singles().append(v.date().value());
      }
    }
      break;
    }

    return result;
  }

  mime::Rfc822Text toRfc822Text() const
  {
    if (type() == TYPE_RFC822_TEXT) {
      return convert<mime::Rfc822Text>();
    }
    return mime::Rfc822Text();
  }

  Lmbcs firstString() const
  {
    switch (type()) {

    case TYPE_TEXT:
      return convert<Lmbcs>();

    case TYPE_TEXT_LIST:
    {
      LmbcsList list = convert<LmbcsList>();
      return list.at(0);
    }

    case TYPE_NUMBER:
      return convert<Number>().toString();

    case TYPE_NUMBER_RANGE:
      return convert<NumberRange>().toString();

    case TYPE_TIME:
      return convert<TimeDate>().toString();

    case TYPE_TIME_RANGE:
      return convert<TimeDateRange>().toString();

    case TYPE_RFC822_TEXT:
    {
      auto v = convert<mime::Rfc822Text>();
      if (v.isTextList()) { return v.textList().at(0); }
//      else if (v.isDate()) { return v.date().toString(); } // そのまま出す
      else { return v.text(); }
    }
      break;
    }

    return Lmbcs();
  }

  Number firstNumber() const
  {
    switch (type()) {

    case TYPE_NUMBER:
      return convert<Number>();

    case TYPE_NUMBER_RANGE:
      NumberRange range = convert<NumberRange>();
      return Number(range.singles().at(0));
    }

    return Number();
  }

  TimeDate firstTimeDate() const
  {
    switch (type()) {

    case TYPE_TIME:
      return convert<TimeDate>();

    case TYPE_TIME_RANGE:
    {
      TimeDateRange range = convert<TimeDateRange>();
      if (!range.singles().isEmpty())
        return TimeDate(range.singles().at(0));
    }
      break;

    case TYPE_RFC822_TEXT:
    {
      auto v = convert<mime::Rfc822Text>();
      if (v.isDate()) { return v.date(); }
    }
      break;
    }

    return TimeDate();
  }

  static Variant fromSummary(void *pSummary, const Lmbcs &name) noexcept
  {
    char *ptr = nullptr;
    WORD wItemLen = 0, wItemType = 0;
    if (NSFLocateSummaryValue(
          pSummary,
          name.constData(),
          &ptr,
          &wItemLen,
          &wItemType
          )) {
      QByteArray bytes(ptr - sizeof(WORD), wItemLen + sizeof(WORD));
      return Variant(bytes);
    }
    return Variant();
  }
};

} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_VARIANT_HPP
