﻿#ifndef GOUDA_NOTES_SUMMARYBUFFER_HPP
#define GOUDA_NOTES_SUMMARYBUFFER_HPP

#include <gouda/notes/notes_global.h>
#include <gouda/notes/lmbcs.hpp>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <nsfnote.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace gouda {
namespace notes {

class SummaryBuffer
{
  void *ptr_;

public:
  SummaryBuffer() : ptr_(nullptr) {}

  template <WORD bufferSize>
  Lmbcs getString(const Lmbcs &name) noexcept
  {
    return getString(ptr_, name);
  }

  template <WORD bufferSize>
  static Lmbcs getString(const void *ptr, const Lmbcs &name) noexcept
  {
    char value[bufferSize];
    if (NSFGetSummaryValue(ptr, name.constData(), value, bufferSize)) {
      return Lmbcs(value);
    }
    return Lmbcs();
  }
};

} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_SUMMARYBUFFER_HPP
