﻿#ifndef GOUDA_NOTES_NOTE_HPP
#define GOUDA_NOTES_NOTE_HPP

#include <gouda/notes/notes_global.h>
#include <gouda/notes/handle.hpp>
#include <gouda/notes/lmbcs.hpp>
#include <gouda/notes/item.hpp>
#include <gouda/notes/compositeitem.hpp>
#include <gouda/notes/blockhandle.hpp>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <nsfnote.h>
#include <mime.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace gouda {
namespace notes {

inline Lmbcs unidToText(const UNID &unid) noexcept
{
  return formatId<DWORD, uint>(unid.File.Innards[1])
      + formatId<DWORD, uint>(unid.File.Innards[0])
      + formatId<DWORD, uint>(unid.Note.Innards[1])
      + formatId<DWORD, uint>(unid.Note.Innards[0])
      ;
}

class Note;

using NotePtr = QSharedPointer<Note>;
using _Note = Handle<
  NOTEHANDLE,
  decltype(&NSFNoteClose),
  decltype(&isNullHandle<NOTEHANDLE>)
>;

class Note
    : public _Note
{
public:
  Note() noexcept
    : _Note(
        NULLHANDLE,
        &NSFNoteClose,
        &isNullHandle<NOTEHANDLE>
        )
  {}

  Note(NOTEHANDLE &&handle) noexcept
    : _Note(
        std::move(handle),
        &NSFNoteClose,
        &isNullHandle<NOTEHANDLE>
        )
  {}

  virtual ~Note() noexcept {}

  WORD getItemFlags(const Item::Info &itemInfo) noexcept
  {
    return getItemFlags(handle(), itemInfo);
  }

  observable<CompositeItem> getCompositeItems(
      const Lmbcs &name
      ) noexcept(false)
  {
    return getCompositeItems(handle(), name);
  }

  Item getItem(const Lmbcs &name) const noexcept(false)
  {
    return getItem(handle(), name);
  }

  observable<Item> getItems(const Lmbcs &name) noexcept(false)
  {
    return getItems(handle(), name);
  }

  DBHANDLE getDbHandle() const noexcept { return getDbHandle(handle()); }

  NOTEID getId() const noexcept { return getId(handle()); }

  OID getOriginatorId() const noexcept { return getOriginatorId(handle()); }

  WORD getNoteClass() const noexcept { return getNoteClass(handle()); }

  TIMEDATE getModified() const noexcept { return getModified(handle()); }

  WORD getFlags() const noexcept { return getFlags(handle()); }

  TIMEDATE getAccessed() const noexcept { return getAccessed(handle()); }

  NOTEID getParentNoteId() const noexcept { return getParentNoteId(handle()); }

  DWORD getResponseCount() const noexcept { return getResponseCount(handle()); }

  HANDLE getResponsedIdTable() const noexcept { return getResponsedIdTable(handle()); }

  void setDbHandle(DBHANDLE data) noexcept { setDbHandle(handle(), data); }

  void setId(NOTEID data) noexcept { setId(handle(), data); }

  void setOriginatorId(OID data) noexcept { setOriginatorId(handle(), data); }

  void setNoteClass(WORD data) noexcept { setNoteClass(handle(), data); }

  void setModified(TIMEDATE data) noexcept { setModified(handle(), data); }

  void setFlags(WORD data) noexcept { setFlags(handle(), data); }

  void setAccessed(TIMEDATE data) noexcept { setAccessed(handle(), data); }

  void setParentNoteId(NOTEID data) noexcept { setParentNoteId(handle(), data); }

  void setResponseCount(DWORD data) noexcept { setResponseCount(handle(), data); }

  void setResponsedIdTable(HANDLE data) noexcept { setResponsedIdTable(handle(), data); }

  UNID getUnid() const noexcept { return getUnid(handle()); }

  Lmbcs getUnidText() const noexcept { return getUnidText(handle()); }

  void downloadAttachment(
      const Item::Info &itemInfo,
      const Lmbcs &filePath,
      ENCRYPTION_KEY *pDecryptionKey = nullptr
      ) const noexcept(false)
  {
    downloadAttachment(
          handle(),
          itemInfo,
          filePath,
          pDecryptionKey
          );
  }

  QByteArray extractAttachment(const Item::Info &itemInfo) const noexcept(false)
  {
    return extractAttachment(handle(), itemInfo);
  }

  Lmbcs getString(const Lmbcs &field) const
  {
    return getString(handle(), field);
  }

  void setString(
      const Lmbcs &field,
      const Lmbcs &value,
      bool isSummary = true
      ) noexcept(false)
  {
    setString(handle(), field, value, isSummary);
  }

  LmbcsList getStringList(const Lmbcs &field) const
  {
    return getStringList(handle(), field);
  }

  Number getNumber(const Lmbcs &field) const
  {
    return getNumber(handle(), field);
  }

  NumberRange getNumberRange(const Lmbcs &field) const
  {
    return getNumberRange(handle(), field);
  }

  TimeDateRange getTimeDateRange(const Lmbcs &field) const
  {
    return getTimeDateRange(handle(), field);
  }

  void update(WORD updateFlags = 0) noexcept(false)
  {
    update(handle(), updateFlags);
  }

  observable<Item> itemScan() noexcept(false)
  {
    return itemScan(handle());
  }

  bool hasComposite() noexcept
  {
    return hasComposite(handle());
  }

  BOOL hasMIME() noexcept
  {
    return hasMIME(handle());
  }

  bool hasMIMEPart() noexcept
  {
    return hasMIMEPart(handle());
  }

  bool hasRFC822Text() noexcept
  {
    return hasRFC822Text(handle());
  }

  void convertCDParts(BOOL bCanonical, BOOL bIsMIME, CCHANDLE hCC) noexcept(false)
  {
    Status status = MIMEConvertCDParts(
          handle(),
          bCanonical,
          bIsMIME,
          hCC
          );
    if (status.hasError()) throw status;
  }

  static NotePtr open(
      DBHANDLE dbHandle,
      NOTEID noteId,
      WORD flag
      ) noexcept(false)
  {
    NOTEHANDLE handle = NULLHANDLE;
    Status status = NSFNoteOpen(dbHandle, noteId, flag, &handle);
    if (status.hasError()) throw status;
    Q_ASSERT(handle);
    return make_handle_ptr<Note>(handle);
  }

  static NotePtr openExt(
      DBHANDLE dbHandle,
      NOTEID noteId,
      DWORD flags
      ) noexcept(false)
  {
    NOTEHANDLE handle = NULLHANDLE;
    Status status = NSFNoteOpenExt(dbHandle, noteId, flags, &handle);
    if (status.hasError()) throw status;
    Q_ASSERT(handle);
    return make_handle_ptr<Note>(handle);
  }

  static NotePtr openByUnid(
      DBHANDLE dbHandle,
      const UNID &unid,
      WORD flag
      ) noexcept(false)
  {
    NOTEHANDLE handle = NULLHANDLE;
    Status status = NSFNoteOpenByUNID(
          dbHandle,
          const_cast<UNID*>(&unid),
          flag,
          &handle
          );
    if (status.hasError()) throw status;
    Q_ASSERT(handle);
    return make_handle_ptr<Note>(handle);
  }

  static observable<Item> getItems(
      NOTEHANDLE hNote,
      const Lmbcs &name
      ) noexcept(false)
  {
    return Item::getInfo(name, hNote)
        .map([&name, &hNote](Item::Info itemInfo) {
      WORD flags = getItemFlags(hNote, itemInfo);
      BlockHandle valueHandle(itemInfo.valueId());
      auto pValue = valueHandle.lock<char>();
      QByteArray value(pValue, static_cast<int>(itemInfo.valueSize()));
      return Item(name, value, flags);
    });
  }

  static observable<CompositeItem> getCompositeItems(
      NOTEHANDLE hNote,
      const Lmbcs &name
      ) noexcept(false)
  {
    return Item::getInfo(name, hNote)
        .map([&name, &hNote](Item::Info info) {
        WORD flags = getItemFlags(hNote, info);
        CompositeItem item;
        CompositeItem::getData(info)
        .reduce(cd::ItemList(), [](cd::ItemList list, cd::ItemPtr cdPtr) {
          list.append(cdPtr);
          return list;
        })
        .subscribe([&name, &flags, &item](cd::ItemList list) {
          item = CompositeItem(name, flags, std::move(list));
        });
        return item;
    });
  }

  static Item getItem(NOTEHANDLE hNote, const Lmbcs &name) noexcept(false)
  {
    Item item;
    getItems(hNote, name)
    .take(1)
    .subscribe(
    [&item](Item i) { item = i; },
    [](std::exception_ptr ep) {
      std::rethrow_exception(ep);
    });
    return item;
  }

  static WORD getItemFlags(
      NOTEHANDLE hNote,
      const Item::Info &itemInfo
      ) noexcept
  {
    WORD flags = 0;
    NSFItemQuery(
          hNote,
          itemInfo.itemId(),
          nullptr,
          0,
          nullptr,
          &flags,
          nullptr,
          nullptr,
          nullptr
          );
    return flags;
  }

  template <typename T, WORD Type>
  static T getInfo(NOTEHANDLE hNote) noexcept
  {
    QByteArray buffer(sizeof(T), '\0');
    NSFNoteGetInfo(hNote, Type, buffer.data());
    return *reinterpret_cast<const T*>(buffer.constData());
  }

  static DBHANDLE getDbHandle(NOTEHANDLE hNote) noexcept
  {
    return getInfo<DBHANDLE, _NOTE_DB>(hNote);
  }

  static NOTEID getId(NOTEHANDLE hNote) noexcept
  {
    return getInfo<NOTEID, _NOTE_ID>(hNote);
  }

  static OID getOriginatorId(NOTEHANDLE hNote) noexcept
  {
    return getInfo<OID, _NOTE_OID>(hNote);
  }

  static WORD getNoteClass(NOTEHANDLE hNote) noexcept
  {
    return getInfo<WORD, _NOTE_CLASS>(hNote);
  }

  static TIMEDATE getModified(NOTEHANDLE hNote) noexcept
  {
    return getInfo<TIMEDATE, _NOTE_MODIFIED>(hNote);
  }

  static WORD getFlags(NOTEHANDLE hNote) noexcept
  {
    return getInfo<WORD, _NOTE_FLAGS>(hNote);
  }

  static TIMEDATE getAccessed(NOTEHANDLE hNote) noexcept
  {
    return getInfo<TIMEDATE, _NOTE_ACCESSED>(hNote);
  }

  static NOTEID getParentNoteId(NOTEHANDLE hNote) noexcept
  {
    return getInfo<NOTEID, _NOTE_PARENT_NOTEID>(hNote);
  }

  static DWORD getResponseCount(NOTEHANDLE hNote) noexcept
  {
    return getInfo<DWORD, _NOTE_RESPONSE_COUNT>(hNote);
  }

  static HANDLE getResponsedIdTable(NOTEHANDLE hNote) noexcept
  {
    return getInfo<HANDLE, _NOTE_RESPONSES>(hNote);
  }

  template <typename T, WORD Type>
  static void setInfo(NOTEHANDLE hNote, T *pData) noexcept
  {
    NSFNoteSetInfo(hNote, Type, pData);
  }

  static void setDbHandle(NOTEHANDLE hNote, DBHANDLE data) noexcept
  {
    setInfo<DBHANDLE, _NOTE_DB>(hNote, &data);
  }

  static void setId(NOTEHANDLE hNote, NOTEID data) noexcept
  {
    setInfo<NOTEID, _NOTE_ID>(hNote, &data);
  }

  static void setOriginatorId(NOTEHANDLE hNote, OID data) noexcept
  {
    setInfo<OID, _NOTE_OID>(hNote, &data);
  }

  static void setNoteClass(NOTEHANDLE hNote, WORD data) noexcept
  {
    setInfo<WORD, _NOTE_CLASS>(hNote, &data);
  }

  static void setModified(NOTEHANDLE hNote, TIMEDATE data) noexcept
  {
    setInfo<TIMEDATE, _NOTE_MODIFIED>(hNote, &data);
  }

  static void setFlags(NOTEHANDLE hNote, WORD data) noexcept
  {
    setInfo<WORD, _NOTE_FLAGS>(hNote, &data);
  }

  static void setAccessed(NOTEHANDLE hNote, TIMEDATE data) noexcept
  {
    setInfo<TIMEDATE, _NOTE_ACCESSED>(hNote, &data);
  }

  static void setParentNoteId(NOTEHANDLE hNote, NOTEID data) noexcept
  {
    setInfo<NOTEID, _NOTE_PARENT_NOTEID>(hNote, &data);
  }

  static void setResponseCount(NOTEHANDLE hNote, DWORD data) noexcept
  {
    setInfo<DWORD, _NOTE_RESPONSE_COUNT>(hNote, &data);
  }

  static void setResponsedIdTable(NOTEHANDLE hNote, HANDLE data) noexcept
  {
    setInfo<HANDLE, _NOTE_RESPONSES>(hNote, &data);
  }

  static UNID getUnid(NOTEHANDLE hNote) noexcept
  {
    OID oid = getOriginatorId(hNote);
    UNID unid;
    unid.File = oid.File;
    unid.Note = oid.Note;
    return unid;
  }

  static Lmbcs getUnidText(NOTEHANDLE hNote) noexcept
  {
    UNID unid = getUnid(hNote);
    return unidToText(unid);
  }

  static void downloadAttachment(
      NOTEHANDLE hNote,
      const Item::Info &itemInfo,
      const Lmbcs &filePath,
      ENCRYPTION_KEY *pDecryptionKey = nullptr
      ) noexcept(false)
  {
    Status status = NSFNoteExtractFile(
          hNote,
          itemInfo.itemId(),
          const_cast<char*>(filePath.constData()),
          pDecryptionKey
          );
    if (status.hasError()) throw status;
  }

  static QByteArray extractAttachment(
        NOTEHANDLE hNote,
        const Item::Info &itemInfo
        ) noexcept(false)
  {
    QByteArray bytes;
    Status status = NSFNoteExtractWithCallback(
          hNote,
          itemInfo.itemId(),
          nullptr,
          0,
          extractAttachmentCallback,
          &bytes
          );
    if (status.hasError()) throw status;
    return bytes;
  }

  static STATUS LNPUBLIC extractAttachmentCallback(
      const BYTE *bytes,
      DWORD length,
      void *ptr
      )
  {
    if (length > 0) {
      QByteArray *pBytes = static_cast<QByteArray*>(ptr);
      QByteArray file(
            reinterpret_cast<char*>(const_cast<BYTE*>(bytes)),
            static_cast<int>(length)
            );
      *pBytes += file;
    }
    return NOERROR;
  }

  static Lmbcs getString(NOTEHANDLE hNote, const Lmbcs &field)
  {
    return getItem(hNote, field).value().toString();
  }

  static void setString(
      NOTEHANDLE hNote,
      const Lmbcs &field,
      const Lmbcs &value,
      bool isSummary = true
      ) noexcept(false)
  {
    Lmbcs v(value);
    v.replace('\n', '\0');
    Status result = NSFItemSetTextSummary(
          hNote,
          field.constData(),
          v.constData(),
          v.byteSize(),
          isSummary
          );
    if (result.hasError()) throw result;
  }

  static LmbcsList getStringList(NOTEHANDLE hNote, const Lmbcs &field)
  {
    return getItem(hNote, field).value().toStringList();
  }

  static Number getNumber(NOTEHANDLE hNote, const Lmbcs &field)
  {
    return getItem(hNote, field).value().firstNumber();
  }

  static NumberRange getNumberRange(NOTEHANDLE hNote, const Lmbcs &field)
  {
    return getItem(hNote, field).value().toNumberRange();
  }

  static TimeDateRange getTimeDateRange(NOTEHANDLE hNote, const Lmbcs &field)
  {
    return getItem(hNote, field).value().toTimeDateRange();
  }

  static void update(NOTEHANDLE hNote, WORD updateFlags = 0) noexcept(false)
  {
    Status result = NSFNoteUpdate(hNote, updateFlags);
    if (result.hasError()) throw result;
  }

  static STATUS LNPUBLIC itemScanCallback(
      WORD,
      WORD ItemFlags,
      char *Name,
      WORD NameLength,
      void *Value,
      DWORD ValueLength,
      void *RoutineParameter
      )
  {
    subscriber<Item> *s
        = static_cast<subscriber<Item>*>(RoutineParameter);
    Lmbcs name(Name, NameLength);
    QByteArray value(reinterpret_cast<char*>(Value), static_cast<int>(ValueLength));
    s->on_next(Item(name, value, ItemFlags));
    return NOERROR;
  }

  static observable<Item> itemScan(NOTEHANDLE handle) noexcept(false)
  {
    return observable<>::create<Item>(
          [handle](subscriber<Item> s) {
      Status status = NSFItemScan(handle, itemScanCallback, &s);
      if (status.hasError()) throw status;
      s.on_completed();
    });
  }

  static bool hasComposite(NOTEHANDLE handle) noexcept
  {
    return (NSFNoteHasComposite(handle)) ? true : false;
  }

  static BOOL hasMIME(NOTEHANDLE handle) noexcept
  {
    return NSFNoteHasMIME(handle);
  }

  static bool hasMIMEPart(NOTEHANDLE handle) noexcept
  {
    return (NSFNoteHasMIMEPart(handle)) ? true : false;
  }

  static bool hasRFC822Text(NOTEHANDLE handle) noexcept
  {
    return (NSFNoteHasRFC822Text(handle)) ? true : false;
  }
};

/**
 * @brief ORIGINATORIDからUNIDを作成する。
 * @param oid　ORIGINATORID
 * @return UNID
 */
inline UNID oidToUnid(const ORIGINATORID &oid) noexcept
{
  UNID unid;
  unid.File = oid.File;
  unid.Note = oid.Note;
  return unid;
}

} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_NOTE_HPP
