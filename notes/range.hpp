﻿#ifndef GOUDA_NOTES_RANGE_HPP
#define GOUDA_NOTES_RANGE_HPP

#include <gouda/notes/lmbcslist.hpp>
#include <QVector>

namespace gouda {
namespace notes {

template <typename PAIR>
class Pair
{
  PAIR value_;

public:
  Pair() : value_() {}

  Pair(const PAIR &value) : value_(value) {}

  PAIR value() const { return value_; }
};

template <typename SINGLE, typename PAIR>
class Range
{
  QVector<SINGLE> singles_;
  QVector<PAIR> pairs_;

public:
  Range() : singles_(), pairs_() {}

  Range(const QByteArray &bytes)
    : singles_()
    , pairs_()
  {
    RANGE *pRange = reinterpret_cast<RANGE*>(
          const_cast<char*>(bytes.constData())
          );
    SINGLE *pListEntry
        = reinterpret_cast<SINGLE*>(pRange + 1);
    PAIR *pPairEntry
        = reinterpret_cast<PAIR*>(pListEntry + pRange->ListEntries);
    for (USHORT i = 0; i < pRange->ListEntries; ++i) {
      singles_.append(pListEntry[i]);
    }
    for (USHORT i = 0; i < pRange->RangeEntries; ++i) {
      pairs_.append(pPairEntry[i]);
    }
  }

  QVector<SINGLE> &singles() { return singles_; }
  QVector<PAIR> &pairs() { return pairs_; }

  virtual Lmbcs convertSingleToText(SINGLE value) const noexcept(false) = 0;
  virtual Lmbcs convertPairToText(PAIR pair) const noexcept(false) = 0;

  LmbcsList toStringList() const noexcept
  {
    LmbcsList sList;
    foreach (SINGLE entry, singles_) {
      sList << convertSingleToText(entry);
    }
    return sList;
  }

  LmbcsList pairsToStringList() const noexcept
  {
    LmbcsList pList;
    foreach (PAIR entry, pairs_) {
      pList << convertPairToText(entry);
    }
    return pList;
  }

  Lmbcs toString(char delimiter = ',') const noexcept
  {
    LmbcsList sList = std::move(toStringList());
    LmbcsList pList = std::move(pairsToStringList());
    LmbcsList list;
    if (!sList.isEmpty()) list << sList.join(delimiter);
    if (!pList.isEmpty()) list << pList.join(delimiter);
    return list.join(';');
  }
};

} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_RANGE_HPP
