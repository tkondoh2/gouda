#ifndef GOUDA_NOTES_CD_ITEM_HPP
#define GOUDA_NOTES_CD_ITEM_HPP

#include <gouda/notes/lmbcs.hpp>

#include <QSharedPointer>
#include <QList>

namespace gouda {
namespace notes {
namespace cd {

class Item;
using ItemPtr = QSharedPointer<Item>;

class Item
{
public:
  virtual QByteArray value() const { return QByteArray(); }
  virtual WORD getSig() const = 0;
  virtual WORD odsLength() const = 0;
  virtual void odsWriteMemory(char **ppBuf) const = 0;
  virtual Lmbcs getText() const { return Lmbcs(); }
  virtual Lmbcs getHtml() const { return Lmbcs(); }
  virtual ~Item(){}
  virtual Item* clone() const = 0;
};

using ItemList = QList<ItemPtr>;

} // namespace cd
} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_CD_ITEM_HPP
