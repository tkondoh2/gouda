#ifndef GOUDA_NOTES_CD_HOTSPOTBEGIN_HPP
#define GOUDA_NOTES_CD_HOTSPOTBEGIN_HPP

#include <gouda/notes/cd/baseitem.hpp>

#include <gouda/notes/lmbcslist.hpp>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <editods.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace gouda {
namespace notes {
namespace cd {

using _HotspotBegin = BaseItem<CDHOTSPOTBEGIN, _CDHOTSPOTBEGIN, SIG_CD_HOTSPOTBEGIN>;

class HotspotBegin
    : public _HotspotBegin
{
  QByteArray data_;

public:
  HotspotBegin()
    : _HotspotBegin()
    , data_()
  {}

  HotspotBegin(char **ppRec)
    : _HotspotBegin(ppRec)
    , data_(*ppRec, rec_.DataLength)
  {}

  HotspotBegin(const QByteArray &bytes)
    : _HotspotBegin()
    , data_()
  {
    const char *p = bytes.constData();
    rec_.Type = *reinterpret_cast<const WORD*>(p); p += sizeof(WORD);
    rec_.Flags = *reinterpret_cast<const DWORD*>(p); p += sizeof(DWORD);
    rec_.DataLength = *reinterpret_cast<const WORD*>(p); p += sizeof(WORD);
    data_ = QByteArray(p, rec_.DataLength);
  }

  virtual Lmbcs getText() const
  {
    QString s = QString("<hotspotbegin"
                        " type=\"%1\","
                        " flags=\"%2\","
                        " dataLength=\"%3\">")
        .arg(rec_.Type)
        .arg(rec_.Flags, 8, 16, QChar('0'))
        .arg(rec_.DataLength);

    switch (rec_.Type) {
    case HOTSPOTREC_TYPE_FILE:
    {
      LmbcsList list = getValuesForTypeFile();
      foreach (Lmbcs item, list) {
        s += QString("<file>%1</file>").arg(lq(item));
      }
    }
      break;
    }

    s += "</hotspotbegin>";
    return ql(s);
  }

  virtual Lmbcs getHtml() const
  {
    return "<div class=\"hotspotbegin\"></div>";
  }

  LmbcsList getValuesForTypeFile() const
  {
    if (rec_.Type == HOTSPOTREC_TYPE_FILE) {
      Lmbcs text(data_, rec_.DataLength);
      LmbcsList list = split(text, '\0');
      return list;
    }
    return LmbcsList();
  }
};

} // namespace cd
} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_CD_HOTSPOTBEGIN_HPP
