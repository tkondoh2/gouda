#ifndef GOUDA_NOTES_CD_TABLEBEGIN_HPP
#define GOUDA_NOTES_CD_TABLEBEGIN_HPP

#include <gouda/notes/cd/baseitem.hpp>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <editods.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace gouda {
namespace notes {
namespace cd {

using _TableBegin = BaseItem<CDTABLEBEGIN, _CDTABLEBEGIN, SIG_CD_TABLEBEGIN>;

class TableBegin
    : public _TableBegin
{
public:
  TableBegin(char **ppRec)
    : _TableBegin(ppRec)
  {}

  virtual Lmbcs getText() const
  {
    return "";
  }

  virtual Lmbcs getHtml() const
  {
    return "<table>";
  }
};

} // namespace cd
} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_CD_TABLEBEGIN_HPP
