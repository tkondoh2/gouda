#ifndef GOUDA_NOTES_CD_NESTEDTABLECELL_HPP
#define GOUDA_NOTES_CD_NESTEDTABLECELL_HPP

#include <gouda/notes/cd/baseitem.hpp>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <editods.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace gouda {
namespace notes {
namespace cd {

using _NestedTableCell = BaseItem<CDTABLECELL, _CDTABLECELL, SIG_CD_NESTEDTABLECELL>;

class NestedTableCell
    : public _NestedTableCell
{
public:
  NestedTableCell(char **ppRec)
    : _NestedTableCell(ppRec)
  {}

  virtual Lmbcs getText() const
  {
    return "";
  }

  virtual Lmbcs getHtml() const
  {
    return "<tr class=\"nested\"><td class=\"nested\">";
  }
};

} // namespace cd
} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_CD_NESTEDTABLECELL_HPP
