#ifndef GOUDA_NOTES_CD_TEXT_HPP
#define GOUDA_NOTES_CD_TEXT_HPP

#include <gouda/notes/cd/baseitem.hpp>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <editods.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace gouda {
namespace notes {
namespace cd {

using _Text = BaseItem<CDTEXT, _CDTEXT, SIG_CD_TEXT>;

class Text
    : public _Text
{
  Lmbcs text_;

public:
  Text()
    : _Text()
    , text_()
  {}

  Text(char **ppRec)
    : _Text(ppRec)
    , text_(*ppRec, rec_.Header.Length - ODSLength(_CDTEXT))
  {
    text_.replace('\0', '\n');
  }

  Text(const QByteArray &bytes)
    : _Text()
    , text_()
  {
    const char* p = bytes.constData();
    rec_.FontID = *reinterpret_cast<const FONTID*>(p);
    text_ = Lmbcs(p + sizeof(FONTID), bytes.size() - sizeof(FONTID));
    text_.replace('\0', '\n');
  }

//  CdText(const Lmbcs &text, FONTID fontId = DEFAULT_FONT_ID);
  virtual Lmbcs getText() const
  {
    return text_;
  }

  virtual Lmbcs getHtml() const
  {
    QString qstr = lq(text_);
    qstr.replace("&", "&amp;")
        .replace("\"", "&quot;")
        .replace(">", "&gt;")
        .replace("<", "&lt;")
        .replace("\n", "<br />");
    return ql(qstr);
  }
};

} // namespace cd
} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_CD_TEXT_HPP
