#ifndef GOUDA_NOTES_CD_BASEITEM_HPP
#define GOUDA_NOTES_CD_BASEITEM_HPP

#include <gouda/notes/cd/item.hpp>

namespace gouda {
namespace notes {
namespace cd {

template <typename CD, WORD _TYPE, WORD SIG_CD_TYPE>
class BaseItem
    : public Item
{
protected:
  mutable CD rec_;

public:
  BaseItem()
    : rec_()
  {
    rec_.Header.Length = ODSLength(_TYPE);
    rec_.Header.Signature = SIG_CD_TYPE;
  }

  BaseItem(char **ppRec)
    : rec_()
  {
    ODSReadMemory(ppRec, _TYPE, &rec_, 1);
  }

  virtual WORD getSig() const { return SIG_CD_TYPE; }

  virtual WORD odsLength() const
  {
    return rec_.Header.Length + rec_.Header.Length % 2;
  }

  virtual void odsWriteMemory(char **ppBuf) const
  {
    ODSWriteMemory(ppBuf, _TYPE, const_cast<CD*>(&rec_), 1);
  }

  virtual Item* clone() const
  {
    return new BaseItem<CD, _TYPE, SIG_CD_TYPE>(*this);
  }

  const CD &constRec() const { return rec_; }

  CD &rec() { return rec_; }
};

} // namespace cd
} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_CD_BASEITEM_HPP
