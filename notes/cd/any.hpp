#ifndef GOUDA_NOTES_CD_ANY_HPP
#define GOUDA_NOTES_CD_ANY_HPP

#include <gouda/notes/cd/item.hpp>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <ods.h>

#ifdef NT
#pragma pack(pop)
#endif

#include <QtDebug>

namespace gouda {
namespace notes {
namespace cd {

enum SizeType { Byte, Word, Long };

inline SizeType getSizeType(char **ppRec) noexcept
{
  WORD sig = *reinterpret_cast<WORD*>(*ppRec);
  if ((sig & WORDRECORDLENGTH) == LONGRECORDLENGTH) return Long;
  else if ((sig & WORDRECORDLENGTH) == WORDRECORDLENGTH) return Word;
  else return Byte;
}

template <typename SIG>
class Any
    : public Item
{
  mutable SIG sig_;
  QByteArray value_;

public:
  Any(char **ppRec)
    : sig_(*reinterpret_cast<SIG*>(*ppRec))
    , value_(*ppRec + sizeof(SIG), static_cast<int>(sig_.Length) - sizeof(SIG))
  {}

  Any(const Any<SIG> &other)
    : sig_(other.sig_)
    , value_(other.value_)
  {}

  virtual QByteArray value() const override { return value_; }

  virtual WORD getSig() const override
  {
    return static_cast<WORD>(sig_.Signature);
  }

  virtual WORD odsLength() const override
  {
    return static_cast<WORD>(sig_.Length + sig_.Length % 2);
  }

  virtual void odsWriteMemory(char **ppBuf) const override
  {
    sig_.Length = static_cast<decltype(sig_.Length)>(value_.length());
    memcpy(*ppBuf, &sig_, sizeof(SIG));
    memcpy(*ppBuf + sizeof(SIG), value_.constData(), static_cast<size_t>(value_.length()));
    *ppBuf += sig_.Length + sig_.Length % 2;
  }

  virtual Item *clone() const override
  {
    return new Any<SIG>(*this);
  }

  virtual Lmbcs getText() const override
  {
//    return Lmbcs();
    WORD sig = getSig();
    QString sSig = QString("/*%1(%2)*/")
        .arg(lq(name(sig)))
        .arg(static_cast<ushort>(sig))
        ;
    return ql(sSig);
  }

  virtual Lmbcs getHtml() const override
  {
    return Lmbcs("<!-- ") + name(getSig()) + Lmbcs(" -->");
  }

  static Lmbcs name(WORD sig)
  {
    switch (sig)
    {
    case SIG_INVALID: return Lmbcs("invalid");
    case SIG_CD_PDEF_MAIN: return Lmbcs("pdef_main");
    case SIG_CD_PDEF_TYPE: return Lmbcs("pdef_type");
    case SIG_CD_PDEF_PROPERTY: return Lmbcs("pdef_property");
    case SIG_CD_PDEF_ACTION: return Lmbcs("pdef_action");
    case SIG_CD_TABLECELL_DATAFLAGS: return Lmbcs("tablecell_dataflags");
    case SIG_CD_EMBEDDEDCONTACTLIST: return Lmbcs("embeddedcontactlist");
    case SIG_CD_IGNORE: return Lmbcs("ignore");
    case SIG_CD_TABLECELL_HREF2: return Lmbcs("tablecell_href2");
    case SIG_CD_HREFBORDER: return Lmbcs("hrefborder");
    case SIG_CD_TABLEDATAEXTENSION: return Lmbcs("tabledataextension");
    case SIG_CD_EMBEDDEDCALCTL: return Lmbcs("embeddedcalctl");
    case SIG_CD_ACTIONEXT: return Lmbcs("actionext");
    case SIG_CD_EVENT_LANGUAGE_ENTRY: return Lmbcs("event_language_entry");
    case SIG_CD_FILESEGMENT: return Lmbcs("filesegment");
    case SIG_CD_FILEHEADER: return Lmbcs("fileheader");
    case SIG_CD_DATAFLAGS: return Lmbcs("dataflags");
    case SIG_CD_BACKGROUNDPROPERTIES: return Lmbcs("backgroundproperties");
    case SIG_CD_EMBEDEXTRA_INFO: return Lmbcs("embedextra_info");
    case SIG_CD_CLIENT_BLOBPART: return Lmbcs("client_blobpart");
    case SIG_CD_CLIENT_EVENT: return Lmbcs("client_event");
    case SIG_CD_BORDERINFO_HS: return Lmbcs("borderinfo_hs");
    case SIG_CD_LARGE_PARAGRAPH: return Lmbcs("large_paragraph");
    case SIG_CD_EXT_EMBEDDEDSCHED: return Lmbcs("ext_embeddedsched");
    case SIG_CD_BOXSIZE: return Lmbcs("boxsize");
    case SIG_CD_POSITIONING: return Lmbcs("positioning");
    case SIG_CD_LAYER: return Lmbcs("layer");
    case SIG_CD_DECSFIELD: return Lmbcs("decsfield");
    case SIG_CD_SPAN_END: return Lmbcs("span_end");
    case SIG_CD_SPAN_BEGIN: return Lmbcs("span_begin");
    case SIG_CD_TEXTPROPERTIESTABLE: return Lmbcs("textpropertiestable");
    case SIG_CD_HREF2: return Lmbcs("href2");
    case SIG_CD_BACKGROUNDCOLOR: return Lmbcs("backgroundcolor");
    case SIG_CD_INLINE: return Lmbcs("inline");
    case SIG_CD_V6HOTSPOTBEGIN_CONTINUATION: return Lmbcs("v6hotspotbegin_continuation");
    case SIG_CD_TARGET_DBLCLK: return Lmbcs("target_dblclk");
    case SIG_CD_CAPTION: return Lmbcs("caption");
    case SIG_CD_LINKCOLORS: return Lmbcs("linkcolors");
    case SIG_CD_TABLECELL_HREF: return Lmbcs("tablecell_href");
    case SIG_CD_ACTIONBAREXT: return Lmbcs("actionbarext");
    case SIG_CD_IDNAME: return Lmbcs("idname");
    case SIG_CD_TABLECELL_IDNAME: return Lmbcs("tablecell_idname");
    case SIG_CD_IMAGESEGMENT: return Lmbcs("imagesegment");
    case SIG_CD_IMAGEHEADER: return Lmbcs("imageheader");
    case SIG_CD_V5HOTSPOTBEGIN: return Lmbcs("v5hotspotbegin");
    case SIG_CD_V5HOTSPOTEND: return Lmbcs("v5hotspotend");
    case SIG_CD_TEXTPROPERTY: return Lmbcs("textproperty");
    case SIG_CD_PARAGRAPH: return Lmbcs("paragraph");
    case SIG_CD_PABDEFINITION: return Lmbcs("pabdefinition");
    case SIG_CD_PABREFERENCE: return Lmbcs("pabreference");
    case SIG_CD_TEXT: return Lmbcs("text");
    case SIG_CD_HEADER: return Lmbcs("header");
    case SIG_CD_LINKEXPORT2: return Lmbcs("linkexport2");
    case SIG_CD_BITMAPHEADER: return Lmbcs("bitmapheader");
    case SIG_CD_BITMAPSEGMENT: return Lmbcs("bitmapsegment");
    case SIG_CD_COLORTABLE: return Lmbcs("colortable");
    case SIG_CD_GRAPHIC: return Lmbcs("graphic");
    case SIG_CD_PMMETASEG: return Lmbcs("pmmetaseg");
    case SIG_CD_WINMETASEG: return Lmbcs("winmetaseg");
    case SIG_CD_MACMETASEG: return Lmbcs("macmetaseg");
    case SIG_CD_CGMMETA: return Lmbcs("cgmmeta");
    case SIG_CD_PMMETAHEADER: return Lmbcs("pmmetaheader");
    case SIG_CD_WINMETAHEADER: return Lmbcs("winmetaheader");
    case SIG_CD_MACMETAHEADER: return Lmbcs("macmetaheader");
    case SIG_CD_TABLEBEGIN: return Lmbcs("tablebegin");
    case SIG_CD_TABLECELL: return Lmbcs("tablecell");
    case SIG_CD_TABLEEND: return Lmbcs("tableend");
    case SIG_CD_STYLENAME: return Lmbcs("stylename");
    case SIG_CD_STORAGELINK: return Lmbcs("storagelink");
    case SIG_CD_TRANSPARENTTABLE: return Lmbcs("transparenttable");
    case SIG_CD_HORIZONTALRULE: return Lmbcs("horizontalrule");
    case SIG_CD_ALTTEXT: return Lmbcs("alttext");
    case SIG_CD_ANCHOR: return Lmbcs("anchor");
    case SIG_CD_HTMLBEGIN: return Lmbcs("htmlbegin");
    case SIG_CD_HTMLEND: return Lmbcs("htmlend");
    case SIG_CD_HTMLFORMULA: return Lmbcs("htmlformula");
    case SIG_CD_NESTEDTABLEBEGIN: return Lmbcs("nestedtablebegin");
    case SIG_CD_NESTEDTABLECELL: return Lmbcs("nestedtablecell");
    case SIG_CD_NESTEDTABLEEND: return Lmbcs("nestedtableend");
    case SIG_CD_COLOR: return Lmbcs("color");
    case SIG_CD_TABLECELL_COLOR: return Lmbcs("tablecell_color");
    case SIG_CD_BLOBPART: return Lmbcs("blobpart");
    case SIG_CD_BEGIN: return Lmbcs("begin");
    case SIG_CD_END: return Lmbcs("end");
    case SIG_CD_VERTICALALIGN: return Lmbcs("verticalalign");
    case SIG_CD_FLOATPOSITION: return Lmbcs("floatposition");
    case SIG_CD_TIMERINFO: return Lmbcs("timerinfo");
    case SIG_CD_TABLEROWHEIGHT: return Lmbcs("tablerowheight");
    case SIG_CD_TABLELABEL: return Lmbcs("tablelabel");
    case SIG_CD_BIDI_TEXT: return Lmbcs("bidi_text");
    case SIG_CD_BIDI_TEXTEFFECT: return Lmbcs("bidi_texteffect");
    case SIG_CD_REGIONBEGIN: return Lmbcs("regionbegin");
    case SIG_CD_REGIONEND: return Lmbcs("regionend");
    case SIG_CD_TRANSITION: return Lmbcs("transition");
    case SIG_CD_FIELDHINT: return Lmbcs("fieldhint");
    case SIG_CD_PLACEHOLDER: return Lmbcs("placeholder");
    case SIG_CD_EMBEDDEDOUTLINE: return Lmbcs("embeddedoutline");
    case SIG_CD_EMBEDDEDVIEW: return Lmbcs("embeddedview");
    case SIG_CD_CELLBACKGROUNDDATA: return Lmbcs("cellbackgrounddata");
    case SIG_CD_FRAMESETHEADER: return Lmbcs("framesetheader");
    case SIG_CD_FRAMESET: return Lmbcs("frameset");
    case SIG_CD_FRAME: return Lmbcs("frame");
    case SIG_CD_TARGET: return Lmbcs("target");
    case SIG_CD_MAPELEMENT: return Lmbcs("mapelement");
    case SIG_CD_AREAELEMENT: return Lmbcs("areaelement");
    case SIG_CD_HREF: return Lmbcs("href");
    case SIG_CD_EMBEDDEDCTL: return Lmbcs("embeddedctl");
    case SIG_CD_HTML_ALTTEXT: return Lmbcs("html_alttext");
    case SIG_CD_EVENT: return Lmbcs("event");
    case SIG_CD_PRETABLEBEGIN: return Lmbcs("pretablebegin");
    case SIG_CD_BORDERINFO: return Lmbcs("borderinfo");
    case SIG_CD_EMBEDDEDSCHEDCTL: return Lmbcs("embeddedschedctl");
    case SIG_CD_EXT2_FIELD: return Lmbcs("ext2_field");
    case SIG_CD_EMBEDDEDEDITCTL: return Lmbcs("embeddededitctl");
    case SIG_CD_DOCUMENT_PRE_26: return Lmbcs("document_pre_26");
    case SIG_CD_FIELD_PRE_36: return Lmbcs("field_pre_36");
    case SIG_CD_FIELD: return Lmbcs("field");
    case SIG_CD_DOCUMENT: return Lmbcs("document");
    case SIG_CD_METAFILE: return Lmbcs("metafile");
    case SIG_CD_BITMAP: return Lmbcs("bitmap");
    case SIG_CD_FONTTABLE: return Lmbcs("fonttable");
    case SIG_CD_LINK: return Lmbcs("link");
    case SIG_CD_LINKEXPORT: return Lmbcs("linkexport");
    case SIG_CD_KEYWORD: return Lmbcs("keyword");
    case SIG_CD_LINK2: return Lmbcs("link2");
    case SIG_CD_CGM: return Lmbcs("cgm");
    case SIG_CD_TIFF: return Lmbcs("tiff");
    case SIG_CD_PATTERNTABLE: return Lmbcs("patterntable");
    case SIG_CD_DDEBEGIN: return Lmbcs("ddebegin");
    case SIG_CD_DDEEND: return Lmbcs("ddeend");
    case SIG_CD_OLEBEGIN: return Lmbcs("olebegin");
    case SIG_CD_OLEEND: return Lmbcs("oleend");
    case SIG_CD_HOTSPOTBEGIN: return Lmbcs("hotspotbegin");
    case SIG_CD_HOTSPOTEND: return Lmbcs("hotspotend");
    case SIG_CD_BUTTON: return Lmbcs("button");
    case SIG_CD_BAR: return Lmbcs("bar");
    case SIG_CD_V4HOTSPOTBEGIN: return Lmbcs("v4hotspotbegin");
    case SIG_CD_V4HOTSPOTEND: return Lmbcs("v4hotspotend");
    case SIG_CD_EXT_FIELD: return Lmbcs("ext_field");
    case SIG_CD_LSOBJECT: return Lmbcs("lsobject");
    case SIG_CD_HTMLHEADER: return Lmbcs("htmlheader");
    case SIG_CD_HTMLSEGMENT: return Lmbcs("htmlsegment");
    case SIG_CD_LAYOUT: return Lmbcs("layout");
    case SIG_CD_LAYOUTTEXT: return Lmbcs("layouttext");
    case SIG_CD_LAYOUTEND: return Lmbcs("layoutend");
    case SIG_CD_LAYOUTFIELD: return Lmbcs("layoutfield");
    case SIG_CD_PABHIDE: return Lmbcs("pabhide");
    case SIG_CD_PABFORMREF: return Lmbcs("pabformref");
    case SIG_CD_ACTIONBAR: return Lmbcs("actionbar");
    case SIG_CD_ACTION: return Lmbcs("action");
    case SIG_CD_DOCAUTOLAUNCH: return Lmbcs("docautolaunch");
    case SIG_CD_LAYOUTGRAPHIC: return Lmbcs("layoutgraphic");
    case SIG_CD_OLEOBJINFO: return Lmbcs("oleobjinfo");
    case SIG_CD_LAYOUTBUTTON: return Lmbcs("layoutbutton");
    case SIG_CD_TEXTEFFECT: return Lmbcs("texteffect");
  //  #define SIG_QUERY_HEADER		(129 | BYTERECORDLENGTH)
  //  #define SIG_QUERY_TEXTTERM		(130 | WORDRECORDLENGTH)
  //  #define SIG_QUERY_BYFIELD		(131 | WORDRECORDLENGTH)
  //  #define SIG_QUERY_BYDATE		(132 | WORDRECORDLENGTH)
  //  #define SIG_QUERY_BYAUTHOR		(133 | WORDRECORDLENGTH)
  //  #define SIG_QUERY_FORMULA		(134 | WORDRECORDLENGTH)
  //  #define SIG_QUERY_BYFORM		(135 | WORDRECORDLENGTH)
  //  #define SIG_QUERY_BYFOLDER		(136 | WORDRECORDLENGTH)
  //  #define SIG_QUERY_USESFORM		(137 | WORDRECORDLENGTH)
  //  #define SIG_QUERY_TOPIC			(138 | WORDRECORDLENGTH)
  //  #define SIG_ACTION_HEADER		(129 | BYTERECORDLENGTH)
  //  #define SIG_ACTION_MODIFYFIELD	(130 | WORDRECORDLENGTH)
  //  #define SIG_ACTION_REPLY		(131 | WORDRECORDLENGTH)
  //  #define SIG_ACTION_FORMULA		(132 | WORDRECORDLENGTH)
  //  #define SIG_ACTION_LOTUSSCRIPT	(133 | WORDRECORDLENGTH)
  //  #define SIG_ACTION_SENDMAIL		(134 | WORDRECORDLENGTH)
  //  #define SIG_ACTION_DBCOPY		(135 | WORDRECORDLENGTH)
  //  #define SIG_ACTION_DELETE		(136 | BYTERECORDLENGTH)
  //  #define SIG_ACTION_BYFORM		(137 | WORDRECORDLENGTH)
  //  #define SIG_ACTION_MARKREAD		(138 | BYTERECORDLENGTH)
  //  #define SIG_ACTION_MARKUNREAD	(139 | BYTERECORDLENGTH)
  //  #define SIG_ACTION_MOVETOFOLDER	(140 | WORDRECORDLENGTH)
  //  #define SIG_ACTION_COPYTOFOLDER	(141 | WORDRECORDLENGTH)
  //  #define SIG_ACTION_REMOVEFROMFOLDER	(142 | WORDRECORDLENGTH)
  //  #define SIG_ACTION_NEWSLETTER	(143 | WORDRECORDLENGTH)
  //  #define SIG_ACTION_RUNAGENT		(144 | WORDRECORDLENGTH)
  //  #define SIG_ACTION_SENDDOCUMENT	(145 | BYTERECORDLENGTH)
  //  #define SIG_ACTION_FORMULAONLY	(146 | WORDRECORDLENGTH)
  //  #define SIG_ACTION_JAVAAGENT	(147 | WORDRECORDLENGTH)
  //  #define SIG_ACTION_JAVA			(148 | WORDRECORDLENGTH)
  //  #define SIG_VIEWMAP_DATASET	(87 | WORDRECORDLENGTH)
    case SIG_CD_VMHEADER: return Lmbcs("vmheader");
    case SIG_CD_VMBITMAP: return Lmbcs("vmbitmap");
    case SIG_CD_VMRECT: return Lmbcs("vmrect");
    case SIG_CD_VMPOLYGON_BYTE: return Lmbcs("vmpolygon_byte");
    case SIG_CD_VMPOLYLINE_BYTE: return Lmbcs("vmpolyline_byte");
    case SIG_CD_VMREGION: return Lmbcs("vmregion");
    case SIG_CD_VMACTION: return Lmbcs("vmaction");
    case SIG_CD_VMELLIPSE: return Lmbcs("vmellipse");
  //  case SIG_CD_VMRNDRECT: return Lmbcs("vmrndrect");
  //  case SIG_CD_VMBUTTON: return Lmbcs("vmbutton");
    case SIG_CD_VMACTION_2: return Lmbcs("vmaction_2");
  //  case SIG_CD_VMTEXTBOX: return Lmbcs("vmtextbox");
    case SIG_CD_VMPOLYGON: return Lmbcs("vmpolygon");
    case SIG_CD_VMPOLYLINE: return Lmbcs("vmpolyline");
  //  case SIG_CD_VMPOLYRGN: return Lmbcs("vmpolyrgn");
    case SIG_CD_VMCIRCLE: return Lmbcs("vmcircle");
  //  case SIG_CD_VMPOLYRGN_BYTE: return Lmbcs("vmpolyrgn_byte");
    case SIG_CD_ALTERNATEBEGIN: return Lmbcs("alternatebegin");
    case SIG_CD_ALTERNATEEND: return Lmbcs("alternateend");
    case SIG_CD_OLERTMARKER: return Lmbcs("olertmarker");
    default: return Lmbcs("Unknown");
    }
  }
};

} // namespace cd
} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_CD_ANY_HPP
