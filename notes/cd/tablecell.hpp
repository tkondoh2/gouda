#ifndef GOUDA_NOTES_CD_TABLECELL_HPP
#define GOUDA_NOTES_CD_TABLECELL_HPP

#include <gouda/notes/cd/baseitem.hpp>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <editods.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace gouda {
namespace notes {
namespace cd {

using _TableCell = BaseItem<CDTABLECELL, _CDTABLECELL, SIG_CD_TABLECELL>;

class TableCell
    : public _TableCell
{
public:
  TableCell(char **ppRec)
    : _TableCell(ppRec)
  {}

  virtual Lmbcs getText() const
  {
    return "";
  }

  virtual Lmbcs getHtml() const
  {
    return "<tr><td>";
  }
};

} // namespace cd
} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_CD_TABLECELL_HPP
