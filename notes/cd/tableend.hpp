#ifndef GOUDA_NOTES_CD_TABLEEND_HPP
#define GOUDA_NOTES_CD_TABLEEND_HPP

#include <gouda/notes/cd/baseitem.hpp>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <editods.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace gouda {
namespace notes {
namespace cd {

using _TableEnd = BaseItem<CDTABLEEND, _CDTABLEEND, SIG_CD_TABLEEND>;

class TableEnd
    : public _TableEnd
{
public:
  TableEnd(char **ppRec)
    : _TableEnd(ppRec)
  {}

  virtual Lmbcs getText() const
  {
    return "";
  }

  virtual Lmbcs getHtml() const
  {
    return "</table>";
  }
};

} // namespace cd
} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_CD_TABLEEND_HPP
