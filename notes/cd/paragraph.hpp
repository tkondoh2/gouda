#ifndef GOUDA_NOTES_CD_PARAGRAPH_HPP
#define GOUDA_NOTES_CD_PARAGRAPH_HPP

#include <gouda/notes/cd/baseitem.hpp>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <editods.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace gouda {
namespace notes {
namespace cd {

using _Paragraph = BaseItem<CDPARAGRAPH, _CDPARAGRAPH, SIG_CD_PARAGRAPH>;

class Paragraph
    : public _Paragraph
{
public:
  Paragraph()
    : _Paragraph()
  {}

  Paragraph(char **ppRec)
    : _Paragraph(ppRec)
  {}

  virtual Lmbcs getText() const
  {
    return "\n";
  }

  virtual Lmbcs getHtml() const
  {
    return "<br />";
  }
};

} // namespace cd
} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_CD_PARAGRAPH_HPP
