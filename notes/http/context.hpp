﻿#ifndef GOUDA_NOTES_HTTP_CONTEXT_HPP
#define GOUDA_NOTES_HTTP_CONTEXT_HPP

#include <gouda/notes/http/request.hpp>
#include <gouda/notes/lmbcs.hpp>
#include <gouda/notes/distinguishedname.hpp>
#include <gouda/notes/lookup.hpp>
#include <QMap>
#include <rxcpp/rx.hpp>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <stdnames.h>
#include <misc.h>

#ifdef NT
#pragma pack(pop)
#endif

using namespace rxcpp;
using namespace rxcpp::operators;

namespace gouda {
namespace notes {
namespace http {

using HeaderMap = QMap<QString, QString>;

class Context
{
  FilterContext *ptr_;

public:
  static const uint BUFFER_SIZE = 4096;

  Context() : ptr_(nullptr) {}

  Context(FilterContext *ptr) : ptr_(ptr) {}

  QByteArray body() const
  {
    uint errId = 0;
    char* pBody = nullptr;
    int size = ptr_->GetRequestContents(ptr_, &pBody, &errId);
    if (errId != 0) throw errId;

    if (size > 0)
      return QByteArray(pBody, size);
    return QByteArray();
  }

  QString body_q() const { return QString::fromUtf8(body()); }

  template <class T>
  void allocAndCopyToPrivate(const T &data) noexcept(false)
  {
    setPrivate(allocMem(data));
  }

  template <class T>
  T *allocMem(const T &data) noexcept(false)
  {
    uint errId = 0;
    T *p = static_cast<T*>(ptr_->AllocMem(ptr_, sizeof(data), 0, &errId));
    if (errId != 0) throw errId;
    memcpy_s(p, sizeof(data), &data, sizeof(data));
    return p;
  }

  void setPrivate(void *pData) noexcept
  {
    ptr_->privateContext = pData;
  }

  template <class T>
  const T &getPrivate(const T &defaultvalue) noexcept
  {
    return (ptr_->privateContext != nullptr)
        ? *reinterpret_cast<T*>(ptr_->privateContext)
        : defaultvalue;
  }

  Request getRequest() const noexcept(false)
  {
    uint errId = 0;
    FilterRequest req;
    ptr_->GetRequest(ptr_, &req, &errId);
    if (errId != 0) throw errId;
    return Request(std::move(req));
  }

  template <class T>
  QStringList getAllHeaders(T *pData, int *pSize = nullptr) noexcept(false)
  {
    uint errId = 0;
    char *pHeaders = nullptr;
    int size = pData->GetAllHeaders(ptr_, &pHeaders, &errId);
    if (errId != 0) throw errId;
    if (pSize != nullptr) *pSize = size;
    return QString::fromLatin1(pHeaders).split("\r\n");
  }

  template <class T>
  QString getHeader(T *pData, const QString &name) const noexcept(false)
  {
    uint errId = 0;
    QByteArray buffer(BUFFER_SIZE, '\0');
    int size = pData->GetHeader(
          ptr_,
          const_cast<char*>(name.toLatin1().constData()),
          buffer.data(),
          BUFFER_SIZE,
          &errId
          );
    if (errId != 0) throw errId;
    return QString::fromLatin1(buffer.data(), size);
  }

  template <class T>
  bool setHeader(
      T *pData,
      const QString &name,
      const QString &value
      ) noexcept(false)
  {
    uint errId = 0;
    int result = pData->SetHeader(
          ptr_,
          const_cast<char*>(name.toLatin1().constData()),
          const_cast<char*>(value.toLatin1().constData()),
          &errId
          );
    if (errId != 0) throw errId;
    return result == TRUE;
  }

  FilterParsedRequestLine getParsedRequestLine() noexcept(false)
  {
    uint errId = 0;
    FilterParsedRequestLine data;
    ptr_->ServerSupport(ptr_, kGetParsedRequest, &data, nullptr, 0, &errId);
    if (errId != 0) throw errId;
    return data;
  }

  FilterAuthenticatedUser getAuthenticatedUser(
      FilterAuthenticatedUserFields fieldFlags
      ) const noexcept(false)
  {
    uint errId = 0;
    FilterAuthenticatedUser data;
    data.fieldFlags = fieldFlags;
    ptr_->ServerSupport(
          ptr_,
          kGetAuthenticatedUserInfo,
          &data,
          nullptr,
          0,
          &errId
          );
    if (errId != 0) throw errId;
    return data;
  }

  bool writeResponse(const QByteArray &content) const noexcept(false)
  {
    uint errId;
    int ret = ptr_->WriteClient(
          ptr_
          , const_cast<char*>(content.constData())
          , static_cast<uint>(content.size())
          , static_cast<uint>(0)
          , &errId
          );
    if (errId != 0) throw errId;
    return (ret == 0);
  }

  void setAuthName(FilterAuthenticate *ptr, const QString &fullName) const
  {
    auto lmbcs = ql(fullName);
    qstrncpy(
          reinterpret_cast<char*>(ptr->authName),
          lmbcs.constData(),
          ptr->authNameSize
          );
  }

  void setAuthType(FilterAuthenticate *ptr, uint type) const
  {
    ptr->authType = type;
  }

  static const char *methodText(uint method) noexcept
  {
    switch (method)
    {
    case kRequestNone: return "None";
    case kRequestHEAD: return "HEAD";
    case kRequestGET: return "GET";
    case kRequestPOST: return "POST";
    case kRequestPUT: return "PUT";
    case kRequestDELETE: return "DELETE";
    case kRequestTRACE: return "TRACE";
    case kRequestCONNECT: return "CONNECT";
    case kRequestOPTIONS: return "OPTIONS";
    case kRequestBAD: return "BAD";
    default: return "UNKNOWN";
    }
  }

  static HeaderMap parseAllHeaders(const QStringList &allHeaders) noexcept
  {
    HeaderMap headers;
    foreach (QString header, allHeaders) {
      if (!header.isEmpty()) {
        int nameLen = header.indexOf(": ");
        QString name = header.left(nameLen);
        QString value = header.right(header.length() - nameLen - 2);
        headers.insert(name, value);
      }
    }
    return headers;
  }
};

} // namespace http
} // namespace notes
} // amespace gouda

#endif // GOUDA_NOTES_HTTP_CONTEXT_HPP
