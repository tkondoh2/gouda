﻿#ifndef GOUDA_NOTES_HTTP_PARSEDREQUEST_HPP
#define GOUDA_NOTES_HTTP_PARSEDREQUEST_HPP

#include <gouda/notes/notes_global.h>
#include <dsapi.h>

namespace gouda {
namespace notes {
namespace http {

class ParsedRequest
{
  FilterParsedRequest *ptr_;

public:
  ParsedRequest(FilterParsedRequest *ptr)
    : ptr_(ptr)
  {}

};

} // namespace http
} // namespace notes
} // amespace gouda

#endif // GOUDA_NOTES_HTTP_PARSEDREQUEST_HPP
