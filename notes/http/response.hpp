﻿#ifndef GOUDA_NOTES_HTTP_RESPONSE_HPP
#define GOUDA_NOTES_HTTP_RESPONSE_HPP

#include <gouda/notes/http/context.hpp>

#include <QMap>
#include <QSharedPointer>

namespace gouda {
namespace notes {
namespace http {

enum class ResponseCode {
  OK = 200,
  Found = 302,
  NotModified = 304,
  BadRequest = 400,
  Unauthorized = 401,
  Forbidden = 403,
  NotFound = 404,
  InternalServerError = 500
};

class Response
{
  QByteArray body_;
  ResponseCode code_;
  QMap<QByteArray, QByteArray> headers_;

public:
  Response()
    : body_()
    , code_(ResponseCode::InternalServerError)
    , headers_()
  {}

  Response(
      QByteArray &&body,
      ResponseCode code = ResponseCode::InternalServerError,
      QMap<QByteArray, QByteArray> &&headers = QMap<QByteArray, QByteArray>()
      )
    : body_(std::move(body))
    , code_(code)
    , headers_(std::move(headers))
  {}

  virtual ~Response() {}

  const QByteArray &body() const { return body_; }
  void setBody(QByteArray &&body) noexcept { body_ = std::move(body); }

  ResponseCode code() const { return code_; }
  void setCode(ResponseCode code) noexcept { code_ = code; }

  const QMap<QByteArray, QByteArray> &headers() const { return headers_; }
  void addHeader(const QByteArray &name, const QByteArray &value) noexcept
  {
    headers_.insert(name, value);
  }

  virtual void end(const Context &ctx) noexcept(false)
  {
    Request req(ctx.getRequest());
    QByteArray buffer;
    buffer += req.version();
    buffer += " ";
    buffer += QByteArray::number(static_cast<int>(code_));
    buffer += " ";
    buffer += responseCodeText(code_);
    buffer += "\n";
    foreach (QByteArray key, headers_.keys()) {
      buffer += key;
      buffer += ": ";
      buffer += headers_.value(key);
      buffer += "\n";
    }
    if (!body_.isEmpty()) {
      buffer += "\n";
      buffer += body_;
    }
    ctx.writeResponse(buffer);
  }

  static QByteArray responseCodeText(ResponseCode code) noexcept
  {
    switch (code) {
    case ResponseCode::OK: return "OK";
    case ResponseCode::Found: return "Found";
    case ResponseCode::NotModified: return "Not Modified";
    case ResponseCode::BadRequest: return "Bad Request";
    case ResponseCode::Unauthorized: return "Unauthorized";
    case ResponseCode::Forbidden: return "Forbidden";
    case ResponseCode::NotFound: return "Not Found";
    case ResponseCode::InternalServerError: return "Internal Server Error";
    default:
      return "Undefined";
    }
  }
};

using ResponsePtr = QSharedPointer<Response>;

template <typename T, typename ...Args>
ResponsePtr makeResponse(Args... args)
{
  return ResponsePtr(new T(args...));
}

template <typename T, typename ...Args>
ResponsePtr makeRedirectResponse(QByteArray url, Args... args)
{
  auto res = ResponsePtr(new T(args...));
  res->setCode(ResponseCode::Found);
  res->addHeader("Location", url);
  return res;
}

} // namespace http
} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_HTTP_RESPONSE_HPP
