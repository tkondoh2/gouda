﻿#ifndef GOUDA_NOTES_HTTP_PROCEDURE_HPP
#define GOUDA_NOTES_HTTP_PROCEDURE_HPP

#include <gouda/notes/http/response.hpp>

namespace gouda {
namespace notes {
namespace http {

/**
 * @brief DSAPIプロシージャ
 */
class Procedure
{
public:
  /**
   * @brief デストラクタ
   */
  virtual ~Procedure() {}

  /**
   * @brief 処理対象のイベントを返す。
   * @return 処理対象のイベント
   */
  virtual uint eventType() const = 0;

  /**
   * @brief ルート可能か判断する。
   * @param ctx コンテキスト
   * @param req リクエストデータ
   * @param ptr イベントデータポインタ
   * @return ルート可能であればTrue
   */
  virtual bool canRoute(const Context &ctx, const Request &req, void *ptr) = 0;

  /**
   * @brief エントリポイント
   * @param ctx コンテキスト
   * @param req リクエストデータ
   * @param res レスポンスデータ(ポインタ)
   * @param result DSAPI処理通知
   * @return HTTPタスクに処理を戻す場合はTrue
   */
  virtual bool operator ()(
      const Context &ctx,
      const Request &req,
      ResponsePtr &res,
      uint &result
      ) const = 0;
};

/**
 * @class テンプレート型DSAPIプロシージャ
 * @tparam T イベントデータ型
 * @tparam EventType イベントタイプ定数
 * @basename Procedure
 */
template <typename T, uint EventType>
class ProcedureBase
    : public Procedure
{
protected:
  T* eventCtx_; ///< イベントデータへのポインタ

public:
  /**
   * @brief コンストラクタ
   */
  ProcedureBase()
    : Procedure()
    , eventCtx_(nullptr)
  {}

  /**
   * @brief デストラクタ
   */
  virtual ~ProcedureBase() {}

  /**
   * @brief イベントデータへのポインタを返す。
   * @return イベントデータへのポインタ
   */
  T* eventCtx() const { return eventCtx_; }

  /**
   * @brief 処理対象のイベントを返す。
   * @return 処理対象のイベント
   */
  virtual uint eventType() const override { return EventType; }

  /**
   * @brief ルート可能か判断する(テンプレート適用後処理)。
   * @param ctx コンテキスト
   * @param req リクエストデータ
   * @return ルート可能であればTrue
   */
  virtual bool canRoute_sub(const Context &ctx, const Request &req) = 0;

  /**
   * @brief ルート可能か判断する。
   * @param ctx コンテキスト
   * @param req リクエストデータ
   * @param ptr イベントデータポインタ
   * @return ルート可能であればTrue
   */
  virtual bool canRoute(
      const Context &ctx,
      const Request &req,
      void *ptr
      ) override
  {
    eventCtx_ = static_cast<T*>(ptr);
    return canRoute_sub(ctx, req);
  }
};

using ProcedurePtr = QSharedPointer<Procedure>;
using ProcedureList = QList<ProcedurePtr>;

/**
 * @brief DSAPIプロシージャを登録する。
 */
class AppendProcedure
{
public:
  /**
   * @brief エントリポイント
   * @param procedureList 登録先リスト
   * @param pProcudure 登録するプロシージャ(関数オブジェクト)へのポインタ
   */
  void operator ()(ProcedureList &procedureList, Procedure *pProcudure)
  {
    procedureList.append(ProcedurePtr(pProcudure));
  }
};

/**
 * @brief プロシージャを実行する。
 */
class ProcessProcedures
{
public:
  /**
   * @brief エントリポイント
   * @param procedureList プロシージャリスト
   * @param pContext コンテキストへのポインタ
   * @param eventType イベントの種類
   * @param ptr イベントに応じた構造体へのポインタ
   * @return DSAPI通知
   */
  uint operator ()(
      ProcedureList &procedureList,
      FilterContext *pContext,
      uint eventType,
      void *ptr
      )
  {
    // 処理結果を初期化
    uint result = kFilterNotHandled;

    // コンテキストオブジェクトを作成
    Context ctx(pContext);

    // リクエストオブジェクトを作成
    Request req = ctx.getRequest();

    // レスポンスオブジェクトを作成
    ResponsePtr resPtr;
    foreach (ProcedurePtr pProcedure, procedureList) {

      // イベントが一致して、ルート可能であれば処理
      if ((pProcedure->eventType() & eventType)
          && pProcedure->canRoute(ctx, req, ptr)) {

        // イベントを処理
        if ((*pProcedure)(ctx, req, resPtr, result)) {

          // レスポンスオブジェクトがあればエンド処理
          if (!resPtr.isNull()) {
            resPtr->end(ctx);
          }
          return result;
        }
      }
    }
    return result;
  }
};


//class Procedure
//{
//public:
//  Procedure() {}

//  virtual ~Procedure() {}

//  virtual uint eventType() const = 0;

//  virtual bool checkIn(const Context &context, void *ptr) = 0;

//  virtual uint operator ()() = 0;
//};

//template <typename T, uint EventType>
//class ProcedureBase
//    : public Procedure
//{
//protected:
//  Context ctx_;
//  T* eventCtx_;

//public:
//  ProcedureBase()
//    : Procedure()
//    , ctx_()
//    , eventCtx_(nullptr)
//  {}

//  virtual ~ProcedureBase() {}

//  virtual bool checkBase() = 0;

//  virtual uint proc() = 0;

//  virtual uint eventType() const { return EventType; }

//  virtual bool checkIn(const Context &context, void *ptr)
//  {
//    ctx_ = context;
//    eventCtx_ = static_cast<T*>(ptr);
//    return checkBase();
//  }

//  virtual uint operator ()() { return proc(); }
//};

} // namespace http
} // namespace notes
} // amespace gouda

#endif // GOUDA_NOTES_HTTP_PROCEDURE_HPP
