﻿#ifndef GOUDA_NOTES_HTTP_AUTHENTICATE_HPP
#define GOUDA_NOTES_HTTP_AUTHENTICATE_HPP

#include <gouda/notes/distinguishedname.hpp>
#include <gouda/notes/lookup.hpp>
#include <gouda/notes/lmbcslist.hpp>

#include <QString>
#include <QList>
#include <QMap>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <stdnames.h>
#include <misc.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace gouda {
namespace notes {
namespace http {

class Authenticate
{
  QList<DName> fullNames_;
  QMap<Lmbcs, Variant> optionalValues_;

public:
  Authenticate()
    : fullNames_()
    , optionalValues_()
  {}

  const QList<DName> &fullNames() const { return fullNames_; }
  const QMap<Lmbcs, Variant> &optionalValues() const { return optionalValues_; }

  LmbcsList canonicals() const
  {
    LmbcsList list;
    foreach (DName n, fullNames_) {
      list.append(n.canonical());
    }
    return list;
  }

  bool operator ()(
      const Lmbcs &username,
      const Lmbcs &password,
      const LmbcsList &optionalItems = LmbcsList()
      ) noexcept(false)
  {
    fullNames_.clear();
    optionalValues_.clear();
    LmbcsList views { NETAUTH_NAMESPACE_USERS };
    LmbcsList names { username };
    LmbcsList items { HTTP_PASSWORD_ITEM, MAIL_FULLNAME_ITEM };
    foreach (Lmbcs optionalItem, optionalItems) {
      items.append(optionalItem);
    }
    Lmbcs digest;
    LmbcsList fullNameList;
    QMap<Lmbcs, Variant> optVals;
    Lookup()("", 0, views, names, items)
    .subscribe(
    [&digest, &fullNameList, &optVals, &optionalItems](
        Lookup::Result result
      ) {
      digest = result
          .items()
          .value(HTTP_PASSWORD_ITEM)
          .value()
          .toString();
      fullNameList = result
          .items()
          .value(MAIL_PEOPLELOOKUPITEMS)
          .value()
          .toStringList();
      foreach (Lmbcs optionalItem, optionalItems) {
        optVals.insert(
              optionalItem,
              result.items().value(optionalItem).value()
              );
      }
    });
    Status status = SECVerifyPassword(
          static_cast<WORD>(password.byteSize())
          , reinterpret_cast<BYTE*>(const_cast<char*>(password.constData()))
          , static_cast<WORD>(digest.byteSize())
          , reinterpret_cast<BYTE*>(const_cast<char*>(digest.constData()))
          , 0
          , nullptr
          );
    if (status.hasError()) { return false; }
    foreach (Lmbcs fullName, fullNameList) {
      fullNames_.append(DName(fullName));
    }
    optionalValues_ = std::move(optVals);
    return true;
  }
};

} // namespace http
} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_HTTP_AUTHENTICATE_HPP
