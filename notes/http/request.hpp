﻿#ifndef GOUDA_NOTES_HTTP_REQUEST_HPP
#define GOUDA_NOTES_HTTP_REQUEST_HPP

#include <gouda/notes/notes_global.h>
#include <dsapi.h>
#include <QUrl>
#include <QUrlQuery>

namespace gouda {
namespace notes {
namespace http {

class Request
{
  FilterRequest req_;

public:
  Request(FilterRequest &&req)
    : req_(std::move(req))
  {}

  const char * const version() const noexcept { return req_.version; }

  uint method() const noexcept
  {
    return req_.method;
  }

  QUrl url(
      QUrl::ParsingMode mode = QUrl::TolerantMode
      ) const noexcept
  {
    return QUrl(req_.URL, mode);
  }

  QString urlPath(
      QUrl::ComponentFormattingOption options = QUrl::FullyDecoded,
      QUrl::ParsingMode mode = QUrl::TolerantMode
      ) const noexcept
  {
    return url(mode).path(options);
  }

  QUrlQuery urlQuery(
      QUrl::ComponentFormattingOption options = QUrl::FullyDecoded,
      QUrl::ParsingMode mode = QUrl::TolerantMode
      ) const noexcept
  {
    return QUrlQuery(url(mode).query(options));
  }

  QByteArray content() const noexcept
  {
    if (req_.contentReadLen > 0) {
      uint maxSize = std::min<uint>(INT_MAX, req_.contentReadLen);
      QByteArray bytes(req_.contentRead, static_cast<int>(maxSize));
      return bytes;
    }
    return QByteArray();
  }
};

} // namespace http
} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_HTTP_REQUEST_HPP
