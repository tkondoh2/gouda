﻿#ifndef GOUDA_NOTES_HTTP_INIT_HPP
#define GOUDA_NOTES_HTTP_INIT_HPP

//#include <gouda/notes/lmbcs.hpp>
//#include <dsapi.h>
#include <gouda/notes/http/procedure.hpp>
//#include <gouda/notes/http/response.hpp>

//#include <QSharedPointer>

namespace gouda {
namespace notes {
namespace http {

///**
// * @brief DSAPIを初期化する。
// * @tparam eventFlags フィルターするイベントの和
// * @tparam appVersion フィルターインターフェースのバージョン
// * @param pData FilterInitDataへのポインタ
// * @param title フィルターに付けるタイトル
// */
//template <uint eventFlags, uint appVersion = kInterfaceVersion>
//void init(FilterInitData *pData, const char *title) noexcept
//{
//  pData->appFilterVersion = appVersion;
//  pData->eventFlags = eventFlags;
//  qstrcpy(pData->filterDesc, title);
//}

//using ProcedurePtr = QSharedPointer<Procedure>;
//using ProcedureList = QList<ProcedurePtr>;

//inline void appendProcedure(ProcedureList &plist, Procedure *p)
//{
//  plist.append(ProcedurePtr(p));
//}

//class Initialize
//{
//public:
//  void operator ()(
//      FilterInitData *pData,
//      const char *title,
//      const ProcedureList &plist
//      )
//  {
//    uint eventFlags = 0;
//    foreach (ProcedurePtr p, plist) {
//      eventFlags |= p->eventType();
//    }

//    pData->appFilterVersion = kInterfaceVersion;
//    pData->eventFlags = eventFlags;
//    qstrcpy(pData->filterDesc, title);
//  }
//};

//uint doProcedure(
//    ProcedureList &plist,
//    FilterContext *pContext,
//    uint eventType,
//    void *ptr
//    )
//{
//  Context ctx(pContext);
//  foreach (ProcedurePtr p, plist) {
//    if ((p->eventType() & eventType) && p->checkIn(ctx, ptr)) {
//      return (*p.get())();
//    }
//  }
//  return kFilterNotHandled;
//}

///**
// * @brief DSAPIを初期化する。
// * @tparam eventFlags フィルターするイベントの和
// * @tparam appVersion フィルターインターフェースのバージョン
// * @param pData FilterInitDataへのポインタ
// * @param title フィルターに付けるタイトル
// */
//template <uint eventFlags, uint appVersion = kInterfaceVersion>
//void init2(FilterInitData *pData, const char *title) noexcept
//{
//  pData->appFilterVersion = appVersion;
//  pData->eventFlags = eventFlags;
//  qstrcpy(pData->filterDesc, title);
//}

/**
 * @brief DSAPIを初期化する。
 */
class Initialize
{
public:
  /**
   * @brief エントリポイント
   * @param pInitData 初期化データ
   * @param title 登録名
   * @param procedureList プロシージャリスト
   */
  void operator ()(
      FilterInitData *pInitData,
      const char *title,
      const ProcedureList &procedureList
      )
  {
    uint eventFlags = 0;
    foreach (ProcedurePtr p, procedureList) {
      eventFlags |= p->eventType();
    }

    pInitData->appFilterVersion = kInterfaceVersion;
    pInitData->eventFlags = eventFlags;
    qstrcpy(pInitData->filterDesc, title);
  }
};

} // namespace http
} // namespace notes
} // amespace gouda

#endif // GOUDA_NOTES_HTTP_INIT_HPP
