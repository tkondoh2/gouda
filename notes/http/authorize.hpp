﻿#ifndef GOUDA_NOTES_HTTP_AUTHORIZE_HPP
#define GOUDA_NOTES_HTTP_AUTHORIZE_HPP

#include <gouda/notes/notes_global.h>
#include <dsapi.h>

namespace gouda {
namespace notes {
namespace http {

class Authorize
{
  FilterAuthorize *ptr_;

public:
  Authorize(FilterAuthorize *ptr)
    : ptr_(ptr)
  {}
};

} // namespace http
} // namespace notes
} // amespace gouda

#endif // GOUDA_NOTES_HTTP_AUTHORIZE_HPP
