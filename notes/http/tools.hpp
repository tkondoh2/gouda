﻿#ifndef GOUDA_NOTES_HTTP_TOOLS_HPP
#define GOUDA_NOTES_HTTP_TOOLS_HPP

#include <gouda/notes/notes_global.h>

#include <QUrlQuery>

namespace gouda {
namespace notes {
namespace http {

inline QString queryValue(
    const QUrlQuery &query,
    const QString &key,
    QUrl::ComponentFormattingOption encoding = QUrl::PrettyDecoded
    ) noexcept
{
  if (query.hasQueryItem(key)) {
    return query.queryItemValue(key, encoding);
  }
  return QString();
}

} // namespace http
} // namespace notes
} // amespace gouda

#endif // GOUDA_NOTES_HTTP_TOOLS_HPP
