﻿#ifndef GOUDA_NOTES_FILEOBJECT_HPP
#define GOUDA_NOTES_FILEOBJECT_HPP

#include <gouda/notes/item.hpp>
#include <gouda/notes/blockhandle.hpp>

namespace gouda {
namespace notes {

class FileObject
{
  FILEOBJECT value_;
  Lmbcs fileName_;

public:
  enum { PARSE_FILENAME = 1 }; // option

  static const WORD value_type = TYPE_OBJECT;

  FileObject()
    : value_()
    , fileName_()
  {}

  FileObject(const QByteArray &value)
    : value_(*reinterpret_cast<FILEOBJECT*>(const_cast<char*>(value.constData())))
    , fileName_()
  {}

  FileObject(const Item::Info &itemInfo, int option = 0)
    : value_()
    , fileName_()
  {
    BlockHandle valueHandle(itemInfo.valueId());
    char *pValue = valueHandle.lock<char>() + sizeof(WORD); // skipping TYPE prefix
    value_ = *reinterpret_cast<FILEOBJECT*>(pValue);

    if (option & PARSE_FILENAME && value_.Header.ObjectType == OBJECT_FILE) {
      fileName_ = Lmbcs(
            pValue + sizeof(FILEOBJECT),
            static_cast<int>(value_.FileNameLength)
            );
    }
  }

//  bool isValid() const;

  const Lmbcs &fileName() const { return fileName_; }

  Lmbcs toString() const
  {
    LmbcsList list;
    TimeDate created(value_.FileCreated);
    TimeDate modified(value_.FileModified);
    list
      << ql(QString("ObjectType:%1").arg(value_.Header.ObjectType))
      << ql(QString("RRV:%1").arg(value_.Header.RRV))
      << ql(QString("FileNameLength:%1").arg(value_.FileNameLength))
      << ql(QString("HostType:%1").arg(value_.HostType))
      << ql(QString("CompressionType:%1").arg(value_.CompressionType))
      << ql(QString("FileAttributes:%1").arg(value_.FileAttributes))
      << ql(QString("Flags:%1").arg(value_.Flags))
      << ql(QString("FileSize:%1").arg(value_.FileSize))
      << ql(QString("FileCreated:%1").arg(lq(created.toString())))
      << ql(QString("FileModified:%1").arg(lq(modified.toString())))
         ;
    return list.join(',');
  }
};

} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_FILEOBJECT_HPP
