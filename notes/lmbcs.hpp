﻿#ifndef GOUDA_NOTES_LMBCS_HPP
#define GOUDA_NOTES_LMBCS_HPP

#include <gouda/notes/status.hpp>
#include <gouda/notes/translate.hpp>
//#include <gouda/notes/lmbcslist.h>

#include <QByteArray>
#include <QString>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <nsfdata.h> // TYPE_TEXT
#include <nls.h> // NLS_string_chars

#ifdef NT
#pragma pack(pop)
#endif

namespace gouda {
namespace notes {

class LmbcsList;

/**
 * @brief LMBCS文字列クラス
 */
class Lmbcs
{
  QByteArray value_;

public:
  static const WORD value_type = TYPE_TEXT;

  Lmbcs() noexcept : value_() {}

  Lmbcs(const char *c_str, int size = -1) noexcept
    : value_(c_str, size)
  {}

  Lmbcs(QByteArray &&lmbcsBytes) noexcept
    : value_(std::move(lmbcsBytes))
  {}

  Lmbcs &operator =(const Lmbcs &other) noexcept
  {
    if (this != &other) { value_ = other.value_; }
    return *this;
  }

  const QByteArray &value() const { return value_; }
  const char *constData() const noexcept { return value_.constData(); }
  bool isEmpty() const noexcept { return value_.isEmpty(); }
  int byteSize() const noexcept { return value_.size(); }

  int size() const noexcept
  {
    QByteArray s = value_.left(MAXWORD - 1);
    WORD numChars = 0;
    NLS_STATUS status = NLS_string_chars(
          reinterpret_cast<const BYTE*>(s.constData())
          , NLS_NULLTERM
          , &numChars
          , OSGetLMBCSCLS()
          );
    if (status != NLS_SUCCESS) return -1;

    return static_cast<int>(numChars);
  }

  Lmbcs &replace(char before, char after) noexcept
  {
    value_.replace(before, after);
    return *this;
  }

  Lmbcs toString() const { return *this; }

  Lmbcs &operator +=(const Lmbcs &other) noexcept
  {
    value_ += other.value_;
    return *this;
  }

  // for QVariant
  friend QDataStream &operator <<(QDataStream &str, const Lmbcs &lmbcs);

  // for QVariant
  friend QDataStream &operator >>(QDataStream &str, Lmbcs &lmbcs);

  friend Lmbcs operator +(const Lmbcs &lhs, const Lmbcs &rhs);

  /**
   * @brief LmbcsList対策で必要な二項演算子は自由関数ではなくメンバ関数だった件
   * @param other
   * @return
   */
  inline bool operator ==(const Lmbcs &other) const noexcept
  {
    return value() == other.value();
  }

  inline bool operator !=(const Lmbcs &other) const noexcept
  {
    return !operator ==(other);
  }
};

inline bool operator <(const Lmbcs &lhs, const Lmbcs &rhs)
{
  return lhs.value() < rhs.value();
}

/**
 * @brief LMBCS文字列をQStringオブジェクトに変換する。
 */
struct LmbcsToQString
{
  QString operator ()(const Lmbcs &lmbcs) const noexcept
  {
    // 必要なバッファサイズを割り出す。
    int bufferSize = lmbcs.size() * sizeof(ushort);
    if (bufferSize < 0) return QString();

    // 関数オブジェクトLmbcsToUnicodeでUnicode(UTF-16)に変換する。
    QByteArray utf16 = LmbcsToUnicode()(lmbcs.value(), bufferSize);

    // バイナリ状のUTF-16からQStringオブジェクトを作成する。
    return QString::fromUtf16(
          reinterpret_cast<ushort*>(
            const_cast<char*>(utf16.constData())
            ),
          utf16.size() / sizeof(ushort)
          );
  }
};

/**
 * @brief QStringオブジェクトをLMBCS文字列に変換する。
 */
struct QStringToLmbcs
{
  Lmbcs operator ()(const QString &qs) const noexcept
  {
    // 必要なバッファサイズを割り出す。
    int bufferSize = qs.size() * 3;

    // QStringをUnicode(UTF-16)にする。
    QByteArray utf16(
          reinterpret_cast<const char*>(qs.utf16()),
          qs.size() * 2
          );

    // LMBCS化してLmbcsオブジェクトに変換して返す。
    return Lmbcs(UnicodeToLmbcs()(utf16, bufferSize));
  }
};

inline QDataStream &operator <<(QDataStream &str, const Lmbcs &lmbcs)
{
  str << lmbcs.value_;
  return str;
}

inline QDataStream &operator >>(QDataStream &str, Lmbcs &lmbcs)
{
  str >> lmbcs.value_;
  return str;
}

inline Lmbcs operator +(const Lmbcs &lhs, const Lmbcs &rhs)
{
  return Lmbcs(lhs.value_ + rhs.value_);
}

inline QString lq(const Lmbcs &lmbcs) noexcept
{
  return LmbcsToQString()(lmbcs);
}

inline Lmbcs ql(const QString &qs) noexcept
{
  return QStringToLmbcs()(qs);
}

/**
 * @tparam T 引数に割り当てる型
 * @tparam F QByteArray::numberに適用する型
 */
template <typename T, typename F>
Lmbcs formatId(T id)
{
  size_t w = sizeof(T) * 2;
  QByteArray s
      = QByteArray("0").repeated(w - 1)
      + QByteArray::number(static_cast<F>(id), 16);
  s = s.right(w).toUpper();
  return Lmbcs(s.constData(), s.size());
}

} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_LMBCS_HPP
