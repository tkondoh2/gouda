﻿#ifndef GOUDA_NOTES_SEARCH_HPP
#define GOUDA_NOTES_SEARCH_HPP

#include <gouda/notes/database.hpp>
//#include <gouda/notes/formula.h>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <nsfsearc.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace gouda {
namespace notes {

template <typename T, typename Func>
struct Search
{
  struct Iterator
  {
    subscriber<T> *pSubscriber_;
    Func func_;
  };

  static STATUS LNPUBLIC searchCallback(
          void *ptr,
          SEARCH_MATCH *pMatch,
          ITEM_TABLE *pSummaryBuffer
          )
  {
    SEARCH_MATCH match;
    memcpy(&match, pMatch, sizeof(SEARCH_MATCH));
    Search<T, Func>::Iterator *iter
        = reinterpret_cast<Search<T, Func>::Iterator*>(ptr);
    T result;
    if (iter->func_(match, pSummaryBuffer, &result)) {
      iter->pSubscriber_->on_next(result);
    }
    return NOERROR;
  }

  observable<T> operator ()(
      DatabasePtr dbPtr,
      FormulaPtr fPtr,
      const Lmbcs &viewTitle,
      WORD flags,
      WORD noteClassMask,
      TIMEDATE *pSince,
      Func func,
      TIMEDATE *retUntil
      ) noexcept
  {
    return operator ()(
          dbPtr->handle(),
          fPtr->handle(),
          viewTitle,
          flags,
          noteClassMask,
          pSince,
          func,
          retUntil
          );
  }

  observable<T> operator ()(
        DBHANDLE hDB,
        FORMULAHANDLE hFormula,
        const Lmbcs &viewTitle,
        WORD flags,
        WORD noteClassMask,
        TIMEDATE *pSince,
        Func func,
        TIMEDATE *retUntil
        ) noexcept
  {

    return observable<>::create<T>(
      [
          hDB,
          hFormula,
          viewTitle,
          flags,
          noteClassMask,
          pSince,
          func,
          retUntil
      ](subscriber<T> s) {
        Search<T, Func>::Iterator iter;
        iter.func_ = func;
        iter.pSubscriber_ = &s;
        Status status = NSFSearch(
              hDB,
              hFormula,
              viewTitle.isEmpty()
                ? nullptr
                : const_cast<char*>(viewTitle.constData()),
              flags,
              noteClassMask,
              pSince,
              searchCallback,
              &iter,
              retUntil
              );
        if (status.hasError()) {
          s.on_error(std::make_exception_ptr(status));
        }
        s.on_completed();
      }
    );
  }
};

} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_SEARCH_HPP
