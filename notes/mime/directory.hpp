﻿#ifndef GOUDA_NOTES_MIME_DIRECTORY_HPP
#define GOUDA_NOTES_MIME_DIRECTORY_HPP

#include <gouda/notes/note.hpp>

#include <QSharedPointer>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <mimedir.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace gouda {
namespace notes {
namespace mime {

class Directory;
using DirectoryPtr = QSharedPointer<Directory>;
using _Directory = Handle<
  HMIMEDIRECTORY,
  decltype(&MIMEFreeDirectory),
  decltype(&isNullHandle<HMIMEDIRECTORY>)
>;

class Directory
    : public _Directory
{
public:
  Directory() noexcept
    : _Directory(
        NULLHANDLE,
        &MIMEFreeDirectory,
        &isNullHandle<HMIMEDIRECTORY>
        )
  {}

  Directory(HMIMEDIRECTORY handle) noexcept
    : _Directory(
        std::move(handle),
        &MIMEFreeDirectory,
        &isNullHandle<HMIMEDIRECTORY>
        )
  {}

  virtual ~Directory()
  {}

  static DirectoryPtr open(const NotePtr & notePtr) noexcept(false)
  {
    return open(notePtr->handle());
  }

  static DirectoryPtr open(NOTEHANDLE hNote) noexcept(false)
  {
    HMIMEDIRECTORY handle = NULLHANDLE;
    Status status = MIMEOpenDirectory(hNote, &handle);
    if (status.hasError()) throw status;
    return make_handle_ptr<Directory>(handle);
  }
};

} // namespace mime
} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_MIME_DIRECTORY_HPP
