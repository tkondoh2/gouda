﻿#ifndef GOUDA_NOTES_MIME_RFC822TEXT_HPP
#define GOUDA_NOTES_MIME_RFC822TEXT_HPP

#include <gouda/notes/lmbcslist.hpp>
#include <gouda/notes/timedate.hpp>
#include <gouda/core/mime/decode.hpp>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <mimeods.h> // RFC822ITEMDESC
#include <ods.h> // _RFC822ITEMDESC

#ifdef NT
#pragma pack(pop)
#endif

namespace gcm { using namespace gouda::core::mime; }

namespace gouda {
namespace notes {
namespace mime {

class Rfc822Text
{
  RFC822ITEMDESC desc_;
  QByteArray native_;
  QByteArray name_;
  QByteArray delim_;
  QByteArray body_;

public:
  static const WORD value_type = TYPE_RFC822_TEXT;

  Rfc822Text() : desc_(), native_(), name_(), delim_(), body_() {}

  Rfc822Text(const QByteArray &bytes)
    : desc_(), native_(), name_(), delim_(), body_()
  {
    *this = fromQByteArray(bytes);
  }

  WORD version() const { return desc_.wVersion; }
  DWORD flags() const { return desc_.dwFlags; }
  DWORD type() const { return flags() & RFC822_ITEM_FORMAT_MASK; }

  WORD nativeLen() const { return desc_.wNotesNativeLen; }
  WORD nameLen() const { return desc_.w822NameLen; }
  WORD delimLen() const { return desc_.w822DelimLen; }
  WORD bodyLen() const { return desc_.w822BodyLen; }

  const QByteArray &native() const { return native_; }
  const QByteArray &name() const { return name_; }
  const QByteArray &delim() const { return delim_; }
  const QByteArray &body() const { return body_; }

  bool isAddress() const { return type() == RFC822_ITEM_FORMAT_ADDR; }
  bool isDate() const { return type() == RFC822_ITEM_FORMAT_DATE; }
  bool isText() const { return type() == RFC822_ITEM_FORMAT_TEXT; }
  bool isStorageStrict() const { return flags() & RFC822_ITEM_STORAGE_STRICT; }
  bool isTextList() const { return flags() & RFC822_ITEM_TEXT_LIST; }
  bool isUnused() const { return flags() & RFC822_TEXT_UNUSED; }

  Lmbcs text() const
  {
    if (bodyLen() > 0) {
      return ql(gcm::decodeMimeHeader(QString::fromLatin1(body())));
    } else {
      return Lmbcs();
    }
  }

  TimeDate date() const
  {
    if (isDate()) {
      auto dt = QDateTime::fromString(QString::fromLatin1(body()), Qt::RFC2822Date);
      return TimeDate::fromQDateTime(dt);
    } else {
      return TimeDate();
    }
  }

  LmbcsList textList() const
  {
    if (isTextList()) {
      return LmbcsList::fromQByteArray(native());
    } else {
      return LmbcsList();
    }
  }

#ifndef NT
  template <typename T>
  static char *_copy(char* from, void *to)
  {
    memcpy(to, from, sizeof(T));
    return from + sizeof(T);
  }
#endif

  static void *copyODStoRFC822ITEMDESC(
      void *ppSrc,
      RFC822ITEMDESC *pDest,
      WORD iterations
      )
  {
    auto pRef = reinterpret_cast<char**>(ppSrc);
    for (WORD i = 0; i < iterations; ++i) {
#ifdef NT
      static const auto size = ODSLength(_RFC822ITEMDESC);
      memcpy_s(pDest, size, *pRef, size);
      *pRef += size;
#else
      *pRef = _copy<WORD>(*pRef, &pDest->wVersion);
      *pRef = _copy<DWORD>(*pRef, &pDest->dwFlags);
      *pRef = _copy<WORD>(*pRef, &pDest->wNotesNativeLen);
      *pRef = _copy<WORD>(*pRef, &pDest->w822NameLen);
      *pRef = _copy<WORD>(*pRef, &pDest->w822DelimLen);
      *pRef = _copy<WORD>(*pRef, &pDest->w822BodyLen);
#endif
    }
    return pRef;
  }

  static QByteArray trimLastCRLF(const QByteArray &bytes)
  {
    if (bytes.right(2) == QByteArray("\x0d\x0a")) {
      return bytes.left(bytes.length() - 2);
    }
    return bytes;
  }

  static Rfc822Text fromQByteArray(const QByteArray &bytes, bool hasPrefix = false)
  {
    Rfc822Text result;
    int index = 0;
    if (hasPrefix) {
      index += sizeof(WORD);
    }
//    memcpy(&result.desc_, bytes.data() + index, sizeof(RFC822ITEMDESC));
    auto ptr = const_cast<char*>(bytes.data()) + index;
    copyODStoRFC822ITEMDESC(&ptr, &result.desc_, 1);

//    index += sizeof(RFC822ITEMDESC);
    index += ODSLength(_RFC822ITEMDESC);
    result.native_ = bytes.mid(index, result.nativeLen());

    index += result.nativeLen();
    result.name_ = bytes.mid(index, result.nameLen());

    index += result.nameLen();
    result.delim_ = bytes.mid(index, result.delimLen());

    index += result.delimLen();
    result.body_ = trimLastCRLF(bytes.mid(index, result.bodyLen()));

    return result;
  }
};

} // namespace mime
} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_MIME_RFC822TEXT_HPP
