﻿#ifndef GOUDA_NOTES_MIME_ENTITY_HPP
#define GOUDA_NOTES_MIME_ENTITY_HPP

#include <gouda/notes/mime/directory.hpp>
#include <gouda/notes/objecthandle.hpp>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <nsferr.h>
#include <miscerr.h>

#ifdef NT
#pragma pack(pop)
#endif

#define ERR_MIME_NO_DATA (PKG_MAILMISC1 + 7)

namespace gouda {
namespace notes {
namespace mime {

class Entity;
using EntityPtr = QSharedPointer<Entity>;
using _Entity = Handle<
  PMIMEENTITY,
  decltype(&doNothing<PMIMEENTITY>),
  decltype(&isNullPtr)
>;

class Entity
    : public _Entity
{
public:
  Entity() noexcept
    : _Entity(nullptr, &doNothing<PMIMEENTITY>, &isNullPtr)
  {}

  Entity(PMIMEENTITY pME) noexcept
    : _Entity(std::move(pME), &doNothing<PMIMEENTITY>, &isNullPtr)
  {}

  EntityPtr getFirstSubpart(const DirectoryPtr &mdPtr) const noexcept
  {
    PMIMEENTITY pMEChild = nullptr;
    Status status = MIMEGetFirstSubpart(mdPtr->handle(), handle(), &pMEChild);
    if (status.hasError()) return EntityPtr();
    return make_handle_ptr<Entity>(pMEChild);
  }

  EntityPtr getNextSubpart(const DirectoryPtr &mdPtr) const noexcept
  {
    PMIMEENTITY pMEChild = nullptr;
    Status status = MIMEGetNextSibling(mdPtr->handle(), handle(), &pMEChild);
    if (status.hasError()) return EntityPtr();
    return make_handle_ptr<Entity>(pMEChild);
  }

  bool isDiscretePart() const noexcept
  {
    return MIMEEntityIsDiscretePart(handle()) ? true : false;
  }

  bool isMessagePart() const noexcept
  {
    return MIMEEntityIsMessagePart(handle()) ? true : false;
  }

  bool isMultiPart() const noexcept
  {
    return MIMEEntityIsMultiPart(handle()) ? true : false;
  }

  Lmbcs getHeader(MIMESYMBOL symbol) const noexcept
  {
    Lmbcs header(MIMEEntityGetHeader(handle(), symbol));
    return header;
  }

  Lmbcs getTypeParam(MIMESYMBOL symbol) const noexcept(false)
  {
    DHANDLE hParam;
    DWORD	dwTypeParamLen;
    Status status = MIMEEntityGetTypeParam(
          handle(),
          symbol,
          &hParam,
          &dwTypeParamLen
          );
    if (status.error() == ERR_MIME_NO_DATA) return Lmbcs();
    if (status.hasError()) throw status;
    ObjectHandle paramObj(std::move(hParam));
    Lmbcs paramText(paramObj.lock<char>(), static_cast<int>(dwTypeParamLen));
    return paramText;
  }

  Lmbcs getCharset() const noexcept(false)
  {
    return getTypeParam(MIME_SYMBOL_CHARSET);
  }

  Lmbcs getFilename() const noexcept(false)
  {
    return getTypeParam(MIME_SYMBOL_FILENAME);
  }

  struct Data {
    DHANDLE handle;
    DWORD len;
  };

  QByteArray getEntityData(const NotePtr &notePtr, WORD dataType) const
  {
    const DWORD chunkLen = 1024;
    DWORD offset = 0;
    QByteArray result;
    observable<>::create<Data>([&, this](subscriber<Data> s) {
      Status status;
      do {
        Data data;
        status = MIMEGetEntityData(
              notePtr->handle(),
              handle(),
              dataType,
              offset,
              chunkLen,
              &data.handle,
              &data.len
              );
        if (status.noError()) s.on_next(data);
      } while (status.noError());
      s.on_completed();
    })
    .map([&offset](Data data) {
      offset += data.len;
      ObjectHandle dataObj(std::move(data.handle));
      return QByteArray(dataObj.lock<char>(), static_cast<int>(data.len));
    })
    .reduce(QByteArray(), [](QByteArray data, QByteArray item) {
      data += item;
      return data;
    })
    .subscribe([&result](QByteArray data) { result = data; });
    return result;
  }

//  inline Lmbcs getText() const noexcept(false)
//  {
//    return getTypeParam(MIME_SYMBOL_TEXT);
//  }

  MIMESYMBOL getContentType() const noexcept
  {
    return MIMEEntityContentType(handle());
  }

  MIMESYMBOL getContentSubType() const noexcept
  {
    return MIMEEntityContentSubtype(handle());
  }

  Lmbcs getContentTypeName() const noexcept
  {
    MIMESYMBOL sym = getContentType();
    switch (sym)
    {
    case MIME_SYMBOL_UNKNOWN:
      Lmbcs header = getHeader(MIME_SYMBOL_CONTENT_TYPE);
      QRegExp re("(\\w+)/(\\w+)");
      if (re.indexIn(lq(header)) > -1) {
        return ql(re.cap(1));
      }
      break;
    }
    return contentTypeToText(sym);
  }

  Lmbcs getContentSubTypeName() const noexcept
  {
    MIMESYMBOL sym = getContentSubType();
    switch (sym)
    {
    case MIME_SYMBOL_UNKNOWN:
      Lmbcs header = getHeader(MIME_SYMBOL_CONTENT_TYPE);
      QRegExp re("(\\w+)/(\\w+)");
      if (re.indexIn(lq(header)) > -1) {
        return ql(re.cap(2));
      }
      break;
    }
    return contentTypeToText(sym);
  }

  template <typename Func>
  static void scan(const DirectoryPtr &mdPtr, const EntityPtr &parent, Func callback)
  {
    callback(parent);
    EntityPtr child = parent->getFirstSubpart(mdPtr);
    while (child->isValid()) {
      scan(mdPtr, child, callback);
      child = child->getNextSubpart(mdPtr);
    }
  }

  static observable<EntityPtr> scanTree(const DirectoryPtr &mdPtr, const EntityPtr &root)
  {
    return observable<>::create<EntityPtr>([mdPtr, root](subscriber<EntityPtr> s) {
      scan(mdPtr, root, [&s](EntityPtr entity) {
        s.on_next(entity);
      });
      s.on_completed();
    });
  }

  static EntityPtr getRootEntity(const DirectoryPtr &dirPtr) noexcept(false)
  {
    PMIMEENTITY pME = nullptr;
    Status status = MIMEGetRootEntity(dirPtr->handle(), &pME);
    if (status.hasError()) throw status;
    return make_handle_ptr<Entity>(pME);
  }

  static Lmbcs contentTypeToText(MIMESYMBOL symbol) noexcept
  {
    switch( symbol )
    {
    case MIME_SYMBOL_UNKNOWN: return Lmbcs("unknown");
    case MIME_SYMBOL_TEXT: return Lmbcs("text");
    case MIME_SYMBOL_MULTIPART: return Lmbcs("multipart");
    case MIME_SYMBOL_MESSAGE: return Lmbcs("message");
    case MIME_SYMBOL_APPLICATION: return Lmbcs("application");
    case MIME_SYMBOL_IMAGE: return Lmbcs("image");
    case MIME_SYMBOL_AUDIO: return Lmbcs("audio");
    case MIME_SYMBOL_VIDEO: return Lmbcs("video");
    case MIME_SYMBOL_NONE: return Lmbcs("none");

    case MIME_SYMBOL_TO: return Lmbcs("to");
    case MIME_SYMBOL_FROM: return Lmbcs("from");
    case MIME_SYMBOL_COMMENTS: return Lmbcs("comments");
    case MIME_SYMBOL_DATE: return Lmbcs("date");
    case MIME_SYMBOL_ENCRYPTED: return Lmbcs("encrypted");
    case MIME_SYMBOL_IN_REPLY_TO: return Lmbcs("in_reply_to");
    case MIME_SYMBOL_KEYWORDS: return Lmbcs("keywords");
    case MIME_SYMBOL_PLAIN: return Lmbcs("plain");
    case MIME_SYMBOL_BCC: return Lmbcs("bcc");
    case MIME_SYMBOL_CC: return Lmbcs("cc");
    case MIME_SYMBOL_OCTET_STREAM: return Lmbcs("octet_stream");
    case MIME_SYMBOL_SUBJECT: return Lmbcs("subject");
    case MIME_SYMBOL_HTML: return Lmbcs("html");
    case MIME_SYMBOL_SENDER: return Lmbcs("sender");
    case MIME_SYMBOL_REPLY_TO: return Lmbcs("reply_to");
    case MIME_SYMBOL_INLINE: return Lmbcs("inline");
    case MIME_SYMBOL_ATTACHMENT: return Lmbcs("attachment");
    case MIME_SYMBOL_FILENAME: return Lmbcs("filename");
    case MIME_SYMBOL_ALTERNATIVE: return Lmbcs("alternative");
    case MIME_SYMBOL_MIXED: return Lmbcs("mixed");
    case MIME_SYMBOL_7BIT: return Lmbcs("7bit");
    case MIME_SYMBOL_8BIT: return Lmbcs("8bit");
    case MIME_SYMBOL_QUOTED_PRINTABLE: return Lmbcs("quoted_printable");
    case MIME_SYMBOL_BASE64: return Lmbcs("base64");
    case MIME_SYMBOL_BINARY: return Lmbcs("binary");
    case MIME_SYMBOL_VERSION: return Lmbcs("version");
    case MIME_SYMBOL_CHARSET: return Lmbcs("charset");
    case MIME_SYMBOL_BOUNDARY: return Lmbcs("boundary");
    case MIME_SYMBOL_NAME: return Lmbcs("name");
    case MIME_SYMBOL_MESSAGE_ID: return Lmbcs("message_id");
    case MIME_SYMBOL_CONTENT_TYPE: return Lmbcs("content_type");
    case MIME_SYMBOL_CONTENT_TRANSFER_ENCODING: return Lmbcs("content_transfer_encoding");
    case MIME_SYMBOL_CONTENT_DISPOSITION: return Lmbcs("content_disposition");
    case MIME_SYMBOL_CONTENT_MD5: return Lmbcs("content_md5");
    case MIME_SYMBOL_CONTENT_LANGUAGE: return Lmbcs("content_language");
    case MIME_SYMBOL_RECEIVED: return Lmbcs("received");
    case MIME_SYMBOL_NEWSGROUPS: return Lmbcs("newsgroups");
    case MIME_SYMBOL_PATH: return Lmbcs("path");
    case MIME_SYMBOL_FOLLOWUP_TO: return Lmbcs("followup_to");
    case MIME_SYMBOL_EXPIRES: return Lmbcs("expires");
    case MIME_SYMBOL_REFERENCES: return Lmbcs("references");
    case MIME_SYMBOL_CONTROL: return Lmbcs("control");
    case MIME_SYMBOL_DISTRIBUTION: return Lmbcs("distribution");
    case MIME_SYMBOL_ORGANIZATION: return Lmbcs("organization");
    case MIME_SYMBOL_SUMMARY: return Lmbcs("summary");
    case MIME_SYMBOL_APPROVED: return Lmbcs("approved");
    case MIME_SYMBOL_LINES: return Lmbcs("lines");
    case MIME_SYMBOL_XREF: return Lmbcs("xref");
    case MIME_SYMBOL_NNTP_POSTING_HOST: return Lmbcs("nntp_posting_host");
    case MIME_SYMBOL_XNEWSREADER: return Lmbcs("xnewsreader");
    case MIME_SYMBOL_XMAILER: return Lmbcs("xmailer");
    case MIME_SYMBOL_RFC822: return Lmbcs("rfc822");
    case MIME_SYMBOL_CONTENT_DESCRIPTION: return Lmbcs("content_description");
    case MIME_SYMBOL_CONTENT_ID: return Lmbcs("content_id");
    case MIME_SYMBOL_RELATED: return Lmbcs("related");
    case MIME_SYMBOL_GIF: return Lmbcs("gif");
    case MIME_SYMBOL_JPEG: return Lmbcs("jpeg");
    case MIME_SYMBOL_PARTIAL: return Lmbcs("partial");
    case MIME_SYMBOL_EXTERNAL_BODY: return Lmbcs("external_body");
    case MIME_SYMBOL_ENRICHED: return Lmbcs("enriched");
    case MIME_SYMBOL_MPEG: return Lmbcs("mpeg");
    case MIME_SYMBOL_BASIC: return Lmbcs("basic");
    case MIME_SYMBOL_START: return Lmbcs("start");
    case MIME_SYMBOL_PARALLEL: return Lmbcs("parallel");
    case MIME_SYMBOL_URL: return Lmbcs("url");
    case MIME_SYMBOL_ACCESS_TYPE: return Lmbcs("access_type");
    case MIME_SYMBOL_EXPIRATION: return Lmbcs("expiration");
    case MIME_SYMBOL_SITE: return Lmbcs("site");
    case MIME_SYMBOL_SERVER: return Lmbcs("server");
    case MIME_SYMBOL_DIRECTORY: return Lmbcs("directory");
    case MIME_SYMBOL_MODE: return Lmbcs("mode");
    case MIME_SYMBOL_PERMISSION: return Lmbcs("permission");
    case MIME_SYMBOL_SIZE: return Lmbcs("size");
    case MIME_SYMBOL_CONTENT_BASE: return Lmbcs("content_base");
    case MIME_SYMBOL_CONTENT_LOCATION: return Lmbcs("content_location");
    case MIME_SYMBOL_X_X509_USER_CERT: return Lmbcs("x_x509_user_cert");
    case MIME_SYMBOL_X_X509_CA_CERT: return Lmbcs("x_x509_ca_cert");
    case MIME_SYMBOL_SIGNED: return Lmbcs("signed");
    case MIME_SYMBOL_XPKCS7SIGNATURE: return Lmbcs("xpkcs7signature");
    case MIME_SYMBOL_PKCS7SIGNATURE: return Lmbcs("pkcs7signature");
    case MIME_SYMBOL_XPKCS7MIME: return Lmbcs("xpkcs7mime");
    case MIME_SYMBOL_PKCS7MIME: return Lmbcs("pkcs7mime");
    case MIME_SYMBOL_APPLEDOUBLE: return Lmbcs("appledouble");
    case MIME_SYMBOL_APPLEFILE: return Lmbcs("applefile");
    case MIME_SYMBOL_METHOD: return Lmbcs("method");
    case MIME_SYMBOL_PROFILE: return Lmbcs("profile");
    case MIME_SYMBOL_CALENDAR: return Lmbcs("calendar");
    case MIME_SYMBOL_VCARD21: return Lmbcs("vcard21");
    case MIME_SYMBOL_X_MAC_TYPE: return Lmbcs("x_mac_type");
    case MIME_SYMBOL_X_MAC_CREATOR: return Lmbcs("x_mac_creator");
    case MIME_SYMBOL_MAC_BINHEX40: return Lmbcs("mac_binhex40");
    case MIME_SYMBOL_X_NOTES_ITEM: return Lmbcs("x_notes_item");
    case MIME_SYMBOL_X_UUENCODE: return Lmbcs("x_uuencode");
    case MIME_SYMBOL_X_UUE: return Lmbcs("x_uue");
    case MIME_SYMBOL_UUENCODE: return Lmbcs("uuencode");
    case MIME_SYMBOL_X_LOTUS_ENCAP: return Lmbcs("x_lotus_encap");
    case MIME_SYMBOL_X_LOTUS_ENCAP1: return Lmbcs("x_lotus_encap1");
    case MIME_SYMBOL_X_LOTUS_ENCAP2: return Lmbcs("x_lotus_encap2");
    case MIME_SYMBOL_X_MIMETRACK: return Lmbcs("x_mimetrack");
    case MIME_SYMBOL_DIGEST: return Lmbcs("digest");
    case MIME_SYMBOL_32KADPCM: return Lmbcs("32kadpcm");
    case MIME_SYMBOL_VOICE_MESSAGE: return Lmbcs("voice_message");
    case MIME_SYMBOL_BMP: return Lmbcs("bmp");
    case MIME_SYMBOL_DELIVERY_STATUS: return Lmbcs("delivery_status");
    case MIME_SYMBOL_REPORT: return Lmbcs("report");
    case MIME_SYMBOL_RFC822_HEADERS: return Lmbcs("rfc822_headers");
    case MIME_SYMBOL_PROTOCOL: return Lmbcs("protocol");
    case MIME_SYMBOL_CGM: return Lmbcs("cgm");
    case MIME_SYMBOL_CSS: return Lmbcs("css");
    case MIME_SYMBOL_POSTSCRIPT: return Lmbcs("postscript");
    case MIME_SYMBOL_RICHTEXT: return Lmbcs("richtext");
    case MIME_SYMBOL_TIFF: return Lmbcs("tiff");
    case MIME_SYMBOL_TNEF: return Lmbcs("tnef");
    case MIME_SYMBOL_PNG: return Lmbcs("png");
    case MIME_SYMBOL_EMBEDDED_XML: return Lmbcs("embedded_xml");
    case MIME_SYMBOL_EMBEDDED_JSON: return Lmbcs("embedded_json");
    default: return Lmbcs("UnKnowName");
    }
  }
};

} // namespace mime
} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_MIME_ENTITY_HPP
