#ifndef GOUDA_NOTES_MIME_CONVERSIONCONTROL_HPP
#define GOUDA_NOTES_MIME_CONVERSIONCONTROL_HPP

#include <gouda/notes/handle.hpp>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <mime.h>
#include <mailmisc.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace gouda {
namespace notes {
namespace mime {

class ConversionControl;
using ConversionControlPtr = QSharedPointer<ConversionControl>;
using _ConversionControl = Handle<
  CCHANDLE,
  decltype(&MMDestroyConvControls),
  decltype(&isNullPtr)
>;

class ConversionControl
    : public _ConversionControl
{
public:
  ConversionControl()
    : _ConversionControl(nullptr, &MMDestroyConvControls, &isNullPtr)
  {}

  ConversionControl(CCHANDLE handle)
    : _ConversionControl(std::move(handle), &MMDestroyConvControls, &isNullPtr)
  {}

  virtual ~ConversionControl() {}

  void setMessageContentEncoding(WORD w) noexcept
  {
    MMSetMessageContentEncoding(handle(), w);
  }

  void setCharSetDetect(BOOL b) noexcept
  {
    MMSetCharSetDetect(handle(), b);
  }

  static ConversionControlPtr create() noexcept(false)
  {
    CCHANDLE handle = nullptr;
    Status status = MMCreateConvControls(&handle);
    if (status.hasError()) throw status;
    return make_handle_ptr<ConversionControl>(handle);
  }
};

} // namespace mime
} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_MIME_CONVERSIONCONTROL_HPP
