﻿#ifndef GOUDA_NOTES_TOOLS_HPP
#define GOUDA_NOTES_TOOLS_HPP

#include <gouda/notes/lmbcs.hpp>

#include <QSharedPointer>

namespace gouda {
namespace notes {

template <typename T>
bool isNullHandle(T handle) noexcept
{
  return handle == NULLHANDLE;
}

inline bool isNullPtr(void *ptr) noexcept
{
  return ptr == nullptr;
}

inline bool isNullBlockId(BLOCKID blockId) noexcept
{
  return ISNULLBLOCKID(blockId);
}

template <typename T>
void doNothing(T) noexcept {}

/**
 * @brief 指定した型の共有ポインタを任意の引数で生成する。
 */
template <typename T, typename H, typename... Args>
QSharedPointer<T> make_handle_ptr(H &&handle, Args... args)
{
  return QSharedPointer<T>(new T(std::move(handle), args...));
}

} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_TOOLS_HPP
