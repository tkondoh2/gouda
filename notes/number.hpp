﻿#ifndef GOUDA_NOTES_NUMBER_HPP
#define GOUDA_NOTES_NUMBER_HPP

#include <gouda/notes/lmbcs.hpp>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <misc.h> // NFMT,ConvertFLOATToText...

#ifdef NT
#pragma pack(pop)
#endif

namespace gouda {
namespace notes {

class Number
{
  NUMBER value_;

public:
  static const WORD value_type = TYPE_NUMBER;

  Number() : value_(0) {}
  Number(NUMBER value) : value_(value) {}
  Number(const QByteArray &bytes)
    : value_(*reinterpret_cast<NUMBER*>(
               const_cast<char*>(bytes.constData())
               ))
  {}

  NUMBER value() const noexcept { return value_; }
  Lmbcs toString() const noexcept(false) { return convertToText(); }

  Lmbcs convertToText(
      NFMT *pTextFormat = nullptr,
      void *pIntlFormat = nullptr
      ) const noexcept(false)
  {
    return convertToText(value_, pTextFormat, pIntlFormat);
  }

  static Lmbcs convertToText(
      NUMBER value,
      NFMT *pTextFormat = nullptr,
      void *pIntlFormat = nullptr
      ) noexcept(false)
  {
    char buffer[MAXALPHANUMBER];
    WORD len = 0;
    Status status = ConvertFLOATToText(
          pIntlFormat,
          pTextFormat,
          &value,
          buffer,
          MAXALPHANUMBER,
          &len
          );
    if (status.hasError()) throw status;
    return Lmbcs(buffer, len);
  }
};

} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_NUMBER_HPP
