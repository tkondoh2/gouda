﻿#ifndef GOUDA_NOTES_DATABASE_HPP
#define GOUDA_NOTES_DATABASE_HPP

#include <gouda/notes/lmbcs.hpp>
#include <gouda/notes/handle.hpp>
#include <gouda/notes/status.hpp>
#include <gouda/notes/lmbcslist.hpp>
#include <gouda/notes/formula.hpp>
#include <gouda/notes/collection.hpp>
#include <gouda/notes/note.hpp>

#include <QSharedPointer>
#include <QPair>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <nsfdb.h>
#include <nsfnote.h>
#include <osfile.h> // OSPathNetConstruct

#ifdef NT
#pragma pack(pop)
#endif

namespace gouda {
namespace notes {

class Note;
using NotePtr = QSharedPointer<Note>;

class Collection;
using CollectionPtr = QSharedPointer<Collection>;

class Database;
using DatabasePtr = QSharedPointer<Database>;
using _Database = Handle<
  DBHANDLE,
  decltype(&NSFDbClose),
  decltype(&isNullHandle<DBHANDLE>)
>;

class Database
    : public _Database
{
public:
  class Path
  {
    Lmbcs server_;
    Lmbcs path_;
    Lmbcs port_;

  public:
    Path() noexcept
      : server_()
      , path_()
      , port_()
    {}

    Path(
        const Lmbcs &path,
        const Lmbcs &server,
        const Lmbcs &port = Lmbcs()
        ) noexcept
      : server_(server)
      , path_(path)
      , port_(port)
    {}

    const Lmbcs &server() const noexcept { return server_; }

    const Lmbcs &path() const noexcept { return path_; }

    const Lmbcs &port() const noexcept { return port_; }

    Lmbcs encode() const noexcept(false)
    {
      char netPath[MAXPATH];
      Status status = OSPathNetConstruct(
            port_.isEmpty() ? nullptr : port_.constData(),
            server_.constData(),
            path_.constData(),
            netPath
            );
      if (status.hasError()) throw status;
      return Lmbcs(netPath);
    }

    static Path decode(const Lmbcs &encoded) noexcept(false)
    {
      char port[MAXPATH], server[MAXPATH], file[MAXPATH];
      Status status = OSPathNetParse(encoded.constData(), port, server, file);
      if (status.hasError()) throw status;
      return Database::Path(Lmbcs(file), Lmbcs(server), Lmbcs(port));
    }

    // for QVariant
    friend QDataStream &operator <<(QDataStream &str, const Path &path)
    {
      str << path.server_ << path.path_ << path.port_;
      return str;
    }

    // for QVariant
    friend QDataStream &operator >>(QDataStream &str, Path &path)
    {
      str >> path.server_ >> path.path_ >> path.port_;
      return str;
    }

  private:
  };

  class Info
  {
    LmbcsList list_;

  public:
    Info() : list_() {}

    Info(const QByteArray &buffer)
      : list_()
    {
      list_.append(parse(buffer, INFOPARSE_TITLE));
      list_.append(parse(buffer, INFOPARSE_CATEGORIES));
      list_.append(parse(buffer, INFOPARSE_CLASS));
      list_.append(parse(buffer, INFOPARSE_DESIGN_CLASS));
    }

    const Lmbcs &title() const noexcept
    {
      return list_.at(INFOPARSE_TITLE);
    }

    const Lmbcs &categories() const noexcept
    {
      return list_.at(INFOPARSE_CATEGORIES);
    }

    const Lmbcs &classFrom() const noexcept
    {
      return list_.at(INFOPARSE_CLASS);
    }

    const Lmbcs &designClass() const noexcept
    {
      return list_.at(INFOPARSE_DESIGN_CLASS);
    }

    static Lmbcs parse(const QByteArray &buffer, WORD what) noexcept
    {
      char buf[NSF_INFO_SIZE];
      NSFDbInfoParse(const_cast<char*>(buffer.constData()), what, buf, NSF_INFO_SIZE);
      buf[NSF_INFO_SIZE - 1] = '\0';
      return Lmbcs(buf);
    }
  };

  Database()
    : _Database(
        NULLHANDLE,
        &NSFDbClose,
        &isNullHandle<DBHANDLE>
        )
  {}

  Database(DBHANDLE &&handle)
    : _Database(
        std::move(handle),
        &NSFDbClose,
        &isNullHandle<DBHANDLE>
        )
  {}

  virtual ~Database() noexcept {}

  bool isDirectory() const noexcept(false) { return isDirectory(handle()); }

  bool isDatabase() const noexcept(false) { return isDatabase(handle()); }

  QPair<Lmbcs, Lmbcs> getPaths() noexcept(false)
  {
    return getPaths(handle());
  }

  DBREPLICAINFO getReplicaInfo() noexcept(false)
  {
    return getReplicaInfo(handle());
  }

  Info getInfo() const noexcept(false)
  {
    return getInfo(handle());
  }

  NOTEID findDesignNoteId(const Lmbcs &name, WORD noteClass) noexcept(false)
  {
    return findDesignNoteId(handle(), name, noteClass);
  }

  CollectionPtr getCollection(
        NOTEID viewNoteId,
        WORD openFlags = 0
      ) noexcept(false)
  {
    return getCollection(handle(), viewNoteId, openFlags);
  }

  CollectionPtr getCollection(
        const Lmbcs &name,
        WORD openFlags = 0
      ) noexcept(false)
  {
    return getCollection(handle(), name, openFlags);
  }

  NotePtr createNote(WORD noteClass) const noexcept(false)
  {
    return createNote(handle(), noteClass);
  }

  NotePtr createDocument() const noexcept(false)
  {
    return createNote(NOTE_CLASS_DOCUMENT);
  }

  static DatabasePtr open(const Path &filePath) noexcept(false)
  {
    DBHANDLE handle = NULLHANDLE;
    Status status = NSFDbOpen(filePath.encode().constData(), &handle);
    if (status.hasError()) throw status;
    return make_handle_ptr<Database>(handle);
  }

  static bool isDirectory(DBHANDLE hDB) noexcept(false)
  {
    USHORT retMode;
    Status status = NSFDbModeGet(hDB, &retMode);
    if (status.hasError()) throw status;
    return retMode == DB_DIRECTORY;
  }

  static bool isDatabase(DBHANDLE hDB) noexcept(false)
  {
    return !isDirectory(hDB);
  }

  static QPair<Lmbcs, Lmbcs> getPaths(DBHANDLE hDB) noexcept(false)
  {
    QByteArray canonical(MAXPATH, '\0');
    QByteArray expanded(MAXPATH, '\0');
    Status status = NSFDbPathGet(hDB, canonical.data(), expanded.data());
    if (status.hasError()) throw status;
    return QPair<Lmbcs, Lmbcs>(Lmbcs(canonical), Lmbcs(expanded));
  }

  static DBREPLICAINFO getReplicaInfo(DBHANDLE hDB) noexcept(false)
  {
    DBREPLICAINFO info;
    Status status = NSFDbReplicaInfoGet(hDB, &info);
    if (status.hasError()) throw status;
    return info;
  }

  static Info getInfo(DBHANDLE hDB) noexcept(false)
  {
    char buffer[NSF_INFO_SIZE];
    Status status = NSFDbInfoGet(hDB, buffer);
    if (status.hasError()) throw status;
    buffer[NSF_INFO_SIZE - 1] = '\0';
    return Info(QByteArray(buffer));
  }

  static NOTEID findDesignNoteId(
        DBHANDLE hDB,
        const Lmbcs &name,
        WORD noteClass
        ) noexcept(false)
  {
    NOTEID noteId = 0;
    Status status = NIFFindDesignNote(hDB, name.constData(), noteClass, &noteId);
    if (status.hasError()) throw status;
    return noteId;
  }

  static CollectionPtr getCollection(
        DBHANDLE hDB,
        NOTEID viewNoteId,
        WORD openFlags = 0
      ) noexcept(false)
  {
    HCOLLECTION hView = NULLHANDLE;
    Status status = NIFOpenCollection(
          hDB,
          hDB,
          viewNoteId,
          openFlags,
          NULLHANDLE, // 未読文書を操作するときに使用
          &hView,
          nullptr,
          nullptr, // ビュー文書のUNIDを取得したいときに使用
          nullptr, // 展開/省略された文書を含む文書IDテーブルのハンドル
          nullptr // 選択された文書を含む文書IDテーブルのハンドル
          );
    if (status.hasError()) throw status;
    return make_handle_ptr<Collection>(hView);
  }

  static CollectionPtr getCollection(
        DBHANDLE hDB,
        const Lmbcs &name,
        WORD openFlags = 0
      ) noexcept(false)
  {
    NOTEID noteId = findDesignNoteId(hDB, name, NOTE_CLASS_VIEW);
    return getCollection(hDB, noteId, openFlags);
  }

  static NotePtr createNote(DBHANDLE hDB, WORD noteClass) noexcept(false)
  {
    NOTEHANDLE hNote = NULLHANDLE;
    Status result = NSFNoteCreate(hDB, &hNote);
    if (result.hasError()) throw result;
    NotePtr notePtr(new Note(std::move(hNote)));
    notePtr->setNoteClass(noteClass);
    return notePtr;
  }
};

} // namespace notes
} // namespace gouda

Q_DECLARE_METATYPE(gouda::notes::Database::Path)

#endif // GOUDA_NOTES_DATABASE_HPP
