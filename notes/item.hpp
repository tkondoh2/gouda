﻿#ifndef GOUDA_NOTES_ITEM_HPP
#define GOUDA_NOTES_ITEM_HPP

#include <gouda/notes/lmbcs.hpp>
#include <gouda/notes/number.hpp>
#include <gouda/notes/timedate.hpp>
#include <gouda/notes/variant.hpp>
#include <gouda/notes/objecthandle.hpp>
#include <gouda/notes/blockhandle.hpp>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <nsfnote.h> // NSFItemScan
#include <nsferr.h> // ERR_ITEM_NOT_FOUND
#include <ods.h> // ConvertItemToText
#include <stdnames.h> // ITEM_NAME_ATTACHMENT

#ifdef NT
#pragma pack(pop)
#endif

#include <iostream>

#include <rxcpp/rx.hpp>
using namespace rxcpp;
using namespace rxcpp::operators;

namespace gouda {
namespace notes {

class Item
{
  Lmbcs name_;
  Variant value_;
  WORD flag_;

public:
  class Info
  {
    BLOCKID itemId_;
    WORD type_;
    BLOCKID valueId_;
    DWORD valueSize_;

  public:
    Info()
      : itemId_()
      , type_(TYPE_INVALID_OR_UNKNOWN)
      , valueId_()
      , valueSize_(0)
    {}

    Info(const BLOCKID& itemId,
             WORD type,
             const BLOCKID &valueId,
             DWORD valueSize
             )
      : itemId_(itemId)
      , type_(type)
      , valueId_(valueId)
      , valueSize_(valueSize)
    {}

    BLOCKID itemId() const { return itemId_; }
    WORD type() const { return type_; }
    BLOCKID valueId() const { return valueId_; }
    DWORD valueSize() const { return valueSize_; }

    Lmbcs convertToText(
        const Lmbcs &lineDelimiter,
        WORD charsPerLine,
        bool isStripTabs
        ) const noexcept(false)
    {
      DHANDLE handle = NULLHANDLE;
      DWORD length = 0;
      Status status = ConvertItemToText(
            valueId_,
            valueSize_,
            lineDelimiter.constData(),
            charsPerLine,
            &handle,
            &length,
            isStripTabs
            );
      if (status.hasError()) throw status;
      ObjectHandle oHandle(std::move(handle));
      char* ptr = oHandle.lock<char>();
      return Lmbcs(ptr, static_cast<int>(length));
    }
  };

  Item()
    : name_()
    , value_()
    , flag_(0)
  {}

  Item(const Lmbcs &name, const QByteArray &value, WORD flag)
    : name_(name)
    , value_(value)
    , flag_(flag)
  {}

  Item(const Lmbcs &name, WORD flag)
    : name_(name)
    , value_()
    , flag_(flag)
  {}

  const Lmbcs &name() const { return name_; }

  const Variant &value() const { return value_; }

  Variant variant() const { return value_; }

  WORD flag() const { return flag_; }

  observable<Item::Info> getInfo(NOTEHANDLE hNote) noexcept(false)
  {
    return getInfo(name_, hNote);
  }

  static observable<Item::Info> getInfo(
      const Lmbcs &itemName,
      NOTEHANDLE hNote
      ) noexcept(false)
  {
    return observable<>::create<Item::Info>(
    [hNote, itemName](subscriber<Item::Info> s) {
      WORD nameSize = static_cast<WORD>(itemName.byteSize());
      BLOCKID itemId, valueId;
      BLOCKID prevId;
      WORD dataType = 0;
      DWORD valueSize = 0;
      Status status = NSFItemInfo(
            hNote,
            itemName.constData(),
            nameSize,
            &itemId,
            &dataType,
            &valueId,
            &valueSize
            );
      while (status.error() != ERR_ITEM_NOT_FOUND) {
        if (status.hasError()) {
          s.on_error(std::make_exception_ptr(status));
          break;
        }
        else {
          Item::Info info(itemId, dataType, valueId, valueSize);
          s.on_next(info);
          prevId = itemId;
          status = NSFItemInfoNext(
                hNote,
                prevId,
                itemName.constData(),
                nameSize,
                &itemId,
                &dataType,
                &valueId,
                &valueSize
                );
        }
      }
      s.on_completed();
    });
  }

  static auto getAttachmentFileNames(NOTEHANDLE hNote) noexcept(false)
  {
    return getInfo(ITEM_NAME_ATTACHMENT, hNote)

    .map([](Item::Info info) {
      BlockHandle value(info.valueId());
      char *ptr = value.lock<char>() + sizeof(WORD);

#ifdef NT
      OBJECT_DESCRIPTOR *pObjDesc = reinterpret_cast<OBJECT_DESCRIPTOR*>(ptr);

      if (pObjDesc->ObjectType == OBJECT_FILE) {
        FILEOBJECT *pFileObj = reinterpret_cast<FILEOBJECT*>(pObjDesc);
        Lmbcs fileName(ptr + sizeof(FILEOBJECT), pFileObj->FileNameLength);
#else
      char *pObjDesc = ptr;
      OBJECT_DESCRIPTOR objDesc;
      ODSReadMemory(&pObjDesc, _OBJECT_DESCRIPTOR, &objDesc, 1);

      if (objDesc.ObjectType == OBJECT_FILE) {
        FILEOBJECT fileObj;
        ODSReadMemory(&ptr, _FILEOBJECT, &fileObj, 1);
        Lmbcs fileName(ptr, fileObj.FileNameLength);
#endif
        return fileName;
      }
      return Lmbcs();
    });
  }
};

} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_ITEM_HPP
