﻿#ifndef GOUDA_NOTES_COLLECTION_HPP
#define GOUDA_NOTES_COLLECTION_HPP

#include <gouda/notes/objecthandle.hpp>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <nif.h>
#include <osmem.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace gouda {
namespace notes {

inline bool isNullCollection(HCOLLECTION hCollection)
{
  return hCollection == NULLHCOLLECTION;
}

class Collection;
using CollectionPtr = QSharedPointer<Collection>;
using _Collection = Handle<HCOLLECTION, decltype(&NIFCloseCollection), decltype(&isNullCollection)>;

using _SummaryBuffer = ObjectHandle;
using SummaryBuffer = QSharedPointer<_SummaryBuffer>;

class Collection
    : public _Collection
{
public:
  using size_type = DWORD;

  Collection()
    : _Collection(
      NULLHANDLE,
      &NIFCloseCollection,
      &isNullCollection
    )
  {}

  Collection(HCOLLECTION &&hCollection)
    : _Collection(
      std::move(hCollection),
      &NIFCloseCollection,
      &isNullCollection
    )
  {}

  virtual ~Collection() {}

  size_type size() const noexcept(false)
  {
    COLLECTIONPOSITION index { 0, 0, 0, { 1 } };
    DHANDLE hSummary = NULLHANDLE;
    size_type entryCount = 0;
    Status result = NIFReadEntries(
          handle(),
          &index,
          NAVIGATE_CURRENT,
          0,
          NAVIGATE_NEXT,
          MAXDWORD,
          0,
          &hSummary,
          nullptr,
          nullptr,
          &entryCount,
          nullptr
          );
    if (hSummary) OSMemFree(hSummary);
    if (result.hasError()) throw result;
    return entryCount;
  }

  observable<NOTEID> getNoteIdFromFirst() noexcept
  {
    return observable<>::create<NOTEID>([this](subscriber<NOTEID> s) {
      COLLECTIONPOSITION index { 0, 0, 0, { 0 } };
      WORD signalFlags = SIGNAL_MORE_TO_DO;
      while (signalFlags & SIGNAL_MORE_TO_DO) {
        DHANDLE hSummary = NULLHANDLE;
        DWORD entryCount = 0;
        Status result = NIFReadEntries(
              handle(),
              &index,
              NAVIGATE_NEXT,
              1,
              NAVIGATE_NEXT,
              0xffffffff,
              READ_MASK_NOTEID,
              &hSummary,
              nullptr,
              nullptr,
              &entryCount,
              &signalFlags
              );
        if (result.hasError()) {
          s.on_error(std::make_exception_ptr(result));
          return;
        }

        SummaryBuffer summary = make_handle_ptr<_SummaryBuffer>(hSummary);
        if (!summary->isValid()) continue;

        NOTEID *pNoteId = summary->lock<NOTEID>();
        for (DWORD i = 0; i < entryCount; ++i) {
          s.on_next(pNoteId[i]);
        }
      }
      s.on_completed();
    });
  }
};

} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_COLLECTION_HPP
