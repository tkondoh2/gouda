﻿#ifndef GOUDA_NOTES_GLOBAL_H
#define GOUDA_NOTES_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(NOTES_LIBRARY)
#  define GOUDA_NOTESSHARED_EXPORT Q_DECL_EXPORT
#else
#  define GOUDA_NOTESSHARED_EXPORT Q_DECL_IMPORT
#endif

namespace gouda {
namespace notes {

class GOUDA_NOTESSHARED_EXPORT Version
{
public:
  static uint major();
  static uint minor();
  static uint patch();
  static const char *ver();
};

} // namespace notes
} // namespace gouda

#endif // GOUDA_NOTES_GLOBAL_H
