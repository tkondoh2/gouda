#-------------------------------------------------
#
# Project created by QtCreator 2019-07-11T20:14:47
#
#-------------------------------------------------

QT       -= gui

TARGET = gouda_notes
TEMPLATE = lib

DEFINES += NOTES_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11 c++14

win32: QMAKE_CXXFLAGS += /utf-8

include(../../environment.pri)

#
# RxCpp
#
INCLUDEPATH += $$RxCppPath/Rx/v2/src
DEPENDPATH += $$RxCppPath/Rx/v2/src

#
# Notes C API
#
include($$NotesCAPIPath/qt_notes_c_api.pri)

TargetProduct = GoudaNotesLib
TargetDescription = Gouda Notes Library
TargetVersion = 1.0.0
TargetCompany = Chiburu Systems
TargetDomain = chiburu.com
TargetCopyright = 2019 Chiburu Systems
include(../../version.pri)

INCLUDEPATH -= $$PWD
DEPENDPATH -= $$PWD

INCLUDEPATH += $$PWD/../..
DEPENDPATH += $$PWD/../..

SOURCES += \
  version.cpp

HEADERS += \
        addin/addinlogsink.hpp \
        addin/logmessagetext.hpp \
        blockhandle.hpp \
        cd/any.hpp \
        cd/baseitem.hpp \
        cd/hotspotbegin.hpp \
        cd/item.hpp \
        cd/nestedtablebegin.hpp \
        cd/nestedtablecell.hpp \
        cd/nestedtableend.hpp \
        cd/paragraph.hpp \
        cd/tablebegin.hpp \
        cd/tablecell.hpp \
        cd/tableend.hpp \
        cd/text.hpp \
        collection.hpp \
        compositeitem.hpp \
        compute.hpp \
        database.hpp \
        distinguishedname.hpp \
        env.hpp \
        fileobject.hpp \
        formula.hpp \
        handle.hpp \
        hookdrv/context.hpp \
        hookdrv/notecontext.hpp \
        hookdrv/opennotecontext.hpp \
        hookdrv/updatenotecontext.hpp \
        http/authenticate.hpp \
        http/authorize.hpp \
        http/context.hpp \
        http/initialize.hpp \
        http/parsedrequest.hpp \
        http/procedure.hpp \
        http/request.hpp \
        http/response.hpp \
        http/tools.hpp \
        item.hpp \
        lmbcs.hpp \
        lmbcslist.hpp \
        lookup.hpp \
        mime/conversioncontrol.hpp \
        mime/directory.hpp \
        mime/entity.hpp \
        mime/rfc822text.hpp \
        note.hpp \
        number.hpp \
        numberrange.hpp \
        objecthandle.hpp \
        range.hpp \
        search.hpp \
        notes_global.h \
        session.hpp \
        status.hpp \
        summarybuffer.hpp \
        timedate.hpp \
        timedaterange.hpp \
        tools.hpp \
        translate.hpp \
        variant.hpp

#unix {
#    target.path = /usr/lib
#    INSTALLS += target
#}

DISTFILES += \
  README.md
