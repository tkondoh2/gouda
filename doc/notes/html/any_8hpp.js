var any_8hpp =
[
    [ "Any", "classgouda_1_1notes_1_1cd_1_1_any.html", "classgouda_1_1notes_1_1cd_1_1_any" ],
    [ "SizeType", "any_8hpp.html#ac145b3de31091d3c0ab1c9142575f13f", [
      [ "Byte", "any_8hpp.html#ac145b3de31091d3c0ab1c9142575f13fa43128d803d5cc535cbcb02825f699d13", null ],
      [ "Word", "any_8hpp.html#ac145b3de31091d3c0ab1c9142575f13fab99d07502bb6688ea318dd47d6c984a3", null ],
      [ "Long", "any_8hpp.html#ac145b3de31091d3c0ab1c9142575f13faeb9d3bde6d102d832f934067d136f7e8", null ]
    ] ],
    [ "getSizeType", "any_8hpp.html#a7cde4d4eabddb1234dfbafd769ad52d1", null ]
];