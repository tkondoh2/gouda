var classgouda_1_1notes_1_1_variant =
[
    [ "Variant", "classgouda_1_1notes_1_1_variant.html#a1f1c54ffe6f465a8db1e9388070e6a40", null ],
    [ "Variant", "classgouda_1_1notes_1_1_variant.html#ad80598350bcefef2d68e90acd43dfdd9", null ],
    [ "canConvert", "classgouda_1_1notes_1_1_variant.html#a080ea671f4d747c6b1c6956241b32820", null ],
    [ "convert", "classgouda_1_1notes_1_1_variant.html#a06b2b236dda1f54cc1441c7bf31be255", null ],
    [ "firstNumber", "classgouda_1_1notes_1_1_variant.html#a08e8a87df8077b453482c85e26042fb3", null ],
    [ "firstString", "classgouda_1_1notes_1_1_variant.html#a02b90e1ab78b3e561401a78b65879f87", null ],
    [ "firstTimeDate", "classgouda_1_1notes_1_1_variant.html#a94d1568b5c7dbe06e6b6ee3ef9d7ff93", null ],
    [ "isValid", "classgouda_1_1notes_1_1_variant.html#aeb8fea837fdd26efe28254c928192731", null ],
    [ "raw", "classgouda_1_1notes_1_1_variant.html#a4a6114f9743994ec4890b4665de249d8", null ],
    [ "rawSize", "classgouda_1_1notes_1_1_variant.html#aa03389710e953475737f4de67a866b42", null ],
    [ "size", "classgouda_1_1notes_1_1_variant.html#a8c1b77b0f1936d469c042c0107b4b345", null ],
    [ "toNumberRange", "classgouda_1_1notes_1_1_variant.html#a7e84be81835a071a71f33f2a5235cb53", null ],
    [ "toString", "classgouda_1_1notes_1_1_variant.html#a1aab087d7f9f859f724cf391e610c828", null ],
    [ "toStringList", "classgouda_1_1notes_1_1_variant.html#a62874e25a3e60325a83a41180f939437", null ],
    [ "toTimeDateRange", "classgouda_1_1notes_1_1_variant.html#a5d282d675a8403d9ab155c7e21a92874", null ],
    [ "type", "classgouda_1_1notes_1_1_variant.html#a9903dd7004d645a43548aaec8a2590ec", null ],
    [ "typeName", "classgouda_1_1notes_1_1_variant.html#a6e7ad5f182d07f0b954654ff0800ce91", null ],
    [ "value", "classgouda_1_1notes_1_1_variant.html#a6f2114249b2641f058fd25a67902b075", null ]
];