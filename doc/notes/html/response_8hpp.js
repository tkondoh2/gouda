var response_8hpp =
[
    [ "Response", "classgouda_1_1notes_1_1http_1_1_response.html", "classgouda_1_1notes_1_1http_1_1_response" ],
    [ "ResponseCode", "response_8hpp.html#a3c1e6c270900ec668cba2e38670ec868", [
      [ "OK", "response_8hpp.html#a3c1e6c270900ec668cba2e38670ec868ae0aa021e21dddbd6d8cecec71e9cf564", null ],
      [ "Found", "response_8hpp.html#a3c1e6c270900ec668cba2e38670ec868a5d695cc28c6a7ea955162fbdd0ae42b9", null ],
      [ "NotModified", "response_8hpp.html#a3c1e6c270900ec668cba2e38670ec868ac169e6d9a1b9442001384de8dcf49ab9", null ],
      [ "BadRequest", "response_8hpp.html#a3c1e6c270900ec668cba2e38670ec868a9edf8fbf00a57d95a0af4923c9a1ec6f", null ],
      [ "Unauthorized", "response_8hpp.html#a3c1e6c270900ec668cba2e38670ec868ae06d1ba70f1331e9f9a113cc2f887d3f", null ],
      [ "Forbidden", "response_8hpp.html#a3c1e6c270900ec668cba2e38670ec868a722969577a96ca3953e84e3d949dee81", null ],
      [ "NotFound", "response_8hpp.html#a3c1e6c270900ec668cba2e38670ec868a38c300f4fc9ce8a77aad4a30de05cad8", null ],
      [ "InternalServerError", "response_8hpp.html#a3c1e6c270900ec668cba2e38670ec868aecbf01325f1c744e9d7bb586ac2eb5ed", null ]
    ] ]
];