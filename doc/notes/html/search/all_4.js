var searchData=
[
  ['data',['Data',['../structgouda_1_1notes_1_1mime_1_1_entity_1_1_data.html',1,'gouda::notes::mime::Entity']]],
  ['database',['Database',['../classgouda_1_1notes_1_1_database.html',1,'gouda::notes::Database'],['../classgouda_1_1notes_1_1_database.html#a29fd15113525b5a7a8fa12074746dd32',1,'gouda::notes::Database::Database()'],['../classgouda_1_1notes_1_1_database.html#a6fb5488b6faa31b24d8c70ee0e22b090',1,'gouda::notes::Database::Database(DBHANDLE &amp;&amp;handle)']]],
  ['database_2ehpp',['database.hpp',['../database_8hpp.html',1,'']]],
  ['databaseptr',['DatabasePtr',['../namespacegouda_1_1notes.html#a87af2e7b086e28d8ce1ee7f4043a333c',1,'gouda::notes']]],
  ['decode',['decode',['../classgouda_1_1notes_1_1_database_1_1_path.html#a3cea1641785caa7635749137ed80b9f4',1,'gouda::notes::Database::Path']]],
  ['delete',['Delete',['../classgouda_1_1notes_1_1hookdrv_1_1_note_context.html#a0c9739d772dc7e2c062043189a09cd0da11b83eff9c298e060309953e901ddf92',1,'gouda::notes::hookdrv::NoteContext']]],
  ['deletestub',['DeleteStub',['../classgouda_1_1notes_1_1hookdrv_1_1_note_context.html#a0c9739d772dc7e2c062043189a09cd0da91ba356cf0db62f0b12166edefd7ed49',1,'gouda::notes::hookdrv::NoteContext']]],
  ['designclass',['designClass',['../classgouda_1_1notes_1_1_database_1_1_info.html#a89cdb8a2f05741886a9efe10bd645aaf',1,'gouda::notes::Database::Info']]],
  ['directory',['Directory',['../classgouda_1_1notes_1_1mime_1_1_directory.html',1,'gouda::notes::mime::Directory'],['../classgouda_1_1notes_1_1mime_1_1_directory.html#a8ba9707812c1daaf1cea5b433d918d08',1,'gouda::notes::mime::Directory::Directory() noexcept'],['../classgouda_1_1notes_1_1mime_1_1_directory.html#ac1c551d377b16905206ab71e786878b7',1,'gouda::notes::mime::Directory::Directory(HMIMEDIRECTORY handle) noexcept']]],
  ['directory_2ehpp',['directory.hpp',['../directory_8hpp.html',1,'']]],
  ['directoryptr',['DirectoryPtr',['../namespacegouda_1_1notes_1_1mime.html#aabeb8d899e3766fe42a0a9edc5cd9a26',1,'gouda::notes::mime']]],
  ['distinguishedname',['DistinguishedName',['../classgouda_1_1notes_1_1_distinguished_name.html',1,'gouda::notes::DistinguishedName'],['../classgouda_1_1notes_1_1_distinguished_name.html#a46d48fc0b333d9a5ac241f479602158d',1,'gouda::notes::DistinguishedName::DistinguishedName()'],['../classgouda_1_1notes_1_1_distinguished_name.html#a1a3b5528593bb9d2374ce73cacada258',1,'gouda::notes::DistinguishedName::DistinguishedName(const Lmbcs &amp;name)']]],
  ['distinguishedname_2ehpp',['distinguishedname.hpp',['../distinguishedname_8hpp.html',1,'']]],
  ['dname',['DName',['../namespacegouda_1_1notes.html#a838a047ffc1090afb92d77060293b403',1,'gouda::notes']]],
  ['donothing',['doNothing',['../namespacegouda_1_1notes.html#a4f9776acdc552bbbaff44d550df975f1',1,'gouda::notes']]],
  ['downloadattachment',['downloadAttachment',['../classgouda_1_1notes_1_1_note.html#ac4092dacf99c715e1586e862617b7813',1,'gouda::notes::Note::downloadAttachment(const Item::Info &amp;itemInfo, const Lmbcs &amp;filePath, ENCRYPTION_KEY *pDecryptionKey=nullptr) const noexcept(false)'],['../classgouda_1_1notes_1_1_note.html#a358173922d16e258cc3c3ef6d6842f53',1,'gouda::notes::Note::downloadAttachment(NOTEHANDLE hNote, const Item::Info &amp;itemInfo, const Lmbcs &amp;filePath, ENCRYPTION_KEY *pDecryptionKey=nullptr) noexcept(false)']]]
];
