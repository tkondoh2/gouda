var searchData=
[
  ['handle',['Handle',['../classgouda_1_1notes_1_1_handle.html',1,'gouda::notes::Handle&lt; T, Deleter, IsNull &gt;'],['../classgouda_1_1notes_1_1_handle.html#a2a3d073495dca82788744420da1095bf',1,'gouda::notes::Handle::Handle()'],['../structgouda_1_1notes_1_1mime_1_1_entity_1_1_data.html#ab6360dd18debbbb7cad18ca706d4fd90',1,'gouda::notes::mime::Entity::Data::handle()'],['../classgouda_1_1notes_1_1_handle.html#ad93568f162fea6af62d848cbd75c76a6',1,'gouda::notes::Handle::handle()']]],
  ['handle_2ehpp',['handle.hpp',['../handle_8hpp.html',1,'']]],
  ['handle_3c_20dhandle_2c_20decltype_20_26osmemfree_2c_20decltype_28_26isnullhandle_3c_20dhandle_20_3e_29_20_3e',['Handle&lt; DHANDLE, decltype &amp;OSMemFree, decltype(&amp;isNullHandle&lt; DHANDLE &gt;) &gt;',['../classgouda_1_1notes_1_1_handle.html',1,'gouda::notes']]],
  ['handle_3c_20t_2c_20deleter_2c_20decltype_28_26isnullhandle_3c_20t_20_3e_29_20_3e',['Handle&lt; T, Deleter, decltype(&amp;isNullHandle&lt; T &gt;) &gt;',['../classgouda_1_1notes_1_1_handle.html',1,'gouda::notes']]],
  ['hascomposite',['hasComposite',['../classgouda_1_1notes_1_1_note.html#aa565672857fbdb5f4967f0a8e57454bf',1,'gouda::notes::Note::hasComposite() noexcept'],['../classgouda_1_1notes_1_1_note.html#a4a546aed0557bef11029cdeaea322abc',1,'gouda::notes::Note::hasComposite(NOTEHANDLE handle) noexcept']]],
  ['haserror',['hasError',['../classgouda_1_1notes_1_1_status.html#aac1c7bccb7daff3f598df5599069ab34',1,'gouda::notes::Status']]],
  ['hasmime',['hasMIME',['../classgouda_1_1notes_1_1_note.html#adb67e3c38d75c949614527ccf7682eb4',1,'gouda::notes::Note::hasMIME() noexcept'],['../classgouda_1_1notes_1_1_note.html#a66cb6399b97198c98d8f3ec884cad2c6',1,'gouda::notes::Note::hasMIME(NOTEHANDLE handle) noexcept']]],
  ['hasmimepart',['hasMIMEPart',['../classgouda_1_1notes_1_1_note.html#a16b0fd22b4cc82a1659a931919cbb266',1,'gouda::notes::Note::hasMIMEPart() noexcept'],['../classgouda_1_1notes_1_1_note.html#a12c24916ced7f1f4ad60fcdb1b4574ee',1,'gouda::notes::Note::hasMIMEPart(NOTEHANDLE handle) noexcept']]],
  ['hasrfc822text',['hasRFC822Text',['../classgouda_1_1notes_1_1_note.html#a36fbadc77c630df0989a420e0da5606d',1,'gouda::notes::Note::hasRFC822Text() noexcept'],['../classgouda_1_1notes_1_1_note.html#aeac368b569fa8338af070547e5058c2f',1,'gouda::notes::Note::hasRFC822Text(NOTEHANDLE handle) noexcept']]],
  ['hdb',['hDB',['../classgouda_1_1notes_1_1hookdrv_1_1_context.html#a2c9b32cc4f882615a347e3ebce1a4ac7',1,'gouda::notes::hookdrv::Context']]],
  ['hdb_5f',['hDB_',['../classgouda_1_1notes_1_1hookdrv_1_1_context.html#acc7ff356ce2810ed0674a00136032604',1,'gouda::notes::hookdrv::Context']]],
  ['headermap',['HeaderMap',['../namespacegouda_1_1notes_1_1http.html#a73f1c9eaf2ce0776bd279936f143f34e',1,'gouda::notes::http']]],
  ['hnote',['hNote',['../classgouda_1_1notes_1_1hookdrv_1_1_note_context.html#a9b8b733373bbaf3b3ab77208af2cbe4a',1,'gouda::notes::hookdrv::NoteContext']]],
  ['hnote_5f',['hNote_',['../classgouda_1_1notes_1_1hookdrv_1_1_note_context.html#a9e7b7811ec60a184c0a76cf29332773f',1,'gouda::notes::hookdrv::NoteContext']]],
  ['hotspotbegin',['HotspotBegin',['../classgouda_1_1notes_1_1cd_1_1_hotspot_begin.html',1,'gouda::notes::cd::HotspotBegin'],['../classgouda_1_1notes_1_1cd_1_1_hotspot_begin.html#a9dd60f692ce7c750246c5ac1ede3e07e',1,'gouda::notes::cd::HotspotBegin::HotspotBegin()'],['../classgouda_1_1notes_1_1cd_1_1_hotspot_begin.html#a2e89e6eaae818dc572698ca52e289c1c',1,'gouda::notes::cd::HotspotBegin::HotspotBegin(char **ppRec)'],['../classgouda_1_1notes_1_1cd_1_1_hotspot_begin.html#a9293336e9512ffd6ed02758e705c7807',1,'gouda::notes::cd::HotspotBegin::HotspotBegin(const QByteArray &amp;bytes)']]],
  ['hotspotbegin_2ehpp',['hotspotbegin.hpp',['../hotspotbegin_8hpp.html',1,'']]]
];
