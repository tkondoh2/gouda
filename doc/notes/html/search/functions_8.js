var searchData=
[
  ['info',['Info',['../classgouda_1_1notes_1_1_database_1_1_info.html#a6b34ff4d95784db04afae2d286cf3710',1,'gouda::notes::Database::Info::Info()'],['../classgouda_1_1notes_1_1_database_1_1_info.html#aa61f1e5d777f233bb0d577edb1461bad',1,'gouda::notes::Database::Info::Info(const QByteArray &amp;buffer)'],['../classgouda_1_1notes_1_1_item_1_1_info.html#a8ac81641bb8f3c7f0170d12ef4840815',1,'gouda::notes::Item::Info::Info()'],['../classgouda_1_1notes_1_1_item_1_1_info.html#acc7188ee760a125c8374c92dcbb07651',1,'gouda::notes::Item::Info::Info(const BLOCKID &amp;itemId, WORD type, const BLOCKID &amp;valueId, DWORD valueSize)']]],
  ['init',['init',['../namespacegouda_1_1notes_1_1http.html#a471596578bd49789b1e2542f0cb33610',1,'gouda::notes::http']]],
  ['isdatabase',['isDatabase',['../classgouda_1_1notes_1_1_database.html#a987875dfbd1b0b6b691c96967efd7cb2',1,'gouda::notes::Database::isDatabase() const noexcept(false)'],['../classgouda_1_1notes_1_1_database.html#ab83e56e9762baea6f97bccaeb78378fc',1,'gouda::notes::Database::isDatabase(DBHANDLE hDB) noexcept(false)']]],
  ['isdirectory',['isDirectory',['../classgouda_1_1notes_1_1_database.html#a6f30d05038ffcb9427f30ea35e35ee78',1,'gouda::notes::Database::isDirectory() const noexcept(false)'],['../classgouda_1_1notes_1_1_database.html#a97a85a883d039bc914fd3124593db4ce',1,'gouda::notes::Database::isDirectory(DBHANDLE hDB) noexcept(false)']]],
  ['isdiscretepart',['isDiscretePart',['../classgouda_1_1notes_1_1mime_1_1_entity.html#a4f806427f493ff8fa3964fb52d612ab0',1,'gouda::notes::mime::Entity']]],
  ['isempty',['isEmpty',['../classgouda_1_1notes_1_1_lmbcs.html#a651e4727af9b7a577e00992bac8379f4',1,'gouda::notes::Lmbcs']]],
  ['ismessagepart',['isMessagePart',['../classgouda_1_1notes_1_1mime_1_1_entity.html#af425bb9c782fbca6f96e9e6fca81e932',1,'gouda::notes::mime::Entity']]],
  ['ismultipart',['isMultiPart',['../classgouda_1_1notes_1_1mime_1_1_entity.html#abf44c68323c8ed44193b90ee3f1cce52',1,'gouda::notes::mime::Entity']]],
  ['isnull',['isNull',['../classgouda_1_1notes_1_1_handle.html#ae18fef3b24da73b35a3d8529260b5f02',1,'gouda::notes::Handle']]],
  ['isnullblockid',['isNullBlockId',['../namespacegouda_1_1notes.html#a916562bbf579a1ea590502c328a7db28',1,'gouda::notes']]],
  ['isnullcollection',['isNullCollection',['../namespacegouda_1_1notes.html#a329cace79a755c4e3cfb7f45d0240cce',1,'gouda::notes']]],
  ['isnullhandle',['isNullHandle',['../namespacegouda_1_1notes.html#accd5195e6df8c0083c12f925a8653e61',1,'gouda::notes']]],
  ['isnullptr',['isNullPtr',['../namespacegouda_1_1notes.html#ab9940b626d5e717c87def9df0a607fe7',1,'gouda::notes']]],
  ['isvalid',['isValid',['../classgouda_1_1notes_1_1_handle.html#aee061588f34209b554809eac8d8559c2',1,'gouda::notes::Handle::isValid()'],['../classgouda_1_1notes_1_1_variant.html#aeb8fea837fdd26efe28254c928192731',1,'gouda::notes::Variant::isValid()']]],
  ['item',['Item',['../classgouda_1_1notes_1_1_item.html#a6e4acb6935584461438af1e70499a660',1,'gouda::notes::Item::Item()'],['../classgouda_1_1notes_1_1_item.html#ab2bc276eaa77aaf1b77b7a0063eecfdb',1,'gouda::notes::Item::Item(const Lmbcs &amp;name, const QByteArray &amp;value, WORD flag)'],['../classgouda_1_1notes_1_1_item.html#afeb7fd3bbfd52940efd5441018cbd3d7',1,'gouda::notes::Item::Item(const Lmbcs &amp;name, WORD flag)']]],
  ['itemid',['itemId',['../classgouda_1_1notes_1_1_item_1_1_info.html#a011ca70333a4081fae00f8c54766f4be',1,'gouda::notes::Item::Info']]],
  ['itemscan',['itemScan',['../classgouda_1_1notes_1_1_note.html#a57df7d6b56746d8e458e62d88e07db06',1,'gouda::notes::Note::itemScan() noexcept(false)'],['../classgouda_1_1notes_1_1_note.html#a2724358e737dd3dff21e097d66af66ba',1,'gouda::notes::Note::itemScan(NOTEHANDLE handle) noexcept(false)']]],
  ['itemscancallback',['itemScanCallback',['../classgouda_1_1notes_1_1_note.html#acdf429b8a7bf3242483cfe5837d03ad8',1,'gouda::notes::Note']]],
  ['iterate',['iterate',['../classgouda_1_1notes_1_1_composite_item.html#a4098f0c2486140dd0e9e388e48995a1d',1,'gouda::notes::CompositeItem']]]
];
