var indexSectionsWithContent =
{
  0: "_abcdefghijlmnopqrstuvw~",
  1: "abcdefhilnopqrstuv",
  2: "g",
  3: "abcdefhilnoprstuv",
  4: "abcdefghijlmnopqrstuvw~",
  5: "bcfghlnoprsuv",
  6: "_cdefhinors",
  7: "nrs",
  8: "bcdfilnopuw",
  9: "o",
  10: "egms",
  11: "n"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "related",
  10: "defines",
  11: "pages"
};

var indexSectionLabels =
{
  0: "全て",
  1: "クラス",
  2: "名前空間",
  3: "ファイル",
  4: "関数",
  5: "変数",
  6: "型定義",
  7: "列挙型",
  8: "列挙値",
  9: "フレンド",
  10: "マクロ定義",
  11: "ページ"
};

