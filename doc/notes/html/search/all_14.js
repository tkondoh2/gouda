var searchData=
[
  ['unauthorized',['Unauthorized',['../namespacegouda_1_1notes_1_1http.html#a3c1e6c270900ec668cba2e38670ec868ae06d1ba70f1331e9f9a113cc2f887d3f',1,'gouda::notes::http']]],
  ['unicodetolmbcs',['UnicodeToLmbcs',['../structgouda_1_1notes_1_1_unicode_to_lmbcs.html',1,'gouda::notes']]],
  ['unidtotext',['unidToText',['../namespacegouda_1_1notes.html#a49abc8c0d8954515ff619513550db47c',1,'gouda::notes']]],
  ['unlock',['unlock',['../classgouda_1_1notes_1_1_block_handle.html#a33e347dc97d6520abbadfa7ff9ec82c3',1,'gouda::notes::BlockHandle::unlock()'],['../classgouda_1_1notes_1_1_lockable_handle.html#affe718664a3abe99f6011f9212418ef8',1,'gouda::notes::LockableHandle::unlock()']]],
  ['update',['update',['../classgouda_1_1notes_1_1_note.html#a424dd7e43ffd6e9c595e976c3078265f',1,'gouda::notes::Note::update(WORD updateFlags=0) noexcept(false)'],['../classgouda_1_1notes_1_1_note.html#a4465a2c133be5217904409e39769906e',1,'gouda::notes::Note::update(NOTEHANDLE hNote, WORD updateFlags=0) noexcept(false)'],['../classgouda_1_1notes_1_1hookdrv_1_1_note_context.html#a0c9739d772dc7e2c062043189a09cd0dad7413185eb6ca2895979abf287772406',1,'gouda::notes::hookdrv::NoteContext::Update()']]],
  ['updatenotecontext',['UpdateNoteContext',['../classgouda_1_1notes_1_1hookdrv_1_1_update_note_context.html',1,'gouda::notes::hookdrv::UpdateNoteContext'],['../classgouda_1_1notes_1_1hookdrv_1_1_update_note_context.html#ab807220210b7c1f974a7fd87754d9846',1,'gouda::notes::hookdrv::UpdateNoteContext::UpdateNoteContext()']]],
  ['updatenotecontext_2ehpp',['updatenotecontext.hpp',['../updatenotecontext_8hpp.html',1,'']]],
  ['url',['url',['../classgouda_1_1notes_1_1http_1_1_request.html#ac6d598b3b3b9925190a910ea14e05c67',1,'gouda::notes::http::Request']]],
  ['urlpath',['urlPath',['../classgouda_1_1notes_1_1http_1_1_request.html#a8abd723f68422c93e85746b7a78d42af',1,'gouda::notes::http::Request']]],
  ['urlquery',['urlQuery',['../classgouda_1_1notes_1_1http_1_1_request.html#ae90f3875212627ad53fc1284009a648b',1,'gouda::notes::http::Request']]],
  ['username',['UserName',['../classgouda_1_1notes_1_1hookdrv_1_1_context.html#a67608c003364d0946d7b8ddb3b675a91',1,'gouda::notes::hookdrv::Context']]],
  ['username_5f',['UserName_',['../classgouda_1_1notes_1_1hookdrv_1_1_context.html#a673b9ec7fc6e904be6f1bddeb1909814',1,'gouda::notes::hookdrv::Context']]]
];
