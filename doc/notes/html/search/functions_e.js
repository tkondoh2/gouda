var searchData=
[
  ['pair',['Pair',['../classgouda_1_1notes_1_1_pair.html#a6e0e81f6dde3654bafbe8bd6dea54297',1,'gouda::notes::Pair::Pair()'],['../classgouda_1_1notes_1_1_pair.html#aeda635b9ac0270c40175abec7ee61e6e',1,'gouda::notes::Pair::Pair(const PAIR &amp;value)']]],
  ['pairs',['pairs',['../classgouda_1_1notes_1_1_range.html#adb054924c07311ff8d2c39766b94a6d8',1,'gouda::notes::Range']]],
  ['pairstostringlist',['pairsToStringList',['../classgouda_1_1notes_1_1_range.html#abed14b1caf2fdf67984d6904f1befd8d',1,'gouda::notes::Range']]],
  ['paragraph',['Paragraph',['../classgouda_1_1notes_1_1cd_1_1_paragraph.html#a78b5e6c4c722ac21798a62b65bb5cbe8',1,'gouda::notes::cd::Paragraph::Paragraph()'],['../classgouda_1_1notes_1_1cd_1_1_paragraph.html#a46f0a1c1c238683b2b5e1557d3798e00',1,'gouda::notes::cd::Paragraph::Paragraph(char **ppRec)']]],
  ['parse',['parse',['../classgouda_1_1notes_1_1_database_1_1_info.html#abb06594105bd128a85f1291036c4fe91',1,'gouda::notes::Database::Info']]],
  ['parseallheaders',['parseAllHeaders',['../classgouda_1_1notes_1_1http_1_1_context.html#a921c98ec88f11ab0c512cfa060bf7b89',1,'gouda::notes::http::Context']]],
  ['parsedrequest',['ParsedRequest',['../classgouda_1_1notes_1_1http_1_1_parsed_request.html#a952c64fe85997e99f8ea16cfb140736a',1,'gouda::notes::http::ParsedRequest']]],
  ['patch',['patch',['../classgouda_1_1notes_1_1_version.html#acbd6a1edb1ea512d77cdb91e7392a884',1,'gouda::notes::Version']]],
  ['path',['Path',['../classgouda_1_1notes_1_1_database_1_1_path.html#a14373c6ced07d0ffcdfa6c2030676b93',1,'gouda::notes::Database::Path::Path() noexcept'],['../classgouda_1_1notes_1_1_database_1_1_path.html#a157ef559552323d728ed7d7c4b937529',1,'gouda::notes::Database::Path::Path(const Lmbcs &amp;path, const Lmbcs &amp;server, const Lmbcs &amp;port=Lmbcs()) noexcept'],['../classgouda_1_1notes_1_1_database_1_1_path.html#a15d7d4444bfc94ac888247b1e3a5fd0c',1,'gouda::notes::Database::Path::path() const noexcept']]],
  ['port',['port',['../classgouda_1_1notes_1_1_database_1_1_path.html#a8236a20bb5f94f5de676630b1311f3c6',1,'gouda::notes::Database::Path']]]
];
