var searchData=
[
  ['len',['len',['../structgouda_1_1notes_1_1mime_1_1_entity_1_1_data.html#ad2331bfdbbad8981efdd0d9c5638378e',1,'gouda::notes::mime::Entity::Data']]],
  ['length',['length',['../classgouda_1_1notes_1_1_compile_error.html#a41a65b1113a57df92b533f60b8833cc5',1,'gouda::notes::CompileError']]],
  ['line',['line',['../classgouda_1_1notes_1_1_compile_error.html#a9ad72efd06d36238db6393b992b60c59',1,'gouda::notes::CompileError']]],
  ['list',['list',['../classgouda_1_1notes_1_1_composite_item.html#aa6f4e216f6ea0f9ac39f144f0e78c92c',1,'gouda::notes::CompositeItem']]],
  ['lmbcs',['Lmbcs',['../classgouda_1_1notes_1_1_lmbcs.html',1,'gouda::notes::Lmbcs'],['../classgouda_1_1notes_1_1_lmbcs.html#a3c738432a4730530bf610824bab9414b',1,'gouda::notes::Lmbcs::Lmbcs() noexcept'],['../classgouda_1_1notes_1_1_lmbcs.html#aec396044d16cf20efb8352fc2952f5d0',1,'gouda::notes::Lmbcs::Lmbcs(const char *c_str, int size=-1) noexcept'],['../classgouda_1_1notes_1_1_lmbcs.html#a6af7f8cd76063534353c77d647d4d8e7',1,'gouda::notes::Lmbcs::Lmbcs(QByteArray &amp;&amp;lmbcsBytes) noexcept']]],
  ['lmbcs_2ehpp',['lmbcs.hpp',['../lmbcs_8hpp.html',1,'']]],
  ['lmbcslist',['LmbcsList',['../classgouda_1_1notes_1_1_lmbcs_list.html',1,'gouda::notes::LmbcsList'],['../classgouda_1_1notes_1_1_lmbcs_list.html#a67a135a75366395ec47f6e8bbb1a097c',1,'gouda::notes::LmbcsList::LmbcsList()'],['../classgouda_1_1notes_1_1_lmbcs_list.html#afdcbeb0fbfb1adfa45f1f146e094d080',1,'gouda::notes::LmbcsList::LmbcsList(const QByteArray &amp;bytes) noexcept(false)'],['../classgouda_1_1notes_1_1_lmbcs_list.html#a919f17b2b42afdb1608d48c440e9456b',1,'gouda::notes::LmbcsList::LmbcsList(const QByteArrayList &amp;list) noexcept']]],
  ['lmbcslist_2ehpp',['lmbcslist.hpp',['../lmbcslist_8hpp.html',1,'']]],
  ['lmbcslisttoqstringlist',['LmbcsListToQStringList',['../structgouda_1_1notes_1_1_lmbcs_list_to_q_string_list.html',1,'gouda::notes']]],
  ['lmbcstoqstring',['LmbcsToQString',['../structgouda_1_1notes_1_1_lmbcs_to_q_string.html',1,'gouda::notes']]],
  ['lmbcstounicode',['LmbcsToUnicode',['../structgouda_1_1notes_1_1_lmbcs_to_unicode.html',1,'gouda::notes']]],
  ['lock',['lock',['../classgouda_1_1notes_1_1_block_handle.html#a5963a085ff5668e97f45ad18325b88b0',1,'gouda::notes::BlockHandle::lock()'],['../classgouda_1_1notes_1_1_lockable_handle.html#ae2124f5700562005aceda26e050fdd97',1,'gouda::notes::LockableHandle::lock()']]],
  ['lockablehandle',['LockableHandle',['../classgouda_1_1notes_1_1_lockable_handle.html',1,'gouda::notes::LockableHandle&lt; T, Deleter &gt;'],['../classgouda_1_1notes_1_1_lockable_handle.html#a1215440c65ea70e9802c0987635ccf43',1,'gouda::notes::LockableHandle::LockableHandle()']]],
  ['lockablehandle_3c_20dhandle_2c_20decltype_28_26osmemfree_29_3e',['LockableHandle&lt; DHANDLE, decltype(&amp;OSMemFree)&gt;',['../classgouda_1_1notes_1_1_lockable_handle.html',1,'gouda::notes']]],
  ['logmessagetext',['logMessageText',['../namespacegouda_1_1notes_1_1addin.html#a331e03007c3a6b044445c9231a4e350c',1,'gouda::notes::addin::logMessageText(STATUS status, const Lmbcs &amp;msg, Args... args) noexcept'],['../namespacegouda_1_1notes_1_1addin.html#a4be95394524bf27368d2e5e4a12aa156',1,'gouda::notes::addin::logMessageText(const Lmbcs &amp;msg, Args... args) noexcept']]],
  ['logmessagetext_2ehpp',['logmessagetext.hpp',['../logmessagetext_8hpp.html',1,'']]],
  ['long',['Long',['../namespacegouda_1_1notes_1_1cd.html#ac145b3de31091d3c0ab1c9142575f13faeb9d3bde6d102d832f934067d136f7e8',1,'gouda::notes::cd']]],
  ['lq',['lq',['../namespacegouda_1_1notes.html#a8409e8a3a5303ff10b8b7c6f84ed3aac',1,'gouda::notes::lq(const Lmbcs &amp;lmbcs) noexcept'],['../namespacegouda_1_1notes.html#a088d39fbe499d92db3dd8d79ec5c5e05',1,'gouda::notes::lq(const LmbcsList &amp;llist) noexcept']]]
];
