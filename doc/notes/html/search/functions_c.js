var searchData=
[
  ['name',['name',['../classgouda_1_1notes_1_1cd_1_1_any.html#a60e9c44ba28162ced260a26773998800',1,'gouda::notes::cd::Any::name()'],['../classgouda_1_1notes_1_1_item.html#a8d79dc7b6f53eb025bc30da635069430',1,'gouda::notes::Item::name()']]],
  ['nestedtablebegin',['NestedTableBegin',['../classgouda_1_1notes_1_1cd_1_1_nested_table_begin.html#aacc01ae1dde28569776371080dd5113c',1,'gouda::notes::cd::NestedTableBegin']]],
  ['nestedtablecell',['NestedTableCell',['../classgouda_1_1notes_1_1cd_1_1_nested_table_cell.html#a0782034a2c23800f698b844889204a7f',1,'gouda::notes::cd::NestedTableCell']]],
  ['nestedtableend',['NestedTableEnd',['../classgouda_1_1notes_1_1cd_1_1_nested_table_end.html#a2df0c8ac0cd4235d7557609eb30a3d0b',1,'gouda::notes::cd::NestedTableEnd']]],
  ['noerror',['noError',['../classgouda_1_1notes_1_1_status.html#ad882c94d57a10b1417aa94d11343f6ac',1,'gouda::notes::Status']]],
  ['note',['Note',['../classgouda_1_1notes_1_1_note.html#aad0d683ece41ec373601cf30b0b03d11',1,'gouda::notes::Note::Note() noexcept'],['../classgouda_1_1notes_1_1_note.html#a22c4b8fcc9cccbaf5fb035975c84d565',1,'gouda::notes::Note::Note(NOTEHANDLE &amp;&amp;handle) noexcept']]],
  ['notecontext',['NoteContext',['../classgouda_1_1notes_1_1hookdrv_1_1_note_context.html#aa4b3f2a317496c41ab639aa2d7816c49',1,'gouda::notes::hookdrv::NoteContext']]],
  ['noteid',['NoteID',['../classgouda_1_1notes_1_1hookdrv_1_1_note_context.html#ac582118bfd0eeb8c1ff5a00ce4ce5bb0',1,'gouda::notes::hookdrv::NoteContext']]],
  ['number',['Number',['../classgouda_1_1notes_1_1_number.html#a98b12d846b0045da8e619da0e60f6f63',1,'gouda::notes::Number::Number()'],['../classgouda_1_1notes_1_1_number.html#a96bfef62fef4b35d92da2455fe6bf32d',1,'gouda::notes::Number::Number(NUMBER value)'],['../classgouda_1_1notes_1_1_number.html#a7e325968a487b79c6dc0158fca1f840c',1,'gouda::notes::Number::Number(const QByteArray &amp;bytes)']]],
  ['numberpair',['NumberPair',['../classgouda_1_1notes_1_1_number_pair.html#a3d81ab4b5d7eceac7926621c960d154a',1,'gouda::notes::NumberPair::NumberPair()'],['../classgouda_1_1notes_1_1_number_pair.html#a7ad7ab151d132d19ee4b10924b28a4a5',1,'gouda::notes::NumberPair::NumberPair(NUMBER_PAIR value)']]],
  ['numberrange',['NumberRange',['../classgouda_1_1notes_1_1_number_range.html#ab852129b9e737d8d4df8d92314bfc522',1,'gouda::notes::NumberRange::NumberRange()'],['../classgouda_1_1notes_1_1_number_range.html#ad7c19e4c8dd6eef2f48877d5cf85c48b',1,'gouda::notes::NumberRange::NumberRange(const QByteArray &amp;bytes)']]]
];
