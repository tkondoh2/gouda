var searchData=
[
  ['filename',['fileName',['../classgouda_1_1notes_1_1_file_object.html#adab9bacf79ebdbb934566d6482eb77b8',1,'gouda::notes::FileObject']]],
  ['fileobject',['FileObject',['../classgouda_1_1notes_1_1_file_object.html#a1d52a77fb6ce4597c53b0293b33f4818',1,'gouda::notes::FileObject::FileObject()'],['../classgouda_1_1notes_1_1_file_object.html#ae3793eb9e9f3e05832cdb5871a327bff',1,'gouda::notes::FileObject::FileObject(const QByteArray &amp;value)'],['../classgouda_1_1notes_1_1_file_object.html#ad7db7f93789fac878571b0d99287a557',1,'gouda::notes::FileObject::FileObject(const Item::Info &amp;itemInfo, int option=0)']]],
  ['finddesignnoteid',['findDesignNoteId',['../classgouda_1_1notes_1_1_database.html#a027fe3ebbed9acaaf160f24c237ae151',1,'gouda::notes::Database::findDesignNoteId(const Lmbcs &amp;name, WORD noteClass) noexcept(false)'],['../classgouda_1_1notes_1_1_database.html#aa4d9f9329f62d92d1b2dd2e6c7d43ba8',1,'gouda::notes::Database::findDesignNoteId(DBHANDLE hDB, const Lmbcs &amp;name, WORD noteClass) noexcept(false)']]],
  ['firstnumber',['firstNumber',['../classgouda_1_1notes_1_1_variant.html#a08e8a87df8077b453482c85e26042fb3',1,'gouda::notes::Variant']]],
  ['firststring',['firstString',['../classgouda_1_1notes_1_1_variant.html#a02b90e1ab78b3e561401a78b65879f87',1,'gouda::notes::Variant']]],
  ['firsttimedate',['firstTimeDate',['../classgouda_1_1notes_1_1_variant.html#a94d1568b5c7dbe06e6b6ee3ef9d7ff93',1,'gouda::notes::Variant']]],
  ['flag',['flag',['../classgouda_1_1notes_1_1_item.html#afed07328f65045ce43c153c457bffc98',1,'gouda::notes::Item']]],
  ['formatid',['formatId',['../namespacegouda_1_1notes.html#ac090eecd22b3ee9541273013c9de298e',1,'gouda::notes']]],
  ['formula',['Formula',['../classgouda_1_1notes_1_1_formula.html#a7b7aa95f39569e3f0a7a88f003dfb449',1,'gouda::notes::Formula::Formula()'],['../classgouda_1_1notes_1_1_formula.html#a334cc44ab7e6b10e27b337ab667a1972',1,'gouda::notes::Formula::Formula(FORMULAHANDLE &amp;&amp;hFormula)']]],
  ['fromqbytearray',['fromQByteArray',['../classgouda_1_1notes_1_1_lmbcs_list.html#ac3b32e85db33db1a864f324c3a4ffd0c',1,'gouda::notes::LmbcsList']]],
  ['fromsummary',['fromSummary',['../classgouda_1_1notes_1_1_variant.html#a2dba8668a32f4aa84feab133d08bdead',1,'gouda::notes::Variant']]]
];
