var dir_421d137a6d6e69b3f737421a73d375f8 =
[
    [ "addin", "dir_f2349537cd7837fcfb2952fc7d1adc15.html", "dir_f2349537cd7837fcfb2952fc7d1adc15" ],
    [ "cd", "dir_100d1e1c99687f5afb91ac79e6335dad.html", "dir_100d1e1c99687f5afb91ac79e6335dad" ],
    [ "hookdrv", "dir_d2b816211b2514ddb7d91f611b938a19.html", "dir_d2b816211b2514ddb7d91f611b938a19" ],
    [ "http", "dir_c519c685939a3e7fdb00a5e2cba79119.html", "dir_c519c685939a3e7fdb00a5e2cba79119" ],
    [ "mime", "dir_249fcd15d3cde6136bc8b64a15acf342.html", "dir_249fcd15d3cde6136bc8b64a15acf342" ],
    [ "blockhandle.hpp", "blockhandle_8hpp.html", "blockhandle_8hpp" ],
    [ "collection.hpp", "collection_8hpp.html", "collection_8hpp" ],
    [ "compositeitem.hpp", "compositeitem_8hpp.html", [
      [ "CompositeItem", "classgouda_1_1notes_1_1_composite_item.html", "classgouda_1_1notes_1_1_composite_item" ]
    ] ],
    [ "compute.hpp", "compute_8hpp.html", "compute_8hpp" ],
    [ "database.hpp", "database_8hpp.html", "database_8hpp" ],
    [ "distinguishedname.hpp", "distinguishedname_8hpp.html", "distinguishedname_8hpp" ],
    [ "env.hpp", "env_8hpp.html", "env_8hpp" ],
    [ "fileobject.hpp", "fileobject_8hpp.html", [
      [ "FileObject", "classgouda_1_1notes_1_1_file_object.html", "classgouda_1_1notes_1_1_file_object" ]
    ] ],
    [ "formula.hpp", "formula_8hpp.html", "formula_8hpp" ],
    [ "handle.hpp", "handle_8hpp.html", [
      [ "Handle", "classgouda_1_1notes_1_1_handle.html", "classgouda_1_1notes_1_1_handle" ]
    ] ],
    [ "item.hpp", "item_8hpp.html", [
      [ "Item", "classgouda_1_1notes_1_1_item.html", "classgouda_1_1notes_1_1_item" ],
      [ "Info", "classgouda_1_1notes_1_1_item_1_1_info.html", "classgouda_1_1notes_1_1_item_1_1_info" ]
    ] ],
    [ "lmbcs.hpp", "lmbcs_8hpp.html", "lmbcs_8hpp" ],
    [ "lmbcslist.hpp", "lmbcslist_8hpp.html", "lmbcslist_8hpp" ],
    [ "note.hpp", "note_8hpp.html", "note_8hpp" ],
    [ "notes_global.h", "notes__global_8h.html", "notes__global_8h" ],
    [ "number.hpp", "number_8hpp.html", [
      [ "Number", "classgouda_1_1notes_1_1_number.html", "classgouda_1_1notes_1_1_number" ]
    ] ],
    [ "numberrange.hpp", "numberrange_8hpp.html", [
      [ "NumberPair", "classgouda_1_1notes_1_1_number_pair.html", "classgouda_1_1notes_1_1_number_pair" ],
      [ "NumberRange", "classgouda_1_1notes_1_1_number_range.html", "classgouda_1_1notes_1_1_number_range" ]
    ] ],
    [ "objecthandle.hpp", "objecthandle_8hpp.html", "objecthandle_8hpp" ],
    [ "range.hpp", "range_8hpp.html", [
      [ "Pair", "classgouda_1_1notes_1_1_pair.html", "classgouda_1_1notes_1_1_pair" ],
      [ "Range", "classgouda_1_1notes_1_1_range.html", "classgouda_1_1notes_1_1_range" ]
    ] ],
    [ "search.hpp", "search_8hpp.html", [
      [ "Search", "structgouda_1_1notes_1_1_search.html", "structgouda_1_1notes_1_1_search" ],
      [ "Iterator", "structgouda_1_1notes_1_1_search_1_1_iterator.html", "structgouda_1_1notes_1_1_search_1_1_iterator" ]
    ] ],
    [ "session.hpp", "session_8hpp.html", [
      [ "ServerLatencyData", "structgouda_1_1notes_1_1_server_latency_data.html", "structgouda_1_1notes_1_1_server_latency_data" ],
      [ "Session", "structgouda_1_1notes_1_1_session.html", "structgouda_1_1notes_1_1_session" ]
    ] ],
    [ "status.hpp", "status_8hpp.html", [
      [ "Status", "classgouda_1_1notes_1_1_status.html", "classgouda_1_1notes_1_1_status" ]
    ] ],
    [ "summarybuffer.hpp", "summarybuffer_8hpp.html", [
      [ "SummaryBuffer", "classgouda_1_1notes_1_1_summary_buffer.html", "classgouda_1_1notes_1_1_summary_buffer" ]
    ] ],
    [ "timedate.hpp", "timedate_8hpp.html", "timedate_8hpp" ],
    [ "timedaterange.hpp", "timedaterange_8hpp.html", [
      [ "TimeDatePair", "classgouda_1_1notes_1_1_time_date_pair.html", "classgouda_1_1notes_1_1_time_date_pair" ],
      [ "TimeDateRange", "classgouda_1_1notes_1_1_time_date_range.html", "classgouda_1_1notes_1_1_time_date_range" ]
    ] ],
    [ "tools.hpp", "tools_8hpp.html", "tools_8hpp" ],
    [ "translate.hpp", "translate_8hpp.html", "translate_8hpp" ],
    [ "variant.hpp", "variant_8hpp.html", [
      [ "Variant", "classgouda_1_1notes_1_1_variant.html", "classgouda_1_1notes_1_1_variant" ]
    ] ],
    [ "version.cpp", "version_8cpp.html", "version_8cpp" ]
];