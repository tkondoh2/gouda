var hierarchy =
[
    [ "gouda::notes::http::Authorize", "classgouda_1_1notes_1_1http_1_1_authorize.html", null ],
    [ "gouda::notes::hookdrv::Context", "classgouda_1_1notes_1_1hookdrv_1_1_context.html", [
      [ "gouda::notes::hookdrv::NoteContext", "classgouda_1_1notes_1_1hookdrv_1_1_note_context.html", [
        [ "gouda::notes::hookdrv::OpenNoteContext", "classgouda_1_1notes_1_1hookdrv_1_1_open_note_context.html", null ],
        [ "gouda::notes::hookdrv::UpdateNoteContext", "classgouda_1_1notes_1_1hookdrv_1_1_update_note_context.html", null ]
      ] ]
    ] ],
    [ "gouda::notes::http::Context", "classgouda_1_1notes_1_1http_1_1_context.html", null ],
    [ "gouda::notes::mime::Entity::Data", "structgouda_1_1notes_1_1mime_1_1_entity_1_1_data.html", null ],
    [ "gouda::notes::DistinguishedName", "classgouda_1_1notes_1_1_distinguished_name.html", null ],
    [ "exception", null, [
      [ "gouda::notes::Status", "classgouda_1_1notes_1_1_status.html", [
        [ "gouda::notes::CompileError", "classgouda_1_1notes_1_1_compile_error.html", null ]
      ] ]
    ] ],
    [ "gouda::notes::FileObject", "classgouda_1_1notes_1_1_file_object.html", null ],
    [ "gouda::notes::Handle< T, Deleter, IsNull >", "classgouda_1_1notes_1_1_handle.html", [
      [ "gouda::notes::BlockHandle", "classgouda_1_1notes_1_1_block_handle.html", null ],
      [ "gouda::notes::Collection", "classgouda_1_1notes_1_1_collection.html", null ],
      [ "gouda::notes::Compute", "classgouda_1_1notes_1_1_compute.html", null ],
      [ "gouda::notes::Database", "classgouda_1_1notes_1_1_database.html", null ],
      [ "gouda::notes::mime::ConversionControl", "classgouda_1_1notes_1_1mime_1_1_conversion_control.html", null ],
      [ "gouda::notes::mime::Directory", "classgouda_1_1notes_1_1mime_1_1_directory.html", null ],
      [ "gouda::notes::mime::Entity", "classgouda_1_1notes_1_1mime_1_1_entity.html", null ],
      [ "gouda::notes::Note", "classgouda_1_1notes_1_1_note.html", null ]
    ] ],
    [ "gouda::notes::Handle< DHANDLE, decltype &OSMemFree, decltype(&isNullHandle< DHANDLE >) >", "classgouda_1_1notes_1_1_handle.html", [
      [ "gouda::notes::LockableHandle< DHANDLE, decltype(&OSMemFree)>", "classgouda_1_1notes_1_1_lockable_handle.html", [
        [ "gouda::notes::ObjectHandle", "classgouda_1_1notes_1_1_object_handle.html", null ]
      ] ]
    ] ],
    [ "gouda::notes::Handle< T, Deleter, decltype(&isNullHandle< T >) >", "classgouda_1_1notes_1_1_handle.html", [
      [ "gouda::notes::LockableHandle< T, Deleter >", "classgouda_1_1notes_1_1_lockable_handle.html", [
        [ "gouda::notes::Formula", "classgouda_1_1notes_1_1_formula.html", null ]
      ] ]
    ] ],
    [ "gouda::notes::Item::Info", "classgouda_1_1notes_1_1_item_1_1_info.html", null ],
    [ "gouda::notes::Database::Info", "classgouda_1_1notes_1_1_database_1_1_info.html", null ],
    [ "gouda::notes::Item", "classgouda_1_1notes_1_1_item.html", [
      [ "gouda::notes::CompositeItem", "classgouda_1_1notes_1_1_composite_item.html", null ]
    ] ],
    [ "gouda::notes::cd::Item", "classgouda_1_1notes_1_1cd_1_1_item.html", [
      [ "gouda::notes::cd::Any< SIG >", "classgouda_1_1notes_1_1cd_1_1_any.html", null ],
      [ "gouda::notes::cd::BaseItem< CD, _TYPE, SIG_CD_TYPE >", "classgouda_1_1notes_1_1cd_1_1_base_item.html", [
        [ "gouda::notes::cd::HotspotBegin", "classgouda_1_1notes_1_1cd_1_1_hotspot_begin.html", null ],
        [ "gouda::notes::cd::NestedTableBegin", "classgouda_1_1notes_1_1cd_1_1_nested_table_begin.html", null ],
        [ "gouda::notes::cd::NestedTableCell", "classgouda_1_1notes_1_1cd_1_1_nested_table_cell.html", null ],
        [ "gouda::notes::cd::NestedTableEnd", "classgouda_1_1notes_1_1cd_1_1_nested_table_end.html", null ],
        [ "gouda::notes::cd::Paragraph", "classgouda_1_1notes_1_1cd_1_1_paragraph.html", null ],
        [ "gouda::notes::cd::TableBegin", "classgouda_1_1notes_1_1cd_1_1_table_begin.html", null ],
        [ "gouda::notes::cd::TableCell", "classgouda_1_1notes_1_1cd_1_1_table_cell.html", null ],
        [ "gouda::notes::cd::TableEnd", "classgouda_1_1notes_1_1cd_1_1_table_end.html", null ],
        [ "gouda::notes::cd::Text", "classgouda_1_1notes_1_1cd_1_1_text.html", null ]
      ] ]
    ] ],
    [ "gouda::notes::Search< T, Func >::Iterator", "structgouda_1_1notes_1_1_search_1_1_iterator.html", null ],
    [ "gouda::notes::Lmbcs", "classgouda_1_1notes_1_1_lmbcs.html", null ],
    [ "gouda::notes::LmbcsListToQStringList", "structgouda_1_1notes_1_1_lmbcs_list_to_q_string_list.html", null ],
    [ "gouda::notes::LmbcsToQString", "structgouda_1_1notes_1_1_lmbcs_to_q_string.html", null ],
    [ "gouda::notes::LmbcsToUnicode", "structgouda_1_1notes_1_1_lmbcs_to_unicode.html", null ],
    [ "gouda::notes::Number", "classgouda_1_1notes_1_1_number.html", null ],
    [ "gouda::notes::Pair< PAIR >", "classgouda_1_1notes_1_1_pair.html", null ],
    [ "gouda::notes::Pair< NUMBER_PAIR >", "classgouda_1_1notes_1_1_pair.html", [
      [ "gouda::notes::NumberPair", "classgouda_1_1notes_1_1_number_pair.html", null ]
    ] ],
    [ "gouda::notes::Pair< TIMEDATE_PAIR >", "classgouda_1_1notes_1_1_pair.html", [
      [ "gouda::notes::TimeDatePair", "classgouda_1_1notes_1_1_time_date_pair.html", null ]
    ] ],
    [ "gouda::notes::http::ParsedRequest", "classgouda_1_1notes_1_1http_1_1_parsed_request.html", null ],
    [ "gouda::notes::Database::Path", "classgouda_1_1notes_1_1_database_1_1_path.html", null ],
    [ "QList", null, [
      [ "gouda::notes::LmbcsList", "classgouda_1_1notes_1_1_lmbcs_list.html", null ]
    ] ],
    [ "gouda::notes::QStringListToLmbcsList", "structgouda_1_1notes_1_1_q_string_list_to_lmbcs_list.html", null ],
    [ "gouda::notes::QStringToLmbcs", "structgouda_1_1notes_1_1_q_string_to_lmbcs.html", null ],
    [ "gouda::notes::Range< SINGLE, PAIR >", "classgouda_1_1notes_1_1_range.html", null ],
    [ "gouda::notes::Range< NUMBER, NUMBER_PAIR >", "classgouda_1_1notes_1_1_range.html", [
      [ "gouda::notes::NumberRange", "classgouda_1_1notes_1_1_number_range.html", null ]
    ] ],
    [ "gouda::notes::Range< TIMEDATE, TIMEDATE_PAIR >", "classgouda_1_1notes_1_1_range.html", [
      [ "gouda::notes::TimeDateRange", "classgouda_1_1notes_1_1_time_date_range.html", null ]
    ] ],
    [ "gouda::notes::http::Request", "classgouda_1_1notes_1_1http_1_1_request.html", null ],
    [ "gouda::notes::http::Response", "classgouda_1_1notes_1_1http_1_1_response.html", null ],
    [ "gouda::notes::Search< T, Func >", "structgouda_1_1notes_1_1_search.html", null ],
    [ "gouda::notes::ServerLatencyData", "structgouda_1_1notes_1_1_server_latency_data.html", null ],
    [ "gouda::notes::Session", "structgouda_1_1notes_1_1_session.html", null ],
    [ "gouda::notes::SummaryBuffer", "classgouda_1_1notes_1_1_summary_buffer.html", null ],
    [ "gouda::notes::TimeDate", "classgouda_1_1notes_1_1_time_date.html", null ],
    [ "gouda::notes::Translate", "structgouda_1_1notes_1_1_translate.html", null ],
    [ "gouda::notes::UnicodeToLmbcs", "structgouda_1_1notes_1_1_unicode_to_lmbcs.html", null ],
    [ "gouda::notes::Variant", "classgouda_1_1notes_1_1_variant.html", null ],
    [ "gouda::notes::Version", "classgouda_1_1notes_1_1_version.html", null ]
];