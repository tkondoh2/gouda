var translate_8hpp =
[
    [ "Translate", "structgouda_1_1notes_1_1_translate.html", "structgouda_1_1notes_1_1_translate" ],
    [ "UnicodeToLmbcs", "structgouda_1_1notes_1_1_unicode_to_lmbcs.html", "structgouda_1_1notes_1_1_unicode_to_lmbcs" ],
    [ "LmbcsToUnicode", "structgouda_1_1notes_1_1_lmbcs_to_unicode.html", "structgouda_1_1notes_1_1_lmbcs_to_unicode" ],
    [ "MAX_TRANSLATE_SIZE", "translate_8hpp.html#ae600d1b9651ce963dab19b435f520490", null ]
];