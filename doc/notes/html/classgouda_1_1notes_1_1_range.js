var classgouda_1_1notes_1_1_range =
[
    [ "Range", "classgouda_1_1notes_1_1_range.html#aac48234cadfc838ca1e6bd71d2b1a721", null ],
    [ "Range", "classgouda_1_1notes_1_1_range.html#a9ddae309671b723d8a89ca1aab9ecbac", null ],
    [ "convertPairToText", "classgouda_1_1notes_1_1_range.html#ae847373db1b05ad8d493ac1840aa985e", null ],
    [ "convertSingleToText", "classgouda_1_1notes_1_1_range.html#a47e613df9331be4cb1a2bf584428b482", null ],
    [ "pairs", "classgouda_1_1notes_1_1_range.html#adb054924c07311ff8d2c39766b94a6d8", null ],
    [ "pairsToStringList", "classgouda_1_1notes_1_1_range.html#abed14b1caf2fdf67984d6904f1befd8d", null ],
    [ "singles", "classgouda_1_1notes_1_1_range.html#a35f4467987dbb4cd25f485ac6d190494", null ],
    [ "toString", "classgouda_1_1notes_1_1_range.html#a70de64a47420314307453dd3b2fd6120", null ],
    [ "toStringList", "classgouda_1_1notes_1_1_range.html#afdfdbee2164d064d584d3e0e2c7501df", null ]
];