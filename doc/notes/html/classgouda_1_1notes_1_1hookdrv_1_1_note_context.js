var classgouda_1_1notes_1_1hookdrv_1_1_note_context =
[
    [ "NOTE_OPERATION", "classgouda_1_1notes_1_1hookdrv_1_1_note_context.html#a0c9739d772dc7e2c062043189a09cd0d", [
      [ "Open", "classgouda_1_1notes_1_1hookdrv_1_1_note_context.html#a0c9739d772dc7e2c062043189a09cd0daf4e8ed10d086846999c712baa63dae5f", null ],
      [ "Update", "classgouda_1_1notes_1_1hookdrv_1_1_note_context.html#a0c9739d772dc7e2c062043189a09cd0dad7413185eb6ca2895979abf287772406", null ],
      [ "Create", "classgouda_1_1notes_1_1hookdrv_1_1_note_context.html#a0c9739d772dc7e2c062043189a09cd0da2e7b58bb1179648eee20467798c8a8a1", null ],
      [ "Delete", "classgouda_1_1notes_1_1hookdrv_1_1_note_context.html#a0c9739d772dc7e2c062043189a09cd0da11b83eff9c298e060309953e901ddf92", null ],
      [ "DeleteStub", "classgouda_1_1notes_1_1hookdrv_1_1_note_context.html#a0c9739d772dc7e2c062043189a09cd0da91ba356cf0db62f0b12166edefd7ed49", null ]
    ] ],
    [ "NoteContext", "classgouda_1_1notes_1_1hookdrv_1_1_note_context.html#aa4b3f2a317496c41ab639aa2d7816c49", null ],
    [ "hNote", "classgouda_1_1notes_1_1hookdrv_1_1_note_context.html#a9b8b733373bbaf3b3ab77208af2cbe4a", null ],
    [ "NoteID", "classgouda_1_1notes_1_1hookdrv_1_1_note_context.html#ac582118bfd0eeb8c1ff5a00ce4ce5bb0", null ],
    [ "operation", "classgouda_1_1notes_1_1hookdrv_1_1_note_context.html#ad6c43428c5d705d912d85946d5a645ae", null ],
    [ "hNote_", "classgouda_1_1notes_1_1hookdrv_1_1_note_context.html#a9e7b7811ec60a184c0a76cf29332773f", null ],
    [ "NoteID_", "classgouda_1_1notes_1_1hookdrv_1_1_note_context.html#a7904744fc7c7ffacf6d13bb56d89a562", null ]
];