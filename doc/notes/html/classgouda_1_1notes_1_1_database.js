var classgouda_1_1notes_1_1_database =
[
    [ "Info", "classgouda_1_1notes_1_1_database_1_1_info.html", "classgouda_1_1notes_1_1_database_1_1_info" ],
    [ "Path", "classgouda_1_1notes_1_1_database_1_1_path.html", "classgouda_1_1notes_1_1_database_1_1_path" ],
    [ "Database", "classgouda_1_1notes_1_1_database.html#a29fd15113525b5a7a8fa12074746dd32", null ],
    [ "Database", "classgouda_1_1notes_1_1_database.html#a6fb5488b6faa31b24d8c70ee0e22b090", null ],
    [ "~Database", "classgouda_1_1notes_1_1_database.html#a5ba6535469090c3c7b5dea6bd3f72876", null ],
    [ "createDocument", "classgouda_1_1notes_1_1_database.html#ad66373fbe4c85d58187e358f9e5d8877", null ],
    [ "createNote", "classgouda_1_1notes_1_1_database.html#a30055973475fa3165c91e3cf154886d7", null ],
    [ "findDesignNoteId", "classgouda_1_1notes_1_1_database.html#a027fe3ebbed9acaaf160f24c237ae151", null ],
    [ "getCollection", "classgouda_1_1notes_1_1_database.html#ab99e8601ffad366487046f280c20df4d", null ],
    [ "getCollection", "classgouda_1_1notes_1_1_database.html#ae497bf85ba309f9d443a363d2a8def35", null ],
    [ "getInfo", "classgouda_1_1notes_1_1_database.html#a00353ac2c551a25e4c860e265cb6725b", null ],
    [ "getPaths", "classgouda_1_1notes_1_1_database.html#aab79c68db540aa199c6c807061123299", null ],
    [ "getReplicaInfo", "classgouda_1_1notes_1_1_database.html#a9a2f241683ba454365c93dab3a4adc86", null ],
    [ "isDatabase", "classgouda_1_1notes_1_1_database.html#a987875dfbd1b0b6b691c96967efd7cb2", null ],
    [ "isDirectory", "classgouda_1_1notes_1_1_database.html#a6f30d05038ffcb9427f30ea35e35ee78", null ]
];