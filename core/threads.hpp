#ifndef GOUDA_CORE_THREADS_HPP
#define GOUDA_CORE_THREADS_HPP

#include <QString>
#include <sstream>
#include <thread>

namespace gouda {
namespace core {

inline QString getThreadId()
{
  std::ostringstream oss;
  oss << std::this_thread::get_id();
  return QString::fromStdString(oss.str());
}

} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_THREADS_HPP
