﻿#ifndef CORE_GLOBAL_H
#define CORE_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(GOUDA_CORE_LIBRARY)
#  define GOUDA_CORESHARED_EXPORT Q_DECL_EXPORT
#else
#  define GOUDA_CORESHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // CORE_GLOBAL_H
