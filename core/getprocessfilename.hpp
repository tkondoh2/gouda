﻿#ifndef GOUDA_CORE_GETPROCESSFILENAME_HPP
#define GOUDA_CORE_GETPROCESSFILENAME_HPP

#include <QByteArray>

#ifdef NT
#include <Windows.h>
#else
#include <unistd.h>
#endif

namespace gouda {
namespace core {

class GetProcessFileName
{
public:
  QByteArray operator ()() noexcept
  {
#ifdef NT
  char buffer[MAX_PATH];
  if (GetModuleFileNameA(nullptr, buffer, MAX_PATH)) {
    return QByteArray(buffer);
  }
  return QByteArray();
#else
  char path[1024];
  ssize_t len = readlink("/proc/self/exe", path, sizeof(path) - 1);
  return QByteArray(path, static_cast<int>(len));
#endif
  }
};

} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_GETPROCESSFILENAME_HPP
