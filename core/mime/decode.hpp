﻿#ifndef GOUDA_CORE_MIME_DECODE_HPP
#define GOUDA_CORE_MIME_DECODE_HPP

#include <QTextStream>

#include <gouda/core/mime/codec.hpp>
#include <gouda/core/quotedprintable.hpp>

namespace gouda {
namespace core {
namespace mime {

struct Base64
{
  static const char pattern = 'B';
  static QByteArray decode(const QByteArray &bytes)
  {
    return QByteArray::fromBase64(bytes);
  }
};

struct Quoted
{
  static const char pattern = 'Q';
  static QByteArray decode(const QByteArray &bytes)
  {
    return QuotedPrintable::decode(bytes);
  }
};

template <typename T>
bool decodeByteArray(const QString &pattern, const QByteArray &from, QByteArray &to)
{
  if (pattern.toUpper().at(0).toLatin1() == T::pattern) {
    to = T::decode(from);
    return true;
  }
  else return false;
}

inline QString decodeEncodingPart(
    const QString &charSet,
    const QString &encoding,
    const QString &s
    ) noexcept(false)
{
  QString result;
  if (s.isEmpty()) return result;
  auto bytes = s.toLatin1();

  if (decodeByteArray<Base64>(encoding, bytes, bytes)) {}
  else if (decodeByteArray<Quoted>(encoding, bytes, bytes)) {}
  else {
    throw std::runtime_error(
          QString("Unknown encoding pattern(%1).").arg(encoding)
          .toStdString()
          );
  }

  if (codecTo<UTF8>(charSet, bytes, result)) { return result; }
  else if (codec(charSet, bytes, result)) { return result; }
  else if (codecTo<US_ASCII>(charSet, bytes, result)) { return result; }
  else {
    throw std::runtime_error(
          QString("Unknown character set(%1).").arg(charSet)
          .toStdString()
          );
  }
}

inline QString decodeMimeHeader(const QString &s) noexcept(false)
{
  auto s1 = s.split(R"(\r\n)").join("");
  auto buffer = QString();
  QTextStream cout(&buffer, QIODevice::WriteOnly);

  int pos = 0;
  while (pos < s1.length()) {
    auto rx = QRegExp(R"rx(\s*=\?([^?]+)\?([^?]+)\?([^?]+)\?=)rx");
    auto offset = rx.indexIn(s1, pos);
    if (offset >= pos) {
      if (offset > pos) {
        cout << s1.mid(pos, offset - pos);
      }
      cout << decodeEncodingPart(rx.cap(1), rx.cap(2), rx.cap(3));
      pos = offset + rx.matchedLength();
    }
    else {
      if (s1.length() > pos) {
        cout << s1.right(s1.length() - pos);
      }
      break;
    }
  }

  cout.flush();
  return buffer;
}

} // namespace mime
} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_MIME_DECODE_HPP
