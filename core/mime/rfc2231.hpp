﻿#ifndef GOUDA_CORE_MIME_RFC2231_HPP
#define GOUDA_CORE_MIME_RFC2231_HPP

#include <QString>
#include <QByteArray>
#include <QRegExp>
#include <QUrl>
#include <QTextCodec>

namespace gouda {
namespace core {
namespace mime {

class Rfc2231
{
public:
  static QString codec(const QString &target)
  {
    auto rx = QRegExp(R"rx(([^']+)'[^']*'(.*))rx");
    if (rx.indexIn(target) >= 0) {
      auto codecName = rx.cap(1).toLatin1().toLower();
      auto decoded = QUrl::fromPercentEncoding(rx.cap(2).toLatin1());
      if (codecName == "utf-8") return decoded;
      auto codec = QTextCodec::codecForName(codecName);
      if (codec) return codec->toUnicode(decoded.toLocal8Bit());
    }
    return QString();
  }

  static QString partDecode(const QString &target)
  {
    auto rx = QRegExp(R"rx(filename\*\d*\*?=([^;]+))rx");
    QString result;
    int pos = 0;
    while ((pos = rx.indexIn(target, pos)) >= 0) {
      result += rx.cap(1);
      pos += rx.matchedLength();
    }
    if (!result.isEmpty()) return codec(result);
    return QString();
  }

  static QString decode(const QByteArray &local7bit)
  {
    return partDecode(QString::fromLatin1(local7bit).trimmed());
  }
};

} // namespace mime
} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_MIME_RFC2231_HPP
