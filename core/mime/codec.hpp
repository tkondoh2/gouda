﻿#ifndef GOUDA_CORE_MIME_CODEC_HPP
#define GOUDA_CORE_MIME_CODEC_HPP

#include <QString>
#include <QByteArray>
#include <QTextCodec>

namespace gouda {
namespace core {
namespace mime {

inline QString standardize(const QString &s)
{
  static const auto rx = QRegExp(R"rx([^a-zA-Z0-9])rx");
  auto r = s;
  r.replace(rx, QChar('-'));
  return r;
}

inline bool equals(const QString &lhs, const QString &rhs)
{
  auto l = standardize(lhs);
  auto r = standardize(rhs);
  return l.compare(r, Qt::CaseInsensitive) == 0;
}

struct US_ASCII
{
  static bool compare(const QString &pattern)
  {
    return equals(pattern, "us-ascii");
  }

  static QString codec(const QByteArray &from)
  {
    return QString::fromLatin1(from);
  }
};

struct UTF8
{
  static bool compare(const QString &pattern)
  {
    return equals(pattern, "utf-8");
  }

  static QString codec(const QByteArray &from)
  {
    return QString::fromUtf8(from);
  }
};

struct ShiftJIS
{
  static bool compare(const QString &pattern)
  {
    return equals(pattern, "Shift-JIS")
        || equals(pattern, "SJIS")
        || equals(pattern, "x-sjis")
        || equals(pattern, "Windows-31J")
        || equals(pattern, "MS_Kanji")
        || equals(pattern, "cp932")
        || equals(pattern, "MS932");
  }

  static QString codec(const QByteArray &from)
  {
    QTextCodec *codec = QTextCodec::codecForName("Shift-JIS");
    return codec->toUnicode(from);
  }
};

struct JIS
{
  static bool compare(const QString &pattern)
  {
    return equals(pattern, "JIS") || equals(pattern, "ISO 2022-JP");
  }

  static QString codec(const QByteArray &from)
  {
    QTextCodec *codec = QTextCodec::codecForName("ISO 2022-JP");
    return codec->toUnicode(from);
  }
};

template <typename T>
bool codecTo(const QString &pattern, const QByteArray &from, QString &to)
{
  if (T::compare(pattern)) {
    to = T::codec(from);
    return true;
  }
  else return false;
}

inline bool codec(const QString &pattern, const QByteArray &from, QString &to)
{
  static const auto keyNames = QTextCodec::availableCodecs();

  foreach (auto keyName, keyNames) {
    auto key = standardize(QString::fromLatin1(keyName));
    auto pat = standardize(pattern);
    if (pat.compare(key, Qt::CaseInsensitive) == 0) {
      QTextCodec *codec = QTextCodec::codecForName(keyName);
      to = codec->toUnicode(from);
      return true;
    }
  }
  return false;
}

} // namespace mime
} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_MIME_CODEC_HPP
