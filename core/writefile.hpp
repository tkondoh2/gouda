﻿#ifndef GOUDA_CORE_WRITEFILE_HPP
#define GOUDA_CORE_WRITEFILE_HPP

#include <gouda/core/file.hpp>

#include <QTextStream>
#include <QTextCodec>

namespace gouda {
namespace core {

class WriteFile
    : public File
{
  QTextCodec *pCodec_;

public:
  WriteFile()
    : File()
    , pCodec_(QTextCodec::codecForLocale())
  {}

  WriteFile(
      const QString &filePath,
      QFile::OpenMode openMode = QFile::Text | QFile::WriteOnly,
      QTextCodec *pCodec = QTextCodec::codecForLocale()
      )
    : File(filePath, openMode)
    , pCodec_(pCodec)
  {}

  virtual ~WriteFile() {}

  virtual void write(const QString &text, bool newline = true) noexcept
  {
    QTextStream str(pFile().data());
    str.setCodec(pCodec_);
    str << text;
    if (newline) {
      str << endl;
    }
  }
};

} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_WRITEFILE_HPP
