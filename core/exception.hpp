﻿#ifndef GOUDA_CORE_EXCEPTION_HPP
#define GOUDA_CORE_EXCEPTION_HPP

#include <QString>
#include <utility>

namespace gouda::core {

class Exception
{
  QString message_;

public:
  explicit Exception(QString &&message)
    : message_(std::forward<QString>(message))
  {}

  const QString &constData() const { return message_; }

  QString &data() { return message_; }
};

} // namespace gouda::core

#endif // GOUDA_CORE_EXCEPTION_HPP
