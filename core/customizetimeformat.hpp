﻿#ifndef GOUDA_CORE_CUSTOMIZETIMEFORMAT_HPP
#define GOUDA_CORE_CUSTOMIZETIMEFORMAT_HPP

#include <QString>
#include <QDateTime>

namespace gouda {
namespace core {

class CustomizeTimeFormat
{
public:
  QString operator ()(
      const QString &org,
      const QDateTime &dateTime
      ) noexcept
  {
    QString text = org;
    return text.replace("%Y", dateTime.toString("yyyy"))
      .replace("%M", dateTime.toString("MM"))
      .replace("%D", dateTime.toString("dd"))
      .replace("%h", dateTime.toString("HH"))
      .replace("%m", dateTime.toString("mm"))
      .replace("%s", dateTime.toString("ss"))
      .replace("%z", dateTime.toString("zzz"));
  }
};

} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_CUSTOMIZETIMEFORMAT_HPP
