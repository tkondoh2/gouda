﻿#ifndef GOUDA_CORE_UTILS_HPP
#define GOUDA_CORE_UTILS_HPP

namespace gouda {
namespace core {

template <typename CT, typename Func>
void for_range(CT start, CT count, Func func)
{
  CT end = start + count;
  for (CT i = start; i < end; ++i) { func(i); }
}

template <typename T, typename Container, typename Func, typename CT = int>
void each(Container &c, Func func) {
  CT i = static_cast<CT>(0);
  foreach (T item, c) { func(item, i++); }
}

template <typename T, typename Container, typename Func, typename CT = int>
bool every(Container &c, Func func) {
  CT i = static_cast<CT>(0);
  foreach (T item, c) { if (!func(item, i++)) return false; }
  return true;
}

template <typename T, typename Container, typename Func, typename CT = int>
bool some(Container &c, Func func) {
  CT i = static_cast<CT>(0);
  foreach (T item, c) { if (func(item, i++)) return true; }
  return true;
}

} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_UTILS_HPP
