﻿#ifndef GOUDA_CORE_ARGUMENT_VALUE_HPP
#define GOUDA_CORE_ARGUMENT_VALUE_HPP

#include <gouda/core/argument/arguments.hpp>

#include <QString>
#include <QStringList>

namespace gouda::core::argument {

template <typename T>
class Value
{
  T value_;
  QString key_;
  QStringList names_;
  QString description_;
  QString valueName_;
  QStringList defaultValues_;
  QCommandLineOption::Flag flags_;

public:
  Value(
      QString &&key,
      const QString &name,
      QCommandLineOption::Flag flags
      = static_cast<QCommandLineOption::Flag>(0)
      )
    : value_()
    , key_(std::move(key))
    , names_(name)
    , description_()
    , valueName_()
    , defaultValues_()
    , flags_(flags)
  {}

  Value(
      QString &&key,
      const QStringList &names,
      QCommandLineOption::Flag flags
      = static_cast<QCommandLineOption::Flag>(0)
      )
    : value_()
    , key_(std::move(key))
    , names_(names)
    , description_()
    , valueName_()
    , defaultValues_()
    , flags_(flags)
  {}

  Value(
      QString &&key,
      const QString &name,
      const QString &description,
      const QString &valueName = QString(),
      const QString &defaultValue = QString(),
      QCommandLineOption::Flag flags
      = static_cast<QCommandLineOption::Flag>(0)
      )
    : value_()
    , key_(std::move(key))
    , names_(name)
    , description_(description)
    , valueName_(valueName)
    , defaultValues_(defaultValue)
    , flags_(flags)
  {}

  Value(
      QString &&key,
      const QStringList &names,
      const QString &description,
      const QString &valueName = QString(),
      const QString &defaultValue = QString(),
      QCommandLineOption::Flag flags
      = static_cast<QCommandLineOption::Flag>(0)
      )
    : value_()
    , key_(std::move(key))
    , names_(names)
    , description_(description)
    , valueName_(valueName)
    , defaultValues_(defaultValue)
    , flags_(flags)
  {}

  const typename T::value_type &value() const { return value_.value(); }

  void setValue(const typename T::value_type &value)
  {
    value_.setValue(value);
  }

  const QString &key() const { return key_; }

  void insertTo(OptionMap &map) const
  {
    auto option = new QCommandLineOption(names_);
    if (!description_.isEmpty()) option->setDescription(description_);
    if (!valueName_.isEmpty()) option->setValueName(valueName_);
    if (!defaultValues_.isEmpty()) option->setDefaultValues(defaultValues_);
    option->setFlags(flags_);
    map.insert(key_, OptionPtr(option));
  }

  void setFromOptions(
      const QCommandLineParser &parser,
      const OptionMap &options
      )
  {
    if (options.contains(key_)) {
      auto option = options.value(key_);
      if (parser.isSet(*option)) {
        value_.setValue(parser, *option);
      }
    }
  }
};

} // namespace gouda::core::argument

#endif // GOUDA_CORE_ARGUMENT_VALUE_HPP
