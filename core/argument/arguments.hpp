﻿#ifndef GOUDA_CORE_ARGUMENT_ARGUMENTS_HPP
#define GOUDA_CORE_ARGUMENT_ARGUMENTS_HPP

#include <QCoreApplication>
#include <QCommandLineParser>
#include <QCommandLineOption>
#include <QSharedPointer>
#include <QMap>

namespace gouda::core::argument {

using OptionPtr = QSharedPointer<QCommandLineOption>;
using OptionMap = QMap<QString,OptionPtr>;

/**
 * @brief コマンドラインデータ処理ルーチンのラッパー関数オブジェクト
 * @tparam T データ本体クラス
 *
 * T() T型はデフォルトコンストラクタを持つ。
 * static T::addVersion()->boolean バージョンオプションを使うかどうか
 * static T::addHelp()->boolean ヘルプオプションを使うかどうか
 * static T::description()->QString アプリケーションの説明
 * T::setupParser(QCommandLineParser&) パーサーをセットアップする。
 * T::getOptions()->OptionPtr 使用したいQCommandLineOptionを返す
 * T::setFromCmdline(QCommandLineParser&,OptionPtr&) Tインスタンスに結果を反映させる
 */
template <typename T>
class Arguments
{
public:
  /**
   * @brief エントリポイント
   * @param app QCoreApplicationインスタンス
   * @return 設定反映済みのT型のインスタンス
   */
  T operator ()(QCoreApplication &app)
  {
    QCommandLineParser parser;
    T values;

    if (T::addVersion()) parser.addVersionOption();
    if (T::addHelp()) parser.addHelpOption();
    auto desc = T::description();
    if (!desc.isEmpty()) parser.setApplicationDescription(desc);

    values.setupParser(parser);

    auto options = values.getOptions();
    foreach (auto option, options) {
      parser.addOption(*option);
    }

    parser.process(app);

    values.setFromCmdline(parser, options);
    return values;
  }
};

} // namespace gouda::core::argument

#endif // GOUDA_CORE_ARGUMENT_ARGUMENTS_HPP
