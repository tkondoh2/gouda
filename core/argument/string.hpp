﻿#ifndef GOUDA_CORE_ARGUMENT_STRING_HPP
#define GOUDA_CORE_ARGUMENT_STRING_HPP

#include <QCommandLineParser>
#include <QCommandLineOption>
#include <QString>

namespace gouda::core::argument {

class String
{
  QString value_;

public:
  using value_type = QString;

  String(){}

  const value_type &value() const { return value_; }

  void setValue(const typename value_type &value)
  {
    value_ = value;
  }

  void setValue(
      const QCommandLineParser &parser,
      const QCommandLineOption &option
      )
  {
    value_ = parser.value(option);
  }
};

} // namespace gouda::core::argument

#endif // GOUDA_CORE_ARGUMENT_STRING_HPP
