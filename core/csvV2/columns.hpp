﻿#ifndef GOUDA_CORE_CSVV2_COLUMNS_HPP
#define GOUDA_CORE_CSVV2_COLUMNS_HPP

#include <gouda/core/csvV2/column.hpp>
#include <gouda/core/csvV2/stringify.hpp>

namespace gouda {
namespace core {
namespace csvv2 {

class ColumnString
    : public ColumnBase<QString, StringifyString>
{
public:
  ColumnString(QString &&value)
    : ColumnBase<QString, StringifyString>(std::move(value), StringifyString())
  {}

  ColumnString(QString &&value, StringifyString &&stringify)
    : ColumnBase<QString, StringifyString>(std::move(value), std::move(stringify))
  {}
};

class ColumnStringList
    : public ColumnBase<QStringList, StringifyStringList>
{
public:
  ColumnStringList(QStringList &&value)
    : ColumnBase<QStringList, StringifyStringList>(std::move(value), StringifyStringList())
  {}

  ColumnStringList(QStringList &&value, StringifyStringList &&stringify)
    : ColumnBase<QStringList, StringifyStringList>(std::move(value), std::move(stringify))
  {}
};

class ColumnInt
    : public ColumnBase<int, StringifyInt>
{
public:
  ColumnInt(int &&value)
    : ColumnBase<int, StringifyInt>(std::move(value), StringifyInt())
  {}

  ColumnInt(int &&value, StringifyInt &&stringify)
    : ColumnBase<int, StringifyInt>(std::move(value), std::move(stringify))
  {}
};

class ColumnDate
    : public ColumnBase<QDate, StringifyDate>
{
public:
  ColumnDate(QDate &&value)
    : ColumnBase<QDate, StringifyDate>(std::move(value), StringifyDate())
  {}

  ColumnDate(QDate &&value, StringifyDate &&stringify)
    : ColumnBase<QDate, StringifyDate>(std::move(value), std::move(stringify))
  {}
};

class ColumnTime
    : public ColumnBase<QTime, StringifyTime>
{
public:
  ColumnTime(QTime &&value)
    : ColumnBase<QTime, StringifyTime>(std::move(value), StringifyTime())
  {}

  ColumnTime(QTime &&value, StringifyTime &&stringify)
    : ColumnBase<QTime, StringifyTime>(std::move(value), std::move(stringify))
  {}
};

class ColumnDateTime
    : public ColumnBase<QDateTime, StringifyDateTime>
{
public:
  ColumnDateTime(QDateTime &&value)
    : ColumnBase<QDateTime, StringifyDateTime>(std::move(value), StringifyDateTime())
  {}

  ColumnDateTime(QDateTime &&value, StringifyDateTime &&stringify)
    : ColumnBase<QDateTime, StringifyDateTime>(std::move(value), std::move(stringify))
  {}
};

} // namespace csvv2
} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_CSVV2_COLUMNS_HPP
