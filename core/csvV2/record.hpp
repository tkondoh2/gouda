﻿#ifndef GOUDA_CORE_CSVV2_RECORD_HPP
#define GOUDA_CORE_CSVV2_RECORD_HPP

#include <gouda/core/csvV2/column.hpp>

namespace gouda {
namespace core {
namespace csvv2 {

class Record
{
  QList<ColumnPtr> columns_;

public:
  Record(std::initializer_list<Column*> l)
    : columns_()
  {
    std::for_each(l.begin(), l.end(), [this](Column *c) {
      append(c);
    });
  }

  void append(Column *c)
  {
    columns_.append(ColumnPtr(c));
  }

  template <typename F>
  void each(F func) const
  {
    foreach (ColumnPtr col, columns_) {
      func(col);
    }
  }
};

} // namespace csvv2
} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_CSVV2_RECORD_HPP
