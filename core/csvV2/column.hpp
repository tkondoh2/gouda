﻿#ifndef GOUDA_CORE_CSVV2_COLUMN_HPP
#define GOUDA_CORE_CSVV2_COLUMN_HPP

#include <QString>
#include <QSharedPointer>

namespace gouda {
namespace core {
namespace csvv2 {

class Column
{
public:
  virtual ~Column(){}
  virtual QString toString() const = 0;
};

using ColumnPtr = QSharedPointer<Column>;

template <typename T, typename Stringify>
class ColumnBase
    : public Column
{
  T value_;
  Stringify stringify_;

public:
  ColumnBase(T &&value, Stringify &&stringify)
    : Column()
    , value_(std::move(value))
    , stringify_(std::move(stringify))
  {}

  virtual ~ColumnBase() override {}

  virtual QString toString() const override
  {
    return stringify_(value_);
  }
};

} // namespace csvv2
} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_CSVV2_COLUMN_HPP
