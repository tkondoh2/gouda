﻿#ifndef GOUDA_CORE_CSVV2_WRITEHEADER_HPP
#define GOUDA_CORE_CSVV2_WRITEHEADER_HPP

#include <gouda/core/csvV2/stringify.hpp>

#include <QTextStream>

namespace gouda {
namespace core {
namespace csvv2 {

class WriteHeader
    : public StringifyBase
{
  QString headers_;

public:
  WriteHeader() : StringifyBase(), headers_() {}

  WriteHeader(const QStringList &headers)
    : headers_()
  {
    QStringList l;
    foreach (QString h, headers) {
      l.append(quot(escape(h)));
    }
    headers_ = l.join(",");
  }

  WriteHeader(QString &&headers)
    : headers_(std::move(headers))
  {}

  void operator ()(QTextStream *str)
  {
    (*str) << headers_ << endl;
  }
};

} // namespace csvv2
} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_CSVV2_WRITEHEADER_HPP
