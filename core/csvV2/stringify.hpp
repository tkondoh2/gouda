﻿#ifndef GOUDA_CORE_CSVV2_STRINGIFY_HPP
#define GOUDA_CORE_CSVV2_STRINGIFY_HPP

#include <QString>
#include <QDateTime>

namespace gouda {
namespace core {
namespace csvv2 {

class StringifyBase
{
public:
  static QString quot(const QString &value)
  {
    return QString("\"%1\"").arg(value);
  }

  static QString escape(const QString &value)
  {
    QString v = value;
    v.replace("\"", "\"\"");
    return v;
  }
};

class StringifyString
    : public StringifyBase
{
public:
  QString operator ()(const QString &value) const
  {
    return quot(escape(value));
  }
};

class StringifyStringList
    : public StringifyBase
{
  QString delimiter_;

public:
  StringifyStringList() : delimiter_(";") {}
  StringifyStringList(const QString delimiter) : delimiter_(delimiter) {}
  QString operator ()(const QStringList &value) const
  {
    return quot(escape(value.join(delimiter_)));
  }
};

class StringifyInt
    : public StringifyBase
{
public:
  QString operator ()(int value) const
  {
    return QString::number(value);
  }
};

class StringifyDate
    : public StringifyBase
{
  QString format_;

public:
  StringifyDate() : format_("yyyy/MM/dd") {}
  StringifyDate(const QString &format) : format_(format) {}
  QString operator ()(const QDate &value) const
  {
    return quot(value.toString(format_));
  }
};

class StringifyTime
    : public StringifyBase
{
  QString format_;

public:
  StringifyTime() : format_("HH:mm:ss") {}
  StringifyTime(const QString &format) : format_(format) {}
  QString operator ()(const QTime &value) const
  {
    return quot(value.toString(format_));
  }
};

class StringifyDateTime
    : public StringifyBase
{
  QString format_;

public:
  StringifyDateTime() : format_("yyyy/MM/dd HH:mm:ss") {}
  StringifyDateTime(const QString &format) : format_(format) {}
  QString operator ()(const QDateTime &value) const
  {
    return quot(value.toString(format_));
  }
};

} // namespace csvv2
} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_CSVV2_STRINGIFY_HPP
