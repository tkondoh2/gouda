﻿#ifndef GOUDA_CORE_QUOTEDPRINTABLE_HPP
#define GOUDA_CORE_QUOTEDPRINTABLE_HPP

#include <QObject>
#include <QByteArray>

namespace gouda {
namespace core {

class QuotedPrintable
    : public QObject
{
    Q_OBJECT
public:

    static QByteArray encode(const QByteArray &latin1) noexcept
    {
        QByteArray output;

        char byte;
        const char hex[] = {
          '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
          'A', 'B', 'C', 'D', 'E', 'F'
        };

        for (int i = 0; i < latin1.length() ; ++i)
        {
            byte = latin1[i];

            if ((byte == ' ')
                || ((byte >= 33) && (byte <= 126)  && (byte != '='))
                )
            {
                output.append(byte);
            }
            else
            {
                output.append('=');
                output.append(hex[((byte >> 4) & 0x0F)]);
                output.append(hex[(byte & 0x0F)]);
            }
        }

        return output;
    }


    static QByteArray decode(const QByteArray &org_input)
    {
        QByteArray input = org_input;
        input.replace("=\r\n", "");

        QByteArray output;
        for (int i = 0; i < input.length(); ++i)
        {
            if (input.at(i) == '=' && i+2<input.length())
            {
                QString strValue = input.mid((++i)++, 2);
                bool converted;
                char character = static_cast<char>(
                      strValue.toUShort(&converted, 16)
                      );
                if( converted )
                    output.append(character);
                else
                    output.append( "=" + strValue);
            }
            else
            {
                output.append(input.at(i));
            }
        }

        return output;
    }

private:
    QuotedPrintable();
};

} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_QUOTEDPRINTABLE_HPP
