﻿#ifndef GOUDA_CORE_ROTATEFILEPATH_HPP
#define GOUDA_CORE_ROTATEFILEPATH_HPP

#include <gouda/core/customizefilepathbydatetime.hpp>
#include <gouda/core/writefile.hpp>

namespace gouda {
namespace core {

template <class Init>
class RotateFilePath
{
  qint64 maxSize_;
  Init init_;
  QString baseTemplate_;
  QString directoryTemplate_;

public:
  RotateFilePath(
      qint64 maxSize,
      Init init,
      const QString &baseTemplate = "${currentBase}-%Y%M%D_%h%m%s_%z",
      const QString &directoryTemplate = "${currentDir}"
      )
    : maxSize_(maxSize)
    , init_(init)
    , baseTemplate_(baseTemplate)
    , directoryTemplate_(directoryTemplate)
  {}

  bool operator ()(WriteFile &file) noexcept
  {
    if (file.size() > maxSize_) {
      QString oldPath = file.filePath();
      QString newPath = CustomizeFilePathByDateTime()(
            oldPath,
            QDateTime::currentDateTime(),
            baseTemplate_,
            directoryTemplate_
            );
      if (newPath != oldPath) {
        file.rename(newPath);
        file.setFilePath(oldPath);
        if (file.open())
          init_(file);
      }
    }
    else if (file.isEmpty()) {
      if (file.open())
        init_(file);
    }
    return true;
  }
};

} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_ROTATEFILEPATH_HPP
