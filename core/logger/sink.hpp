﻿#ifndef GOUDA_CORE_LOGGER_SINK_HPP
#define GOUDA_CORE_LOGGER_SINK_HPP

#include <QString>

namespace gouda {
namespace core {
namespace logger {

class Sink
{
public:
  virtual ~Sink() {} // 注意！これがないと継承先クラスのデストラクタが動かない

  virtual void log(const QString &msg) = 0;
};

} // namespace logger
} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_LOGGER_SINK_HPP
