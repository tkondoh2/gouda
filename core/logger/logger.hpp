﻿#ifndef GOUDA_CORE_LOGGER_LOGGER_HPP
#define GOUDA_CORE_LOGGER_LOGGER_HPP

#include <gouda/core/logger/level.hpp>
#include <gouda/core/logger/sink.hpp>

#include <QString>
#include <QList>
#include <QReadWriteLock>

#include <sstream>

namespace gouda {
namespace core {
namespace logger {

class Logger
{
  LEVEL threshold_;
  QString timeFormat_;
  QString pattern_;
  QList<Sink*> sinks_;

  static QReadWriteLock mutex_;

public:
  Logger()
    : threshold_(LEVEL::Max_Level),
      timeFormat_("yyyy/MM/dd HH:mm:ss.zzz"),
      pattern_("${time} (${thread}) [${level}] ${msg}"),
      sinks_()
  {}

  virtual ~Logger()
  {
    qDeleteAll(sinks_);
    sinks_.clear();
  }

  template <class... Args>
  void fatal(const QString &msg, Args... args)
  {
    log(LEVEL::Fatal, msg, args...);
  }

  template <class... Args>
  void error(const QString &msg, Args... args)
  {
    log(LEVEL::Error, msg, args...);
  }

  template <class... Args>
  void warning(const QString &msg, Args... args)
  {
    log(LEVEL::Warning, msg, args...);
  }

  template <class... Args>
  void info(const QString &msg, Args... args)
  {
    log(LEVEL::Info, msg, args...);
  }

  template <class... Args>
  void debug(const QString &msg, Args... args)
  {
    log(LEVEL::Debug, msg, args...);
  }

  template <class... Args>
  void trace(const QString &msg, Args... args)
  {
    log(LEVEL::Trace, msg, args...);
  }

  static const QString &levelText(LEVEL level)
  {
    static const QList<QString> list{
      "Fatal",
      "Error",
      "Warning",
      "Info",
      "Debug",
      "Trace"
    };
    return list[static_cast<int>(level)];
  }

  void addSink(Sink *sink)
  {
    sinks_.append(sink);
  }

protected:
  void log(LEVEL level, const QString &msg)
  {
    if (level <= threshold_) {
      QDateTime time = QDateTime::currentDateTime();
      QString text = pattern_;
      std::ostringstream oss;
      oss << std::this_thread::get_id();
      text.replace("${time}", time.toString(timeFormat_))
          .replace("${thread}", QString::fromStdString(oss.str()))
          .replace("${level}", levelText(level))
          .replace("${msg}", msg);
      foreach (Sink *pSink, sinks_) {
        QWriteLocker lock(&mutex_);
        pSink->log(text);
      }
    }
  }

  template <class First, class... Args>
  void log(LEVEL level, const QString &msg, First first, Args... args)
  {
    log(level, msg.arg(first), args...);
  }
};

} // namespace logger
} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_LOGGER_LOGGER_HPP
