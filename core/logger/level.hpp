﻿#ifndef GOUDA_CORE_LOGGER_LEVEL_HPP
#define GOUDA_CORE_LOGGER_LEVEL_HPP

namespace gouda {
namespace core {
namespace logger {

/**
 * @brief ログレベルの列挙
 */
enum class LEVEL {
  Fatal,
  Error,
  Warning,
  Info,
  Debug,
  Trace,
  Max_Level
};

/**
 * @brief ログレベルに応じたテキストに変換する。
 * @param level ログレベル
 * @return ログレベルのテキスト
 */
inline const char *levelText(LEVEL level)
{
  switch (level) {
  case LEVEL::Fatal: return "FATAL";
  case LEVEL::Error: return "ERROR";
  case LEVEL::Warning: return "WARNING";
  case LEVEL::Info: return "INFO";
  case LEVEL::Debug: return "DEBUG";
  case LEVEL::Trace: return "TRACE";
  default: return "UNDEFINED";
  }
}

} // namespace logger
} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_LOGGER_LEVEL_HPP
