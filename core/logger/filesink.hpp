﻿#ifndef GOUDA_CORE_LOGGER_FILESINK_HPP
#define GOUDA_CORE_LOGGER_FILESINK_HPP

#include <gouda/core/logger/sink.hpp>

namespace gouda {
namespace core {
namespace logger {

template <class T>
class FileSink
    : public Sink
{
  T writer_;

public:
  FileSink(T &&writer)
    : Sink()
    , writer_(writer)
  {}

  virtual ~FileSink() {}

  virtual void log(const QString &msg) override
  {
    if (writer_.open())
      writer_.write(msg);
//    if (writer_.open())
//      writer_.file().write(msg);
  }
};

} // namespace logger
} // namespace core
} // namesoace gouda

#endif // GOUDA_CORE_LOGGER_FILESINK_HPP
