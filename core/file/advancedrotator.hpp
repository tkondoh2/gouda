﻿#ifndef GOUDA_CORE_FILE_ADVANCEDROTATOR_HPP
#define GOUDA_CORE_FILE_ADVANCEDROTATOR_HPP

#include <gouda/core/file.hpp>

#include <QString>
#include <QFile>
#include <QFileInfo>
#include <QDateTime>

//#include <iostream>

namespace gouda {
namespace core {
namespace file {

class AdvancedTimeFormatter
{
public:
  QString operator ()(
      const QString &org,
      const QDateTime &dateTime
      ) noexcept
  {
    QString text = org;
    return text.replace("%Y", dateTime.toString("yyyy"))
      .replace("%M", dateTime.toString("MM"))
      .replace("%D", dateTime.toString("dd"))
      .replace("%h", dateTime.toString("HH"))
      .replace("%m", dateTime.toString("mm"))
      .replace("%s", dateTime.toString("ss"))
      .replace("%z", dateTime.toString("zzz"));
  }
};

template <class T = AdvancedTimeFormatter>
class AdvancedCustomizer
{
  T formatter_;
  QString baseTemplate_;
  QString dirTemplate_;

public:
  AdvancedCustomizer(
      T &&formatter,
      QString &&baseTemplate = "${currentBase}-%Y%M%D_%h%m%s_%z",
      QString &&directoryTemplate = "${currentDir}"
      )
    : formatter_(std::move(formatter))
    , baseTemplate_(std::move(baseTemplate))
    , dirTemplate_(std::move(directoryTemplate))
  {}

  AdvancedCustomizer(
      QString &&baseTemplate = "${currentBase}-%Y%M%D_%h%m%s_%z",
      QString &&directoryTemplate = "${currentDir}"
      )
    : formatter_(AdvancedTimeFormatter())
    , baseTemplate_(std::move(baseTemplate))
    , dirTemplate_(std::move(directoryTemplate))
  {}

  const QString &baseTemplate() const { return baseTemplate_; }
  const QString &dirTemplate() const { return dirTemplate_; }

  QString operator ()(
      const QString &filePath,
      const QDateTime &dateTime = QDateTime::currentDateTime()
      ) noexcept
  {
    QFileInfo fileInfo(filePath);
    QString dirName = fileInfo.absolutePath();
    QString baseName = fileInfo.completeBaseName();
    QString suffix = fileInfo.completeSuffix();

    QString customDir = dirTemplate();
    QString customBase = baseTemplate();

    customDir.replace("${currentDir}", dirName);
    customBase.replace("${currentBase}", baseName);

    customDir = formatter_(customDir, dateTime).replace("\\", "/");
    customBase = formatter_(customBase, dateTime).replace("\\", "/");

    return QString("%1%2.%3")
        .arg(File::addDelimiter(customDir))
        .arg(customBase)
        .arg(suffix);
  }
};

template <typename T = AdvancedCustomizer<>>
class AdvancedRotator
{
  T customizer_;
  QString currentPath_;

public:
  AdvancedRotator(
      T &&customizer,
      const QString &currentPath
      )
    : customizer_(std::move(customizer))
    , currentPath_(currentPath)
  {}

  AdvancedRotator(
      const QString &currentPath
      )
    : customizer_(AdvancedCustomizer<>())
    , currentPath_(currentPath)
  {}

  void setCurrent(QFile *file);

  void rotate(QFile *file, const QDateTime &now);
};

template <typename T>
void AdvancedRotator<T>::setCurrent(QFile *file)
{
  file->setFileName(currentPath_);
}

template <typename T>
void AdvancedRotator<T>::rotate(QFile *file, const QDateTime &now)
{
  QString newPath = customizer_(currentPath_, now);
  if (newPath != currentPath_) {
    QFileInfo info(newPath);
    auto dir = info.dir();
    if (!dir.exists()) {
      dir.mkpath(dir.path());
    }
    file->rename(newPath);
    setCurrent(file);
  }
}

} // namespace file
} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_FILE_ADVANCEDROTATOR_HPP
