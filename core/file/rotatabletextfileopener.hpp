﻿#ifndef GOUDA_CORE_FILE_ROTATABLETEXTFILEOPENER_HPP
#define GOUDA_CORE_FILE_ROTATABLETEXTFILEOPENER_HPP

#include <QFile>
#include <QTextStream>

namespace gouda {
namespace core {
namespace file {

template <typename Rotator, typename Header>
class RotatableTextFileOpener
{
  Rotator rotator_;
  Header header_;

public:
  RotatableTextFileOpener(Rotator &&rotator, Header &&header)
    : rotator_(std::move(rotator))
    , header_(std::move(header))
  {}

  virtual ~RotatableTextFileOpener() {}

  virtual bool operator ()(QFile *file, QTextStream *str);
};

template <typename Rotator, typename Header>
bool RotatableTextFileOpener<Rotator, Header>::operator ()(
    QFile *file,
    QTextStream *str
    )
{
  if (file->fileName().isEmpty()) {
    rotator_.setCurrent(file);
  }
  rotator_(file);

  bool result = file->open(QFile::Text | QFile::WriteOnly | QFile::Append);
  if (result && file->size() == 0) {
    header_(str);
  }
  return result;
}

class NoHeader
{
public:
  void operator ()(QTextStream *) {}
};

} // namespace file
} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_FILE_ROTATABLETEXTFILEOPENER_HPP
