﻿#ifndef GOUDA_CORE_FILE_TEXTFILEWRITER_HPP
#define GOUDA_CORE_FILE_TEXTFILEWRITER_HPP

#include <gouda/core/file/textstreamwriter.hpp>

#include <QFile>

namespace gouda::core::file {

class TextFileWriter
    : public TextStreamWriter
{
  QSharedPointer<QFile> filePtr_;

public:
  TextFileWriter(QFile *pFile) : TextStreamWriter(pFile), filePtr_(pFile) {}

  TextFileWriter(const TextFileWriter &other)
    : TextStreamWriter(other)
    , filePtr_(other.filePtr_)
  {}

  virtual ~TextFileWriter() override {}
};

} // namespace gouda::core::file

#endif // GOUDA_CORE_FILE_TEXTFILEWRITER_HPP
