﻿#ifndef GOUDA_CORE_FILE_CONSOLEWRITER_HPP
#define GOUDA_CORE_FILE_CONSOLEWRITER_HPP

#include <gouda/core/file/textstreamwriter.hpp>

namespace gouda::core::file {

class ConsoleWriter
    : public TextStreamWriter
{
public:
  ConsoleWriter() : TextStreamWriter(stdout) {}

  virtual ~ConsoleWriter() override {}
};

} // namespace gouda::core::file

#endif // GOUDA_CORE_FILE_CONSOLEWRITER_HPP
