﻿#ifndef GOUDA_CORE_FILE_TIMEROTATOR_HPP
#define GOUDA_CORE_FILE_TIMEROTATOR_HPP

#include <gouda/core/file/rotator.hpp>

#include <QFileInfo>

namespace gouda {
namespace core {
namespace file {

template <typename Compare>
class TimeRotator
    : public Rotator
{
protected:
  Compare compare_;

public:
//  TimeRotator(
//      const QString &currentPath,
//      const QString &templatePath,
//      const QString &dateTimeFormat,
//      const Compare &compare
//      )
//    : Rotator(currentPath, templatePath, dateTimeFormat)
//    , compare_(compare)
//  {}

  TimeRotator() : Rotator() {}

  TimeRotator(
      const QString &currentPath,
      const QString &templatePath,
      const QString &dateTimeFormat,
      Compare &&compare
      )
    : Rotator(currentPath, templatePath, dateTimeFormat)
    , compare_(std::move(compare))
  {}

  virtual ~TimeRotator() {}

  virtual void operator ()(QFile *file);
};

template <typename Compare>
void TimeRotator<Compare>::operator ()(QFile *file)
{
  if (file->exists()) {
    auto now = QDateTime::currentDateTime();
    QFileInfo info(*file);
    auto modified = info.lastModified();
    if (compare_(modified, now)) {
      rotate(file, QDateTime::currentDateTime());
    }
  }
}

} // namespace file
} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_FILE_TIMEROTATOR_HPP
