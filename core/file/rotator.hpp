﻿#ifndef GOUDA_CORE_FILE_ROTATOR_HPP
#define GOUDA_CORE_FILE_ROTATOR_HPP

#include <QString>
#include <QFile>
#include <QDateTime>

namespace gouda {
namespace core {
namespace file {

class Rotator
{
  QString currentPath_;
  QString templatePath_;
  QString dateTimeFormat_;

public:
  Rotator()
    : currentPath_()
    , templatePath_()
    , dateTimeFormat_()
  {}

  Rotator(
      const QString &currentPath,
      const QString &templatePath,
      const QString &dateTimeFormat
      );

  virtual ~Rotator() {}

  const QString &currentPath() const { return currentPath_; }
  const QString &templatePath() const { return templatePath_; }
  const QString &dateTimeFormat() const { return dateTimeFormat_; }

  virtual void setCurrent(QFile *file);

  virtual void rotate(QFile *file, const QDateTime &now);
};

inline Rotator::Rotator(
    const QString &currentPath,
    const QString &templatePath,
    const QString &dateTimeFormat
    )
  : currentPath_(currentPath)
  , templatePath_(templatePath)
  , dateTimeFormat_(dateTimeFormat)
{}

inline void Rotator::setCurrent(QFile *file)
{
  file->setFileName(currentPath_);
}

inline void Rotator::rotate(QFile *file, const QDateTime &now)
{
  file->rename(
        templatePath()
        .arg(now.toString(dateTimeFormat()))
        );
  setCurrent(file);
}

} // namespace file
} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_FILE_ROTATOR_HPP
