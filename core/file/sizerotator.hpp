﻿#ifndef GOUDA_CORE_FILE_SIZEROTATOR_HPP
#define GOUDA_CORE_FILE_SIZEROTATOR_HPP

#include <gouda/core/file/rotator.hpp>

namespace gouda {
namespace core {
namespace file {

template <typename T = Rotator>
class SizeRotator
//    : public Rotator
{
  qint64 threshold_;
  T rotator_;

public:
//  SizeRotator(
//      const QString &currentPath,
//      const QString &templatePath,
//      const QString &dateTimeFormat,
//      qint64 threshold
//      );

  SizeRotator(qint64 threshold, T &&rotator);

  virtual ~SizeRotator() {}

  void setCurrent(QFile *file);

  void operator ()(QFile *file);
};

//SizeRotator::SizeRotator(
//    const QString &currentPath,
//    const QString &templatePath,
//    const QString &dateTimeFormat,
//    qint64 threshold
//    )
//  : Rotator(currentPath, templatePath, dateTimeFormat)
//  , threshold_(threshold)
//{}

template <typename T>
SizeRotator<T>::SizeRotator(qint64 threshold, T &&rotator)
  : threshold_(threshold)
  , rotator_(std::move(rotator))
{}

template <typename T>
void SizeRotator<T>::setCurrent(QFile *file)
{
  rotator_.setCurrent(file);
}

template <typename T>
void SizeRotator<T>::operator ()(QFile *file)
{
  if (file->size() > threshold_) {
    rotator_.rotate(file, QDateTime::currentDateTime());
  }
}

} // namespace file
} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_FILE_SIZEROTATOR_HPP
