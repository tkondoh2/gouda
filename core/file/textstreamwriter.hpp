﻿#ifndef GOUDA_CORE_FILE_TEXTSTREAMWRITER_HPP
#define GOUDA_CORE_FILE_TEXTSTREAMWRITER_HPP

#include <QSharedPointer>
#include <QTextStream>
#include <QTextCodec>
#include <QReadWriteLock>
#include <QWriteLocker>

namespace gouda::core::file {

class TextStreamWriter
{
  QSharedPointer<QTextStream> str_;
  QReadWriteLock lock_;

  inline static QIODevice::OpenMode getOpenMode(bool append)
  {
    QIODevice::OpenMode openMode = QIODevice::Text | QIODevice::WriteOnly;
    if (append) openMode |= QIODevice::Append;
    return openMode;
  }

public:
  TextStreamWriter() : str_() {}

  TextStreamWriter(QIODevice *device) : str_(new QTextStream(device)) {}

  TextStreamWriter(FILE *fileHandle, bool append = false)
    : str_(new QTextStream(fileHandle, getOpenMode(append)))
  {}

  TextStreamWriter(QString *string, bool append = false)
    : str_(new QTextStream(string, getOpenMode(append)))
  {}

  TextStreamWriter(QByteArray *array, bool append = false)
    : str_(new QTextStream(array, getOpenMode(append)))
  {}

  TextStreamWriter(const TextStreamWriter &other)
    : str_(other.str_)
    , lock_()
  {}

  virtual ~TextStreamWriter() {}

  void setCodec(QTextCodec *codec)
  {
    str_->setCodec(codec);
  }

  void operator ()(const QString &text, bool bLF = true, bool bFlush = true)
  {
    QWriteLocker locker(&lock_);
    *str_ << text;
    if (bLF) *str_ << '\n';
    if (bFlush) str_->flush();
  }
};

} // namespace gouda::core::file

#endif // GOUDA_CORE_FILE_TEXTSTREAMWRITER_HPP
