﻿#ifndef GOUDA_CORE_FILE_COMPARE_HPP
#define GOUDA_CORE_FILE_COMPARE_HPP

#include <QDateTime>

namespace gouda {
namespace core {
namespace file {

class CompareMinutes
{
public:
  bool operator ()(const QDateTime &lastModified, const QDateTime &now)
  {
    auto a = lastModified.toString("yyyyMMddHHmm");
    auto b = now.toString("yyyyMMddHHmm");
    return b > a;
  }
};

class CompareHours
{
public:
  bool operator ()(const QDateTime &lastModified, const QDateTime &now)
  {
    auto a = lastModified.toString("yyyyMMddHH");
    auto b = now.toString("yyyyMMddHH");
    return b > a;
  }
};

class CompareDays
{
public:
  bool operator ()(const QDateTime &lastModified, const QDateTime &now)
  {
    return lastModified.daysTo(now) > 0;
  }
};

} // namespace file
} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_FILE_COMPARE_HPP
