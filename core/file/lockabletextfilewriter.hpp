﻿#ifndef GOUDA_CORE_FILE_LOCKABLETEXTFILEWRITER_HPP
#define GOUDA_CORE_FILE_LOCKABLETEXTFILEWRITER_HPP

#include <QSharedPointer>
#include <QFile>
#include <QTextStream>
#include <QReadWriteLock>

using QFilePtr = QSharedPointer<QFile>;
using QTextStreamPtr = QSharedPointer<QTextStream>;

namespace gouda {
namespace core {
namespace file {

template <typename Opener>
class LockableTextFileWriter
{
  static QReadWriteLock lock_;
  Opener opener_;
  QFilePtr file_;
  QTextStreamPtr str_;

public:
  LockableTextFileWriter(const Opener &opener);

  LockableTextFileWriter(Opener &&opener);

  QTextStreamPtr &str() { return str_; }

  bool open();

  void operator ()(const QString &text);
};

template <typename Opener>
LockableTextFileWriter<Opener>::LockableTextFileWriter(const Opener &opener)
  : opener_(opener)
  , file_(new QFile)
  , str_(new QTextStream)
{
  str_->setDevice(file_.get());
}

template <typename Opener>
LockableTextFileWriter<Opener>::LockableTextFileWriter(Opener &&opener)
  : opener_(std::move(opener))
  , file_(new QFile)
  , str_(new QTextStream)
{
  str_->setDevice(file_.get());
}

template <typename Opener>
bool LockableTextFileWriter<Opener>::open()
{
  return opener_(file_.get(), str_.get());
}

template <typename Opener>
void LockableTextFileWriter<Opener>::operator ()(const QString &text)
{
  QWriteLocker locker(&lock_);
  open();
  *str_ << text << endl;
  file_->close();
}

template <typename Opener>
QReadWriteLock LockableTextFileWriter<Opener>::lock_;

} // namespace file
} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_FILE_LOCKABLETEXTFILEWRITER_HPP
