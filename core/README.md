﻿Qt Extension Library
====================

# History

## v0.0.28 (to v1.0.0)

* loggerv2::Logger::getThreadId()をthreads.hppの独立した関数に移行。

## v0.0.27 (to v1.0.0)

* class Exception
* class file::TextStreamWriter
* class file::ConsoleWriter
* class file::TextFileWriter
* template<Formatter,Writer> class loggerv2::TextWriterSink
* mime::decodeMimeHeader(QString) ロジックの見直し
* mime::Rfc2231::codec(QString) ロジックの見直し QUrl::fromPercentEncodingで変換した値は、すでにUTF-8になっている
* namespace gouda::core::argument(gca)
* template<T> class gca::Arguments
* template<T> class gca::Value
* class gca::String

## v0.0.26 (to v1.0.0)

* mimeのコーデック定義関数内で使用するパターン比較処理を関数化する。
* mime::ShiftJISを復活、定義パターンにいくつかの別名を追加する。
* mime::ISO2202JPをmime::JISとして復活、定義パターンにJIS,ISO 2202-JPを設定する。
* mime::codec()でベタ打ちしていたコーデックリストをQTextCodec::availableCodecs()からの取得に切り替える。

## v0.0.25 (to v1.0.0)

* LockableTextFileWriter::lock_定義を追加

## v0.0.24 (to v1.0.0)

* mime::Rfc2231

## v0.0.23 (to v1.0.0)

* mime::US_ASCII
* mime::UTF8
* template<T> bool mime::codecTo(QString&,ByteArray&,QString&)
* QString mime::standardize(QString&)
* bool mime::codec(QString&,QByteArray&,QString&)
* mime::Base64
* mime::Quoted
* template<T> bool mime::decodeByteArray(QString&,QByteArray&,QString&)
* QString mime::decodeEncodingPart(QString&,QString&, QString&)
* QString mime::decodeMimeHeader(QString&)

## v0.0.21 (to v1.0.0)

* for_range<T,F>()
* each<T,C,F,CT>() => stdにないか確認する。
* every<T,C,F,CT>() => std::all_ofを検討する。
* some<T,C,F,CT>() => std::any_ofを検討する。
* File::unifyDelimiter()
* file::AdvancedTimeFormatter
* file::AdvancedCustomizer<T>
* file::AdvancedRotator<T>
* file::CompareMinutes
* file::CompareHours
* file::CompareDays
* file::LockableTextFileWriter<T>
* file::RotatableTextFileOpener<T>
* file::Rotator
* file::SizeRotator<T>
* file::TimeRotator<T>
* logger::v2 => loggerv2
* loggerv2::Formatter
* loggerv2::Level enum
* loggerv2::levelText()
* loggerv2::Logger
* loggerv2::Sink
* loggerv2::TextFileSink
* csvv2::Column
* csvv2::ColumnBase
* csvv2::ColumnString
* csvv2::ColumnStringList
* csvv2::ColumnInt
* csvv2::ColumnDate
* csvv2::ColumnTime
* csvv2::ColumnDateTime
* csvv2::Record
* csvv2::StringifyBase
* csvv2::StringifyString
* csvv2::StringifyStringList
* csvv2::StringifyInt
* csvv2::StringifyDate
* csvv2::StringifyTime
* csvv2::StringifyDateTime
* csvv2::WriteHeader

## v0.0.20 (to v1.0.0)

* GetProcessFileNameを追加。
* CustomTextWriterでのWriteFileを、属性から継承元に変更。
* logger::LEVEL::ERRORがコンパイルできなくなったので、Errorとし、他の値もキャメル表記に変更。
* logger::v2を追加(但し使用するかは未定)

## v0.0.18 (to v1.0.0)

* csv::Record::columnCount() -> int 追加
* csv::Record::appendDouble(double) -> void 修正
* csv::Record::appendDateTime(QDateTime) -> void 修正
* csv::Record::appendString(QString,bool) -> void 修正

## v0.0.15 (to v1.0.0)

* WriteFileにコーデック指定機能を追加。
* CustomTextWriterにコーデック指定機能を追加。

## v0.0.12 (to v1.0.0)

* class QuotedPrintable

## v0.0.11 (to v1.0.0)

* class CustomizeFilePathByDateTime
* class CustomizeTimeFormat
* tclass CustomTextWriter
* class DefaultCustomTextWriter
* class File
* tclass RotateFilePath
* class WriteFile
* class csv::Column
* tclass csv::ColumnBase
* class csv::InitCSVFile
* class csv::Record
* class csv::RotateCSVFilePath
* tclass csv::Writer
* tclass logger::FileSink
* enum class logger::LEVEL
* class logger::Logger
* iclass logger::Sink
