﻿#ifndef GOUDA_CORE_VERSION_H
#define GOUDA_CORE_VERSION_H

#include "core_global.h"

namespace gouda {
namespace core {

class GOUDA_CORESHARED_EXPORT Version
{
public:
  static uint major();
  static uint minor();
  static uint patch();
  static const char *ver();
};

} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_VERSION_H
