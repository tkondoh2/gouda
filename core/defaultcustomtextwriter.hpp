﻿#ifndef GOUDA_CORE_DEFAULTCUSTOMTEXTWRITER_HPP
#define GOUDA_CORE_DEFAULTCUSTOMTEXTWRITER_HPP

#include <gouda/core/writefile.hpp>

namespace gouda {
namespace core {

class DefaultCustomTextWriter
{
public:
  bool operator ()(WriteFile &) noexcept
  {
    return true;
  }
};

} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_DEFAULTCUSTOMTEXTWRITER_HPP
