﻿#ifndef GOUDA_CORE_FILE_HPP
#define GOUDA_CORE_FILE_HPP

#include <QSharedPointer>
#include <QString>
#include <QFile>
#include <QFileInfo>
#include <QDir>

namespace gouda {
namespace core {

class File
{
  QSharedPointer<QFile> pFile_;
  QString filePath_;
  QFile::OpenMode openMode_;

public:
  static const char DELIMITER = '/';

  File()
    : pFile_(QSharedPointer<QFile>::create())
    , filePath_()
    , openMode_()
  {}

  File(const QString &filePath, QFile::OpenMode openMode)
    : pFile_(QSharedPointer<QFile>::create(filePath))
    , filePath_(filePath)
    , openMode_(openMode)
  {}

  virtual ~File()
  {
    close();
    pFile_.clear();
  }

  QSharedPointer<QFile> &pFile() noexcept { return pFile_; }

  QString filePath() const { return filePath_; }

  void setFilePath(const QString& filePath)
  {
    filePath_ = filePath;
    if (pFile_)
      pFile_->setFileName(filePath_);
  }

  virtual bool open() noexcept
  {
    if (!pFile_) {
      if (!filePath_.isEmpty()) {
        pFile_.reset(new QFile(filePath_));
      } else {
        return false;
      }
    } else if (pFile_->isOpen()) {
      return true;
    }
    return pFile_->open(openMode_);
  }

  virtual void close() noexcept
  {
    if (isOpen()) {
      pFile_->close();
    }
  }

  bool isOpen() const noexcept
  {
    return pFile_ && pFile_->isOpen();
  }

  virtual qint64 size() const noexcept
  {
    return !pFile_ || !pFile_->exists() ? 0 : pFile_->size();
  }

  virtual bool isEmpty() const noexcept { return size() == 0; }

  virtual bool rename(const QString &newPath) noexcept
  {
    if (!pFile_)
      return false;

    QFileInfo fileInfo(newPath);
    QDir dir(fileInfo.absoluteDir());
    if (!dir.exists()) {
      dir.mkpath(".");
    }
    if (pFile_->rename(newPath)) {
      setFilePath(newPath);
      return true;
    }
    return false;
  }

  static QString addDelimiter(const QString &path) noexcept
  {
    const QString delimiter(DELIMITER);
    if (path.right(delimiter.count()) != delimiter) {
      return path + delimiter;
    }
    return path;
  }

  static QString unifyDelimiter(const QString &path) noexcept
  {
    const QString delimiter(DELIMITER);
    QString p = path;
    p.replace("\\", delimiter);
    return p;
  }
};

} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_FILE_HPP
