﻿#ifndef GOUDA_CORE_LOGGERV2_LEVEL_HPP
#define GOUDA_CORE_LOGGERV2_LEVEL_HPP

#include <QString>
#include <QList>

namespace gouda {
namespace core {
namespace loggerv2 {

enum class Level {
    Fatal,
    Error,
    Warning,
    Info,
    Debug,
    Trace,
    Max_Level
  };

inline const QString &levelText(Level level)
{
  static const QList<QString> list{
    "Fatal",
    "Error",
    "Warning",
    "Info",
    "Debug",
    "Trace"
  };
  return list[static_cast<int>(level)];
}

} // namespace loggerv2
} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_LOGGERV2_LEVEL_HPP
