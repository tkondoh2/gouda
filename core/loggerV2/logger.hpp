﻿#ifndef GOUDA_CORE_LOGGERV2_LOGGER_HPP
#define GOUDA_CORE_LOGGERV2_LOGGER_HPP

#include <gouda/core/loggerV2/level.hpp>
#include <gouda/core/loggerV2/sink.hpp>
#include <gouda/core/threads.hpp>

#include <QVector>

namespace gouda {
namespace core {
namespace loggerv2 {

/**
 * @brief ロガークラス
 * @note 通常、このオブジェクトをシングルトン化して使用する。
 */
class Logger
{
  QVector<SinkPtr> sinkList_; ///< ロガーシンクリスト

protected:
  /**
   * @brief ログ処理をする。
   * @param level ログレベル
   * @param msg メッセージ
   */
  void output(Level level, const QString &msg) const;

public:
  Logger();

  bool sinksIsEmpty() const { return sinkList_.isEmpty(); }

  /**
   * @brief ロガーシンクを追加する。
   * @param pSink ロガーシンクへのポインタ
   */
  void appendSink(Sink *pSink);

  /**
   * @brief 重大なエラーレベルでログ出力する。
   * @param msg メッセージ
   */
  void fatal(const QString &msg) const { this->output(Level::Fatal, msg); }

  /**
   * @brief エラーレベルでログ出力する。
   * @param msg メッセージ
   */
  void error(const QString &msg) const { this->output(Level::Error, msg); }

  /**
   * @brief 警告レベルでログ出力する。
   * @param msg メッセージ
   */
  void warning(const QString &msg) const { this->output(Level::Warning, msg); }

  /**
   * @brief 情報レベルでログ出力する。
   * @param msg メッセージ
   */
  void info(const QString &msg) const { this->output(Level::Info, msg); }

  /**
   * @brief デバッグレベルでログ出力する。
   * @param msg メッセージ
   */
  void debug(const QString &msg) const { this->output(Level::Debug, msg); }

  /**
   * @brief 詳細なトレースレベルでログ出力する。
   * @param msg メッセージ
   */
  void trace(const QString &msg) const { this->output(Level::Trace, msg); }
};

inline Logger::Logger()
  : sinkList_()
{}

inline void Logger::appendSink(Sink *pSink)
{
  sinkList_.append(SinkPtr(pSink));
}

inline void Logger::output(Level level, const QString &msg) const
{
  auto now = QDateTime::currentDateTime();
  auto threadId = getThreadId();
  foreach (SinkPtr sink, sinkList_) {
    sink->output(now, threadId, level, msg);
  }
}

} // namespace loggerv2
} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_LOGGERV2_LOGGER_HPP
