﻿#ifndef GOUDA_CORE_LOGGERV2_SINK_HPP
#define GOUDA_CORE_LOGGERV2_SINK_HPP

#include <gouda/core/loggerV2/level.hpp>

#include <QDateTime>
#include <QSharedPointer>

namespace gouda {
namespace core {
namespace loggerv2 {

class Sink
{
  Level threshold_;

public:
  Sink()
    : threshold_(Level::Info)
  {}

  Sink(Level threshold)
    : threshold_(threshold)
  {}

  virtual ~Sink() {}

  Level threshold() const { return threshold_; }

  void setThreshold(Level threshold) { threshold_ = threshold; }

  virtual void _output(
      const QDateTime &time,
      const QString &threadId,
      Level level,
      const QString &msg
      ) = 0;

  void output(
      const QDateTime &time,
      const QString &threadId,
      Level level,
      const QString &msg
      )
  {
    if (level <= threshold()) {
      _output(time, threadId, level, msg);
    }
  }
};

using SinkPtr = QSharedPointer<Sink>;

} // namespace loggerv2
} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_LOGGERV2_SINK_HPP
