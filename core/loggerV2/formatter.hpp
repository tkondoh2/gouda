﻿#ifndef GOUDA_CORE_LOGGERV2_FORMATTER_HPP
#define GOUDA_CORE_LOGGERV2_FORMATTER_HPP

#include <gouda/core/loggerV2/level.hpp>

#include <QDateTime>

namespace gouda {
namespace core {
namespace loggerv2 {

class Formatter
{
  QString pattern_;
  QString timeFormat_;

public:
  Formatter()
    : pattern_("${time} (${thread}) [${level}] ${msg}")
    , timeFormat_("yyyy/MM/dd HH:mm:ss.zzz")
  {}

  Formatter(const QString &pattern, const QString &timeFormat)
    : pattern_(pattern)
    , timeFormat_(timeFormat)
  {}

  Formatter(QString &&pattern, QString &&timeFormat)
    : pattern_(std::move(pattern))
    , timeFormat_(std::move(timeFormat))
  {}

  QString operator ()(
      const QDateTime &time,
      const QString &threadId,
      Level level,
      const QString &msg
      ) const;
};

inline QString Formatter::operator ()(
    const QDateTime &time,
    const QString &threadId,
    Level level,
    const QString &msg
    ) const
{
  auto tmpl = pattern_;
  tmpl.replace("${time}", time.toString(timeFormat_))
      .replace("${thread}", threadId)
      .replace("${level}", levelText(level))
      .replace("${msg}", msg);
  return tmpl;
}

} // namespace loggerv2
} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_LOGGERV2_FORMATTER_HPP
