﻿#ifndef GOUDA_CORE_LOGGERV2_TEXTFILESINK_HPP
#define GOUDA_CORE_LOGGERV2_TEXTFILESINK_HPP

#include <gouda/core/loggerV2/sink.hpp>

namespace gouda {
namespace core {
namespace loggerv2 {

template <typename Formatter, typename Writer>
class TextFileSink
    : public Sink
{
  Formatter formatter_;
  Writer writer_;

public:
  TextFileSink(Formatter &&formatter, Writer &&writer);

  TextFileSink(Level threshold, Formatter &&formatter, Writer &&writer);

  virtual ~TextFileSink() override {}

  virtual void _output(
      const QDateTime &time,
      const QString &threadId,
      Level level,
      const QString &msg
      ) override;
};

template <typename Formatter, typename Writer>
TextFileSink<Formatter, Writer>::TextFileSink(
    Formatter &&formatter,
    Writer &&writer
    )
  : Sink()
  , formatter_(std::move(formatter))
  , writer_(std::move(writer))
{}

template <typename Formatter, typename Writer>
TextFileSink<Formatter, Writer>::TextFileSink(
    Level threshold,
    Formatter &&formatter,
    Writer &&writer
    )
  : Sink(threshold)
  , formatter_(std::move(formatter))
  , writer_(std::move(writer))
{}

template <typename Formatter, typename Writer>
void TextFileSink<Formatter, Writer>::_output(
    const QDateTime &time,
    const QString &threadId,
    Level level,
    const QString &msg
    )
{
  writer_(formatter_(time, threadId, level, msg));
}

} // namespace loggerv2
} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_LOGGERV2_TEXTFILESINK_HPP
