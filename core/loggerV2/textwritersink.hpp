﻿#ifndef GOUDA_CORE_LOGGERV2_TEXTWRITERSINK_HPP
#define GOUDA_CORE_LOGGERV2_TEXTWRITERSINK_HPP

#include <gouda/core/loggerV2/sink.hpp>

namespace gouda::core::loggerv2 {

template <typename Formatter, typename Writer>
class TextWriterSink
    : public Sink
{
  Formatter formatter_;
  Writer writer_;

public:
  TextWriterSink() : Sink(), formatter_(), writer_() {}

  TextWriterSink(Level threshold)
    : Sink(threshold)
    , formatter_()
    , writer_()
  {}

  TextWriterSink(Level threshold, Writer &&writer)
    : Sink(threshold)
    , formatter_()
    , writer_(std::move(writer))
  {}

  TextWriterSink(Level threshold, Formatter &&formatter, Writer &&writer)
    : Sink(threshold)
    , formatter_(std::move(formatter))
    , writer_(std::move(writer))
  {}

  virtual ~TextWriterSink() override {}
  /**
   * @brief 出力用仮想関数
   * @param time 時間
   * @param threadId スレッドID
   * @param level 出力レベル
   * @param msg メッセージ
   */
  virtual void _output(
      const QDateTime &time,
      const QString &threadId,
      Level level,
      const QString &msg
      ) override
  {
    writer_(formatter_(time, threadId, level, msg));
  }
};

} // namespace gouda::core::loggerv2

#endif // GOUDA_CORE_LOGGERV2_TEXTWRITERSINK_HPP
