﻿#ifndef GOUDA_CORE_CUSTOMIZEFILEPATHBYDATETIME_HPP
#define GOUDA_CORE_CUSTOMIZEFILEPATHBYDATETIME_HPP

#include <gouda/core/customizetimeformat.hpp>
#include <gouda/core/file.hpp>

#include <QString>
#include <QDateTime>
#include <QFileInfo>

namespace gouda {
namespace core {

class CustomizeFilePathByDateTime
{
public:
  CustomizeFilePathByDateTime() {}

  QString operator ()(
      const QString &filePath,
      const QDateTime &dateTime = QDateTime::currentDateTime(),
      const QString &baseTemplate = "${currentBase}-%Y%M%D_%h%m%s_%z",
      const QString &directoryTemplate = "${currentDir}"
      ) noexcept
  {
    QFileInfo fileInfo(filePath);
    QString dirName = fileInfo.absolutePath();
    QString baseName = fileInfo.completeBaseName();
    QString suffix = fileInfo.completeSuffix();

    QString customDir = directoryTemplate;
    QString customBase = baseTemplate;

    customDir.replace("${currentDir}", dirName);
    customBase.replace("${currentBase}", baseName);

    CustomizeTimeFormat customizeTimeFormat;
    customDir = customizeTimeFormat(customDir, dateTime);
    customBase = customizeTimeFormat(customBase, dateTime);

    return QString("%1%2.%3")
        .arg(File::addDelimiter(customDir))
        .arg(customBase)
        .arg(suffix);
  }
};

} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_CUSTOMIZEFILEPATHBYDATETIME_HPP
