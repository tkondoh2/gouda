﻿#ifndef GOUDA_CORE_CSV_COLUMN_HPP
#define GOUDA_CORE_CSV_COLUMN_HPP

#include <QSharedPointer>

namespace gouda {
namespace core {
namespace csv {

class Column;
using ColumnPtr = QSharedPointer<Column>;

class Column
{
public:
  virtual ~Column(){}
  virtual QString toString() const noexcept = 0;

  static QString escape(const QString &value) noexcept
  {
    QString v = value;
    v.replace("\"", "\"\"");
    return v;
  }

  static QString quot(const QString &value) noexcept
  {
    return QString("\"%1\"").arg(value);
  }
};

template <typename T, typename ToString>
class ColumnBase
    : public Column
{
  T value_;
  ToString toStr_;

public:
  ColumnBase(const T &value, ToString f)
    : Column()
    , value_(value)
    , toStr_(f)
  {}

  virtual ~ColumnBase() {}

  virtual QString toString() const noexcept
  {
    return toStr_(value_);
  }
};

} // namespace csv
} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_CSV_COLUMN_HPP
