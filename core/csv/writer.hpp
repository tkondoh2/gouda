﻿#ifndef GOUDA_CORE_CSV_WRITER_HPP
#define GOUDA_CORE_CSV_WRITER_HPP

#include <gouda/core/csv/record.hpp>

namespace gouda {
namespace core {
namespace csv {

template <class T>
class Writer
{
  T writer_;

public:
  Writer(T &&writer)
    : writer_(writer)
  {}

  void write(const Record &rec, bool bClose = false) noexcept
  {
    if (writer_.open()) {
      writer_.write(rec.toRowString());
      if (bClose) writer_.close();
    }
//    if (writer_.open()) {
//      writer_.file().write(rec.toRowString());
//      if (bClose) writer_.file().close();
//    }
  }
};

} // namespace csv
} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_CSV_WRITER_HPP
