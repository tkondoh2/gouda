﻿#ifndef GOUDA_CORE_CSV_ROTATECSVFILEPATH_HPP
#define GOUDA_CORE_CSV_ROTATECSVFILEPATH_HPP

#include <gouda/core/rotatefilepath.hpp>

#include <gouda/core/csv/initcsvfile.hpp>

#include <QStringList>

namespace gouda {
namespace core {
namespace csv {

class RotateCSVFilePath
    : public RotateFilePath<InitCSVFile>
{
public:
  RotateCSVFilePath(
      qint64 maxSize,
      QStringList &&headers,
      const QString &baseTemplate = "${currentBase}-%Y%M%D_%h%m%s_%z",
      const QString &directoryTemplate = "${currentDir}"
      )
    : RotateFilePath<InitCSVFile>(
        maxSize,
        InitCSVFile(std::move(headers)),
        baseTemplate,
        directoryTemplate
        )
  {}
};

} // namespace csv
} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_CSV_ROTATECSVFILEPATH_HPP
