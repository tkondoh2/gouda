﻿#ifndef GOUDA_CORE_CSV_INITCSVFILE_HPP
#define GOUDA_CORE_CSV_INITCSVFILE_HPP

#include <gouda/core/writefile.hpp>

#include <QStringList>

namespace gouda {
namespace core {
namespace csv {

class InitCSVFile
{
  QStringList headers_;

public:
  InitCSVFile(QStringList &&headers)
    : headers_(headers)
  {}

  void operator ()(WriteFile &file) noexcept
  {
    file.write(headers_.join(','));
  }
};

} // namespace csv
} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_CSV_INITCSVFILE_HPP
