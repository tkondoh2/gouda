﻿#ifndef GOUDA_CORE_CSV_RECORD_HPP
#define GOUDA_CORE_CSV_RECORD_HPP

#include <gouda/core/csv/column.hpp>

#include <QString>
#include <QDateTime>

namespace gouda {
namespace core {
namespace csv {

class Record
{
  QList<ColumnPtr> columnList_;

public:
  Record() {}

  virtual ~Record()
  {
    columnList_.clear();
  }

  int columnCount() const { return columnList_.count(); }

  QString toRowString() const noexcept
  {
    QStringList list;
    foreach (ColumnPtr col, columnList_) {
      list.append(col->toString());
    }
    return list.join(',');
  }

  void appendDouble(double value) noexcept
  {
    auto f = [](double v) {
      return Column::escape(QString::number(v));
    };
    columnList_.append(ColumnPtr(new ColumnBase<double, decltype(f)>(value, f)));
  }

  void appendDateTime(const QDateTime &value) noexcept
  {
    auto f = [](QDateTime v) {
      return Column::quot(Column::escape(v.toString("yyyy/MM/dd HH:mm:ss.zzz")));
    };
    columnList_.append(
          ColumnPtr(new ColumnBase<QDateTime, decltype(f)>(value, f))
          );
  }

  void appendString(const QString &value, bool doEscape = true) noexcept
  {
    auto f = [doEscape](QString v) {
      if (doEscape) { v = Column::escape(v); }
      return Column::quot(v);
    };
    ColumnPtr col(new ColumnBase<QString, decltype(f)>(value, f));
    columnList_.append(col);
  }
};

} // namespace csv
} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_CSV_RECORD_HPP
