#-------------------------------------------------
#
# Project created by QtCreator 2019-08-08T19:18:30
#
#-------------------------------------------------

QT       -= gui

TARGET = gouda_core
TEMPLATE = lib

DEFINES += GOUDA_CORE_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11 c++14

win32: QMAKE_CXXFLAGS += /utf-8

include(../../environment.pri)

#
# RxCpp
#
INCLUDEPATH += $$RxCppPath/Rx/v2/src
DEPENDPATH += $$RxCppPath/Rx/v2/src

#
# Notes C API
#
#include(../notescapi_cmp.pri)

TargetProduct = GoudaCoreLib
TargetDescription = Gouda Core Library
TargetVersion = 1.0.0
TargetCompany = Chiburu Systems
TargetDomain = chiburu.com
TargetCopyright = 2019 Chiburu Systems
include(../../version.pri)

INCLUDEPATH -= $$PWD
DEPENDPATH -= $$PWD

INCLUDEPATH += $$PWD/../..
DEPENDPATH += $$PWD/../..

SOURCES += \
        version.cpp

HEADERS += \
        argument/arguments.hpp \
        argument/string.hpp \
        argument/value.hpp \
        csv/column.hpp \
        csv/initcsvfile.hpp \
        csv/record.hpp \
        csv/rotatecsvfilepath.hpp \
        csv/writer.hpp \
        csvV2/column.hpp \
        csvV2/columns.hpp \
        csvV2/record.hpp \
        csvV2/stringify.hpp \
        csvV2/writeheader.hpp \
        customizefilepathbydatetime.hpp \
        customizetimeformat.hpp \
        customtextwriter.hpp \
        defaultcustomtextwriter.hpp \
        exception.hpp \
        file.hpp \
        file/advancedrotator.hpp \
        file/compare.hpp \
        file/consolewriter.hpp \
        file/lockabletextfilewriter.hpp \
        file/rotatabletextfileopener.hpp \
        file/rotator.hpp \
        file/sizerotator.hpp \
        file/textfilewriter.hpp \
        file/textstreamwriter.hpp \
        file/timerotator.hpp \
        getprocessfilename.hpp \
        logger/filesink.hpp \
        logger/level.hpp \
        logger/logger.hpp \
        logger/sink.hpp \
        loggerV2/formatter.hpp \
        loggerV2/level.hpp \
        loggerV2/logger.hpp \
        loggerV2/sink.hpp \
        loggerV2/textfilesink.hpp \
        loggerV2/textwritersink.hpp \
        mime/codec.hpp \
        mime/decode.hpp \
        mime/rfc2231.hpp \
        quotedprintable.hpp \
        rotatefilepath.hpp \
        threads.hpp \
        utils.hpp \
        version.h \
        core_global.h \
        writefile.hpp

DISTFILES += \
    README.md
