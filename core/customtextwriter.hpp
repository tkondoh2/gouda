﻿#ifndef GOUDA_CORE_CUSTOMTEXTWRITER_HPP
#define GOUDA_CORE_CUSTOMTEXTWRITER_HPP

#include <gouda/core/writefile.hpp>

#include <QString>

namespace gouda {
namespace core {

template <class BeforeOpen, class AfterOpen>
class CustomTextWriter
    : public WriteFile
{
  BeforeOpen beforeOpen_;
  AfterOpen afterOpen_;

public:
  CustomTextWriter(BeforeOpen beforeOpen, AfterOpen afterOpen)
    : WriteFile()
    , beforeOpen_(beforeOpen)
    , afterOpen_(afterOpen)
  {}

  CustomTextWriter(
      const QString &filePath,
      QFile::OpenMode openMode,
      BeforeOpen beforeOpen,
      AfterOpen afterOpen
      )
    : WriteFile(filePath, openMode)
    , beforeOpen_(beforeOpen)
    , afterOpen_(afterOpen)
  {}

  CustomTextWriter(
      const QString &filePath,
      QFile::OpenMode openMode,
      QTextCodec *pCodec,
      BeforeOpen beforeOpen,
      AfterOpen afterOpen
      )
    : WriteFile(filePath, openMode, pCodec)
    , beforeOpen_(beforeOpen)
    , afterOpen_(afterOpen)
  {}

  virtual ~CustomTextWriter() {}

  virtual bool open() noexcept
  {
    if (beforeOpen_(*this)) {
      if (WriteFile::open()) {
        afterOpen_(*this);
        return true;
      }
    }
    return false;
  }

//  WriteFile &file() { return file_; }
};

} // namespace core
} // namespace gouda

#endif // GOUDA_CORE_CUSTOMTEXTWRITER_HPP
