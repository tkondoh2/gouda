#
# Notes C API Compiler Defines
#

DEFINES += PRODUCTION_VERSION
!macx {
  DEFINES += W32 W DTRACE
}
!win32 {
  DEFINES += HANDLE_IS_32BITS LARGE64_FILES OVERRIDEDEBUG
}
win32 {
  DEFINES += NT _CRT_SECURE_NO_WARNINGS DUMMY
  QMAKE_CXXFLAGS += -wd4503 -wd4005
  contains(QMAKE_TARGET.arch, x86_64) {
    DEFINES += W64 ND64 _AMD64_ ND64SERVER
  } else {
    DEFINES += ND32 _X86_
  }
} else:macx {
  DEFINES += MAC MAC_OSX MAC_CARBON NO_NULL_VTABLE_ENTRY __CF_USE_FRAMEWORK_INCLUDES__ TARGET_API_MAC_CARBON
  DEFINES += LONGIS64BIT
} else:unix {
  DEFINES += UNIX LINUX LINUX86 GCC3 GCC4 GCC_LBLB_NOT_SUPPORTED PTHREAD_KERNEL _REENTRANT USE_THREADSAFE_INTERFACES _POSIX_THREAD_SAFE_FUNCTIONS HAS_IOCP HAS_BOOL HAS_DLOPEN USE_PTHREAD_INTERFACES _LARGEFILE_SOURCE _LARGEFILE64_SOURCE
  contains(QMAKE_TARGET.arch, x86_64) {
   DEFINES += ND64 LINUX64 LINUX86_64 NDUNIX64 LONGIS64BIT
  }
  LIBS += -Wl,-rpath,$$NotesLibsPath
}
INCLUDEPATH += $$NotesCAPIPath/include
DEPENDPATH += $$NotesCAPIPath/include
LIBS += -lnotes -L$$NotesLibsPath
