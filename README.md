﻿Gouda Project for Notes/Domino C API wrapped Modern C++
=======================================================

# History

## v0.0.28 (to v1.0.0)

* coreライブラリ修正

## v0.0.27 (to v1.0.0)

* coreライブラリ修正

## v0.0.26 (to v1.0.0)

* coreライブラリ修正

## v0.0.25 (to v1.0.0)

* coreライブラリ修正
* notesライブラリ修正

## v0.0.24 (to v1.0.0)

* coreライブラリ修正

## v0.0.23 (to v1.0.0)

* coreライブラリ修正
* notesライブラリ修正

## v0.0.22-1 (to v1.0.0)

* notesライブラリ修正

## v0.0.22 (to v1.0.0)

* notesライブラリ修正

## v0.0.21 (to v1.0.0)

* coreライブラリ修正
* notesライブラリ修正

## v0.0.20 (to v1.0.0)

* coreライブラリ修正
* notesライブラリ修正
* opensslライブラリ修正

## v0.0.19 (to v1.0.0)

* Doxygen導入
* notesライブラリ修正

## v0.0.18 (to v1.0.0)

* coreライブラリ修正
* notesライブラリ修正

## v0.0.17 (to v1.0.0)

* notesライブラリ修正

## v0.0.16 (to v1.0.0)

* notesライブラリ修正

## v0.0.15 (to v1.0.0)

* coreライブラリ修正
* notesライブラリ修正

## v0.0.14 (to v1.0.0)

* notesライブラリ修正

## v0.0.13 (to v1.0.0)

* platform_env.pri => environment.pri に統一
* notesライブラリ修正

## v0.0.12 (to v1.0.0)

* coreライブラリ修正
* notesライブラリ修正
* nclサブディレクトリプロジェクト修正

## v0.0.11 (to v1.0.0)

* coreライブラリ追加
* notesライブラリ修正
* nclサブディレクトリプロジェクト修正
* rxcpp_testサブディレクトリプロジェクト修正

## v0.0.10 (to v1.0.0)

* opensslライブラリ修正
* notesライブラリ修正
* nclサブディレクトリプロジェクト修正

## v0.0.9 (to v1.0.0)

* opensslライブラリを修正。
* notesライブラリを修正。

## v0.0.8 (to v1.0.0)

* notesライブラリを修正。

## v0.0.7 (to v1.0.0)

* notesライブラリを修正。
* nclライブラリを修正。
* opensslライブラリを追加。

## v0.0.6 (to v1.0.0)

* notesライブラリを修正。
* nclライブラリを修正。

## v0.0.5 (to v1.0.0)

* notes修正

## v0.0.4 (to v1.0.0)

* プロジェクトファイルの構成を一新。
* notesライブラリを追加。
* goudaサブプロジェクトを削除。

## v0.0.3

* nclサブディレクトリプロジェクト追加
* rxcpp_testプロジェクト追加

## v0.0.2-5

* npp修正

## v0.0.2-4

* gouda修正
* npp修正

## v0.0.2-3

* gouda修正
* npp修正

## v0.0.2-2

* gouda修正

## v0.0.2-1

* npp修正

## v0.0.2

* gouda修正
* npp修正

## v0.0.1-4

* gouda.proファイルの設定を整理する。

## v0.0.1-3

* .gitignoreを追加する。

## v0.0.1-2

* Ubuntuでc++11指定がないエラーが出たので、すべてのサブプロジェクトに明示的にc++11とc++14を追加する。

## v0.0.1-1

* gouda修正

## v0.0.1

* コマンドラインアプリケーションgoudaを作成する。
* コマンド`GetServerList`を実装する。

## v0.0.0

* 最初のコミット。
