TEMPLATE = subdirs

CONFIG += ordered

SUBDIRS += \
  core \
  notes \
  openssl \
  npp \
  rxcpp_test \
  ncl \
  Booklet

DISTFILES += \
  README.md
