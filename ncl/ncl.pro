TEMPLATE = subdirs

SUBDIRS += \
  ncl-commons \
  ncl-ls \
  ncl-server-latency \
  ncl-server-list

DISTFILES += \
  README.md
