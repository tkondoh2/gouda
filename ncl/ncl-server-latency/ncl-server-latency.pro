QT -= gui

CONFIG += c++11 c++14 console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32: QMAKE_CXXFLAGS += /utf-8

include(../../../environment.pri)

#
# RxCpp
#
INCLUDEPATH += $$RxCppPath/Rx/v2/src
DEPENDPATH += $$RxCppPath/Rx/v2/src

#
# Notes C API
#
include($$NotesCAPIPath/qt_notes_c_api.pri)
#include(../../notescapi_cmp.pri)

#win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../notes/release/ -lgouda_notes1
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../notes/debug/ -lgouda_notes1
#else:unix: LIBS += -L$$OUT_PWD/../../notes/ -lgouda_notes

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../ncl-commons/release/ -lncl-commons
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../ncl-commons/debug/ -lncl-commons
else:unix: LIBS += -L$$OUT_PWD/../ncl-commons/ -lncl-commons

INCLUDEPATH -= $$PWD
DEPENDPATH -= $$PWD

INCLUDEPATH += $$PWD/../../..
DEPENDPATH += $$PWD/../../..

SOURCES += \
        main.cpp

HEADERS += \
  getparameters.hpp \
  getserverlatency.hpp

DISTFILES += \
  README.md
