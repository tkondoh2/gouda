﻿#ifndef GETSERVERLATENCY_HPP
#define GETSERVERLATENCY_HPP

#include "getparameters.hpp"

#include <gouda/ncl/ncl-commons/console.h>
#include <gouda/ncl/ncl-commons/exceptionprocess.hpp>

#include <gouda/notes/session.hpp>
namespace gn {
using namespace gouda::notes;
}

//#include <rxcpp/rx.hpp>
//using namespace rxcpp;
//using namespace rxcpp::operators;

class GetServerLatency
{
public:
  void operator ()(const Parameters &params)
  {
    observable<>::just(params)
    .filter([](Parameters params) {
      return !params.server.isEmpty();
    })
    .map([](Parameters params) {
      return std::make_tuple(gn::ql(params.server), params.timeout);
    })
    .map([](std::tuple<gn::Lmbcs, DWORD> data) {
      return gn::Session::getServerLatency(std::get<0>(data), std::get<1>(data));
    })
    .subscribe([&params](gn::ServerLatencyData value) {
      con_out << params.server
              << ", ctos:" << value.clientToServerMS
              << ", stoc:" << value.serverToClientMS
              << ", ver:" << value.serverVersion
              << endl;
    }, ExceptionProcess::onError);
  }
};

#endif // GETSERVERLATENCY_HPP
