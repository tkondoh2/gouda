﻿#ifndef GETPARAMETERS_HPP
#define GETPARAMETERS_HPP

#include <QCommandLineParser>
#include <QCommandLineOption>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <global.h> // DWORD

#ifdef NT
#pragma pack(pop)
#endif

class Parameters
{
public:
  QString server;
  DWORD timeout;

  Parameters() : server(), timeout(0) {}

  static Parameters get(const QCoreApplication &app)
  {
    QCommandLineParser parser;
    parser.addHelpOption();
    parser.addVersionOption();

    parser.addPositionalArgument(
          "server",
          QObject::tr("Server name")
          );

    QCommandLineOption timeoutOption(
          QStringList() << "t" << "timeout",
          QObject::tr("Timeout"),
          "timeout", "0"
          );
    parser.addOption(timeoutOption);

    parser.process(app);
    Parameters params;
    QStringList args = parser.positionalArguments();
    if (args.count() < 1) return params;
    params.server = args.first();

    if (parser.isSet(timeoutOption)) {
      params.timeout = static_cast<DWORD>(
            QString(parser.value(timeoutOption)).toULong()
            );
    } else {

    }
    return params;
  }
};

#endif // GETPARAMETERS_HPP
