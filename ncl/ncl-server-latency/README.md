﻿NCL Server Latency
==================

## v0.0.6 (to v1.0.0)

* Notesラッパーをgouda/nppからgouda/notesに移行する。
* サーバ名の指定をオプションから必須に変更。

## v0.0.3

* goudaサブプロジェクトから機能を独立。
