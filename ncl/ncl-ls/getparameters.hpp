﻿#ifndef GETPARAMETERS_HPP
#define GETPARAMETERS_HPP

#include <QCommandLineParser>
#include <QCommandLineOption>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <global.h> // DWORD

#ifdef NT
#pragma pack(pop)
#endif

class Parameters
{
public:
  QString dir;
  QString server;

  Parameters() : dir(), server() {}

  static Parameters get(const QCoreApplication &app)
  {
    QCommandLineParser parser;
    parser.addHelpOption();
    parser.addVersionOption();

    parser.addPositionalArgument(
          "dir",
          QObject::tr("Directory")
          );

    QCommandLineOption serverOption(
          QStringList() << "s" << "server",
          QObject::tr("Server name"),
          "server", ""
          );
    parser.addOption(serverOption);

    parser.process(app);
    Parameters params;
    QStringList args = parser.positionalArguments();
    if (args.count() < 1) return params;
    params.dir = args.first();

    if (parser.isSet(serverOption)) {
      params.server = parser.value(serverOption);
    }
    return params;
  }
};

#endif // GETPARAMETERS_HPP
