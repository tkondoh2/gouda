﻿#include "searchfiles.h"

#include <gouda/ncl/ncl-commons/console.h>
#include <gouda/ncl/ncl-commons/exceptionprocess.hpp>

#include <gouda/notes/search.hpp>
#include <gouda/notes/variant.hpp>
namespace gn {
using namespace gouda::notes;
}

#ifdef NT
#pragma pack(push, 1)
#endif

#include <stdnames.h>
#include <osfile.h>

#ifdef NT
#pragma pack(pop)
#endif

struct FileData
{
  QString path;
  QString type;
  qint64 size;
  QString modified;
};

bool searchIterator(const SEARCH_MATCH &match, ITEM_TABLE *pSummary, FileData *pData)
{
  if (match.SERetFlags & SE_FMATCH) {
    gn::Variant vPath = gn::Variant::fromSummary(pSummary, DBDIR_PATH_ITEM);
    pData->path = gn::lq(vPath.firstString());
    gn::Variant vType = gn::Variant::fromSummary(pSummary, DBDIR_TYPE_ITEM);
    pData->type = gn::lq(vType.firstString());
    gn::Variant vLen = gn::Variant::fromSummary(pSummary, DBDIR_LENGTH_ITEM);
    pData->size = static_cast<qint64>(vLen.firstNumber().value());
    gn::Variant vMod = gn::Variant::fromSummary(pSummary, DBDIR_MODIFIED_ITEM);
    pData->modified = gn::lq(vMod.firstTimeDate().convertToText());
    return true;
  }
  return false;
}

void SearchFiles::operator ()(const Parameters &params)
{
  auto oFormula = observable<>::just(gn::Formula::compile("@all"));

  auto oDb = observable<>::just(
        gn::Database::Path(gn::ql(params.dir), gn::ql(params.server))
        )
  .map([](gn::Database::Path dirPath) {
    return gn::Database::open(dirPath);
  });

  oDb.zip(oFormula)
  .flat_map([](std::tuple<gn::DatabasePtr, gn::FormulaPtr> data) {
    return gn::Search<FileData, decltype(&searchIterator)>()(
          std::get<0>(data),
          std::get<1>(data),
          gn::Lmbcs(),
          SEARCH_FILETYPE | SEARCH_SUMMARY,
          FILE_DBDESIGN | FILE_DIRS | FILE_NOUPDIRS,
          nullptr,
          &searchIterator,
          nullptr
          );
  })
  .subscribe([](FileData data) {
    con_out
        << data.modified
        << "  "
        << QString("%1").arg(data.type, 9)
        << " "
        << QString("%1").arg(data.size, 10)
        << " "
        << data.path
        << endl;
  }, ExceptionProcess::onError);
}
