﻿#ifndef SEARCHFILES_H
#define SEARCHFILES_H

#include "getparameters.hpp"

class SearchFiles
{
public:
  void operator ()(const Parameters &params);
};

#endif // SEARCHFILES_H
