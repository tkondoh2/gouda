﻿NCL Sub Directory Project
=========================

## v0.0.12 (to v1.0.0)

* 環境依存プロジェクト設定の見直し。

## v0.0.11 (to v1.0.0)

* notes::Session()の仕様変更に対応。

## v0.0.10 (to v1.0.0)

* notes/opensslヘッダーオンリー化に対応。

## v0.0.7 (to v1.0.0)

* ncl-lsコマンドを追加。

## v0.0.6 (to v1.0.0)

* Notesラッパーをgouda/nppからgouda/notesに移行する。

## v0.0.3

* 最初のコミット。
