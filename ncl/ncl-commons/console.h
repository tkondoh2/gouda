#ifndef CONSOLE_H
#define CONSOLE_H

#include <gouda/ncl/ncl-commons/ncl-commons_global.h>

#include <QTextStream>

NCLCOMMONSSHARED_EXPORT
extern QTextStream con_in;

NCLCOMMONSSHARED_EXPORT
extern QTextStream con_out;

NCLCOMMONSSHARED_EXPORT
extern QTextStream con_err;

#endif // CONSOLE_H
