﻿#ifndef EXCEPTIONPROCESS_HPP
#define EXCEPTIONPROCESS_HPP

#include <stdexcept>

#include <gouda/ncl/ncl-commons/console.h>

#include <gouda/notes/lmbcs.hpp>
namespace gn {
using namespace gouda::notes;
}

/**
 * @brief 例外プロセス
 */
struct ExceptionProcess
{
  static void onError(std::exception_ptr &ep) noexcept
  {
    try {
      std::rethrow_exception(ep);
    }
    catch (gn::Status &status) {
      con_err << QString("Notes Error: %1")
                 .arg(gn::lq(gn::Lmbcs(status.what())))
              << endl;
    }
    catch (std::exception &ex) {
      con_err << QString("Runtime Error: %1").arg(ex.what()) << endl;
    }
  }
};

#endif // EXCEPTIONPROCESS_HPP
