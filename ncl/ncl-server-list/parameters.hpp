#ifndef GETPARAMETERS_HPP
#define GETPARAMETERS_HPP

#include <QCommandLineParser>

struct Parameters
{
  // this class is no parameter.

  static Parameters get(const QCoreApplication &app)
  {
    QCommandLineParser parser;
    parser.addHelpOption();
    parser.addVersionOption();

    parser.process(app);
    Parameters params;
    return params;
  }
};

#endif // GETPARAMETERS_HPP
