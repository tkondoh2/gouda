﻿#ifndef SERVERLIST_H
#define SERVERLIST_H

#include "parameters.hpp"

#include <gouda/ncl/ncl-commons/console.h>
#include <gouda/ncl/ncl-commons/exceptionprocess.hpp>

#include <gouda/notes/session.hpp>
namespace gn {
using namespace gouda::notes;
}

struct GetServerList
{
  void operator ()(const Parameters &)
  {
    con_out << "Start." << endl;
    gn::Session::getServerList()
//        .observe_on(synchronize_new_thread())
//        .as_blocking()
    .subscribe([](gn::Lmbcs serverName) {
      con_out << gn::lq(serverName) << endl;
    }, ExceptionProcess::onError);
    con_out << "End." << endl;
  }
};

#endif // SERVERLIST_H
