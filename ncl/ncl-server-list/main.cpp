﻿#include <QCoreApplication>

#include "getserverlist.hpp"

#include <QTimer>

#include <gouda/notes/session.hpp>

int main(int argc, char *argv[])
{
  return gouda::notes::Session()(argc, argv, [](int argc, char *argv[]) {
    QCoreApplication app(argc, argv);
    QTimer::singleShot(0, [&]() {
      auto params = Parameters::get(app);
      GetServerList()(params);
      app.exit(0);
    });
    return app.exec();
  });
}
