﻿rxcpp test app
==================================

# History

## v0.0.10 (to v1.0.0)

* notes/opensslヘッダーオンリー化に対応。

## v0.0.3

* 最初のコミット。
* as_blockingのテスト。
* observe_on_new_threadのテスト。
