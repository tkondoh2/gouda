﻿#include <QCoreApplication>

#include <QTimer>

#include <gouda/notes/status.hpp>
#include <gouda/notes/session.hpp>
//#include <gouda/notes/object.h>
//#include <gouda/notes/server.h>
#include <thread>
#include <iostream>
#include <rxcpp/rx.hpp>
using namespace rxcpp;
using namespace rxcpp::operators;

void test1()
{
  std::cout << "//! [threaded range sample]" << std::endl;
  std::cout << "[thread " << std::this_thread::get_id() << "] Start task" << std::endl;
  auto values = observable<>::range(1, 3, observe_on_new_thread());
  auto s = values
      .map([](int v) { return std::make_tuple(std::this_thread::get_id(), v);});
  s.as_blocking()
  .subscribe( util::apply_to( [](const std::thread::id pid, int v) {
    std::cout << "[thread " << pid << "] OnNext: " << v << std::endl;
  }), [](){
    std::cout << "[thread " << std::this_thread::get_id() << "] OnCompleted" << std::endl;
  });
  std::cout << "[thread " << std::this_thread::get_id() << "] Finish task" << std::endl;
  std::cout << "//! [threaded range sample]" << std::endl;
}

void test2()
{
  std::cout << "//! [threaded range sample]" << std::endl;
  std::cout << "[thread " << std::this_thread::get_id() << "] Start task" << std::endl;
  auto values = observable<>::range(1, 3, observe_on_new_thread());
  auto s = values
      .map([](int v) { return std::make_tuple(std::this_thread::get_id(), v);});
  s
//      .as_blocking()
      .subscribe( util::apply_to( [](const std::thread::id pid, int v) {
    std::cout << "[thread " << pid << "] OnNext: " << v << std::endl;
  }), [](){
    std::cout << "[thread " << std::this_thread::get_id() << "] OnCompleted" << std::endl;
  });
  std::cout << "[thread " << std::this_thread::get_id() << "] Finish task" << std::endl;
  std::cout << "//! [threaded range sample]" << std::endl;
}

observable<int> getObservable()
{
  return observable<>::create<int>([](subscriber<int> s) {
    s.on_next(1);
    s.on_next(2);
    s.on_next(3);
    s.on_completed();
  });
}

void test3()
{
  getObservable()
      .subscribe([](int v) { std::cout << v; });
}

int main(int argc, char *argv[])
{
  return gouda::notes::Session()(argc, argv, [](int argc, char *argv[]) {
    QCoreApplication app(argc, argv);
    QTimer::singleShot(0, [&]() {
      test3();
      app.exit(0);
    });
    return app.exec();
  });
}
