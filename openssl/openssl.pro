#-------------------------------------------------
#
# Project created by QtCreator 2019-07-12T10:19:50
#
#-------------------------------------------------

QT       -= gui

TARGET = gouda_openssl
TEMPLATE = lib

DEFINES += OPENSSL_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11 c++14

win32: QMAKE_CXXFLAGS += /utf-8

include(../../environment.pri)

#
# RxCpp
#
INCLUDEPATH += $$RxCppPath/Rx/v2/src
DEPENDPATH += $$RxCppPath/Rx/v2/src

#
# OpenSSL 1.1
#
INCLUDEPATH += $$OpenSSLIncludePath
DEPENDPATH += $$OpenSSLIncludePath

TargetProduct = GoudaOpenSSLLib
TargetDescription = Gouda OpenSSL Library
TargetVersion = 1.0.0
TargetCompany = Chiburu Systems
TargetDomain = chiburu.com
TargetCopyright = 2019 Chiburu Systems
include(../../version.pri)

INCLUDEPATH -= $$PWD
DEPENDPATH -= $$PWD

INCLUDEPATH += $$PWD/../..
DEPENDPATH += $$PWD/../..

SOURCES += \
        version.cpp

HEADERS += \
        base64.hpp \
        bio.hpp \
        evp/messagedigestcontext.hpp \
        evp/pkey.hpp \
        jsonwebtoken.hpp \
        jwtclass.hpp \
        openssl_global.h \
        base64.hpp \
        rsa/rsa.hpp \
        rsa/signature.hpp

#unix {
#    target.path = /usr/lib
#    INSTALLS += target
#}

DISTFILES += \
  README.md
