﻿#ifndef GOUDA_OPENSSL_RSA_RSAPTR_HPP
#define GOUDA_OPENSSL_RSA_RSAPTR_HPP

#include <gouda/openssl/openssl_global.h>
#include <gouda/openssl/bio.hpp>

#include <QScopedPointer>

#include <openssl/rsa.h>
#include <openssl/pem.h>

namespace gouda {
namespace openssl {
namespace rsa {

struct RSADeleter { static void cleanup(RSA *pRSA) { RSA_free(pRSA); } };

using RSAPtr = QScopedPointer<RSA, RSADeleter>;

class Rsa
{
  RSAPtr ptr_;

public:
  Rsa(RSA *ptr = nullptr)
    : ptr_(ptr)
  {}

  bool loadPrivateKey(const Bio &privateKey)
  {
    RSA *p = PEM_read_bio_RSAPrivateKey(privateKey(), nullptr, nullptr, nullptr);
    if (p) {
      ptr_.reset(p);
      return true;
    }
    return false;
  }

  bool loadPublicKey(const Bio &publicKey)
  {
    RSA *p = PEM_read_bio_RSA_PUBKEY(publicKey(), nullptr, nullptr, nullptr);
    if (p) {
      ptr_.reset(p);
      return true;
    }
    return false;
  }

  RSA *operator ()() {
    return ptr_.data();
  }
};

} // namespace rsa
} // namespace openssl
} // namespace gouda

#endif // GOUDA_OPENSSL_RSA_RSAPTR_HPP
