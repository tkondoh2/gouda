﻿#ifndef GOUDA_OPENSSL_RSA_SIGNATURE_HPP
#define GOUDA_OPENSSL_RSA_SIGNATURE_HPP

#include <gouda/openssl/openssl_global.h>
#include <gouda/openssl/rsa/rsa.hpp>
#include <gouda/openssl/evp/pkey.hpp>
#include <gouda/openssl/evp/messagedigestcontext.hpp>

#include <openssl/evp.h>

#include <rxcpp/rx.hpp>
using namespace rxcpp;
using namespace rxcpp::operators;

namespace gouda {
namespace openssl {
namespace rsa {

class Signature
{
  const EVP_MD *pMDType_;

public:
  Signature(const EVP_MD *pMDType)
    : pMDType_(pMDType)
  {}

  const EVP_MD *pMDType() const
  {
    return pMDType_;
  }

  QByteArray sign(
      const QByteArray &source,
      const QByteArray &privateKey
      ) const
  {
    Bio bioPrivateKey(privateKey);
    Rsa rsaPrivateKey;
    evp::PKey pkey;
    const EVP_MD *pType = pMDType();
    evp::MessageDigestContext mdCtx;

  #if OPENSSL_VERSION_NUMBER >= 0x10100000L
    size_t sigSize = 0;
    QByteArray sigBytes;

    observable<>::just(rsaPrivateKey.loadPrivateKey(bioPrivateKey))
    .filter([](bool result) { return result; })
    .filter([&](bool) { return pkey.setRSA(rsaPrivateKey); })
    .filter([&](bool) { return mdCtx.initSign(pType, pkey()); })
    .filter([&](bool) { return mdCtx.updateSign(source.data(), source.size()); })
    .filter([&](bool) { return mdCtx.finalSign(nullptr, &sigSize); })
    .subscribe([&](bool) {
      QByteArray buffer(static_cast<int>(sigSize), '\0');
      mdCtx.finalSign(reinterpret_cast<uchar*>(buffer.data()), &sigSize);
      sigBytes = buffer;
    });

    return sigBytes;

  #else
    Bio bioSource(source);
    Bio bio;

    observable<>::just(rsaPrivateKey.loadPrivateKey(bioPrivateKey))
    .filter([](bool result) { return result; })
    .filter([&](bool) { return pkey.setRSA(rsaPrivateKey); })
    .filter([&](bool) { return mdCtx.initSignEx(pType); })
    .filter([&](bool) { return mdCtx.updateSign(bioSource); })
    .map([&](bool) { return mdCtx.finalSign(pkey); })
    .subscribe([&](QByteArray sign) { bio.write(sign, sign.length()); });

    return bio.toBytes();

  #endif
  }

  bool verify(
      const QByteArray &source,
      const QByteArray &signedSource,
      const QByteArray &publicKey
      ) const
  {
    Bio bioPublicKey(publicKey);
    Rsa rsaPublicKey;
    evp::PKey pkey;
    const EVP_MD *pType = pMDType();
    evp::MessageDigestContext mdCtx;
    bool result = false;

  #if OPENSSL_VERSION_NUMBER >= 0x10100000L

    observable<>::just(rsaPublicKey.loadPublicKey(bioPublicKey))
    .filter([](bool res) { return res; })
    .filter([&](bool) { return pkey.setRSA(rsaPublicKey); })
    .filter([&](bool) { return mdCtx.initVerify(pType, pkey()); })
    .filter([&](bool) { return mdCtx.updateVerify(source.data(), source.size()); })
    .subscribe([&](bool) {
      result = mdCtx.finalVerify(
            reinterpret_cast<const uchar*>(signedSource.constData()),
            static_cast<size_t>(signedSource.size())
            );
    });

  #else
    Bio bioSource(source);
    Bio bioSigned(signedSource);

    observable<>::just(rsaPublicKey.loadPublicKey(bioPublicKey))
    .filter([](bool res) { return res; })
    .filter([&](bool) { return pkey.setRSA(rsaPublicKey); })
    .filter([&](bool) { return mdCtx.initVerifyEx(pType); })
    .filter([&](bool) { return mdCtx.updateVerify(bioSource); })
    .map([&](bool) { return mdCtx.finalVerify(pkey, bioSigned); })
    .subscribe([&](bool b) { result = b; });

  #endif

    return result;
  }
};

} // namespace rsa
} // namespace openssl
} // namespace gouda

#endif // GOUDA_OPENSSL_RSA_SIGNATURE_HPP
