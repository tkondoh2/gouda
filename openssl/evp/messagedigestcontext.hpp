﻿#ifndef GOUDA_OPENSSL_EVP_MESSAGEDIGESTCONTEXT_HPP
#define GOUDA_OPENSSL_EVP_MESSAGEDIGESTCONTEXT_HPP

#include <gouda/openssl/openssl_global.h>

#include <QScopedPointer>
#include <gouda/openssl/evp/pkey.hpp>

namespace gouda {
namespace openssl {

class Bio;

namespace evp {

#if OPENSSL_VERSION_NUMBER >= 0x10100000L
struct MDCtxDeleter {
  static void cleanup(EVP_MD_CTX *pMDCtx) { EVP_MD_CTX_destroy(pMDCtx); }
};

using MDCtxPtr = QScopedPointer<EVP_MD_CTX, MDCtxDeleter>;

#endif

class MessageDigestContext
{
#if OPENSSL_VERSION_NUMBER >= 0x10100000L
  MDCtxPtr ptr_;
#else
  EVP_MD_CTX ctx_;
#endif

public:
  MessageDigestContext()
    : ptr_(EVP_MD_CTX_create())
  {}

#if OPENSSL_VERSION_NUMBER >= 0x10100000L

  EVP_MD_CTX *operator ()() {
    return ptr_.data();
  }

  int initSign(
      const EVP_MD *pMD,
      EVP_PKEY *pKey,
      EVP_PKEY_CTX **ppPKCtx = nullptr,
      ENGINE *impl = nullptr
      )
  {
    return EVP_DigestSignInit(ptr_.data(), ppPKCtx, pMD, impl, pKey);
  }

  int updateSign(const void *pMessage, size_t pMessageSize)
  {
    return EVP_DigestSignUpdate(ptr_.data(), pMessage, pMessageSize);
  }

  int finalSign(uchar *pSignature, size_t *pSigSize)
  {
    return EVP_DigestSignFinal(ptr_.data(), pSignature, pSigSize);
  }

  int initVerify(
      const EVP_MD *pMD,
      EVP_PKEY *pKey,
      EVP_PKEY_CTX **ppPKCtx = nullptr,
      ENGINE *impl = nullptr
      )
  {
    return EVP_DigestVerifyInit(ptr_.data(), ppPKCtx, pMD, impl, pKey);
  }

  int updateVerify(const void *pMessage, size_t messageSize)
  {
    return EVP_DigestVerifyUpdate(ptr_.data(), pMessage, messageSize);
  }

  int finalVerify(const uchar *pSignature, size_t sigSize)
  {
    return EVP_DigestVerifyFinal(ptr_.data(), pSignature, sigSize);
  }

#else
  MessageDigestContext()
    : ctx_()
  {
    EVP_MD_CTX_init(&ctx_);
  }

  virtual ~MessageDigestContext()
  {
    EVP_MD_CTX_cleanup(&ctx_);
  }

  bool initSignEx(const EVP_MD *pMD, ENGINE *impl = nullptr)
  {
    return EVP_SignInit_ex(&ctx_, pMD, impl) != 0;
  }

  bool updateSign(const Bio &bio, int bufferSize = 1024)
  {
    QByteArray buffer(bufferSize, '\0');
    int readSize;
    while ((readSize = bio.read(buffer.data(), bufferSize)) > 0)
    {
      int result = EVP_SignUpdate(&ctx_, buffer.constData(), readSize);
      if (result == 0) return false;
    }
    return true;
  }

  QByteArray finalSign(PKey &pkey)
  {
    uint bufferSize = pkey.size();
    QByteArray buffer(bufferSize, '\0');
    if (EVP_SignFinal(&ctx_, (uchar*)buffer.data(), &bufferSize, pkey()) != 0) {
      return buffer;
    }
    return QByteArray();
  }

  bool initVerifyEx(const EVP_MD *pMD, ENGINE *impl = nullptr)
  {
    return EVP_VerifyInit_ex(&ctx_, pMD, impl) != 0;
  }

  bool updateVerify(const Bio &bio, int bufferSize = 1024)
  {
    QByteArray buffer(bufferSize, '\0');
    int readSize;
    while ((readSize = bio.read(buffer.data(), bufferSize)) > 0)
    {
      int result = EVP_VerifyUpdate(&ctx_, buffer.constData(), readSize);
      if (result == 0) return false;
    }
    return true;
  }

  bool finalVerify(PKey &pkey, const Bio &bioSigned)
  {
    uint bufferSize = pkey.size();
    QByteArray buffer(bufferSize, '\0');
    bioSigned.read(buffer.data(), bufferSize);
    int result = EVP_VerifyFinal(
          &ctx_,
          (const uchar*)buffer.constData(),
          bufferSize,
          pkey()
          );
    return (result == 1) ? true : false;
  }
#endif
};

} // namespace evp
} // namespace openssl
} // namespace gouda

#endif // GOUDA_OPENSSL_EVP_MESSAGEDIGESTCONTEXT_HPP
