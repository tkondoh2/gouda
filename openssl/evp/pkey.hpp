﻿#ifndef GOUDA_OPENSSL_ENV_PKEY_HPP
#define GOUDA_OPENSSL_ENV_PKEY_HPP

#include <gouda/openssl/openssl_global.h>

#include <openssl/evp.h>
#include <QScopedPointer>
#include <gouda/openssl/rsa/rsa.hpp>

namespace gouda {
namespace openssl {
namespace evp {

struct PKeyDeleter {
  static void cleanup(EVP_PKEY *pPKey) { EVP_PKEY_free(pPKey); }
};

using PKeyPtr = QScopedPointer<EVP_PKEY, PKeyDeleter>;

class  PKey
{
  PKeyPtr ptr_;

public:
  PKey()
    : ptr_(EVP_PKEY_new())
  {}

  EVP_PKEY *operator ()() {
    return ptr_.data();
  }

  bool setRSA(rsa::Rsa &rsa)
  {
    return EVP_PKEY_set1_RSA(ptr_.data(), rsa()) != 0;
  }

  uint size() const
  {
    return EVP_PKEY_size(ptr_.data());
  }
};

} // namespace evp
} // namespace openssl
} // namespace gouda

#endif // GOUDA_OPENSSL_ENV_PKEY_HPP
