﻿#ifndef GOUDA_OPENSSL_JSONWEBTOKEN_HPP
#define GOUDA_OPENSSL_JSONWEBTOKEN_HPP

#include <gouda/openssl/base64.hpp>
#include <gouda/openssl/rsa/signature.hpp>

#include <QList>
#include <QJsonDocument>
#include <QJsonObject>

#include <openssl/evp.h>

#include <rxcpp/rx.hpp>
using namespace rxcpp;
using namespace rxcpp::operators;

namespace gouda {
namespace openssl {

class JsonWebToken
{
  QByteArray header_;
  QByteArray payload_;
  QByteArray signature_;

public:
  JsonWebToken()
    : header_()
    , payload_()
    , signature_()
  {}

  const QByteArray &header() const { return header_; }
  const QByteArray &payload() const { return payload_; }
  const QByteArray &signature() const { return signature_; }

  QJsonDocument jsonDocHeader() const
  {
    return base64ToJsonDoc(header_);
  }

  QJsonDocument jsonDocPayload() const
  {
    return base64ToJsonDoc(payload_);
  }

  QByteArray rawSignature() const
  {
    return fromBase64(signature_);
  }

  static QJsonValue algolithm(const QJsonDocument &jsonDoc)
  {
    QJsonValue retValue;
    observable<>::just(jsonDoc)
    .filter([](QJsonDocument jsonDoc) { return jsonDoc.isObject(); })
    .map([](QJsonDocument jsonDoc) { return jsonDoc.object(); })
    .filter([](QJsonObject obj) { return obj.contains("alg"); })
    .map([](QJsonObject obj) { return obj.value("alg"); })
    .subscribe([&retValue](QJsonValue alg) { retValue = alg; });
    return retValue;
  }

  bool isValid() const
  {
    return algolithm(jsonDocHeader()).isString()
        && jsonDocPayload().isObject()
        && !rawSignature().isEmpty();
  }

  QByteArray token() const
  {
    QList<QByteArray> array;
    array += header_;
    array += payload_;
    array += signature_;
    return array.join('.');
  }


  static QJsonDocument base64ToJsonDoc(const QByteArray &b64)
  {
    QByteArray utf8json = fromBase64(b64);
    return QJsonDocument::fromJson(utf8json);
  }

  static QByteArray jsonDocToBase64(const QJsonDocument &jsonDoc, bool isCompact = true)
  {
    QByteArray utf8json = jsonDoc.toJson(isCompact ? QJsonDocument::Compact : QJsonDocument::Indented);
    return toBase64(utf8json);
  }

  static const EVP_MD *getHashAlgolithm(const QString &strAlg)
  {
    if (strAlg == "RS256") return EVP_sha256();
    if (strAlg == "RS384") return EVP_sha384();
    if (strAlg == "RS512") return EVP_sha512();
    return nullptr;
  }

  static JsonWebToken makeJsonWebToken(
      const QJsonDocument &header,
      const QJsonDocument &payload,
      const QByteArray &privateKey
      )
  {
    JsonWebToken retJwt;
    observable<>::just(std::make_tuple(header, payload))
    .map([](std::tuple<QJsonDocument, QJsonDocument> data) {
      QJsonDocument jsonHeader = std::get<0>(data);
      QByteArray srcHeader = jsonDocToBase64(jsonHeader);
      QByteArray srcPayload = jsonDocToBase64(std::get<1>(data));
      QString strAlg = algolithm(jsonHeader).toString();
      const EVP_MD *pAlg = getHashAlgolithm(strAlg);
      return std::make_tuple(srcHeader, srcPayload, pAlg);
    })
    .filter([](std::tuple<QByteArray, QByteArray, const EVP_MD*> data) {
      return std::get<1>(data) != nullptr;
    })
    .subscribe([&](std::tuple<QByteArray, QByteArray, const EVP_MD*> data) {
      JsonWebToken jwt;
      jwt.header_ = std::get<0>(data);
      jwt.payload_ = std::get<1>(data);
      QByteArray source = jwt.header() + '.' + jwt.payload();
      rsa::Signature signature(std::get<2>(data));
      jwt.signature_ = toBase64(signature.sign(source, privateKey));
      retJwt = jwt;
    });
    return retJwt;
  }

  static JsonWebToken parseJsonWebToken(
      const QByteArray &token,
      const QByteArray &publicKey
      )
  {
    JsonWebToken retJwt;

    observable<>::just(token)
    .filter([](QByteArray token) {
      return token.count('.') == 2;
    })
   .map([](QByteArray token) {
      QList<QByteArray> tokens = token.split('.');
      JsonWebToken jwt;
      jwt.header_ = tokens[0];
      jwt.payload_ = tokens[1];
      jwt.signature_ = tokens[2];
      return std::make_tuple(
            jwt,
            algolithm(jwt.jsonDocHeader())
            );
    })
    .filter([](std::tuple<JsonWebToken, QJsonValue> data) {
      return std::get<1>(data).isString();
    })
    .map([](std::tuple<JsonWebToken, QJsonValue> data) {
      QString alg = std::get<1>(data).toString();
      const EVP_MD *pAlg = getHashAlgolithm(alg);
      return std::make_tuple(std::get<0>(data), pAlg);
    })
    .filter([](std::tuple<JsonWebToken, const EVP_MD*> data) {
      return (std::get<1>(data) != nullptr);
    })
    .filter([&publicKey](std::tuple<JsonWebToken, const EVP_MD*> data) {
      JsonWebToken jwt = std::get<0>(data);
      QByteArray source = jwt.header() + '.' + jwt.payload();
      rsa::Signature signature(std::get<1>(data));
      return signature.verify(source, jwt.rawSignature(), publicKey);
    })
    .subscribe(
          [&retJwt](std::tuple<JsonWebToken, const EVP_MD*> data) {
      retJwt = std::move(std::get<0>(data));
    });

    return retJwt;
  }
};

} // namespace openssl
} // namespace gouda

#endif // GOUDA_OPENSSL_JSONWEBTOKEN_HPP
