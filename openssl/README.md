﻿OpenSSL (1.1) wrapper Library
==================================

# History

## v0.0.20 (to v1.0.0)

* 微調整

## v0.0.10 (to v1.0.0)

* ヘッダーオンリー化

## v0.0.9 (to v1.0.0)

* JWTClass::setString => JWTClass::stringToJson
* JWTClass::stringFromJson
* JWTClass::stringListToJson
* JWTClass::stringListFromJson
* JWTClass::qint64ToJson
* JWTClass::qint64FromJson

## v0.0.7 (to v1.0.0)

* fromBase64関数の追加。
* toBase64関数の追加。
* JsonWebTokenクラスの追加。
* JWTClassクラスの追加。
* JWTHeaderクラスの追加。
* JWTClaimクラスの追加。
* Bioクラスの追加。
* evp::MessageDigestContextクラスの追加。
* evp::PKeyクラスの追加。
* rsa::Rsaクラスの追加。
* rsa::Signatureクラスの追加。
