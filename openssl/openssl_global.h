﻿#ifndef GOUDA_OPENSSL_GLOBAL_HPP
#define GOUDA_OPENSSL_GLOBAL_HPP

#include <QtCore/qglobal.h>

#if defined(OPENSSL_LIBRARY)
#  define GOUDA_OPENSSLSHARED_EXPORT Q_DECL_EXPORT
#else
#  define GOUDA_OPENSSLSHARED_EXPORT Q_DECL_IMPORT
#endif

namespace gouda {
namespace openssl {

class GOUDA_OPENSSLSHARED_EXPORT Version
{
public:
  static uint major();
  static uint minor();
  static uint patch();
  static const char *ver();
};

} // namespace openssl
} // namespace gouda

#endif // GOUDA_OPENSSL_GLOBAL_HPP
