﻿#include <gouda/openssl/openssl_global.h>

#define STRING(str) #str

namespace gouda {
namespace openssl {

uint Version::major()
{
  return PROJECT_VER_MAJ;
}

uint Version::minor()
{
  return PROJECT_VER_MIN;
}

uint Version::patch()
{
  return PROJECT_VER_PAT;
}

const char *Version::ver()
{
  return STRING(PROJECT_VERSION);
}

} // namespace openssl
} // namespace gouda
