﻿#ifndef GOUDA_OPENSSL_BIO_HPP
#define GOUDA_OPENSSL_BIO_HPP

#include <QByteArray>
#include <QScopedPointer>

#include <openssl/evp.h>

namespace gouda {
namespace openssl {

struct BIODeleter { static void cleanup(BIO *pBIO) { BIO_free(pBIO); } };

using BIOPtr = QScopedPointer<BIO, BIODeleter>;

class Bio
{
  BIOPtr ptr_;

public:
  Bio()
    : ptr_(BIO_new(BIO_s_mem()))
  {}

  Bio(const QByteArray &bytes)
    : ptr_(BIO_new_mem_buf(bytes.constData(), bytes.length()))
  {}

  BIO *operator ()() const
  {
    return ptr_.data();
  }

  QByteArray toBytes() const
  {
    char* ptr;
    int len = BIO_get_mem_data(ptr_.data(), &ptr);
    return QByteArray(ptr, len);
  }

  int read(void *pBuffer, int bufferSize) const
  {
    return BIO_read(ptr_.data(), pBuffer, bufferSize);
  }

  int write(const void *pBuffer, int bufferSize)
  {
    return BIO_write(ptr_.data(), pBuffer, bufferSize);
  }
};

} // namespace openssl
} // namespace gouda

#endif // GOUDA_OPENSSL_BIO_HPP
