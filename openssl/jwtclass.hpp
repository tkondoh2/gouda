﻿#ifndef GOUDA_OPENSSL_JWTCLASS_HPP
#define GOUDA_OPENSSL_JWTCLASS_HPP

#include <gouda/openssl/jsonwebtoken.hpp>

#include <QJsonObject>
#include <QJsonArray>
#include <QVariant>
#include <QDateTime>

namespace gouda {
namespace openssl {

class JWTClass
{
public:
  virtual ~JWTClass() {}

  virtual QJsonDocument toJson() const = 0;

  template <typename T, typename Func>
  static void valueFromJson(
      const QJsonObject &obj,
      const QString &name,
      T &value,
      Func func
      )
  {
    if (obj.contains(name)) {
      value = func(obj.value(name));
    }
  }

  template <typename T, typename Tester>
  static void valueToJson(
      const T &value,
      QJsonObject &obj,
      const QString &name,
      Tester tester
      )
  {
    if (tester(value)) {
      obj.insert(name, QJsonValue(value));
    }
  }

  static void stringFromJson(
      const QJsonObject &obj,
      const QString &name,
      QString &value
      )
  {
    valueFromJson(obj, name, value, [](QJsonValue v) { return v.toString(); });
  }

  static void stringToJson(
      const QString &value,
      QJsonObject &obj,
      const QString &name
      )
  {
    valueToJson(value, obj, name, [](QString v) { return !v.isEmpty(); });
  }

  static void stringListFromJson(
      const QJsonObject &obj,
      const QString &name,
      QStringList &value
      )
  {
    QJsonValue v = obj.value(name);
    QJsonArray a = v.toArray();
    QVariantList vlist = a.toVariantList();
    foreach (QVariant i, vlist) {
      value.append(i.toString());
    }
  }

  static void stringListToJson(
      const QStringList &value,
      QJsonObject &obj,
      const QString &name
      )
  {
    if (!value.isEmpty()) {
      QJsonArray list = QJsonArray::fromStringList(value);
      obj.insert(name, list);
    }
  }

  static void qint64FromJson(
      const QJsonObject &obj,
      const QString &name,
      qint64 &value
      )
  {
    valueFromJson(obj, name, value, [](QJsonValue v) {
      return v.toVariant().toLongLong();
    });
  }

  static void qint64ToJson(
      const qint64 &value,
      QJsonObject &obj,
      const QString &name
      )
  {
    valueToJson(value, obj, name, [](qint64 v) { return v != 0; });
  }
};

class JWTHeader
    : public JWTClass
{
  QString typ_; // Type, トークン形式
  QString alg_; // Algorithm, アルゴリズム

public:
  JWTHeader()
    : JWTClass()
    , typ_("JWT")
    , alg_("RS256")
  {}

  virtual ~JWTHeader() override {}

  const QString &alg() const { return alg_; }

  void setAlg(const QString& alg) { alg_ = alg; }

  const QString &typ() const { return typ_; }

  void setTyp(const QString& typ) { typ_ = typ; }

  virtual QJsonDocument toJson() const override
  {
    QJsonObject obj;
    stringToJson(typ_, obj, "typ");
    stringToJson(alg_, obj, "alg");
    return QJsonDocument(obj);
  }
};

class JWTClaim
    : public JWTClass
{
  QString iss_; // Issuer, 発行者
  QString sub_; // Subject, 主題
  QString aud_; // Audience, 受信者
  qint64 exp_; // Expiration Time, 有効期限
  qint64 nbf_; // Not Before, 開始日時
  qint64 iat_; // Issued at, 発行日時
  QString jti_; // JWT ID, 識別子

public:
  JWTClaim()
    : iss_()
    , sub_()
    , aud_()
    , exp_(0)
    , nbf_(0)
    , iat_(0)
    , jti_()
  {}

  JWTClaim(qint64 expiresInSecs)
    : iss_()
    , sub_()
    , aud_()
    , exp_(0)
    , nbf_(0)
    , iat_(QDateTime::currentSecsSinceEpoch())
    , jti_()
  {
    nbf_ = iat_;
    exp_ = iat_ + expiresInSecs;
  }

  JWTClaim(const QJsonDocument& json)
    : iss_()
    , sub_()
    , aud_()
    , exp_(0)
    , nbf_(0)
    , iat_(0)
    , jti_()
  {
    QJsonObject obj = json.object();
    stringFromJson(obj, "iss", iss_);
    stringFromJson(obj, "sub", sub_);
    stringFromJson(obj, "aud", aud_);
    qint64FromJson(obj, "exp", exp_);
    qint64FromJson(obj, "nbf", nbf_);
    qint64FromJson(obj, "iat", iat_);
    stringFromJson(obj, "jti", jti_);
  }

  virtual ~JWTClaim() override {}

  virtual bool isValid() const
  {
    qint64 now = QDateTime::currentSecsSinceEpoch();
    return (now < exp_ && now >= nbf_);
  }

  virtual QJsonDocument toJson() const override
  {
    QJsonObject obj;
    stringToJson(iss_, obj, "iss");
    stringToJson(sub_, obj, "sub");
    stringToJson(aud_, obj, "aud");
    qint64ToJson(exp_, obj, "exp");
    qint64ToJson(nbf_, obj, "nbf");
    qint64ToJson(iat_, obj, "iat");
    stringToJson(jti_, obj, "jti");
    return QJsonDocument(obj);
  }

  const QString &iss() const { return iss_; }

  void setIss(const QString& iss) { iss_ = iss; }

  const QString &sub() const { return sub_; }

  void setSub(const QString& sub) { sub_ = sub; }

  const QString &aud() const { return aud_; }

  void setAud(const QString& aud) { aud_ = aud; }

  qint64 exp() const { return exp_; }

  void setExp(const qint64& exp) { exp_ = exp; }

  qint64 nbf() const { return nbf_; }

  void setNbf(const qint64& nbf) { nbf_ = nbf; }

  qint64 iat() const { return iat_; }

  void setIat(const qint64& iat) { iat_ = iat; }

  const QString &jti() const { return jti_; }

  void setJti(const QString& jti) { jti_ = jti; }

  QDateTime expirationTime() const
  {
    return QDateTime::fromSecsSinceEpoch(exp_);
  }

  void setExpirationTime(const QDateTime &t) { exp_ = t.toSecsSinceEpoch(); }

  QDateTime notBeforeTime() const
  {
    return QDateTime::fromSecsSinceEpoch(nbf_);
  }

  void setNotBeforeTime(const QDateTime &t) { nbf_ = t.toSecsSinceEpoch(); }

  QDateTime issuedAtTime() const
  {
    return QDateTime::fromSecsSinceEpoch(iat_);
  }

  void setIssuedAtTime(const QDateTime &t) { iat_ = t.toSecsSinceEpoch(); }
};

} // namespace openssl
} // namespace gouda

#endif // GOUDA_OPENSSL_JWTCLASS_HPP
