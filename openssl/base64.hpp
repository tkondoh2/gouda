﻿#ifndef GOUDA_OPENSSL_BASE64_HPP
#define GOUDA_OPENSSL_BASE64_HPP

#include <gouda/openssl/openssl_global.h>
#include <QByteArray>

namespace gouda {
namespace openssl {

inline QByteArray fromBase64(
    const QByteArray &base64,
    const QByteArray::Base64Options options
      = QByteArray::Base64UrlEncoding | QByteArray::OmitTrailingEquals
    )
{
  return QByteArray::fromBase64(base64, options);
}

inline QByteArray toBase64(
    const QByteArray &text,
    const QByteArray::Base64Options options
      = QByteArray::Base64UrlEncoding | QByteArray::OmitTrailingEquals
    )
{
  return text.toBase64(options);
}

} // namespace openssl
} // namespace gouda

#endif // GOUDA_OPENSSL_BASE64_HPP
